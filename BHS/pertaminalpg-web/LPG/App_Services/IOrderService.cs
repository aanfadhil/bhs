﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IOrderService
    {
        bool deleteOrder(int id);
        List<GetOrderList_Result> GetAllOrders();
        DataTableModel<OrderListPagingModel> GetAllOrdersPaging(DatatablesRequest model);
        OrderModel getOrderDetails(int orderId, int buyType);
        List<ProductModel> GetProductListForOrder(OrderModel model, int orderId);
        void ReadAndSendPushNotification(string messageTypeKey, string msgTitleKey, string appToken, int orderId, int driverId, int orderCount, string applicationId, string senderId, int type, int userId, string userName, string mobile, string role);
        bool UpdateOrderStatus(int id, int loggedBy, short orderStatusID);
    }
}