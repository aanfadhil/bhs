//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LPG.Models
{
    using System;
    
    public partial class GetOrderList_Result
    {
        public Nullable<int> NumberOfProducts { get; set; }
        public Nullable<int> OrdrID { get; set; }
        public Nullable<int> CityID { get; set; }
        public string CityName { get; set; }
        public string InvoiceNumber { get; set; }
        public Nullable<int> BuyingStatus { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string AgentAdminName { get; set; }
        public string MobileNumber { get; set; }
        public string SlotName { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string ProductList { get; set; }
        public string Quantity { get; set; }
        public string Voucher { get; set; }
        public Nullable<int> PromoID { get; set; }
        public string Agen { get; set; }
        public Nullable<int> AgenID { get; set; }
        public string Driver { get; set; }
        public Nullable<int> DriverID { get; set; }
        public Nullable<int> Rating { get; set; }
    }
}
