﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LPG.App_Classes
{
    public enum UserType
    {
        Admin=1,
        UserAgent=2
    }
    public enum BannerCategory
    {
        [Display(Name = "Cover Banner")]
        CoverBanner = 1,
        [Display(Name = "Footer Banner")]
        FooterBanner = 2,
        [Display(Name = "Promo Banner")]
        PromoBanner = 3
    }
    public enum OrderStatus
    {

        Belumditerima = 1,
        Diterima = 2,
        Dikirim = 3,
        Berhasil = 4,
        Dibatalkanoleh_Consumer= 5,
        Dibatalkanoleh_System = 6,
        Dibatalkanoleh_Super_User = 7,
        Dibatalkanoleh_HQ_Super_User = 8,
        Belumdikirim=9
    }
    public enum BuyingStatus
    {
        [Display (Name ="Delivery (Aplikasi)")]
        OrderApp=1,
        [Display(Name = "Delivery (Telepon)")]
        OrderTelp =2,
        [Display(Name ="Delivery (Pickup)")]
        OrderPick=3,
    }
    public enum PublishStatus
    {
        Publish=1,
        Unpublish=2
    }
    // Refill/Exchange/New
    public enum OrderedProdType
    {
        [Display(Name = "Tukar Tambah")]
        New = 1,
        [Display(Name = "Refill")]
        Refill = 2,
        [Display(Name = "Tabung + Isi")]
        Exchange = 3
    }
    public enum ExcelOrderStatus
    {
        [Description("String 1")]
        OrderApp =1,
        OrderTelp=2,
        OrderPickUp=3
    }
    

}