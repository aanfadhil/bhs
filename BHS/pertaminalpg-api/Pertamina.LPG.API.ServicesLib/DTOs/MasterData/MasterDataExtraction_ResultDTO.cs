﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Pertamina.LPG.API.ServicesLib.DTOs.Orders.Infrastructure;
using Pertamina.LPG.API.Models;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Pertamina.LPG.API.ServicesLib.DTOs.MasterData
{
    [DataContract]
    public class MasterDataExtraction_ResultDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public Nullable<int> OrdrID { get; set; }
        [DataMember]
        public string InvoiceNumber { get; set; }
        [DataMember]
        public Nullable<int> BuyingStatus { get; set; }
        [DataMember]
        public Nullable<int> StatusID { get; set; }
        [DataMember]
        public string orderstatus { get; set; }
        [DataMember]
        public Nullable<decimal> GrandTotal { get; set; }
        [DataMember]
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string AgentAdminName { get; set; }
        [DataMember]
        public string MobileNumber { get; set; }
        [DataMember]
        public string SlotName { get; set; }
        [DataMember]
        public Nullable<System.DateTime> OrderDate { get; set; }
        [DataMember]
        public string RegionCode { get; set; }
        [DataMember]
        public string RegionName { get; set; }
        [DataMember]
        public string ProductList { get; set; }
        [DataMember]
        public Nullable<int> QUANTITY { get; set; }
        [DataMember]
        public Nullable<double> Weight { get; set; }
        [DataMember]
        public string Rating { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string ReasonText { get; set; }
        [DataMember]
        public Nullable<int> Category { get; set; }
    }

    public class MasterDataExtraction_ResultMapper : MapperBase<MasterDataExtraction_Result, MasterDataExtraction_ResultDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<MasterDataExtraction_Result, MasterDataExtraction_ResultDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<MasterDataExtraction_Result, MasterDataExtraction_ResultDTO>>) (p => new MasterDataExtraction_ResultDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    OrdrID = p.OrdrID,
                    InvoiceNumber = p.InvoiceNumber,
                    BuyingStatus = p.BuyingStatus,
                    StatusID = p.StatusID,
                    orderstatus = p.orderstatus,
                    GrandTotal = p.GrandTotal,
                    DeliveryDate = p.DeliveryDate,
                    Name = p.Name,
                    Address = p.Address,
                    AgentAdminName = p.AgentAdminName,
                    MobileNumber = p.MobileNumber,
                    SlotName = p.SlotName,
                    OrderDate = p.OrderDate,
                    RegionCode = p.RegionCode,
                    RegionName = p.RegionName,
                    ProductList = p.ProductList,
                    QUANTITY = p.QUANTITY,
                    Weight = p.Weight,
                    Rating = p.Rating,
                    Comments = p.Comments,
                    ReasonText = p.ReasonText,
                    Category = p.Category
                }));
            }
        }

        public override void MapToModel(MasterDataExtraction_ResultDTO dto, MasterDataExtraction_Result model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.OrdrID = dto.OrdrID;
            model.InvoiceNumber = dto.InvoiceNumber;
            model.BuyingStatus = dto.BuyingStatus;
            model.StatusID = dto.StatusID;
            model.orderstatus = dto.orderstatus;
            model.GrandTotal = dto.GrandTotal;
            model.DeliveryDate = dto.DeliveryDate;
            model.Name = dto.Name;
            model.Address = dto.Address;
            model.AgentAdminName = dto.AgentAdminName;
            model.MobileNumber = dto.MobileNumber;
            model.SlotName = dto.SlotName;
            model.OrderDate = dto.OrderDate;
            model.RegionCode = dto.RegionCode;
            model.RegionName = dto.RegionName;
            model.ProductList = dto.ProductList;
            model.QUANTITY = dto.QUANTITY;
            model.Weight = dto.Weight;
            model.Rating = dto.Rating;
            model.Comments = dto.Comments;
            model.ReasonText = dto.ReasonText;
            model.Category = dto.Category;

        }
    }
}
