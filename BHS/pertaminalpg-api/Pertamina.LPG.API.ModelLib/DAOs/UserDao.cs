﻿using Pertamina.LPG.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pertamina.LPG.API.DAOs
{
    public class UserDao : DAO
    {
        public Consumer Insert(Consumer consumer)
        {
            _context.Consumers.Add(consumer);
            _context.SaveChanges();
            return consumer;
        }

        /// <summary>
        /// Retrieves a consumer record and returns the entity instance, if available.
        /// </summary>
        /// <param name="id">Promary key id of the consumer.</param>
        /// <param name="withDetails">Whether to include all child data like addresses etc.</param>
        /// <returns>Instance of Consumer with data or null if record not found.</returns>
        public Consumer FindById(int id, bool withDetails = false)
        {
            if (withDetails)
            {
                var consumers = _context.Consumers.Include("ConsumerAddresses").Where(c => c.ConsID == id && c.StatusID == 1);
                if (consumers.Count() > 0)
                {
                    return consumers.Single();
                }
                return null;
            }
            return _context.Consumers.Find(id);
        }

        public bool CheckPhoneExists(string phoneNumber)
        {
            bool exists = _context.Consumers.Any(c => c.PhoneNumber.Replace("+62","") == phoneNumber.Replace("+62", "") && c.StatusID == 1);
            return exists;
        }

        public void Update(Consumer consumer)
        {
            _context.Entry(consumer).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }

        public Consumer FindByMobileNumber(string mobileNumber)
        {
            var consumers = _context.Consumers.Where(c => c.PhoneNumber.Replace("+62", "") == mobileNumber.Replace("+62", "") && c.StatusID == 1);
            if (consumers.Count() > 0)
            {
                return consumers.Single();
            }
            
            return null;
        }

        public void UpdateAddress(ConsumerAddress address)
        {
            // _context.
            address.PostalCode = address.PostalCode ?? "";
            _context.Entry(address).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }

        public void AddAddress(ConsumerAddress address)
        {
            address.PostalCode = address.PostalCode ?? "";
            _context.ConsumerAddresses.Add(address);
            _context.SaveChanges();
        }

        public bool DeleteAddress(int addressId,out bool isDefault)
        {
            ConsumerAddress addr = _context.ConsumerAddresses.Find(addressId);            
            if (addr != null)
            {
                isDefault = addr.IsDefault;
                if (!isDefault)
                {
                    addr.StatusID = 0;
                    _context.Entry(addr).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    return true;
                }
            }
            isDefault = false;
            return false;
        }

        public List<GetUserNotificationHistory_Result> GetNotificationList(int userId, string search,int pagelength, int pagenumber)
        {
            return _context.GetUserNotificationHistory(pagelength, pagenumber, search, userId, "pelanggan").ToList();
        }

        public int GetNotificationCount(int userId, string search, int pagelength, int pagenumber)
        {
            var count = _context.GetUserNotificationHistoryCount(pagelength, pagenumber, search, userId, "pelanggan").FirstOrDefault();
            if(count == null)
            {
                return 0;
            }
            return count.Total.Value;
        }

        public int GetNewNotificationCount(int userId)
        {
            var count = _context.GetUserNewNotificationCount(userId, "pelanggan").FirstOrDefault();
            if (count == null)
            {
                if(count.Total == null)
                    return 0;
            }
            return count.Total.Value;
        }

        public ConsumerAddress FindAddressById(int addressId)
        {
            return _context.ConsumerAddresses.Where(x=>x.AddrID == addressId && x.StatusID == 1).FirstOrDefault();
        }

        public ConsumerAddress FindDefaultAddressFor(int userId)
        {
            return _context.ConsumerAddresses.Where(a => a.ConsID == userId && a.IsDefault && a.StatusID == 1).First();
        }

        public ConsumerAddress FindDefaultAddressForUser(int addressID)
        {
            return _context.ConsumerAddresses.Where(a => a.AddrID  == addressID && a.IsDefault && a.StatusID == 1).FirstOrDefault();
        }

        public List<ConsumerAddress> GetAllAddresses()
        {
            return _context.ConsumerAddresses.OrderBy(t => t.AddrID).ToList();
        }

        public void UpdateCity(int id, string cityName)
        {

            var data = _context.ConsumerAddresses.Where(t => t.AddrID == id).FirstOrDefault();
            
            if (data != null)
            {

                data.RegionName = cityName;
                var log = _context.MKeyValues.Find(14);
                log.Value = id.ToString();

                _context.SaveChanges();
            }

        }
    }
}