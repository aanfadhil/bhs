namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Reminder")]
    public partial class Reminder
    {
        [Key]
        public int RmdrID { get; set; }

        [StringLength(255)]
        public string ReminderImage { get; set; }

        public bool UserType { get; set; }

        [Required]
        public string Description { get; set; }

        public bool StatusId { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int? ProdID { get; set; }

        public virtual Product Product { get; set; }

        public virtual SuperAdmin SuperAdmin { get; set; }

        public virtual SuperAdmin SuperAdmin1 { get; set; }
    }
}
