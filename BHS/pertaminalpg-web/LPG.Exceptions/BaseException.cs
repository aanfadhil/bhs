﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBG.Shared.Exceptions
{
    /// <summary>
    /// Parent class for custom exception classes
    /// </summary>
    public abstract class BaseException : Exception
    {
        /// <summary>
        /// Gets/Sets the severity level of the current exception
        /// </summary>
        public ExceptionSeverity Severity { get; set; }

        /// <summary>
        /// Gets the Date & Time when the exception instance is created
        /// </summary>
        public DateTime TimeStamp { get; private set; }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        protected BaseException(string message) : base(message) { Severity = ExceptionSeverity.Normal; TimeStamp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"); }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        protected BaseException(Exception actualException) : base(actualException.Message, actualException) { Severity = ExceptionSeverity.Normal; TimeStamp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"); }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        protected BaseException(string message, Exception actualException) : base(message, actualException) { Severity = ExceptionSeverity.Normal; TimeStamp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"); }

        /// <summary>
        /// Log the exception
        /// </summary>
        /// <param name="applicationName">Name of the current application</param>
        /// <param name="additionalInfo">Optional, additional information to be included in the log</param>
        public virtual void Publish(string applicationName, string additionalInfo = "")
        {
            ExceptionHandler.PublishException(applicationName, this, additionalInfo, ExceptionPublishMode.File);
        }

        /// <summary>
        /// Log the exception
        /// </summary>
        /// <param name="applicationName">Name of the current application</param>
        /// <param name="additionalInfo">Optional, additional information to be included in the log</param>
        /// <param name="publishModes">Modes in which the exception has to be published</param>
        public virtual void Publish(string applicationName, string additionalInfo = "", params ExceptionPublishMode[] publishModes)
        {
            ExceptionHandler.PublishException(applicationName, this, additionalInfo, publishModes);
        }
    }
}
