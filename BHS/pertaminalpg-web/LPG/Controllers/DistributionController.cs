﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class DistributionController : AbstractController<IDistributionService>
    {
        SessionManagement sessionManagement;
        private IAgencyService agencyObject;// = new AgencyService();

        public DistributionController(IDistributionService _serviceObject, IAgencyService _agencyObject) : base(_serviceObject)
        {
            agencyObject = _agencyObject;
        }

        //DistributionService serviceObject = new DistributionService();
        // GET: Distribution
        public ActionResult ListDistribution()
        {
            var vm = new DistributionModel();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.DistributionList = serviceObject.ListDistribution();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(vm);
        }

        public ActionResult AddDistribution(int? id)
        {
            try
            {
                DistributionModel model = new DistributionModel();


                sessionManagement = new SessionManagement();
                var agencyList = agencyObject.ListComapany();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    var convid = id ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.getDistributionDetails(convid);
                        if (vm != null)
                        {
                            string[] cord = vm.Lat?.Split(',');
                            if (cord.Count() == 2)
                            {
                                vm.Latitude = cord[0];
                                vm.Longitude = cord[1];
                            }
                            vm.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                            return View(vm);
                        }
                        else
                        {
                            model.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                            return View(model);
                        }
                    }
                    else
                    {
                        model.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        return View(model);

                    }

                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public ActionResult AddDistribution(DistributionModel vm)
        {

            try
            {
                sessionManagement = new SessionManagement();
                //AgencyService agencyObject = new AgencyService();

                var agencyList = agencyObject.ListComapany();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    if (vm.AgenDriverAgenId == 0)
                    {
                        vm.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        vm.errorMessage = MessageSource.GetMessage("required.data");
                        return View(vm);
                    }

                    string[] cord = vm.Lat?.Split(',');
                    if (cord.Count() == 2)
                    {
                        vm.Lat = cord[0];
                        vm.Long = cord[1];
                    }

                    serviceObject.saveDistributionDetails(vm, adminSessionInfo.UserId);

                    SetFlash(FlashMessageType.Success, "Input tiitk distribusi berhasil");

                    return RedirectToAction("ListDistribution", "Distribution");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }
        }

        public ActionResult DeleteDistribution(int id)
        {
            sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {
                if (serviceObject.DeleteDistribution(id, adminSessionInfo.UserId))
                {
                    //return View();
                    return RedirectToAction("ListDistribution", "Distribution");
                }

            }
            return RedirectToAction("Index", "Error");
        }

    }
}
