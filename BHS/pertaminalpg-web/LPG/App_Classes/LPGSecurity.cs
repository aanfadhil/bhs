﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.App_Classes
{
    public static class LPGSecurity
    {
        public static string EncryptString(this string value)
        {
            return Security.Encrypt(value);
        }

        public static string DecryptString(this string value)
        {
            if (value != "[]")
                return Security.Decrypt(value);
            else
                return string.Empty;
        }
    }
}