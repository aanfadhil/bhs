﻿using LPG.App_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG
{
    public static class SessionHelper
    {
        public static int UserId { get { return new SessionManagement().getAdminSession()?.UserId ?? 0; } }
    }
}