﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pertamina.LPG.API.DTOs.Orders
{
    public class OrderDto
    {
        public int order_id { get; set; }
        public string invoice_number { get; set; }
        public decimal grand_total { get; set; }
        public string consumer_name { get; set; }
        public string consumer_mobile { get; set; }
        public string consumer_address { get; set; }
        public string consumer_location { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
        public string more_info { get; set; }
        public decimal? potongan_voucher { get; set; }
        public string voucher { get; set; }
        public string order_date { get; set; }
        public TimeSpan order_time { get; set; }
    }
}