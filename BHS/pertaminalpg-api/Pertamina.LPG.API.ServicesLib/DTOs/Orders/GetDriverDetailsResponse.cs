﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pertamina.LPG.API.DTOs.Orders
{
    public class GetDriverDetailsResponse : ResponseDto
    {
        public DriverDetailsDto driver_details { get; set; }
    }

    public class GetDriverDetailsForReviewResponse : ResponseDto
    {
        public int driver_id { get; set; }
        public string driver_name { get; set; }
        public string driver_image { get; set; }
        public string driver_mobile { get; set; }
        public string driver_profile_image { get; set; }
        public decimal driver_rating { get; set; }
    }
}
