﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IBannerPromoService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        bool deleteBannerPromo(int id);
        List<PromoBannerModel> getActiveBanners();
        PromoBannerModel getHomeBannerDetails(int homeBannerId);
        List<PromoBannerModel> ListHomeBanner();
        PromoBannerModel saveHomeBannerDetails(PromoBannerModel entity, string Cmd, int loggedBy);
    }
}