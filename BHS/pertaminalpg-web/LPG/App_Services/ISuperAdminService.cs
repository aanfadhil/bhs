﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface ISuperAdminService
    {
        void CallActivityLog(string action, string action_item, int userId);
        bool DeleteSuperAdminUser(int Id, int loggeduserId);
        void UpdateSuperAdminCities(int saID, List<int> cityIds, int loggeduserId);
        void UpdateSuperAdminPhone(int saID, string phone);
        List<SellerReportModel> GetOrderStatusReport();
        HighChartBar<decimal> GetRegisteredUserByMonth(int monthsToShow);
        int GetRegisteredUserTotal();
        List<SellerReportModel> GetSellerRatingReport(ChartRequestModel model);
        List<SellerReportModel> GetSellerReport(ChartRequestModel model);
        SuperAdminModel GetSuperAdminUser(int Id);
        RegionModel GetSuperAdminDefaultRegion(int saID);
        bool IsExists(int id);
        bool IsExistsEmailID(string email, int userId);
        bool IsExistsMobileNo(string mobileNo, int userId);
        List<SuperAdminModel> ListSuperAdminUser();
        List<MSuperUserTypeModel> ListSuperUserType();
        List<ProvinceModel> ListProvince();
        SuperAdminModel SaveSuperAdminPassword(SuperAdminModel model);
        SuperAdminModel SaveSuperAdminUser(SuperAdminModel model, int loggeduserId);
        List<CityModel> ListCities(int provinceId=0);

    }
}