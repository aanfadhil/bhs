namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OneTimePwd")]
    public partial class OneTimePwd
    {
        [Key]
        public int OtgID { get; set; }

        public int UserID { get; set; }

        [Required]
        [StringLength(1)]
        public string UserType { get; set; }

        [StringLength(6)]
        public string Otg { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
