﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class PengaturanPromoModel
    {
        public int PromoID { get; set; }
        public string Title { get; set; }
        public decimal Potongan { get; set; }
        public string Items { get; set; }
        public int RegionID { get; set; }
        public int ProdID { get; set; }
        public string UserID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public DateTime? DStartDate { get; set; }
        public DateTime? DEndDate { get; set; }
        public int? Quota { get; set; }
        public int? UserQuota { get; set; }
        public int? QuotaUsed { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public DateTime? DCreatedDate { get; set; }
        
        public string Voucher { get; set; }
        public string ErrorMessage { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public DateTime? DUpdatedDate { get; set; }
        public string LastActivity { get; set; }

        public bool isSuccess { get; set; }

        public List<RegionModel> Regions { get; set; }
        public List<ProductModel> Products { get; set; }
        public List<CustomerModel> Customers { get; set; }
        public List<BannerInfoModel> BannerInfo { get; set; }
    }
}