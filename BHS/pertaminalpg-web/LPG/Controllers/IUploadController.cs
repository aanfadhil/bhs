﻿using System.Net.Http;
using System.Threading.Tasks;

namespace LPG.Controllers
{
    public interface IUploadController
    {
        Task<HttpResponseMessage> PostProfileImage();
    }
}
