﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace LPG.Reporting.DTOs.Orders
{
    [DataContract]
    public class OrderDetailDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public int OrdtID { get; set; }
        [DataMember]
        public int OrdrID { get; set; }
        [DataMember]
        public int ProdID { get; set; }
        [DataMember]
        public int Quantity { get; set; }
        [DataMember]
        public decimal UnitPrice { get; set; }
        [DataMember]
        public decimal SubTotal { get; set; }
        [DataMember]
        public decimal PromoProduct { get; set; }
        [DataMember]
        public decimal ShippingCharge { get; set; }
        [DataMember]
        public decimal PromoShipping { get; set; }
        [DataMember]
        public decimal TotamAmount { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int RefillQuantity { get; set; }
        [DataMember]
        public decimal RefillPrice { get; set; }
        [DataMember]
        public decimal PromoRefill { get; set; }
        [DataMember]
        public decimal RefillSubTotal { get; set; }
        [DataMember]
        public decimal RefillTotalAmount { get; set; }
    }

    public class OrderDetailMapper : MapperBase<OrderDetail, OrderDetailDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 

        public override Expression<Func<OrderDetail, OrderDetailDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<OrderDetail, OrderDetailDTO>>) (p => new OrderDetailDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    OrdtID = p.OrdtID,
                    OrdrID = p.OrdrID,
                    ProdID = p.ProdID,
                    Quantity = p.Quantity,
                    UnitPrice = p.UnitPrice,
                    SubTotal = p.SubTotal,
                    PromoProduct = p.PromoProduct,
                    ShippingCharge = p.ShippingCharge,
                    PromoShipping = p.PromoShipping,
                    TotamAmount = p.TotamAmount,
                    CreatedDate = p.CreatedDate,
                    RefillQuantity = p.RefillQuantity,
                    RefillPrice = p.RefillPrice,
                    PromoRefill = p.PromoRefill,
                    RefillSubTotal = p.RefillSubTotal,
                    RefillTotalAmount = p.RefillTotalAmount
                }));
            }
        }

        public override void MapToModel(OrderDetailDTO dto, OrderDetail model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.OrdtID = dto.OrdtID;
            model.OrdrID = dto.OrdrID;
            model.ProdID = dto.ProdID;
            model.Quantity = dto.Quantity;
            model.UnitPrice = dto.UnitPrice;
            model.SubTotal = dto.SubTotal;
            model.PromoProduct = dto.PromoProduct;
            model.ShippingCharge = dto.ShippingCharge;
            model.PromoShipping = dto.PromoShipping;
            model.TotamAmount = dto.TotamAmount;
            model.CreatedDate = dto.CreatedDate;
            model.RefillQuantity = dto.RefillQuantity;
            model.RefillPrice = dto.RefillPrice;
            model.PromoRefill = dto.PromoRefill;
            model.RefillSubTotal = dto.RefillSubTotal;
            model.RefillTotalAmount = dto.RefillTotalAmount;

        }
    }
}
