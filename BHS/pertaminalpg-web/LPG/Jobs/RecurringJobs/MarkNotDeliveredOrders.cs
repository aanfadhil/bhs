﻿using Hangfire;
using Hangfire.Console;
using Hangfire.Server;
using log4net;
using LPG.App_Services;
using LPG.Models;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LPG.Jobs.RecurringJobs
{
    public class MarkNotDeliveredOrders
    {
        public static int CurrentId = -1;

        private RecurringJobManager manager = new RecurringJobManager();

        private lpgdevEntities _context = new lpgdevEntities();



        public enum DeliveryStatus : int
        {
            Assigned = 1,
            OutForDelivery = 2,
            Cancelled = 3,
            OnTimeDelivery = 4,
            LateDelivery = 5,
            EarlyDelivery = 6
        }

        public enum OrderStatus : int
        {
            Received = 1,
            Processed = 2,
            OutForDelivery = 3,
            Delivered = 4,
            CancelledByConsumer = 5,
            CancelledBySystem = 6,
            CancelledBySuperUser = 7,
            CancelledByHQSuperUser = 8,
            ProcessedButNotDelivered = 9

        }

        private static ILog _logger;

        private static ILog Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                }

                return _logger;
            }
        }

        [Queue("main")]
        [AutomaticRetry(Attempts = 0, LogEvents = true, OnAttemptsExceeded = AttemptsExceededAction.Fail)]
        public void RunJob(IJobCancellationToken cancellationToken, PerformContext context)
        {

            Logger.Debug("[" + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") + "]" + " Started MarkNotDeliveredOrders Job at " + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"));

            var start = new TimeSpan(0, 0, 0);
            var end = new TimeSpan(12, 0, 0);
            var now = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;

            var dayFirstDeliverySlot = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").Date + new TimeSpan(8, 0, 0);

            // We only do work
            if ((now > start) && (now < end))
            {

                using (lpgdevEntities _context = new lpgdevEntities())
                {

                    var AssignedStatus = DeliveryStatus.Assigned.ToEnumValue<int>();

                    //|| d.StartDate == null
                    var assignedToDriverOrders = _context.OrderDeliveries.Where(d => (d.StatusId == 1) && d.DeliveryDate < dayFirstDeliverySlot && d.DeliveryDate != null);

                    var totalOrders = assignedToDriverOrders.Count();

                    if (totalOrders == 0)
                    {

                        var m = "[" + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") + "]" + " Got " + totalOrders + " not delivered orders with Delivery Date less than " + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").ToString("dd-MM-yyyy") + ". Stopping here. Bye!";

                        throw new JobAbortedException();
                    }

                    var msg = "[" + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") + "]" + " Got " + totalOrders + " not delivered orders with Delivery Date less than " + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").ToString("dd-MM-yyyy");

                    Logger.Debug(msg);

                    context.WriteLine(msg);

                    context.WriteLine("[" + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") + "]" + " Starting to schedule orders to OrderStatus and DeliveryStatus change");

                    var progress = context.WriteProgressBar();

                    var counter = 0;

                    foreach (var o in assignedToDriverOrders)
                    {
                        var m = "[" + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") + "]" + " Scheduling Order ID: " + o.OrdrID + ". Accepted On: " + o.AcceptedDate.ToString("dd-MM-yyyy") + " With Delivery On: " + o.DeliveryDate?.ToString("dd-MM-yyyy");
                        Logger.Debug(m);

                        context.WriteLine(m);

                        BackgroundJob.Enqueue(() => ChangeOrderStatusAsync(o.OrdrID, o.DelvID, OrderStatus.ProcessedButNotDelivered, DeliveryStatus.Cancelled, null));
                        counter++;

                        progress.SetValue(counter / totalOrders);
                    }

                    progress.SetValue(100);

                }


            }
            else
            {
                context.WriteLine("[" + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") + "]" + " This task can only be executed between 00:00 and 8:00 Indonesian Time. Stopping now. Bye!");
            }


        }

        [Queue("orders")]
        [AutomaticRetry(Attempts = 0, LogEvents = true, OnAttemptsExceeded = AttemptsExceededAction.Fail)]
        public async Task ChangeOrderStatusAsync(int OrderId, int DeliveryId, OrderStatus newOrderStatus, DeliveryStatus newDeliveryStatus, PerformContext context)
        {

            var intOrderStatus = (int) newOrderStatus;
            var intDeliveryStatus = (int) newDeliveryStatus;

            var msg = "[" + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") + "]" + " Changing Order ID: " + OrderId + " OrderStatus: " + newOrderStatus.ToString() + " DeliveryStatus: " + newDeliveryStatus.ToString();

            Logger.Debug(msg);
            context.WriteLine(msg);

            var progress = context.WriteProgressBar();
            progress.SetValue(20);
            using (lpgdevEntities _context = new lpgdevEntities())
            {
                var order = await _context.Orders.FindAsync(OrderId);

                progress.SetValue(40);

                var delivery = await _context.OrderDeliveries.FindAsync(DeliveryId);

                progress.SetValue(60);

                if (order == null || delivery == null)
                {
                    if (order == null)
                    {

                        var m = "Cannot find an order with ID: " + OrderId + ". Stopping here.";
                        context.WriteLine(m);
                    }

                    if (delivery == null)
                    {

                        var m = "Cannot find an order delivery with ID: " + DeliveryId + ". Stopping here.";
                        context.WriteLine(m);
                    }

                    throw new JobAbortedException();
                }

                order.StatusID = (short) intOrderStatus;
                order.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                delivery.StatusId = (short) intDeliveryStatus;
                progress.SetValue(70);

                var res = await _context.SaveChangesAsync();

                //progress.SetValue(95);

                progress.SetValue(100);

                context.WriteLine("[" + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") + "]" + " Finished Status Update for Order " + OrderId + " New OrderStatus: " + newOrderStatus.ToString() + " New DeliveryStatus: " + newDeliveryStatus.ToString());
            }


        }

    }
}
