﻿using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LPG.App_Classes;
using Microsoft.ApplicationInsights;

namespace LPG.App_Services
{
    public class ActivityService : AbstractAppService, IActivityService
    {
        public ActivityService(lpgdevEntities context, TelemetryClient _telemetry) : base(context, _telemetry)
        {
            this._activityService = this;
        }

        public List<ActivityModel> ListActivity(int loggedBy)
        {
            var contactList = new List<ActivityModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from activity in context.ActivityLogs
                           join user in context.SuperAdmins
                           on activity.SAdminID equals user.SAdminID
                           where activity.SAdminID == loggedBy
                           orderby activity.CreatedDate descending
                           select new ActivityModel
                           {
                               ActvID = activity.ActvID,
                               Item = activity.Item,
                               Activity = activity.Activity,
                               ActDate = activity.ActDate,
                               UserType = activity.SAdminID,
                               ActTime = activity.ActTime,
                               UserName = user.FullName
                           };

                contactList = data.ToList();
            }
            return contactList;
        }

        /// <summary>
        /// Add Activity Log - Common method for
        /// Product
        /// Agent
        /// FAQ
        /// BannerInfo
        /// Banner Adv
        /// </summary>     
        /// <returns></returns>
        public ActivityModel AddActivityLog(ActivityModel model, string pageName)
        {
            try
            {
                using (var context = new lpgdevEntities())
                {
                    ActivityLog activity = new ActivityLog();
                    activity.ActvID = model.ActvID;
                    activity.Item = model.Item;
                    activity.Activity = model.Activity;
                    activity.SAdminID = model.UserType.ToShort();
                    activity.ActDate = model.ActDate;
                    activity.ActTime = model.ActTime;
                    activity.CreatedDate = model.CreatedDate;
                    context.ActivityLogs.Add(activity);
                    context.SaveChanges();
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
