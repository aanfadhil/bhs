﻿using LPG.App_Classes;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace LPG.Filters
{
    public class SessionHandleAttribute : ActionFilterAttribute
    {
        //public SessionManagement sessionManagement { get; private set; }
        //public AdminSessionEntity adminSessionInfo { get; private set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            //sessionManagement = new SessionManagement();
            //adminSessionInfo = sessionManagement.getAdminSession();

            if (ctx.Session["AdminSession"] == null)
            {
                var sessioncookie = ctx.Request.Cookies["bhssession"];

                if (sessioncookie != null)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    try
                    {
                        var jsonSession = sessioncookie.Value.DecryptString();

                        var sessionObj = serializer.Deserialize<AdminSessionEntity>(jsonSession);

                        if(sessionObj != null)
                        {
                            ctx.Session["AdminSession"] = sessionObj;
                            base.OnActionExecuting(filterContext);
                            return;

                        }

                    }
                    catch(Exception e)
                    {
                        filterContext.Result = new RedirectResult("~/Login/Index");
                        return;
                    }
                }
                
                filterContext.Result = new RedirectResult("~/Login/Index");
                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}