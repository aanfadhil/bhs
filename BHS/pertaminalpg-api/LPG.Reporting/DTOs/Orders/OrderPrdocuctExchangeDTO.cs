﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.Orders
{
    public class OrderPrdocuctExchangeDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int OrdPrdExID { get; set; }
        public int OrdrID { get; set; }
        public int ProdID { get; set; }
        public string ExchangeWith { get; set; }
        public int ExchangeQuantity { get; set; }
        public decimal ExchangePrice { get; set; }
        public decimal ExchangePromoPrice { get; set; }
        public bool StatusId { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TotalAmount { get; set; }
    }

    public class OrderPrdocuctExchangeMapper : MapperBase<OrderPrdocuctExchange, OrderPrdocuctExchangeDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<OrderPrdocuctExchange, OrderPrdocuctExchangeDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<OrderPrdocuctExchange, OrderPrdocuctExchangeDTO>>) (p => new OrderPrdocuctExchangeDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    OrdPrdExID = p.OrdPrdExID,
                    OrdrID = p.OrdrID,
                    ProdID = p.ProdID,
                    ExchangeWith = p.ExchangeWith,
                    ExchangeQuantity = p.ExchangeQuantity,
                    ExchangePrice = p.ExchangePrice,
                    ExchangePromoPrice = p.ExchangePromoPrice,
                    StatusId = p.StatusId,
                    CreatedDate = p.CreatedDate,
                    SubTotal = p.SubTotal,
                    TotalAmount = p.TotalAmount
                }));
            }
        }

        public override void MapToModel(OrderPrdocuctExchangeDTO dto, OrderPrdocuctExchange model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.OrdPrdExID = dto.OrdPrdExID;
            model.OrdrID = dto.OrdrID;
            model.ProdID = dto.ProdID;
            model.ExchangeWith = dto.ExchangeWith;
            model.ExchangeQuantity = dto.ExchangeQuantity;
            model.ExchangePrice = dto.ExchangePrice;
            model.ExchangePromoPrice = dto.ExchangePromoPrice;
            model.StatusId = dto.StatusId;
            model.CreatedDate = dto.CreatedDate;
            model.SubTotal = dto.SubTotal;
            model.TotalAmount = dto.TotalAmount;

        }
    }
}
