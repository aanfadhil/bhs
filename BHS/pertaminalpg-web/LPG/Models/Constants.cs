﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class Constants
    {
        #region ProfilePhoto Path
        public const string ProfileImagePath_SuperUser = "~/extfiles/profile/superuser/";
        public const string ProfileImagePath_AgentBoss = "~/extfiles/profile/agentboss/";
        public const string ProfileImagePath_AgentAdmin = "~/extfiles/profile/agentadmin/";
        public const string ProfileImagePath_Driver = "~/extfiles/profile/driver/";
        public const string ProfileImagePath_Customer = "~/extfiles/profile/customer/";
        #endregion

        #region APIs
        public const string UpdateProfilePhoto_SuperUser = "api/SuperUser/change_profilephoto_super_user/";
        public const string UpdateProfilePhoto_AgentBoss = "api/AgentBoss/change_profilephoto_agent_boss/";
        public const string UpdateProfilePhoto_AgentAdmin = "api/AgentAdmin/change_profilephoto_agent_admin/";
        public const string UpdateProfilePhoto_Driver = "api/Driver/change_profilephoto_driver/";
        public const string UpdateProfilePhoto_Customer = "api/Users/change_profilephoto_user/";
        #endregion

        #region EmailTemplate
        //For sending mails
        public static readonly string[] EmailSubject = { "New user " , " has been created in CMS"};
        public static readonly string EmailSubjectForgotPwd = "Password reset in CMS ";
        public static readonly string[] EmailBody = { "Hi ", "<br> <br> Please find your credentials for CMS, <br> <br> User Name: ", "<br> Password: ", " <br> <br> Thanks & Regards <br> System" };

        #endregion

        #region Common
        public const string APPSETTING_REPORTPERIOD_RANGE = "ReportPeriodRange";
        public const string APPSETTING_COUNTRY_CODE = "+62";
        #endregion
    }
}