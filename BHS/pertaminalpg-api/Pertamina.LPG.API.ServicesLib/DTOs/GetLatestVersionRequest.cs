﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs
{
    public class GetLatestVersionRequest
    {
        public string app_os { get; set; }
    }

    public class GetLatestVersionResponse : ResponseDto
    {
        public string version { get; set; }
        public string version_code { get; set; }
        public string app_id { get; set; }
    }
}
