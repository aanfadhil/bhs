﻿using Hangfire.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin;
using LPG.App_Classes;

namespace LPG.Jobs.Hangfire
{
    public class HangfireDashboardAuthFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            // In case you need an OWIN context, use the next line, `OwinContext` class
            // is the part of the `Microsoft.Owin` package.
            var owinContext = new OwinContext(context.GetOwinEnvironment());

            //var sessionManagement = new SessionManagement();

            //if (sessionManagement.getAdminSession() != null)
            //{
            //    //owinContext.Authentication.SignIn();
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}

            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            //return owinContext.Authentication.User.Identity.IsAuthenticated;

            return true;
        }
    }

}
