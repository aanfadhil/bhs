﻿using System;
using Microsoft.Owin;
using Owin;
using i18n;
using Hangfire;
using Hangfire.Console;
using Hangfire.Logging.LogProviders;
using Pertamina.LPG.API.Jobs.Hangfire;
using System.Web.Http;

[assembly: OwinStartup(typeof(Pertamina.LPG.API.Startup))]
namespace Pertamina.LPG.API
{

    public partial class Startup
    {

        public static void ConfigureHandfire(HttpConfiguration config)
        {
            Hangfire.GlobalConfiguration.Configuration
               .UseLogProvider(new ColouredConsoleLogProvider())
               .UseSqlServerStorage("hangfireDbConnection")
               .UseConsole();
        }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureHangfire(IAppBuilder app)
        {

            Hangfire.GlobalConfiguration.Configuration
                .UseLogProvider(new ColouredConsoleLogProvider())
                .UseSqlServerStorage("hangfireDbConnection")
                .UseConsole();


            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                ServerName = "LPG API Jobs",
                Queues = new[] { "sms","holdorder" }
            });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new HangfireDashboardAuthFilter() }
            });


            ConfigureRecuringJobs();

        }

        private string CronDailyMidnight04 = "4 0 * * *"; //Actually 00:04 Every day 
        private string CronDailyMidnight12 = "12 0 * * *"; // 00:12 Every day
        private string CronEverySecondMinute = "*/2 * * * * "; // Every two minute
        private string CronEveryMinute = "*/1 * * * *"; //Every Minute

        //private RecurringJobManager manager = new RecurringJobManager();

        private void ConfigureRecuringJobs()
        {
            //#if RELEASE
            //RecurringJob.AddOrUpdate<MarkNotDeliveredOrders>(c => c.RunJob(JobCancellationToken.Null, null), () => CronDailyMidnight04, TimeZoneInfo.Local, "main");
            //RecurringJob.AddOrUpdate<MarkNotDeliveredOrders>(c => c.RunJob(JobCancellationToken.Null, null), () => CronDailyMidnight12, TimeZoneInfo.Local, "main");
            //#endif
        }
    }
}
