﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPG.App_Classes
{
    public class AdminSessionEntity 
    {

        public int UserId { get; set; }
        public string accessToken { get; set; }

        public string EmailId { get; set; }
        
        public string FullName { get; set; }

        public int  UserType { get; set; }
        public string UserRole { get; set; }
        public string imgProfile { get; set; }
        public int CityId { get; set; }
    }
    
}
