﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public enum ToastType
    {
        Error,
        Info,
        Success,
        Warning
    }
}