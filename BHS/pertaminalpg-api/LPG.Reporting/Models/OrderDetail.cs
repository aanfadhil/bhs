﻿namespace LPG.Reporting
{
    using LPG.Reporting.ReportServices;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Globalization;

    public partial class OrderDetail
    {
        [Key]
        public int OrdtID { get; set; }

        public int OrdrID { get; set; }

        public int ProdID { get; set; }

        public int Quantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal UnitPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PromoProduct { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ShippingCharge { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PromoShipping { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TotamAmount { get; set; }

        public DateTime CreatedDate { get; set; }

        public int RefillQuantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal RefillPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PromoRefill { get; set; }

        [Column(TypeName = "numeric")]
        public decimal RefillSubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal RefillTotalAmount { get; set; }

        public virtual Order Order { get; set; }

        public virtual Product Product { get; set; }


    }
}
