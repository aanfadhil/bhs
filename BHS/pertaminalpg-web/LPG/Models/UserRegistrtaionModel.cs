﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LPG.Models
{
    public class UserRegistrtaionModel
    {
        //Agen Company
        public int AgencyId { get; set; }
        [Required]
        public string SoldTono { get; set; }
        [Required]
        public string AgencyName { get; set; }
       
        [Required(ErrorMessage = "Region is required")]
        public int RegionId { get; set; }
        public string Region { get; set; }
        public bool IsAgencyExists { get; set; }

        public string CountryCode { get { return Constants.APPSETTING_COUNTRY_CODE; } }

        //Agen Boss
        public int AgencyBossId { get; set; }
        public string AgencyOwnername { get; set; }

        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid Mobile Number.")]
        public string AgencyBossMobile { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string AgentBossEmail { get; set; }

        [Required(ErrorMessage = "Nama Perusahaan (PT) is required")]
        public int ABAgencyId { get; set; }
        public string AgencyBossImage { get; set; }
        public bool IsAgentBossExists { get; set; }

        //Agen Distribution

        public int AgenDistId { get; set; }
        public string AgenDistributionName { get; set; }
        [Required(ErrorMessage = "Agent Address is required")]
        public string AgenAddress { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string DistCityId { get; set; }
        public string DistCityName { get; set; }


        [Required(ErrorMessage = "Name of Company Agent is required")]
        public int AgenDistAgencyId { get; set; }
        public string AgenDistAgencyName { get; set; }
        public List<SelectListItem> AgencyList { get; set; }
        public List<SelectListItem> DistributionList { get; set; }
        public List<SelectListItem> RegionList { get; set; }
        public bool IsDistributionExixts { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsActive { get; set; }

        //Agen Admin
        public int AgenAdminId { get; set; }
        [Required(ErrorMessage = "Distrubution point is required")]
        public int AgentDistId { get; set; }
        [Required(ErrorMessage = "Agent Admin  is required")]
        public int AgenAdAgenId { get; set; }
        public string AgenAdminName { get; set; }
        public int AgenDistribuName { get; set; }
        public int AgenCompany { get; set; }
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string AgentAdminEmail { get; set; }


        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid Mobile Number.")]
        public string AgnAdminMObile { get; set; }
        public bool IsAgentAdminExists { get; set; }

        //Agen Driver
        public int AgendriverId { get; set; }
        public string AgenDriverName { get; set; }

        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid Mobile Number.")]
        public string AgenDriverMobileNo { get; set; }
        [Required(ErrorMessage = "Distrubution point is required")]
        public int AgenDriverDistId { get; set; }
        [Required(ErrorMessage = "Agent admin is required")]
        public int AgenDriverAgenId { get; set; }
        public bool IsAgentDriverExixts { get; set; }
        public List<SelectListItem> DriverList { get; set; }

        //locationAddress
        public string address { get; set; }
       // public string errorMessage { get; set; }

    }
}