﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class DetailPriceModel
    {
        public int ID { get; set; }

        public int ProductID { get; set; }

        public int CityID { get; set; }

        public int TubeNormalPrice { get; set; }

        public int TubePromoPrice { get; set; }

        public int RefillNormalPrice { get; set; }

        public int RefillPromoPrice { get; set; }

        public String CityName { get; set; }

        public String ProductName { get; set; }

        public String ProductImg { get; set; }

        public MCity MCity { get; set; }

        public Product Product { get; set; }
    }
}