﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using LPG.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using ClosedXML.Excel;
using System.Globalization;
using i18n;
using LPG.Reporting.ReportServices;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class PengaturanPromoController : AbstractController<IPengaturanPromoServices>
    {
        SessionManagement sessionManagement;
        private IRegionServices RegionServices;
        private IProductService ProductService;
        private ICustomerService CustomerService;

        private const string SESSION_TIME_OUT = "SESSION_TIMEOUT";

        public PengaturanPromoController(
            IPengaturanPromoServices _serviceObject,
            IRegionServices _regionServices,
            IProductService _productService,
            ICustomerService _customerService
            ) : base(_serviceObject)
        {
            this.RegionServices = _regionServices;
            this.ProductService = _productService;
            this.CustomerService = _customerService;
        }
        
        // GET: PengaturanPromo
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult AddPromo()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products List" });
            }
        }

        [HttpPost]
        public ActionResult GetPromo(int promoId)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    return Json(serviceObject.GetPromo(promoId));
                }
                else
                {
                    return Json(SESSION_TIME_OUT);
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(SESSION_TIME_OUT);
            }
        }

        public ActionResult ListPromo()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    return View(serviceObject.ListVoucher());
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products List" });
            }
        }

        [HttpPost]
        public ActionResult GetRegion()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var regions = RegionServices.GetList().OrderBy(t => t.RegionName);
                    return Json(regions);
                }
                else
                {
                    return Json(null);
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(null);
            }
        }
        [HttpPost]
        public ActionResult GetProduct()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var products = ProductService.ListProducts();
                    return Json(products);
                }
                else
                {
                    return Json(SESSION_TIME_OUT);
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult GetCustomer(string name)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    name = name.ToLower();
                    var customer = CustomerService.ListCustomers().Where(t => t.Name.ToLower().Contains(name) || t.MobileNo.ToLower().Contains(name)).OrderBy(o => o.Name).ToList();
                    return Json(customer);
                }
                else
                {
                    return Json(SESSION_TIME_OUT);
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult GetVoucher()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    return Json(serviceObject.GetVoucher());
                }
                else
                {
                    return Json(null);
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult CheckVoucher(string voucher)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    return Json(serviceObject.CheckVoucher(voucher));
                }
                else
                {
                    return Json(null);
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult AddPromo(PengaturanPromoModel vm)
        {
            try
            {
                PengaturanPromoModel model = new PengaturanPromoModel();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    return Json(serviceObject.AddVoucher(vm,adminSessionInfo.UserId));
                }
                else
                {
                    return Json(SESSION_TIME_OUT);
                }

            }
            catch (Exception e)
            {
                return Json(SESSION_TIME_OUT);
            }
        }

        [HttpPost]
        public ActionResult GetUserById(List<int> ids)
        {
            try
            {
                PengaturanPromoModel model = new PengaturanPromoModel();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    return Json(CustomerService.ListCustomers().Where(t => ids.Contains(t.CustomerId)));
                }
                else
                {
                    return Json(SESSION_TIME_OUT);
                }

            }
            catch (Exception e)
            {
                return Json(SESSION_TIME_OUT);
            }
        }

        [HttpPost]
        public ActionResult GetTemplate()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var data = System.IO.File.ReadAllBytes(Server.MapPath("~/Content/template/TemplateUserPromo.xls"));
                    return File(data, "application/vnd.ms-excel", "Template list user promo.xls");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products List" });
            }

        }



    }
}