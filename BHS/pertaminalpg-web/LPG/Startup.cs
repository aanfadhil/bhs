﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Hosting;
using Microsoft.Owin.Cors;
using LPG.App_Start;
using System.Web.Mvc;
using System.IO;

[assembly: OwinStartupAttribute(typeof(LPG.Startup))]
namespace LPG
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {



            //AreaRegistration.RegisterAllAreas();
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //I18NConfig.InitWithDefaultLanguage("id");

            //Register API
            //HttpConfiguration config = new HttpConfiguration();

            //config.MapHttpAttributeRoutes();
            //config.Routes.MapHttpRoute(
            //     name: "Default",
            //    routeTemplate: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Login", action = "Index", id = RouteParameter.Optional }

            //);

            //app.UseStaticFiles();


            //app.UseErrorPage();
            //app.UseCors(CorsOptions.AllowAll);
            //app.UseWebApi(config);

            ConfigureAuth(app);
            ConfigureHangfire(app);
            //log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));
        }


    }
}
