﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LPG.Models
{
    public class CityAndProvinceModel
    {
        public CityModel city { get; set; }
        public List<ProvinceModel> provinceList { get; set; }
        public List<SelectListItem> DDList { get; set; }
    }
}