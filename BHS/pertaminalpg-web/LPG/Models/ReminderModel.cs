﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LPG.Models
{
    public class ReminderModel
    {
        public int ReminderId { get; set; }
        [Required]
        public string BannerImage { get; set; }
        [Required]
        public string Description { get; set; }

        public string Drivername { get; set; }

        public bool UserType { get; set; }

        public bool IsActive { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public List<ReminderModel> ReminderList { get; set; }

        public string ErrorMessage { get; set; }

        public string UpdatedUser { get; set; }

        public bool IsReminderExixts { get; set; }

       
    }
}