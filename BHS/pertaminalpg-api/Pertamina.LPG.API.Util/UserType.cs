﻿namespace Pertamina.LPG.API.Utils
{
    public enum UserType
    {
        SuperUser = 1,
        AgentBoss = 2,
        AgentAdmin = 3,
        Driver = 4,
        Consumer = 5
    }
}