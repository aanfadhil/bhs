﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LPG.Models
{
    public class FaqModel
    {
        public int FaqId { get; set; }
        [Required]
        public string Question { get; set; }
        [Required]
        public string Answer { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Position must be greater than 0")]
        public int Position { get; set; }
        public bool StatusId { get; set; }
        public bool IsFaqExists { get; set; }
        public List<FaqModel> FaqList { get; set; }
        public int Posisi { get; set; }
        
    }

}