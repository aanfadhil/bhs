﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IReminderService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        bool deleteReminder(int id, int loggeduserId);
        ReminderModel getReminderDetails(int reminderId);
        List<ReminderModel> ListReminder();
        ReminderModel saveReminderDetails(ReminderModel entity, string Cmd, int loggedBy);
    }
}