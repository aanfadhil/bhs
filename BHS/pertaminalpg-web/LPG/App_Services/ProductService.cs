﻿using LPG.App_Classes;
using LPG.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Configuration;
using System.Web.Configuration;
using System.IO;
using System.Web.Mvc;
using Microsoft.ApplicationInsights;
using OfficeOpenXml;
using System.Web.UI.WebControls;

namespace LPG.App_Services
{
    public class ProductService : AbstractAppService, IProductService
    {
        string endPoint = Properties.Settings.Default.EndPoint;
        string filepath = ConfigurationManager.AppSettings["DefaultPath_Product"].ToString();

        public ProductService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<ProductModel> ListProducts()
        {
            var productList = new List<ProductModel>();
            var data = from product in context.Products
                       join user in context.SuperAdmins
                       on product.UpdatedBy equals user.SAdminID
                       where product.StatusId == true
                       orderby product.Position ascending
                       select new ProductModel
                       {
                           ProdID = product.ProdID,
                           //ProductImage = product.ProductImage,
                           //ProductImageDetails = product.ProductImageDetails,
                           ProductImage = product.ProductImage,//(product.ProductImage != "") ? (filepath + product.ProductImage) : "",
                           ProductImageDetails = product.ProductImageDetails, //(product.ProductImageDetails != "") ? (filepath + product.ProductImageDetails) : "",
                           ProductName = product.ProductName,
                           Position = product.Position,
                           StatusId = product.StatusId,
                           CreatedDate = product.UpdatedDate,
                           Published = product.Published,
                           UserName = user.FullName
                       };

            productList = data.ToList();
            return productList;
        }
        /// <summary>
        /// INSERT/UPDATE : Product and Product exchange details into DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="loggeduserId"></param>
        /// <returns></returns>
        /// 


        public ProductModel SaveProduct(ProductModel model, short loggeduserId)
        {
            try
            {
                var Data = context.Products.Where(r => r.ProdID == model.ProdID).FirstOrDefault();
                bool bPublished = model.PublishBit.Equals("publish") ? true : false;
                if (Data != null)
                {
                    Data.ProductName = model.ProductName;
                    Data.ProductCode = model.ProductCode;
                    Data.Position = model.Position;
                    Data.ProductImage = model.ProductImage;//filename_ProductImage;
                    Data.ProductImageDetails = model.ProductImageDetails; //filename_ProductDetImage;
                    Data.ProductPriceFile = model.ProductFile;
                    Data.ShippingPrice = model.ShippingPrice;
                    Data.ShippingPromoPrice = model.ShippingPromoPrice;
                    Data.Published = bPublished;
                    Data.StatusId = true;
                    Data.CreatedBy = model.CreatedBy;
                    Data.CreatedDate = model.CreatedDate;
                    Data.UpdatedBy = loggeduserId;
                    Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    if (model.ExchangeProductLists != null && model.ExchangeProductLists.Count > 0)
                    {
                        DeletedOldExchange(model.ProdID);
                        foreach (ProductExchangeList prodExItem in model.ExchangeProductLists.ToList())
                        {
                            ProductExchange prodEx = new ProductExchange();
                            prodEx.ProdID = model.ProdID;
                            prodEx.ExchangeWith = prodExItem.exchange_with;
                            prodEx.ExchangeQuantity = prodExItem.exchange_quantity;
                            prodEx.ExchangePrice = prodExItem.exchange_price;
                            prodEx.ExchangePromoPrice = prodExItem.exchange_promo_price;
                            prodEx.StatusId = true;
                            prodEx.CreatedBy = loggeduserId;
                            prodEx.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                            prodEx.UpdatedBy = loggeduserId;
                            prodEx.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                            Data.ProductExchanges.Add(prodEx);
                        }
                    }
                    context.SaveChanges();

                    if (!string.IsNullOrEmpty(model.ProductFile))
                    {

                        _UpdateDetailPrice(model.ProductFile, Data.ProdID);
                    }

                    CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), model.ProductName, loggeduserId);
                }
                else
                {
                    Product product = new Product();
                    product.ProductName = model.ProductName;
                    product.ProductCode = model.ProductCode;
                    product.Position = model.Position;
                    product.ProductImage = model.ProductImage;//filename_ProductImage;
                    product.ProductImageDetails = model.ProductImageDetails;
                    product.ProductPriceFile = model.ProductFile;
                    product.ShippingPrice = model.ShippingPrice;
                    product.ShippingPromoPrice = model.ShippingPromoPrice;
                    product.Published = bPublished;
                    product.StatusId = true;
                    product.CreatedBy = loggeduserId;
                    product.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    product.UpdatedBy = loggeduserId;
                    product.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    if (model.ExchangeProductLists != null && model.ExchangeProductLists.Count > 0)
                    {
                        foreach (ProductExchangeList prodExItem in model.ExchangeProductLists.ToList())
                        {
                            ProductExchange prodEx = new ProductExchange();
                            prodEx.ProdID = model.ProdID;
                            prodEx.ExchangeWith = prodExItem.exchange_with;
                            prodEx.ExchangeQuantity = prodExItem.exchange_quantity;
                            prodEx.ExchangePrice = prodExItem.exchange_price;
                            prodEx.ExchangePromoPrice = prodExItem.exchange_promo_price;
                            prodEx.StatusId = true;
                            prodEx.CreatedBy = loggeduserId;
                            prodEx.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                            prodEx.UpdatedBy = loggeduserId;
                            prodEx.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                            product.ProductExchanges.Add(prodEx);
                        }
                    }
                    context.Products.Add(product);
                    context.SaveChanges();

                    if (!string.IsNullOrEmpty(model.ProductFile))
                        _UpdateDetailPrice(model.ProductFile, product.ProdID);

                    CallActivityLog(ConfigurationManager.AppSettings["add"], model.ProductName, loggeduserId);
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// GET : Product details
        /// </summary>
        /// <param name="productID"></param>
        /// <returns></returns>
        public ProductModel GetProduct(int productID)
        {
            List<ProductExchangeList> Exchange = new List<ProductExchangeList>();
            var data = (from prod in context.Products
                        where prod.ProdID == productID && (prod.StatusId == true
                        )
                        select new ProductModel
                        {
                            ProdID = prod.ProdID,
                            ProductName = prod.ProductName,
                            ProductCode = prod.ProductCode,
                            Position = prod.Position,
                            //ProductImage = prod.ProductImage,
                            //ProductImageDetails = prod.ProductImageDetails,
                            ProductImage = prod.ProductImage, //(prod.ProductImage != "") ? (filepath + prod.ProductImage) : "",
                            ProductImageDetails = prod.ProductImageDetails, //(prod.ProductImageDetails != "") ? (filepath + prod.ProductImageDetails) : "",
                            TubePrice = prod.TubePrice,
                            TubePromoPrice = prod.TubePromoPrice,
                            RefillPrice = prod.RefillPrice,
                            RefillPromoPrice = prod.RefillPromoPrice,
                            ShippingPrice = prod.ShippingPrice,
                            ShippingPromoPrice = prod.ShippingPromoPrice,
                            Published = prod.Published,
                            StatusId = prod.StatusId,
                            CreatedBy = prod.CreatedBy,
                            CreatedDate = prod.CreatedDate,
                            UpdatedBy = prod.UpdatedBy,
                            UpdatedDate = prod.UpdatedDate,
                            ProductFile = prod.ProductPriceFile//,
                        }).FirstOrDefault();
            Exchange = GetProductExchange(data, productID);
            if (Exchange != null && Exchange.Count() > 0)
            {
                data.ExchangeProductLists = GetProductExchange(data, productID);
            }

            return data;
            //}

        }

        /// <summary>
        /// GET : Product Exchange details
        /// </summary>
        /// <param name="model"></param>
        /// <param name="productID"></param>
        /// <returns></returns>
        public List<ProductExchangeList> GetProductExchange(ProductModel model, int productID)
        {
            List<ProductExchangeList> prodEx = new List<ProductExchangeList>();
            //using (var context = new lpgdevEntities())
            //{
            var exQuery = (from prodExchange in context.ProductExchanges
                           where prodExchange.ProdID == productID && prodExchange.StatusId == true
                           select new ProductExchangeList
                           {
                               exchanhe_id = prodExchange.PrExID,
                               exchange_with = prodExchange.ExchangeWith,
                               exchange_quantity = prodExchange.ExchangeQuantity,
                               exchange_price = prodExchange.ExchangePrice.Value,
                               exchange_promo_price = prodExchange.ExchangePromoPrice
                           });
            prodEx = exQuery.ToList();
            //}


            return prodEx;
        }

        /// <summary>
        /// DELETE :  Hard delete - delete existing product exchange data during update product
        /// </summary>
        /// <param name="productID"></param>
        public void DeletedOldExchange(int productID)
        {
            //using (var context = new lpgdevEntities())
            //{
            var oldProdEx = from prodEx in context.ProductExchanges
                            where prodEx.ProdID == productID && prodEx.StatusId == true
                            select prodEx;
            foreach (ProductExchange prodExItem in oldProdEx.ToList())
            {
                context.ProductExchanges.Remove(prodExItem);
            }

            context.SaveChanges();
            //}
        }

        /// <summary>
        /// DELETE : Soft Delete - Product Exchange data status ID = false
        /// </summary>
        /// <param name="prodExID"></param>
        /// <returns></returns>
        public bool DeleteProductExchange(int prodExID)
        {

            try
            {
                //using (var context = new lpgdevEntities())
                //{
                var Data = context.ProductExchanges.Where(r => r.PrExID == prodExID).FirstOrDefault();
                if (Data != null)
                {
                    Data.StatusId = false;
                    context.SaveChanges();
                    return true;
                }
                return false;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// DELETE : Soft Delete - Product statusID =false
        /// </summary>
        /// <param name="productID"></param>
        /// <returns></returns>
        public bool DeleteProduct(int productID, int loggedBy)
        {
            try
            {
                using (var context = new lpgdevEntities())
                {
                    var Data = context.Products.Where(r => r.ProdID == productID).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.StatusId = false;
                        Data.UpdatedBy = loggedBy.ToShort();
                        Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), Data.ProductName, loggedBy);
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// ADD : Activity Log table
        /// </summary>
        /// <param name="action"></param>
        /// <param name="action_item"></param>
        /// <param name="loggedBy"></param>
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = action_item;
            _activityService.AddActivityLog(actModel, "Product");
        }

        public List<ProductChartModel> GetProductsByCode(List<string> productCodes)
        {
            var productList = new List<ProductChartModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from product in context.Products
                           where product.StatusId == true && product.Published == true
                           orderby product.Position
                           select new ProductChartModel
                           {
                               Id = product.ProdID,
                               Name = product.ProductName,
                               Code = product.ProductCode
                           };
                productList = data.ToList();
            }
            return productList;
        }

        public List<SelectListItem> GetAvailablePositions(int? ProdId)
        {
            var PositionList = new List<SelectListItem>();
            using (var context = new lpgdevEntities())
            {
                if (ProdId == null)
                {
                    var data = from Product in context.Products
                               where Product.ProdID == Product.ProdID & Product.StatusId == true

                               select new SelectListItem
                               {
                                   Value = Product.Position.ToString(),
                                   Text = Product.Position.ToString(),
                               };
                    if (data.Count() > 0)
                        PositionList = data.ToList();
                }
                else
                {
                    var data = from Product in context.Products
                               where Product.ProdID != ProdId & Product.StatusId == true

                               select new SelectListItem
                               {
                                   Value = Product.Position.ToString(),
                                   Text = Product.Position.ToString(),
                               };
                    if (data.Count() > 0)
                        PositionList = data.ToList();
                }
            }

            return PositionList;
        }

        public List<DetailPriceModel> ListPrices(int productID)
        {
            var priceList = new List<DetailPriceModel>();
            MCity city = new MCity();
            using (var context = new lpgdevEntities())
            {
                var data = from det in context.DetailPrices
                           where det.ProductID == productID
                           select new DetailPriceModel
                           {
                               ID = det.ID,
                               CityName = det.MCity.CityName,
                               CityID = det.CityID,
                               RefillNormalPrice = det.RefillNormalPrice,
                               RefillPromoPrice = det.RefillPromoPrice,
                               TubeNormalPrice = det.TubeNormalPrice,
                               TubePromoPrice = det.TubePromoPrice
                           };
                priceList = data.ToList();
            }

            return priceList;
        }

        public DetailPriceModel GetDetailPrice(int detailPriceId)
        {
            var data = (from det in context.DetailPrices
                        where det.ID == detailPriceId
                        select new DetailPriceModel
                        {
                            ID = det.ID,
                            ProductID = det.ProductID,
                            ProductName = det.Product.ProductName,
                            ProductImg = det.Product.ProductImage,
                            CityName = det.MCity.CityName,
                            CityID = det.CityID,
                            RefillNormalPrice = det.RefillNormalPrice,
                            RefillPromoPrice = det.RefillPromoPrice,
                            TubeNormalPrice = det.TubeNormalPrice,
                            TubePromoPrice = det.TubePromoPrice
                        }).FirstOrDefault();
            return data;
        }

        public DetailPriceModel SavePrice(DetailPriceModel model, short loggedBy)
        {
            try
            {
                using (var context = new lpgdevEntities())
                {
                    var Data = context.DetailPrices.Where(r => r.ID == model.ID).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.RefillNormalPrice = model.RefillNormalPrice;
                        Data.RefillPromoPrice = model.RefillPromoPrice;
                        Data.TubeNormalPrice = model.TubeNormalPrice;
                        Data.TubePromoPrice = model.TubePromoPrice;

                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), Data.Product.ProductName, loggedBy);
                    }
                    return model;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void _SaveDetailPrice(DetailPrice detailPrice)
        {
            try
            {
                var item = new DetailPrice();
                item.ProductID = detailPrice.ProductID;
                item.CityID = detailPrice.CityID;
                item.TubeNormalPrice = detailPrice.TubeNormalPrice;
                item.TubePromoPrice = detailPrice.TubePromoPrice;
                item.RefillNormalPrice = detailPrice.RefillNormalPrice;
                item.RefillPromoPrice = detailPrice.RefillPromoPrice;
                context.DetailPrices.Add(item);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int _GetRowValue(object rowValue)
        {
            return (rowValue != null) ? int.Parse(rowValue.ToString()) : 0;
        }

        private void _UpdateDetailPrice(string fileName, int? prodId)
        {
            try
            {
                String path = HttpContext.Current.Server.MapPath("~/extfiles/products/" + Path.GetFileName(fileName));

                if (!File.Exists(path))
                {
                    return;
                }

                var package = new ExcelPackage(new FileInfo(path));
                int startColumn = 1; //column start at 1, row 5
                int startRow = 2;
                ExcelWorksheet workSheet = package.Workbook.Worksheets.First(); //read sheet 1

                while (workSheet.Cells[startRow, startColumn].Value != null)
                {
                    DetailPrice newData = new DetailPrice();

                    newData.ProductID = prodId.Value;
                    newData.CityID = _GetRowValue(workSheet.Cells[startRow, startColumn].Value);
                    newData.TubeNormalPrice = _GetRowValue(workSheet.Cells[startRow, startColumn + 3].Value);
                    newData.TubePromoPrice = _GetRowValue(workSheet.Cells[startRow, startColumn + 4].Value);
                    newData.RefillNormalPrice = _GetRowValue(workSheet.Cells[startRow, startColumn + 5].Value);
                    newData.RefillPromoPrice = _GetRowValue(workSheet.Cells[startRow, startColumn + 6].Value);

                    DetailPrice detailPrice = context.DetailPrices.Where(x => x.ProductID == newData.ProductID && x.CityID == newData.CityID).FirstOrDefault();

                    if (detailPrice != null)
                    {
                        try
                        {
                            detailPrice.TubeNormalPrice = newData.TubeNormalPrice;
                            detailPrice.TubePromoPrice = newData.TubePromoPrice;
                            detailPrice.RefillNormalPrice = newData.RefillNormalPrice;
                            detailPrice.RefillPromoPrice = newData.RefillPromoPrice;
                            context.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        
                    }
                    else
                    {
                        if (newData.ProductID != 0 && newData.CityID != 0 && newData!=null)
                        {
                            _SaveDetailPrice(newData);
                        }
                    }
                    startRow++;
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
