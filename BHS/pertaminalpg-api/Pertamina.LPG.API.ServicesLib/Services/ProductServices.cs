﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pertamina.LPG.API.DTOs.Product;
using Pertamina.LPG.API.DAOs;
using Pertamina.LPG.API.Models;
using Pertamina.LPG.API.Services.Helpers;

namespace Pertamina.LPG.API.Services
{
    public class ProductServices
    {
        private UserServices _userServices = new UserServices();

        public GetProductListResponse GetProductList(GetProductListRequest request)
        {
            GetProductListResponse response = new GetProductListResponse();
            try
            {
                if (request.is_admin)
                {
                    if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                    {
                        //_userServices.MakeNouserResponse(response);
                        return response;
                    }
                }
                else
                {
                    if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                    {
                        _userServices.MakeNouserResponse(response);
                        return response;
                    }
                }

                using (ProductDao dao = new ProductDao())
                {
                    List<Product> pList = dao.GetProducts(request.page_number, request.row_per_page);
                    int totalCount = dao.GetTotalCount();
                    response.product_details = new ProductPagenationDetailsDto();
                    response.product_details.total_num_products = totalCount;
                    ProductDto[] prodDtos = new ProductDto[pList.Count()];
                    for (int i = 0; i < pList.Count; i++)
                    {
                        ProductDto dto = new ProductDto();
                        ProductHelper.CopyFromEntity(dto, pList[i]);
                        prodDtos[i] = dto;

                        response.products = prodDtos;
                        //response.has_exchange = (pList[i].ProductExchanges.Count > 0 ? 1 : 0);
                        //if (response.has_exchange == 1)
                        //{
                        //    if (response.exchange == null)
                        //        response.exchange = new List<ExchangeDto>();
                        //    foreach (var item in pList[i].ProductExchanges.ToList())
                        //    {
                        //        ExchangeDto exDto = new ExchangeDto();
                        //        ProductHelper.CopyFromEntity(exDto, item);
                        //        response.exchange.Add(exDto);
                        //    }
                        //}
                    }

                    var reminder = dao.GetRemindersForProducts();
                    response.has_reminder = (reminder == null ? 0 : 1);
                    ProductHelper.CopyFromEntity(response, reminder);

                    response.has_resource = 1;
                    response.code = 0;
                    response.message = MessagesSource.GetMessage("has.products");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public GetProductPriceDetailsResponse GetProductPriceDetails(GetProductPriceDetailsRequest request)
        {
            GetProductPriceDetailsResponse response = new GetProductPriceDetailsResponse();
            try
            {
                if (request.is_admin)
                {
                    if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                    {
                        //_userServices.MakeNouserResponse(response);
                        return response;
                    }
                }
                else
                {
                    if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                    {
                        _userServices.MakeNouserResponse(response);
                        return response;
                    }
                }

                using (ProductDao dao = new ProductDao())
                {
                    ProductPriceDetailsDto dto = new ProductPriceDetailsDto();
                    DetailPrice prd = dao.FindProductDetailByCityId(request.product_id, request.city_id, request.city_code, request.city_name);
                    if (prd != null)
                    {
                        DetailPriceHelper.CopyFromEntity(dto, prd);
                        response.product_details = dto;
                        response.code = 0;
                        response.has_resource = 1;
                        response.message = MessagesSource.GetMessage("has.prod.details");
                    }
                    else
                    {
                        Product products = dao.FindProductById(request.product_id);
                        if (products != null)
                        {
                            DetailPriceHelper.CopyFromEntity(dto, products);
                            response.product_details = dto;
                            response.code = 0;
                            response.has_resource = 1;
                            response.message = MessagesSource.GetMessage("has.prod.details");
                        }
                        else
                        {
                            response.code = 1;
                            response.has_resource = 0;
                            response.message = MessagesSource.GetMessage("no.prod.details");

                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public GetProductDetailsResponse GetProductDetails(GetProductDetailsRequest request)
        {
            GetProductDetailsResponse response = new GetProductDetailsResponse();
            try
            {
                if (request.is_admin)
                {
                    if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                    {
                        //_userServices.MakeNouserResponse(response);
                        return response;
                    }
                }
                else
                {
                    if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                    {
                        _userServices.MakeNouserResponse(response);
                        return response;
                    }
                }

                using (ProductDao dao = new ProductDao())
                {
                    ProductDetailsDto dto = new ProductDetailsDto();
                    Product prd = dao.FindProductById(request.product_id);
                    ProductHelper.CopyFromEntity(dto, prd);
                    response.product_details = dto;
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("has.prod.details");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }
        public GetProductPriceByCityResponse GetProductPriceByCity(GetProductPriceByCityRequest request)
        {
            GetProductPriceByCityResponse response = new GetProductPriceByCityResponse();
            try
            {
                if (request.is_admin)
                {
                    if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                    {
                        return response;
                    }
                }
                else
                {
                    if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                    {
                        _userServices.MakeNouserResponse(response);
                        return response;
                    }
                }

                using (DetailPriceDao dao = new DetailPriceDao())
                {
                    List<DetailPrice> pList = dao.GetProducts(request.page_number, request.row_per_page, request.city_id, request.city_code, request.city_name);
                    int totalCount = dao.GetTotalCount(request.city_id, request.city_code, request.city_name);
                    response.product_details = new ProductPagenationDetailsDto();
                    response.product_details.total_num_products = totalCount;
                    DetailPriceDto[] detailPriceDtos = new DetailPriceDto[pList.Count()];

                    for (int i = 0; i < pList.Count; i++)
                    {
                        DetailPriceDto dto = new DetailPriceDto();
                        DetailPriceHelper.CopyFromEntity(dto, pList[i]);
                        detailPriceDtos[i] = dto;
                    }

                    response.products = detailPriceDtos;
                    var reminder = dao.GetRemindersForProducts();
                    response.has_reminder = (reminder == null ? 0 : 1);
                    DetailPriceHelper.CopyFromEntity(response, reminder);

                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("has.prod.details");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }
    }
}