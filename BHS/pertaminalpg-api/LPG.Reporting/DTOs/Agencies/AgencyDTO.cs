﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace LPG.Reporting.DTOs.Agencies
{
    [DataContract]
    public class AgencyDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public int AgenID { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string SoldToNumber { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string AgencyName { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string Region { get; set; }
        [DataMember]
        public bool StatusId { get; set; }
        [DataMember]
        public short CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public short UpdatedBy { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public int RegionId { get; set; }
    }

    public class AgencyMapper : MapperBase<Agency, AgencyDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<Agency, AgencyDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Agency, AgencyDTO>>) (p => new AgencyDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    AgenID = p.AgenID,
                    SoldToNumber = p.SoldToNumber,
                    AgencyName = p.AgencyName,
                    Region = p.Region,
                    StatusId = p.StatusId,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate,
                    RegionId = p.RegionId
                }));
            }
        }

        public override void MapToModel(AgencyDTO dto, Agency model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.AgenID = dto.AgenID;
            model.SoldToNumber = dto.SoldToNumber;
            model.AgencyName = dto.AgencyName;
            model.Region = dto.Region;
            model.StatusId = dto.StatusId;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;
            model.RegionId = dto.RegionId;

        }
    }
}
