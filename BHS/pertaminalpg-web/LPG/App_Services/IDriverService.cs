﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IDriverService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        bool DeleteDriver(int Id, int loggedBy);
        DriverModel getDriverDetails(int driverId);
        bool IsExists(int id);
        bool IsExistsMobileNo(string mobileNo, int userId);
        List<DriverModel> ListDriver();
        DriverModel saveDriverDetails(DriverModel entity, int loggedBy);
    }
}