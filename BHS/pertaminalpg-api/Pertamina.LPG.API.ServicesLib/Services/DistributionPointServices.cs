﻿using Pertamina.LPG.API.DAOs;
using Pertamina.LPG.API.DTOs.DistributionPoint;
using Pertamina.LPG.API.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace Pertamina.LPG.API.Services
{
    public class DistributionPointService
    {
        public void UpdateCity()
        {
            DistributionPointDao dao = new DistributionPointDao();

            var list = dao.GetAllDistributionPoint();

            foreach (var item in list)
            {
                var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+item.Latitude+","+item.Longitude+ "&key=AIzaSyCi0cz9Vqdcu49U-U09TB7kOvWSRCRt1wQ&language=id";
                var client = new RestClient(url);
                var request = new RestRequest(Method.GET);
                //request.AddParameter("location", latitude.ToString().Replace(',','.') + "," + longitude.ToString().Replace(',', '.'));
                //request.AddParameter("timestamp", utcDate.ToTimestamp());
                //request.AddParameter("key ", Common.GetAppSetting<string>("TimezoneApiKey", string.Empty));
                var response = client.Execute(request).Content;

                var data = System.Web.Helpers.Json.Decode(response);

                foreach(var result in data.results)
                {
                    foreach (var restype in result.types)
                    {
                        if(restype == "administrative_area_level_2")
                        {
                            foreach (var addrcm in result.address_components)
                            {
                                foreach (var addrtype in addrcm.types)
                                {
                                    if(addrtype == "administrative_area_level_2")
                                    {
                                        dao.UpdateCity(item.DbptID, addrcm.long_name);
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        public CheckCoverageAreaResponse CheckOnCoverageArea (CheckCoverageAreaRequest request)
        {
            CheckCoverageAreaResponse response = new CheckCoverageAreaResponse();
            try
            {
                if(_IsOnCoverageArea(request))
                {
                    response.on_coverage_area = true;
                    response.code = 1;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("order.on_coverage_area");

                }
                else
                {
                    response.on_coverage_area = false;
                    response.code = 0;
                    response.has_resource = 0;
                    response.message = MessagesSource.GetMessage("order.error.not_on_coverage_area");
                }
            }
            catch(Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        private bool _IsOnCoverageArea(CheckCoverageAreaRequest request)
        {
            using (DistributionPointDao dao = new DistributionPointDao())
            {
                List<DistributionPoint> dPList = dao.GetDistributionPoint();
                foreach (DistributionPoint point in dPList)
                {
                    double distPointRadius = 5;
                    double earthRadius = 6372.8; // Radius of the earth in km
                    double point_lat = double.Parse(point.Latitude, CultureInfo.InvariantCulture);
                    double point_long = double.Parse(point.Longitude, CultureInfo.InvariantCulture);

                    var dLat = _ToRadians(request.user_lat - point_lat);  // deg2rad below
                    var dLon = _ToRadians(request.user_long - point_long);
                    var a =
                        Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                        Math.Cos(_ToRadians(request.user_lat)) * Math.Cos(_ToRadians(point_lat)) *
                        Math.Sin(dLon / 2) * Math.Sin(dLon / 2);

                    var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                    var d = earthRadius * c;

                    if (d <= distPointRadius)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        double _ToRadians(double deg)
        {
            return deg * (Math.PI / 180);
        }
    }
}
