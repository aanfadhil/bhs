﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;

namespace Pertamina.LPG.API.Config
{
    public class ModelLibDatabaseConfiguration : DbConfiguration
    {

        public ModelLibDatabaseConfiguration()
        {
            SetExecutionStrategy(
            "System.Data.SqlClient",
            () => new SqlAzureExecutionStrategy(5, TimeSpan.FromSeconds(30)));

            SetDefaultConnectionFactory(new SqlConnectionFactory());
        }

    }
}
