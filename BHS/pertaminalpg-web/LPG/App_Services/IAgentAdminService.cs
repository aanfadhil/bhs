﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IAgentAdminService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        bool DeleteAgentAdmin(int Id, int loggedBy);
        AgentAdminModel getActiveAgentAdmin(int agentAdminId);
        bool IsExists(int id);
        bool IsExistsEmail(string email, int userId);
        bool IsExistsMobileNo(string mobileNo, int userId);
        List<AgentAdminModel> ListAgentAdmin();
        AgentAdminModel saveAgentDetails(AgentAdminModel entity, int loggedBy);
    }
}