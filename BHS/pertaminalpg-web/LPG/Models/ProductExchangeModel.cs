﻿using System;

namespace LPG.Models
{
    public class ProductExchangeModel
    {
        public int exchanhe_id { get; set; }
        public string exchange_with { get; set; }
        public int exchange_quantity { get; set; }
        public decimal exchange_price { get; set; }
        public decimal exchange_promo_price { get; set; }
        public short CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public short UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}