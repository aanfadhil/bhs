﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LPG.Controllers
{
    public class TestController : AbstractController<ProductService>
    {
        //ProductService serviceObject = new ProductService();

        public TestController(ProductService _serviceObject) : base(_serviceObject)
        {
        }

        // GET: Test
        public ActionResult Index()
        {
            var vm = new ProductModel();
            try
            {
                SessionManagement sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var temp = serviceObject.ListProducts();
                    return View(temp);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products List" });
            }
        }
    }
}
