﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.DistributionPoint
{
    public class CheckCoverageAreaRequest
    {
        public double user_long { get; set; }
        public double user_lat { get; set; }
    }
}
