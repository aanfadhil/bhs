﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using LPG.Reporting.DTOs.Orders;
using LPG.Reporting.DTOs.Activities;
using LPG.Reporting.DTOs.Agencies;
using LPG.Reporting.DTOs.SystemActors;
using LPG.Reporting.DTOs.DistributionPoints;
using LPG.Reporting.DTOs.Products;
using LPG.Reporting.DTOs.Promo;

namespace LPG.Reporting.DTOs.Reminders
{
    [DataContract]
    public class ReminderDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public int RmdrID { get; set; }
        [DataMember]
        [StringLength(255)]
        public string ReminderImage { get; set; }
        [DataMember]
        public bool UserType { get; set; }
        [DataMember]
        [Required]
        public string Description { get; set; }
        [DataMember]
        public bool StatusId { get; set; }
        [DataMember]
        public short CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public short UpdatedBy { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public int? ProdID { get; set; }
        [DataMember]
        public int ProductProdID { get; set; }
        [DataMember]
        [Required]
        [StringLength(150)]
        public string ProductProductName { get; set; }
        [DataMember]
        public int ProductPosition { get; set; }
        [DataMember]
        [StringLength(100)]
        public string ProductProductImage { get; set; }
        [DataMember]
        [StringLength(100)]
        public string ProductProductImageDetails { get; set; }
        [DataMember]
        public decimal ProductTubePrice { get; set; }
        [DataMember]
        public decimal ProductTubePromoPrice { get; set; }
        [DataMember]
        public decimal ProductRefillPrice { get; set; }
        [DataMember]
        public decimal ProductRefillPromoPrice { get; set; }
        [DataMember]
        public decimal ProductShippingPrice { get; set; }
        [DataMember]
        public decimal ProductShippingPromoPrice { get; set; }
        [DataMember]
        public bool ProductPublished { get; set; }
        [DataMember]
        public bool ProductStatusId { get; set; }
        [DataMember]
        public short ProductCreatedBy { get; set; }
        [DataMember]
        public DateTime ProductCreatedDate { get; set; }
        [DataMember]
        public short ProductUpdatedBy { get; set; }
        [DataMember]
        public DateTime ProductUpdatedDate { get; set; }
        [DataMember]
        [StringLength(25)]
        public string ProductProductCode { get; set; }
        [DataMember]
        public double? ProductWeight { get; set; }
        [DataMember]
        public IEnumerable<OrderDetailDTO> ProductOrderDetails { get; set; }
        [DataMember]
        public IEnumerable<OrderPrdocuctExchangeDTO> ProductOrderPrdocuctExchanges { get; set; }
        [DataMember]
        public short ProductSuperAdminSAdminID { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string ProductSuperAdminFullName { get; set; }
        [DataMember]
        [Required]
        [StringLength(20)]
        public string ProductSuperAdminMobileNum { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string ProductSuperAdminPassword { get; set; }
        [DataMember]
        [StringLength(255)]
        public string ProductSuperAdminProfileImage { get; set; }
        [DataMember]
        public string ProductSuperAdminAppToken { get; set; }
        [DataMember]
        public DateTime? ProductSuperAdminLastLogin { get; set; }
        [DataMember]
        public short ProductSuperAdminUserTypeID { get; set; }
        [DataMember]
        public bool ProductSuperAdminStatusID { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string ProductSuperAdminEmail { get; set; }
        [DataMember]
        [StringLength(64)]
        public string ProductSuperAdminAccToken { get; set; }
        [DataMember]
        [StringLength(255)]
        public string ProductSuperAdminAppID { get; set; }
        [DataMember]
        public int? ProductSuperAdminCreatedBy { get; set; }
        [DataMember]
        public DateTime? ProductSuperAdminCreatedDate { get; set; }
        [DataMember]
        public int? ProductSuperAdminUpdatedBy { get; set; }
        [DataMember]
        public DateTime? ProductSuperAdminUpdatedDate { get; set; }
        [DataMember]
        public IEnumerable<ActivityLogDTO> ProductSuperAdminActivityLogs { get; set; }
        [DataMember]
        public IEnumerable<AgencyDTO> ProductSuperAdminAgencies { get; set; }
        [DataMember]
        public IEnumerable<AgencyDTO> ProductSuperAdminAgencies1 { get; set; }
        [DataMember]
        public IEnumerable<AgentAdminDTO> ProductSuperAdminAgentAdmins { get; set; }
        [DataMember]
        public IEnumerable<AgentBossDTO> ProductSuperAdminAgentBosses { get; set; }
        [DataMember]
        public IEnumerable<ContactInfoDTO> ProductSuperAdminContactInfoes { get; set; }
        [DataMember]
        public IEnumerable<ContactInfoDTO> ProductSuperAdminContactInfoes1 { get; set; }
        [DataMember]
        public IEnumerable<DistributionPointDTO> ProductSuperAdminDistributionPoints { get; set; }
        [DataMember]
        public IEnumerable<DistributionPointDTO> ProductSuperAdminDistributionPoints1 { get; set; }
        [DataMember]
        public IEnumerable<DriverDTO> ProductSuperAdminDrivers { get; set; }
        [DataMember]
        public IEnumerable<DriverDTO> ProductSuperAdminDrivers1 { get; set; }
        [DataMember]
        public IEnumerable<ProductDTO> ProductSuperAdminProducts { get; set; }
        [DataMember]
        public IEnumerable<ProductDTO> ProductSuperAdminProducts1 { get; set; }
        [DataMember]
        public IEnumerable<ProductExchangeDTO> ProductSuperAdminProductExchanges { get; set; }
        [DataMember]
        public IEnumerable<ProductExchangeDTO> ProductSuperAdminProductExchanges1 { get; set; }
        [DataMember]
        public IEnumerable<PromoBannerDTO> ProductSuperAdminPromoBanners { get; set; }
        [DataMember]
        public IEnumerable<PromoBannerDTO> ProductSuperAdminPromoBanners1 { get; set; }
        [DataMember]
        public IEnumerable<PromoInfoDTO> ProductSuperAdminPromoInfoes { get; set; }
        [DataMember]
        public IEnumerable<PromoInfoDTO> ProductSuperAdminPromoInfoes1 { get; set; }
        [DataMember]
        public IEnumerable<ReminderDTO> ProductSuperAdminReminders { get; set; }
        [DataMember]
        public IEnumerable<ReminderDTO> ProductSuperAdminReminders1 { get; set; }
        [DataMember]
        public short ProductSuperAdmin1SAdminID { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string ProductSuperAdmin1FullName { get; set; }
        [DataMember]
        [Required]
        [StringLength(20)]
        public string ProductSuperAdmin1MobileNum { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string ProductSuperAdmin1Password { get; set; }
        [DataMember]
        [StringLength(255)]
        public string ProductSuperAdmin1ProfileImage { get; set; }
        [DataMember]
        public string ProductSuperAdmin1AppToken { get; set; }
        [DataMember]
        public DateTime? ProductSuperAdmin1LastLogin { get; set; }
        [DataMember]
        public short ProductSuperAdmin1UserTypeID { get; set; }
        [DataMember]
        public bool ProductSuperAdmin1StatusID { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string ProductSuperAdmin1Email { get; set; }
        [DataMember]
        [StringLength(64)]
        public string ProductSuperAdmin1AccToken { get; set; }
        [DataMember]
        [StringLength(255)]
        public string ProductSuperAdmin1AppID { get; set; }
        [DataMember]
        public int? ProductSuperAdmin1CreatedBy { get; set; }
        [DataMember]
        public DateTime? ProductSuperAdmin1CreatedDate { get; set; }
        [DataMember]
        public int? ProductSuperAdmin1UpdatedBy { get; set; }
        [DataMember]
        public DateTime? ProductSuperAdmin1UpdatedDate { get; set; }
        [DataMember]
        public IEnumerable<ActivityLogDTO> ProductSuperAdmin1ActivityLogs { get; set; }
        [DataMember]
        public IEnumerable<AgencyDTO> ProductSuperAdmin1Agencies { get; set; }
        [DataMember]
        public IEnumerable<AgencyDTO> ProductSuperAdmin1Agencies1 { get; set; }
        [DataMember]
        public IEnumerable<AgentAdminDTO> ProductSuperAdmin1AgentAdmins { get; set; }
        [DataMember]
        public IEnumerable<AgentBossDTO> ProductSuperAdmin1AgentBosses { get; set; }
        [DataMember]
        public IEnumerable<ContactInfoDTO> ProductSuperAdmin1ContactInfoes { get; set; }
        [DataMember]
        public IEnumerable<ContactInfoDTO> ProductSuperAdmin1ContactInfoes1 { get; set; }
        [DataMember]
        public IEnumerable<DistributionPointDTO> ProductSuperAdmin1DistributionPoints { get; set; }
        [DataMember]
        public IEnumerable<DistributionPointDTO> ProductSuperAdmin1DistributionPoints1 { get; set; }
        [DataMember]
        public IEnumerable<DriverDTO> ProductSuperAdmin1Drivers { get; set; }
        [DataMember]
        public IEnumerable<DriverDTO> ProductSuperAdmin1Drivers1 { get; set; }
        [DataMember]
        public IEnumerable<ProductDTO> ProductSuperAdmin1Products { get; set; }
        [DataMember]
        public IEnumerable<ProductDTO> ProductSuperAdmin1Products1 { get; set; }
        [DataMember]
        public IEnumerable<ProductExchangeDTO> ProductSuperAdmin1ProductExchanges { get; set; }
        [DataMember]
        public IEnumerable<ProductExchangeDTO> ProductSuperAdmin1ProductExchanges1 { get; set; }
        [DataMember]
        public IEnumerable<PromoBannerDTO> ProductSuperAdmin1PromoBanners { get; set; }
        [DataMember]
        public IEnumerable<PromoBannerDTO> ProductSuperAdmin1PromoBanners1 { get; set; }
        [DataMember]
        public IEnumerable<PromoInfoDTO> ProductSuperAdmin1PromoInfoes { get; set; }
        [DataMember]
        public IEnumerable<PromoInfoDTO> ProductSuperAdmin1PromoInfoes1 { get; set; }
        [DataMember]
        public IEnumerable<ReminderDTO> ProductSuperAdmin1Reminders { get; set; }
        [DataMember]
        public IEnumerable<ReminderDTO> ProductSuperAdmin1Reminders1 { get; set; }
        [DataMember]
        public IEnumerable<ProductExchangeDTO> ProductProductExchanges { get; set; }
        [DataMember]
        public IEnumerable<ReminderDTO> ProductReminders { get; set; }
        [DataMember]
        public IEnumerable<TeleOrderDetailDTO> ProductTeleOrderDetails { get; set; }
        [DataMember]
        public IEnumerable<TeleOrderPrdocuctExchangeDTO> ProductTeleOrderPrdocuctExchanges { get; set; }
        [DataMember]
        public short SuperAdminSAdminID { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string SuperAdminFullName { get; set; }
        [DataMember]
        [Required]
        [StringLength(20)]
        public string SuperAdminMobileNum { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string SuperAdminPassword { get; set; }
        [DataMember]
        [StringLength(255)]
        public string SuperAdminProfileImage { get; set; }
        [DataMember]
        public string SuperAdminAppToken { get; set; }
        [DataMember]
        public DateTime? SuperAdminLastLogin { get; set; }
        [DataMember]
        public short SuperAdminUserTypeID { get; set; }
        [DataMember]
        public bool SuperAdminStatusID { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string SuperAdminEmail { get; set; }
        [DataMember]
        [StringLength(64)]
        public string SuperAdminAccToken { get; set; }
        [DataMember]
        [StringLength(255)]
        public string SuperAdminAppID { get; set; }
        [DataMember]
        public int? SuperAdminCreatedBy { get; set; }
        [DataMember]
        public DateTime? SuperAdminCreatedDate { get; set; }
        [DataMember]
        public int? SuperAdminUpdatedBy { get; set; }
        [DataMember]
        public DateTime? SuperAdminUpdatedDate { get; set; }
        [DataMember]
        public IEnumerable<ActivityLogDTO> SuperAdminActivityLogs { get; set; }
        [DataMember]
        public IEnumerable<AgencyDTO> SuperAdminAgencies { get; set; }
        [DataMember]
        public IEnumerable<AgencyDTO> SuperAdminAgencies1 { get; set; }
        [DataMember]
        public IEnumerable<AgentAdminDTO> SuperAdminAgentAdmins { get; set; }
        [DataMember]
        public IEnumerable<AgentBossDTO> SuperAdminAgentBosses { get; set; }
        [DataMember]
        public IEnumerable<ContactInfoDTO> SuperAdminContactInfoes { get; set; }
        [DataMember]
        public IEnumerable<ContactInfoDTO> SuperAdminContactInfoes1 { get; set; }
        [DataMember]
        public IEnumerable<DistributionPointDTO> SuperAdminDistributionPoints { get; set; }
        [DataMember]
        public IEnumerable<DistributionPointDTO> SuperAdminDistributionPoints1 { get; set; }
        [DataMember]
        public IEnumerable<DriverDTO> SuperAdminDrivers { get; set; }
        [DataMember]
        public IEnumerable<DriverDTO> SuperAdminDrivers1 { get; set; }
        [DataMember]
        public IEnumerable<ProductDTO> SuperAdminProducts { get; set; }
        [DataMember]
        public IEnumerable<ProductDTO> SuperAdminProducts1 { get; set; }
        [DataMember]
        public IEnumerable<ProductExchangeDTO> SuperAdminProductExchanges { get; set; }
        [DataMember]
        public IEnumerable<ProductExchangeDTO> SuperAdminProductExchanges1 { get; set; }
        [DataMember]
        public IEnumerable<PromoBannerDTO> SuperAdminPromoBanners { get; set; }
        [DataMember]
        public IEnumerable<PromoBannerDTO> SuperAdminPromoBanners1 { get; set; }
        [DataMember]
        public IEnumerable<PromoInfoDTO> SuperAdminPromoInfoes { get; set; }
        [DataMember]
        public IEnumerable<PromoInfoDTO> SuperAdminPromoInfoes1 { get; set; }
        [DataMember]
        public IEnumerable<ReminderDTO> SuperAdminReminders { get; set; }
        [DataMember]
        public IEnumerable<ReminderDTO> SuperAdminReminders1 { get; set; }
    }

    public class ReminderMapper : MapperBase<Reminder, ReminderDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<Reminder, ReminderDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Reminder, ReminderDTO>>) (p => new ReminderDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    RmdrID = p.RmdrID,
                    ReminderImage = p.ReminderImage,
                    UserType = p.UserType,
                    Description = p.Description,
                    StatusId = p.StatusId,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate,
                    ProdID = p.ProdID,
                    ProductProdID = p.Product != null ? p.Product.ProdID : default(int),
                    ProductProductName = p.Product != null ? p.Product.ProductName : default(string),
                    ProductPosition = p.Product != null ? p.Product.Position : default(int),
                    ProductProductImage = p.Product != null ? p.Product.ProductImage : default(string),
                    ProductProductImageDetails = p.Product != null ? p.Product.ProductImageDetails : default(string),
                    ProductTubePrice = p.Product != null ? p.Product.TubePrice : default(decimal),
                    ProductTubePromoPrice = p.Product != null ? p.Product.TubePromoPrice : default(decimal),
                    ProductRefillPrice = p.Product != null ? p.Product.RefillPrice : default(decimal),
                    ProductRefillPromoPrice = p.Product != null ? p.Product.RefillPromoPrice : default(decimal),
                    ProductShippingPrice = p.Product != null ? p.Product.ShippingPrice : default(decimal),
                    ProductShippingPromoPrice = p.Product != null ? p.Product.ShippingPromoPrice : default(decimal),
                    ProductPublished = p.Product != null ? p.Product.Published : default(bool),
                    ProductStatusId = p.Product != null ? p.Product.StatusId : default(bool),
                    ProductCreatedBy = p.Product != null ? p.Product.CreatedBy : default(short),
                    ProductCreatedDate = p.Product != null ? p.Product.CreatedDate : default(DateTime),
                    ProductUpdatedBy = p.Product != null ? p.Product.UpdatedBy : default(short),
                    ProductUpdatedDate = p.Product != null ? p.Product.UpdatedDate : default(DateTime),
                    ProductProductCode = p.Product != null ? p.Product.ProductCode : default(string),
                    ProductWeight = p.Product != null ? p.Product.Weight : default(double?),
                    //OrderDetails = p.Product.OrderDetails.AsQueryable().Select(this._orderDetailMapper.SelectorExpression),
                    //OrderPrdocuctExchanges = p.Product.OrderPrdocuctExchanges.AsQueryable().Select(this._orderPrdocuctExchangeMapper.SelectorExpression),
                    ProductSuperAdminSAdminID = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.SAdminID : default(short),
                    ProductSuperAdminFullName = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.FullName : default(string),
                    ProductSuperAdminMobileNum = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.MobileNum : default(string),
                    ProductSuperAdminPassword = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.Password : default(string),
                    ProductSuperAdminProfileImage = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.ProfileImage : default(string),
                    ProductSuperAdminAppToken = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.AppToken : default(string),
                    ProductSuperAdminLastLogin = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.LastLogin : default(DateTime?),
                    ProductSuperAdminUserTypeID = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.UserTypeID : default(short),
                    ProductSuperAdminStatusID = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.StatusID : default(bool),
                    ProductSuperAdminEmail = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.Email : default(string),
                    ProductSuperAdminAccToken = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.AccToken : default(string),
                    ProductSuperAdminAppID = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.AppID : default(string),
                    ProductSuperAdminCreatedBy = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.CreatedBy : default(int?),
                    ProductSuperAdminCreatedDate = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.CreatedDate : default(DateTime?),
                    ProductSuperAdminUpdatedBy = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.UpdatedBy : default(int?),
                    ProductSuperAdminUpdatedDate = p.Product != null && p.Product.SuperAdmin != null ? p.Product.SuperAdmin.UpdatedDate : default(DateTime?),
                    //ActivityLogs = p.Product.SuperAdmin.ActivityLogs.AsQueryable().Select(this._activityLogMapper.SelectorExpression),
                    //Agencies = p.Product.SuperAdmin.Agencies.AsQueryable().Select(this._agencyMapper.SelectorExpression),
                    //Agencies1 = p.Product.SuperAdmin.Agencies1.AsQueryable().Select(this._agencyMapper.SelectorExpression),
                    //AgentAdmins = p.Product.SuperAdmin.AgentAdmins.AsQueryable().Select(this._agentAdminMapper.SelectorExpression),
                    //AgentBosses = p.Product.SuperAdmin.AgentBosses.AsQueryable().Select(this._agentBossMapper.SelectorExpression),
                    //ContactInfoes = p.Product.SuperAdmin.ContactInfoes.AsQueryable().Select(this._contactInfoMapper.SelectorExpression),
                    //ContactInfoes1 = p.Product.SuperAdmin.ContactInfoes1.AsQueryable().Select(this._contactInfoMapper.SelectorExpression),
                    //DistributionPoints = p.Product.SuperAdmin.DistributionPoints.AsQueryable().Select(this._distributionPointMapper.SelectorExpression),
                    //DistributionPoints1 = p.Product.SuperAdmin.DistributionPoints1.AsQueryable().Select(this._distributionPointMapper.SelectorExpression),
                    //Drivers = p.Product.SuperAdmin.Drivers.AsQueryable().Select(this._driverMapper.SelectorExpression),
                    //Drivers1 = p.Product.SuperAdmin.Drivers1.AsQueryable().Select(this._driverMapper.SelectorExpression),
                    //Products = p.Product.SuperAdmin.Products.AsQueryable().Select(this._productMapper.SelectorExpression),
                    //Products1 = p.Product.SuperAdmin.Products1.AsQueryable().Select(this._productMapper.SelectorExpression),
                    //ProductExchanges = p.Product.SuperAdmin.ProductExchanges.AsQueryable().Select(this._productExchangeMapper.SelectorExpression),
                    //ProductExchanges1 = p.Product.SuperAdmin.ProductExchanges1.AsQueryable().Select(this._productExchangeMapper.SelectorExpression),
                    //PromoBanners = p.Product.SuperAdmin.PromoBanners.AsQueryable().Select(this._promoBannerMapper.SelectorExpression),
                    //PromoBanners1 = p.Product.SuperAdmin.PromoBanners1.AsQueryable().Select(this._promoBannerMapper.SelectorExpression),
                    //PromoInfoes = p.Product.SuperAdmin.PromoInfoes.AsQueryable().Select(this._promoInfoMapper.SelectorExpression),
                    //PromoInfoes1 = p.Product.SuperAdmin.PromoInfoes1.AsQueryable().Select(this._promoInfoMapper.SelectorExpression),
                    //Reminders = p.Product.SuperAdmin.Reminders.AsQueryable().Select(this._reminderMapper.SelectorExpression),
                    //Reminders1 = p.Product.SuperAdmin.Reminders1.AsQueryable().Select(this._reminderMapper.SelectorExpression),
                    ProductSuperAdmin1SAdminID = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.SAdminID : default(short),
                    ProductSuperAdmin1FullName = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.FullName : default(string),
                    ProductSuperAdmin1MobileNum = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.MobileNum : default(string),
                    ProductSuperAdmin1Password = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.Password : default(string),
                    ProductSuperAdmin1ProfileImage = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.ProfileImage : default(string),
                    ProductSuperAdmin1AppToken = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.AppToken : default(string),
                    ProductSuperAdmin1LastLogin = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.LastLogin : default(DateTime?),
                    ProductSuperAdmin1UserTypeID = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.UserTypeID : default(short),
                    ProductSuperAdmin1StatusID = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.StatusID : default(bool),
                    ProductSuperAdmin1Email = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.Email : default(string),
                    ProductSuperAdmin1AccToken = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.AccToken : default(string),
                    ProductSuperAdmin1AppID = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.AppID : default(string),
                    ProductSuperAdmin1CreatedBy = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.CreatedBy : default(int?),
                    ProductSuperAdmin1CreatedDate = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.CreatedDate : default(DateTime?),
                    ProductSuperAdmin1UpdatedBy = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.UpdatedBy : default(int?),
                    ProductSuperAdmin1UpdatedDate = p.Product != null && p.Product.SuperAdmin1 != null ? p.Product.SuperAdmin1.UpdatedDate : default(DateTime?),
                    //ActivityLogs = p.Product.SuperAdmin1.ActivityLogs.AsQueryable().Select(this._activityLogMapper.SelectorExpression),
                    //Agencies = p.Product.SuperAdmin1.Agencies.AsQueryable().Select(this._agencyMapper.SelectorExpression),
                    //Agencies1 = p.Product.SuperAdmin1.Agencies1.AsQueryable().Select(this._agencyMapper.SelectorExpression),
                    //AgentAdmins = p.Product.SuperAdmin1.AgentAdmins.AsQueryable().Select(this._agentAdminMapper.SelectorExpression),
                    //AgentBosses = p.Product.SuperAdmin1.AgentBosses.AsQueryable().Select(this._agentBossMapper.SelectorExpression),
                    //ContactInfoes = p.Product.SuperAdmin1.ContactInfoes.AsQueryable().Select(this._contactInfoMapper.SelectorExpression),
                    //ContactInfoes1 = p.Product.SuperAdmin1.ContactInfoes1.AsQueryable().Select(this._contactInfoMapper.SelectorExpression),
                    //DistributionPoints = p.Product.SuperAdmin1.DistributionPoints.AsQueryable().Select(this._distributionPointMapper.SelectorExpression),
                    //DistributionPoints1 = p.Product.SuperAdmin1.DistributionPoints1.AsQueryable().Select(this._distributionPointMapper.SelectorExpression),
                    //Drivers = p.Product.SuperAdmin1.Drivers.AsQueryable().Select(this._driverMapper.SelectorExpression),
                    //Drivers1 = p.Product.SuperAdmin1.Drivers1.AsQueryable().Select(this._driverMapper.SelectorExpression),
                    //Products = p.Product.SuperAdmin1.Products.AsQueryable().Select(this._productMapper.SelectorExpression),
                    //Products1 = p.Product.SuperAdmin1.Products1.AsQueryable().Select(this._productMapper.SelectorExpression),
                    //ProductExchanges = p.Product.SuperAdmin1.ProductExchanges.AsQueryable().Select(this._productExchangeMapper.SelectorExpression),
                    //ProductExchanges1 = p.Product.SuperAdmin1.ProductExchanges1.AsQueryable().Select(this._productExchangeMapper.SelectorExpression),
                    //PromoBanners = p.Product.SuperAdmin1.PromoBanners.AsQueryable().Select(this._promoBannerMapper.SelectorExpression),
                    //PromoBanners1 = p.Product.SuperAdmin1.PromoBanners1.AsQueryable().Select(this._promoBannerMapper.SelectorExpression),
                    //PromoInfoes = p.Product.SuperAdmin1.PromoInfoes.AsQueryable().Select(this._promoInfoMapper.SelectorExpression),
                    //PromoInfoes1 = p.Product.SuperAdmin1.PromoInfoes1.AsQueryable().Select(this._promoInfoMapper.SelectorExpression),
                    //Reminders = p.Product.SuperAdmin1.Reminders.AsQueryable().Select(this._reminderMapper.SelectorExpression),
                    //Reminders1 = p.Product.SuperAdmin1.Reminders1.AsQueryable().Select(this._reminderMapper.SelectorExpression),
                    //ProductExchanges = p.Product.ProductExchanges.AsQueryable().Select(this._productExchangeMapper.SelectorExpression),
                    //Reminders = p.Product.Reminders.AsQueryable().Select(this._reminderMapper.SelectorExpression),
                    //TeleOrderDetails = p.Product.TeleOrderDetails.AsQueryable().Select(this._teleOrderDetailMapper.SelectorExpression),
                    //TeleOrderPrdocuctExchanges = p.Product.TeleOrderPrdocuctExchanges.AsQueryable().Select(this._teleOrderPrdocuctExchangeMapper.SelectorExpression),
                    SuperAdminSAdminID = p.SuperAdmin != null ? p.SuperAdmin.SAdminID : default(short),
                    SuperAdminFullName = p.SuperAdmin != null ? p.SuperAdmin.FullName : default(string),
                    SuperAdminMobileNum = p.SuperAdmin != null ? p.SuperAdmin.MobileNum : default(string),
                    SuperAdminPassword = p.SuperAdmin != null ? p.SuperAdmin.Password : default(string),
                    SuperAdminProfileImage = p.SuperAdmin != null ? p.SuperAdmin.ProfileImage : default(string),
                    SuperAdminAppToken = p.SuperAdmin != null ? p.SuperAdmin.AppToken : default(string),
                    SuperAdminLastLogin = p.SuperAdmin != null ? p.SuperAdmin.LastLogin : default(DateTime?),
                    SuperAdminUserTypeID = p.SuperAdmin != null ? p.SuperAdmin.UserTypeID : default(short),
                    SuperAdminStatusID = p.SuperAdmin != null ? p.SuperAdmin.StatusID : default(bool),
                    SuperAdminEmail = p.SuperAdmin != null ? p.SuperAdmin.Email : default(string),
                    SuperAdminAccToken = p.SuperAdmin != null ? p.SuperAdmin.AccToken : default(string),
                    SuperAdminAppID = p.SuperAdmin != null ? p.SuperAdmin.AppID : default(string),
                    SuperAdminCreatedBy = p.SuperAdmin != null ? p.SuperAdmin.CreatedBy : default(int?),
                    SuperAdminCreatedDate = p.SuperAdmin != null ? p.SuperAdmin.CreatedDate : default(DateTime?),
                    SuperAdminUpdatedBy = p.SuperAdmin != null ? p.SuperAdmin.UpdatedBy : default(int?),
                    SuperAdminUpdatedDate = p.SuperAdmin != null ? p.SuperAdmin.UpdatedDate : default(DateTime?),
                    //ActivityLogs = p.SuperAdmin.ActivityLogs.AsQueryable().Select(this._activityLogMapper.SelectorExpression),
                    //Agencies = p.SuperAdmin.Agencies.AsQueryable().Select(this._agencyMapper.SelectorExpression),
                    //Agencies1 = p.SuperAdmin.Agencies1.AsQueryable().Select(this._agencyMapper.SelectorExpression),
                    //AgentAdmins = p.SuperAdmin.AgentAdmins.AsQueryable().Select(this._agentAdminMapper.SelectorExpression),
                    //AgentBosses = p.SuperAdmin.AgentBosses.AsQueryable().Select(this._agentBossMapper.SelectorExpression),
                    //ContactInfoes = p.SuperAdmin.ContactInfoes.AsQueryable().Select(this._contactInfoMapper.SelectorExpression),
                    //ContactInfoes1 = p.SuperAdmin.ContactInfoes1.AsQueryable().Select(this._contactInfoMapper.SelectorExpression),
                    //DistributionPoints = p.SuperAdmin.DistributionPoints.AsQueryable().Select(this._distributionPointMapper.SelectorExpression),
                    //DistributionPoints1 = p.SuperAdmin.DistributionPoints1.AsQueryable().Select(this._distributionPointMapper.SelectorExpression),
                    //Drivers = p.SuperAdmin.Drivers.AsQueryable().Select(this._driverMapper.SelectorExpression),
                    //Drivers1 = p.SuperAdmin.Drivers1.AsQueryable().Select(this._driverMapper.SelectorExpression),
                    //Products = p.SuperAdmin.Products.AsQueryable().Select(this._productMapper.SelectorExpression),
                    //Products1 = p.SuperAdmin.Products1.AsQueryable().Select(this._productMapper.SelectorExpression),
                    //ProductExchanges = p.SuperAdmin.ProductExchanges.AsQueryable().Select(this._productExchangeMapper.SelectorExpression),
                    //ProductExchanges1 = p.SuperAdmin.ProductExchanges1.AsQueryable().Select(this._productExchangeMapper.SelectorExpression),
                    //PromoBanners = p.SuperAdmin.PromoBanners.AsQueryable().Select(this._promoBannerMapper.SelectorExpression),
                    //PromoBanners1 = p.SuperAdmin.PromoBanners1.AsQueryable().Select(this._promoBannerMapper.SelectorExpression),
                    //PromoInfoes = p.SuperAdmin.PromoInfoes.AsQueryable().Select(this._promoInfoMapper.SelectorExpression),
                    //PromoInfoes1 = p.SuperAdmin.PromoInfoes1.AsQueryable().Select(this._promoInfoMapper.SelectorExpression),
                    //Reminders = p.SuperAdmin.Reminders.AsQueryable().Select(this._reminderMapper.SelectorExpression),
                    //Reminders1 = p.SuperAdmin.Reminders1.AsQueryable().Select(this._reminderMapper.SelectorExpression)
                }));
            }
        }

        public override void MapToModel(ReminderDTO dto, Reminder model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.RmdrID = dto.RmdrID;
            model.ReminderImage = dto.ReminderImage;
            model.UserType = dto.UserType;
            model.Description = dto.Description;
            model.StatusId = dto.StatusId;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;
            model.ProdID = dto.ProdID;

        }
    }
}
