namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Order")]
    public partial class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            ConsumerReviews = new HashSet<ConsumerReview>();
            OrderAllocationLogs = new HashSet<OrderAllocationLog>();
            OrderDeliveries = new HashSet<OrderDelivery>();
            OrderDetails = new HashSet<OrderDetail>();
            OrderPrdocuctExchanges = new HashSet<OrderPrdocuctExchange>();
        }

        [Key]
        public int OrdrID { get; set; }

        public int ConsID { get; set; }

        public int AddrID { get; set; }

        public int? AgadmID { get; set; }

        [StringLength(100)]
        public string InvoiceNumber { get; set; }

        [Column(TypeName = "date")]
        public DateTime OrderDate { get; set; }

        public TimeSpan OrderTime { get; set; }

        [Column(TypeName = "date")]
        public DateTime DeliveryDate { get; set; }

        public short DeliverySlotID { get; set; }

        public int NumberOfProducts { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PromoProduct { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ShippingCharge { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PromoShipping { get; set; }

        [Column(TypeName = "numeric")]
        public decimal GrandTotal { get; set; }

        public short StatusID { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int? DrvrID { get; set; }

        [Column(TypeName = "numeric")]
        public decimal RefillSubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PromoRefill { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ExchangeSubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PromoExchange { get; set; }

        public Nullable<int> CityId { get; set; }

        public virtual AgentAdmin AgentAdmin { get; set; }

        public virtual Consumer Consumer { get; set; }

        public virtual ConsumerAddress ConsumerAddress { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsumerReview> ConsumerReviews { get; set; }

        public virtual Driver Driver { get; set; }

        public virtual MDeliverySlot MDeliverySlot { get; set; }

        public virtual MOrderStatu MOrderStatu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderAllocationLog> OrderAllocationLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderDelivery> OrderDeliveries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderPrdocuctExchange> OrderPrdocuctExchanges { get; set; }
    }
}
