//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pertamina.LPG.API.Models
{
    using System;
    
    public partial class GetNotificationHistory_Result
    {
        public long LogId { get; set; }
        public string FCMResponse { get; set; }
        public string JSONMessage { get; set; }
        public string MobileNumber { get; set; }
        public Nullable<int> UserID { get; set; }
        public string UserName { get; set; }
        public string UserRole { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}
