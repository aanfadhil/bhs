﻿using LPG.App_Classes;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using Microsoft.ApplicationInsights;

namespace LPG.App_Services
{
    public class PengaturanPromoServices : ServiceBase, IPengaturanPromoServices
    {
        public PengaturanPromoServices(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public PengaturanPromoModel AddVoucher(PengaturanPromoModel model,int userId)
        {
            model.Products = model.Products ?? (new List<ProductModel>());
            model.Customers = model.Customers ?? (new List<CustomerModel>());
            model.Regions = model.Regions ?? (new List<RegionModel>());

            model.isSuccess = true;
            
            var Data = context.PromoVouchers.FirstOrDefault(t => t.PromoID == model.PromoID);
            
            if (Data != null)
            {
                var samecode = context.PromoVouchers.Where(t => t.PromoID != Data.PromoID && t.Voucher.ToUpper().Trim() == model.Voucher.ToUpper().Trim()).FirstOrDefault();

                if(samecode != null)
                {
                    model.isSuccess = false;
                    model.ErrorMessage = "Kode Voucher sudah dipakai";
                    return model;
                }

                Data.Quota = model.Quota;
                Data.UserQuota = model.UserQuota;
                if (model.StartDate != null)
                {
                    Data.StartDate = DateTime.ParseExact(model.StartDate, "yyyy-MM-ddTHH:mm:ss", null);
                }
                else
                {
                    Data.StartDate = null;
                }

                if (model.EndDate != null)
                {
                    Data.EndDate = DateTime.ParseExact(model.EndDate, "yyyy-MM-ddTHH:mm:ss", null);
                }
                else
                {
                    Data.EndDate = null;
                }


                Data.IsActive = model.IsActive;
                Data.CreatedBy = userId.ToString();
                Data.CreatedDate = DateTime.Now;
                Data.Voucher = model.Voucher;
                Data.IsActive = model.IsActive;
                Data.Title = model.Title;
                Data.Potongan = model.Potongan;
                

                var products = context.PromoProducts.ToList();
                foreach (var item in products)
                {
                    context.PromoProducts.Remove(item);
                }

                var cons = context.PromoConsumers.ToList();

                foreach (var item in cons)
                {
                    context.PromoConsumers.Remove(item);
                }
                var reg = context.PromoRegions.Where(t => t.PromoID == Data.PromoID);
                foreach (var item in reg)
                {
                    context.PromoRegions.Remove(item);
                }
                context.SaveChanges();

                model.Products.Select(t => new PromoProduct() { ProdID = t.ProdID, PromoID = Data.PromoID }).ToList()
                    .ForEach(t => Data.PromoProducts.Add(t));



                model.Regions.Select(t => new PromoRegion() { RegionID = t.RegionId }).ToList()
                    .ForEach(t => Data.PromoRegions.Add(t));



                model.Customers.Select(t => new PromoConsumer() { ConsID = t.CustomerId }).ToList()
                    .ForEach(t => Data.PromoConsumers.Add(t));
                context.SaveChanges();
                CallActivityLog(ConfigurationManager.AppSettings["update"], model.Title, userId);
            }
            else
            {
                var samecode = context.PromoVouchers.Where(t => t.Voucher.ToUpper().Trim() == model.Voucher.ToUpper().Trim()).FirstOrDefault();

                if (samecode != null)
                {
                    model.isSuccess = false;
                    model.ErrorMessage = "Kode Voucher sudah dipakai";
                    return model;
                }

                PromoVoucher data = new PromoVoucher();
                data.Quota = model.Quota;
                data.UserQuota = model.UserQuota;
                if (model.StartDate != null)
                {
                    data.StartDate = DateTime.ParseExact(model.StartDate, "yyyy-MM-ddTHH:mm:ss", null);
                }

                if (model.EndDate != null)
                {
                    data.EndDate = DateTime.ParseExact(model.EndDate, "yyyy-MM-ddTHH:mm:ss", null);
                }

                data.IsActive = model.IsActive;
                data.CreatedBy = userId.ToString();
                data.CreatedDate = DateTime.Now;
                data.Voucher = model.Voucher;
                data.IsActive = model.IsActive;
                data.Title = model.Title;
                data.Potongan = model.Potongan;

                context.PromoVouchers.Add(data);

                model.Products.Select(t => new PromoProduct() { ProdID = t.ProdID, PromoID = data.PromoID }).ToList()
                    .ForEach(t => data.PromoProducts.Add(t));

                model.Regions.Select(t => new PromoRegion() { RegionID = t.RegionId }).ToList()
                    .ForEach(t => data.PromoRegions.Add(t));

                model.Customers.Select(t => new PromoConsumer() { ConsID = t.CustomerId }).ToList()
                    .ForEach(t => data.PromoConsumers.Add(t));

                context.SaveChanges();

                model.PromoID = data.PromoID;

                

                context.SaveChanges();
                CallActivityLog(ConfigurationManager.AppSettings["add"], model.Title, userId);
            }


            return model;

        }

        public bool CheckVoucher(string vouncher)
        {
            return context.PromoVouchers.Where(t => t.Voucher == vouncher).ToList().Count == 0;
        }

        public string GetVoucher()
        {
            int voucherLength = (WebConfigurationManager.AppSettings["VoucherLength"] ?? "0").ToString().ToInt();
            string result = RandomString(voucherLength);
            bool voucherExists = true;
            do
            {
                result = RandomString(voucherLength);
                voucherExists = context.PromoVouchers.Where(t => t.Voucher == result).ToList().Count > 0;
                
            }
            while (voucherExists);

            return result;
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public List<PengaturanPromoModel> ListVoucher()
        {

            var canceled = new int[] { 5, 6, 7, 8 };

            var data = context.PromoVouchers.OrderByDescending(t => t.PromoID).Select(t => new PengaturanPromoModel() {
                PromoID = t.PromoID,
                Voucher = t.Voucher,
                DStartDate = t.StartDate,//(t.StartDate == null) ? "N/A" : t.StartDate.Value.ToString("YYYY-MM-DDTHH:mm:ss"),
                DEndDate = t.EndDate,//(t.EndDate == null) ? "N/A" : t.EndDate.Value.ToString("YYYY-MM-DDTHH:mm:ss"),
                Products = t.PromoProducts.Select(p => new ProductModel() { ProdID = p.ProdID, ProductName = p.Product.ProductName }).ToList(),
                Regions = t.PromoRegions.Select(p => new RegionModel() { RegionId = p.RegionID, RegionName = p.MRegion.RegionName, RegionCode = p.MRegion.RegionCode }).ToList(),
                Customers = t.PromoConsumers.Select(p => new CustomerModel() { CustomerId = p.ConsID, Name = p.Consumer.Name }).ToList(),
                BannerInfo = t.PromoInfoes.Select(bannerInfo => new BannerInfoModel() {
                    BannerInfoId = bannerInfo.InfoID,
                    Caption = bannerInfo.Caption,
                    Position = bannerInfo.Position,
                    LastModifiedDate = bannerInfo.UpdatedDate,
                    Status = bannerInfo.StatusID
                }).ToList(),
                Potongan = t.Potongan,
                IsActive = t.IsActive,
                //LastActivity = t.UpdatedBy??t.CreatedBy + " at " + (t.UpdatedDate??t.CreatedDate).Value.ToString("DD MMM YYYY HH:mm"),
                Quota = t.Quota.Value,
                QuotaUsed = t.QuotaUsed,
                UserQuota = t.UserQuota,
                Title = t.Title
                

            }).ToList();

            data.ForEach(t =>
            {
                //yyyy-MM-ddTHH:mm:ss
                t.StartDate = (t.DStartDate != null) ? t.DStartDate.Value.ToString("s") : null;
                t.EndDate = (t.DEndDate != null) ? t.DEndDate.Value.ToString("s") : null;
                t.QuotaUsed = context.Orders.Where(o => o.PromoID == t.PromoID && !canceled.Contains(o.StatusID)).Count();
            });

            return data.OrderByDescending(t => t.PromoID).ToList();

        }

        public PengaturanPromoModel GetPromo(int promoId)
        {
            var data = context.PromoVouchers.FirstOrDefault(t => t.PromoID == promoId);
            if (data != null)
            {
                var result =  new PengaturanPromoModel()
                {
                    PromoID = data.PromoID,
                    Title = data.Title,
                    Voucher = data.Voucher,
                    DEndDate = data.EndDate,
                    DStartDate = data.StartDate,
                    StartDate = (data.StartDate == null && data.StartDate.HasValue) ? null : data.StartDate?.ToString("s"),
                    EndDate = (data.EndDate == null && data.EndDate.HasValue) ? null : data.EndDate?.ToString("s"),
                    Products = data.PromoProducts.Select(p => new ProductModel() { ProdID = p.ProdID, ProductName = p.Product.ProductName }).ToList(),
                    Regions = data.PromoRegions.Select(p => new RegionModel() { RegionId = p.RegionID, RegionName = p.MRegion.RegionName, RegionCode = p.MRegion.RegionCode }).ToList(),
                    Customers = data.PromoConsumers.Select(p => new CustomerModel() { CustomerId = p.ConsID, Name = p.Consumer.Name, MobileNo = p.Consumer.PhoneNumber }).ToList(),
                    Potongan = data.Potongan,
                    IsActive = data.IsActive,
                    //LastActivity = data.UpdatedBy ?? data.CreatedBy + " at " + (data.UpdatedDate ?? data.CreatedDate).Value.ToString("DD MMM YYYY HH:mm"),
                    Quota = data.Quota,
                    QuotaUsed = data.QuotaUsed,
                    UserQuota = data.UserQuota

                };

                return result;

            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// ADD : Activity Log table
        /// </summary>
        /// <param name="action"></param>
        /// <param name="action_item"></param>
        /// <param name="loggedBy"></param>
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = action_item;
            _activityService.AddActivityLog(actModel, "Product");
        }
    }
}