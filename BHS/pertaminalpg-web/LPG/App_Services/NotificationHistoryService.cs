﻿using LPG.App_Classes;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using Microsoft.ApplicationInsights;


namespace LPG.App_Services
{
    public class NotificationHistoryService : ServiceBase, INotificationHistoryService
    {

        public const string NOTIFICATION_LOG_ROLE_ADMINAGEN = "admin agen";

        public NotificationHistoryService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }
        
        public List<NotificationHistory> GetAll()
        {
            var result = new List<NotificationHistory>();
            //var count = context.GetNotificationHistoryCount(null,1, "").FirstOrDefault();
            
            result = context.GetNotificationHistoryPaging(null, 1, "").Select(t => new NotificationHistory()
            {
                CreatedDate = t.CreatedDate,
                LogId = t.LogId,
                MobileNumber = t.MobileNumber,
                Username = t.UserName,
                Role = t.UserRole,
                UserId = t.UserID,
                Message = t.Message,
                Title  = t.Title,
                IsRead = t.IsRead,
                Success = t.Success,
                ReadAt = t.ReadAt,
                CreatedDateFormated = (t.CreatedDate == null)? null : t.CreatedDate.Value.ToString("s"),
                ReadAtFormated = (t.ReadAt == null) ? null : t.ReadAt.Value.ToString("s")
                // 1 sudah dibaca , 0 belum dibaca

            }).ToList();

            foreach (var item in result)
            {
                if (item.Role == NOTIFICATION_LOG_ROLE_ADMINAGEN)
                {
                    if (item.UserId != null)
                    {

                        var agamin = context.AgentAdmins.Include("Agency").Where(t => t.AgadmID == item.UserId.Value).FirstOrDefault();
                        if (agamin != null)
                            if (agamin.Agency != null)
                                item.NamaAgen = agamin.Agency.AgencyName;
                    }
                }
            }

            return result;

        }

        public DataTableModel<NotificationHistory> GetList(DatatablesRequest data)
        {
            var result = new DataTableModel<NotificationHistory>();
            var count = context.GetNotificationHistoryCount(data.length, data.start + 1, data.search[0]).FirstOrDefault();
            
            if(count != null)
            {
                result.recordsFiltered = count.Filtered;
                result.recordsTotal = count.UnFiltered;
            }

            //context.GetNotificationHistory(data.length, data.start + 1, data.search[0]).Select(t => new NotificationHistory() {
            result.data = context.GetNotificationHistoryPaging(data.length, data.start + 1, data.search[0]).Select(t => new NotificationHistory() {
                CreatedDate = t.CreatedDate,
                LogId = t.LogId,
                MobileNumber = t.MobileNumber,
                Username = t.UserName,
                Role = t.UserRole,
                UserId = t.UserID,
                Message = t.Message,
                Title = t.Title,
                IsRead = t.IsRead,
                Success = t.Success,
                ReadAt = t.ReadAt,
                CreatedDateFormated = (t.CreatedDate == null) ? null : t.CreatedDate.Value.ToString("s"),
                ReadAtFormated = (t.ReadAt == null) ? null : t.ReadAt.Value.ToString("s")
            }).ToList();

            foreach (var item in result.data)
            {
                if (item.ReadAt != null)
                {
                    var rddate = new DateTime(item.ReadAt.Value.Year, item.ReadAt.Value.Month, item.ReadAt.Value.Day, item.ReadAt.Value.Hour, item.ReadAt.Value.Minute, item.ReadAt.Value.Second, DateTimeKind.Utc);
                    item.ReadAtFormated = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(rddate, "SE Asia Standard Time").ToString("s");
                }
                if (item.Role == NOTIFICATION_LOG_ROLE_ADMINAGEN)
                {
                    if (item.UserId != null)
                    {

                        var agamin = context.AgentAdmins.Include("Agency").Where(t => t.AgadmID == item.UserId.Value).FirstOrDefault();
                        if (agamin != null)
                            if (agamin.Agency != null)
                                item.NamaAgen = agamin.Agency.AgencyName;
                    }
                }
            }

            result.draw = data.draw;

            return result;
        }
    }
}