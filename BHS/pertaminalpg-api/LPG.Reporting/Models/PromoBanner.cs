namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PromoBanner")]
    public partial class PromoBanner
    {
        [Key]
        public int BannerID { get; set; }

        [Required]
        [StringLength(50)]
        public string Caption { get; set; }

        [Required]
        [StringLength(100)]
        public string BannerImage { get; set; }

        public int Category { get; set; }

        public bool StatusId { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public virtual SuperAdmin SuperAdmin { get; set; }

        public virtual SuperAdmin SuperAdmin1 { get; set; }
    }
}
