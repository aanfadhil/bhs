namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Agency")]
    public partial class Agency
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Agency()
        {
            AgentAdmins = new HashSet<AgentAdmin>();
            AgentBosses = new HashSet<AgentBoss>();
            DistributionPoints = new HashSet<DistributionPoint>();
            Drivers = new HashSet<Driver>();
            InvoiceNumbers = new HashSet<InvoiceNumber>();
        }

        [Key]
        public int AgenID { get; set; }

        [Required]
        [StringLength(50)]
        public string SoldToNumber { get; set; }

        [Required]
        [StringLength(100)]
        public string AgencyName { get; set; }

        [Required]
        [StringLength(100)]
        public string Region { get; set; }

        public bool StatusId { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int RegionId { get; set; }

        public virtual MRegion MRegion { get; set; }

        public virtual SuperAdmin SuperAdmin { get; set; }

        public virtual SuperAdmin SuperAdmin1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AgentAdmin> AgentAdmins { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AgentBoss> AgentBosses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DistributionPoint> DistributionPoints { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Driver> Drivers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceNumber> InvoiceNumbers { get; set; }
    }
}
