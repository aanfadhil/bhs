﻿using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.App_Services
{
    public class AbstractAppService
    {
        protected lpgdevEntities context;
        protected TelemetryClient telemetry;
        protected IActivityService _activityService;

        public AbstractAppService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry)
        {
            this.context = context;
            _activityService = activityService;
            telemetry = _telemetry;
        }

        public AbstractAppService(lpgdevEntities context, TelemetryClient _telemetry)
        {
            this.context = context;
            telemetry = _telemetry;
        }
    }
}
