﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.Orders
{
    public class OrderDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int OrdrID { get; set; }
        public int ConsID { get; set; }
        public int AddrID { get; set; }
        public int? AgadmID { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public TimeSpan OrderTime { get; set; }
        public DateTime DeliveryDate { get; set; }
        public short DeliverySlotID { get; set; }
        public int NumberOfProducts { get; set; }
        public decimal SubTotal { get; set; }
        public decimal PromoProduct { get; set; }
        public decimal ShippingCharge { get; set; }
        public decimal PromoShipping { get; set; }
        public decimal GrandTotal { get; set; }
        public short StatusID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int? DrvrID { get; set; }
        public decimal RefillSubTotal { get; set; }
        public decimal PromoRefill { get; set; }
        public decimal ExchangeSubTotal { get; set; }
        public decimal PromoExchange { get; set; }
        public IEnumerable<OrderAllocationLogDTO> OrderAllocationLogs { get; set; }
        public IEnumerable<OrderDeliveryDTO> OrderDeliveries { get; set; }
        public IEnumerable<OrderDetailDTO> OrderDetails { get; set; }
        public IEnumerable<OrderPrdocuctExchangeDTO> OrderPrdocuctExchanges { get; set; }
    }

    public class OrderMapper : MapperBase<Order, OrderDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private OrderAllocationLogMapper _orderAllocationLogMapper = new OrderAllocationLogMapper();
        private OrderDeliveryMapper _orderDeliveryMapper = new OrderDeliveryMapper();
        private OrderDetailMapper _orderDetailMapper = new OrderDetailMapper();
        private OrderPrdocuctExchangeMapper _orderPrdocuctExchangeMapper = new OrderPrdocuctExchangeMapper();
        public override Expression<Func<Order, OrderDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Order, OrderDTO>>) (p => new OrderDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    OrdrID = p.OrdrID,
                    ConsID = p.ConsID,
                    AddrID = p.AddrID,
                    AgadmID = p.AgadmID,
                    InvoiceNumber = p.InvoiceNumber,
                    OrderDate = p.OrderDate,
                    OrderTime = p.OrderTime,
                    DeliveryDate = p.DeliveryDate,
                    DeliverySlotID = p.DeliverySlotID,
                    NumberOfProducts = p.NumberOfProducts,
                    SubTotal = p.SubTotal,
                    PromoProduct = p.PromoProduct,
                    ShippingCharge = p.ShippingCharge,
                    PromoShipping = p.PromoShipping,
                    GrandTotal = p.GrandTotal,
                    StatusID = p.StatusID,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate,
                    DrvrID = p.DrvrID,
                    RefillSubTotal = p.RefillSubTotal,
                    PromoRefill = p.PromoRefill,
                    ExchangeSubTotal = p.ExchangeSubTotal,
                    PromoExchange = p.PromoExchange,
                    OrderAllocationLogs = p.OrderAllocationLogs.AsQueryable().Select(this._orderAllocationLogMapper.SelectorExpression),
                    OrderDeliveries = p.OrderDeliveries.AsQueryable().Select(this._orderDeliveryMapper.SelectorExpression),
                    OrderDetails = p.OrderDetails.AsQueryable().Select(this._orderDetailMapper.SelectorExpression),
                    OrderPrdocuctExchanges = p.OrderPrdocuctExchanges.AsQueryable().Select(this._orderPrdocuctExchangeMapper.SelectorExpression)
                }));
            }
        }

        public override void MapToModel(OrderDTO dto, Order model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.OrdrID = dto.OrdrID;
            model.ConsID = dto.ConsID;
            model.AddrID = dto.AddrID;
            model.AgadmID = dto.AgadmID;
            model.InvoiceNumber = dto.InvoiceNumber;
            model.OrderDate = dto.OrderDate;
            model.OrderTime = dto.OrderTime;
            model.DeliveryDate = dto.DeliveryDate;
            model.DeliverySlotID = dto.DeliverySlotID;
            model.NumberOfProducts = dto.NumberOfProducts;
            model.SubTotal = dto.SubTotal;
            model.PromoProduct = dto.PromoProduct;
            model.ShippingCharge = dto.ShippingCharge;
            model.PromoShipping = dto.PromoShipping;
            model.GrandTotal = dto.GrandTotal;
            model.StatusID = dto.StatusID;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;
            model.DrvrID = dto.DrvrID;
            model.RefillSubTotal = dto.RefillSubTotal;
            model.PromoRefill = dto.PromoRefill;
            model.ExchangeSubTotal = dto.ExchangeSubTotal;
            model.PromoExchange = dto.PromoExchange;

        }
    }
}
