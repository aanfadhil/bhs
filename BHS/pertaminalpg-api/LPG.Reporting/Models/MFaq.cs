namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MFaq")]
    public partial class MFaq
    {
        [Key]
        public int FaqID { get; set; }

        [Required]
        [StringLength(500)]
        public string Question { get; set; }

        [Required]
        public string Answer { get; set; }

        public int Position { get; set; }

        public bool StatusID { get; set; }
    }
}
