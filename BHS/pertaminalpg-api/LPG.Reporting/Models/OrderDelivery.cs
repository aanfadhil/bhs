namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderDelivery")]
    public partial class OrderDelivery
    {
        [Key]
        public int DelvID { get; set; }

        public int OrdrID { get; set; }

        public int DrvrID { get; set; }

        public int AgadmID { get; set; }

        public DateTime AcceptedDate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public short StatusId { get; set; }

        public int deviation { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual AgentAdmin AgentAdmin { get; set; }

        public virtual Driver Driver { get; set; }

        public virtual MDeliveryStatu MDeliveryStatu { get; set; }

        public virtual Order Order { get; set; }
    }
}
