﻿using System.Collections.Generic;
using System.Web.Mvc;
using LPG.Models;

namespace LPG.App_Services
{
    public interface ICityService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        CityModel GetCity(int cityID);
        List<CityModel> ListCity();
        CityModel SaveCity(CityModel model);
    }
}