namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DriverNotificationLog")]
    public partial class DriverNotificationLog
    {
        [Key]
        public long LogId { get; set; }

        [Column(TypeName = "date")]
        public DateTime LogDate { get; set; }

        public DateTime LogDateTime { get; set; }

        public int NumberOfMessages { get; set; }
    }
}
