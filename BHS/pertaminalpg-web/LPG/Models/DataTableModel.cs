﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class DataTableModel<T>
    {
        public int draw { get; set; }
        public int? recordsTotal { get; set; }
        public int? recordsFiltered { get; set; }
        public List<T> data { get; set; }
    }

    public class DatatablesRequest
    {
        public int draw { get; set; }
        public int start { get; set; }
        public int? length { get; set; }
        public List<string> search { get; set; }
        public string searchtype { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
    }
}