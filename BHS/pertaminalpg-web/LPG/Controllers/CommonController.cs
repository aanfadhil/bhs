﻿using LPG.App_Classes;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class CommonController : Controller
    {
        SessionManagement sessionManagement;

        public ActionResult Error()
        {
            return View();
        }

        public JsonResult changeLanguage()
        {
            CommenExecutionStatus EntityModel = new CommenExecutionStatus();

            try
            {
                sessionManagement = new SessionManagement();

                EntityModel.ExecuteStatus = true;

                return Json(EntityModel, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                EntityModel.ExecuteStatus = false;
                return Json(EntityModel, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AdminSignout()
        {
            CommenExecutionStatus EntityModel = new CommenExecutionStatus();

            try
            {
                sessionManagement = new SessionManagement();

                //while logout work set admin session to null
                sessionManagement.setAdminSession(null);

                EntityModel.ExecuteStatus = true;

                return Json(EntityModel, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                EntityModel.ExecuteStatus = false;
                return Json(EntityModel, JsonRequestBehavior.AllowGet);
            }
        }
    }
}