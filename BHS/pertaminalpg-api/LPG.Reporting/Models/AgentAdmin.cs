namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AgentAdmin")]
    public partial class AgentAdmin
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AgentAdmin()
        {
            Orders = new HashSet<Order>();
            OrderDeliveries = new HashSet<OrderDelivery>();
            TeleCustomers = new HashSet<TeleCustomer>();
            TeleOrders = new HashSet<TeleOrder>();
            TeleOrders1 = new HashSet<TeleOrder>();
            TeleOrderDeliveries = new HashSet<TeleOrderDelivery>();
        }

        [Key]
        public int AgadmID { get; set; }

        [Required]
        [StringLength(50)]
        public string AgentAdminName { get; set; }

        public int AgenID { get; set; }

        public int DbptID { get; set; }

        [Required]
        [StringLength(20)]
        public string MobileNumber { get; set; }

        [StringLength(255)]
        public string ProfileImage { get; set; }

        public string AppToken { get; set; }

        public DateTime? LastLogin { get; set; }

        [StringLength(50)]
        public string Password { get; set; }

        public bool StatusId { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [StringLength(64)]
        public string AccToken { get; set; }

        [StringLength(255)]
        public string AppID { get; set; }

        [StringLength(64)]
        public string email { get; set; }

        public virtual Agency Agency { get; set; }

        public virtual DistributionPoint DistributionPoint { get; set; }

        public virtual SuperAdmin SuperAdmin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Orders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderDelivery> OrderDeliveries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleCustomer> TeleCustomers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleOrder> TeleOrders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleOrder> TeleOrders1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleOrderDelivery> TeleOrderDeliveries { get; set; }
    }
}
