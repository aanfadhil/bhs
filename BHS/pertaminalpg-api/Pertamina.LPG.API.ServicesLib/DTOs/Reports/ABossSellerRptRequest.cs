﻿using Pertamina.LPG.API.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Reports
{
    public class ABossSellerRptRequest : AuthBase
    {

        //Default values
        public ABossSellerRptRequest()
        {
            number_of_periods = 4;
            //total_type = 1;
            //periodical_data = 2;
        }

        /// <summary>
        /// 1 - Cash
        /// 2 - Pcs
        /// </summary>
        [Range(1, 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int total_type { get; set; }

        /// <summary>
        /// 1 - Month
        /// 2 - Week
        /// 3 - Year
        /// </summary>
        [Range(1, 3, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int periodical_data { get; set; }

        /// <summary>
        /// Ids of products. If empty - no results will be there
        /// </summary>
        [Required]
        [LimitCountAttribute(1, 50, ErrorMessage = "{0} must include at least {1} element and maximum of {2}")]
        public List<int> products { get; set; }


        public int number_of_periods { get; set; }
    }
}
