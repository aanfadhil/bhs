﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using LPG.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using ClosedXML.Excel;
using System.Globalization;
using i18n;
using LPG.Reporting.ReportServices;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class HomeController : Controller
    {
        // GET: Home
        //LoginService loginService;
        //SessionManagement sessionManagement;

        public AdminSessionEntity AdminSessionInfo { get; private set; }

        //Dependency Injection
        private IAgencyService AgencyService;
        private IProductService ProductService;
        private IUserService UserService;
        private ISessionManagement SessionManagement;
        private ISuperAdminService SuperAdminService;

        //public HomeController() : base()
        //{

        //}

        private SalesReportService _salesReportService;

        public HomeController(IAgencyService _AgencyService, 
            IProductService _ProductService,
            IUserService _UserService,
            ISessionManagement _SessionManagement,
            ISuperAdminService _SuperAdminService) : base()
        {
            SessionManagement = _SessionManagement;
            AgencyService = _AgencyService;
            ProductService = _ProductService;
            UserService = _UserService;
            SuperAdminService = _SuperAdminService;
            _salesReportService = new SalesReportService();
        }

        public ActionResult Index()
        {

            DashboardModel model = new DashboardModel
            {
                Companies = AgencyService.ListComapanyGetbyRegion(0),
                Products = ProductService.GetProductsByCode(new List<string>() { "BG12", "BG55", "ELPG" }),
                RegionList = UserService.ListRegion()
            };

            //sessionManagement = new SessionManagement();
            AdminSessionInfo = SessionManagement.getAdminSession();
            ViewBag.Role = AdminSessionInfo.UserRole;

            return View(model);
        }


        [HttpPost]
        public ActionResult LoadChart(ChartRequestModel request)
        {


            HighChartBar<decimal> model = new HighChartBar<decimal>();
            ViewBag.Title = "[[[Dashboard]]] > [[[Installation]]]";

            var productId = request.ProductId;

            var period = ReportPeriod.Week;

            switch (request.Period)
            {
                case 2: // Week
                    period = ReportPeriod.Week;
                    break;
                case 1: // Month
                    period = ReportPeriod.Month;
                    break;
                case 3: //Year
                    period = ReportPeriod.Year;
                    break;

                default: // Default - week
                    period = ReportPeriod.Week;
                    break;
            }

            if (request.ReportType == 1)
            {
                //var reportList = SuperAdminService.GetSellerReport(request);
                //if (reportList.Count > 0)
                //{
                //    model.x = reportList.Select(x => x.key).ToList<string>();
                //    model.y = reportList.Select(x => x.value).ToList<decimal>();
                //}



                if (request.TotalType == 1) //Cash
                {
                    var reportList = _salesReportService.GetSalesReportInCash(new SalesReportRequestDTO()
                    {
                        AgencyIDs = request.AgencyId,
                        NumberOfPeriods = 6,
                        Period = period,
                        ProductIds = new List<int> { productId }
                    });

                    if (reportList.Count > 0)
                    {
                        model.x = reportList.Select(x => FormatPeriodStart(x.Key, period)).ToList<string>();
                        model.y = reportList.Select(y => (decimal) y.Value).ToList<decimal>();
                    }
                }
                else if (request.TotalType == 2)  //Units
                {
                    var reportList = _salesReportService.GetSalesReportInUnits(new SalesReportRequestDTO()
                    {
                        AgencyIDs = request.AgencyId,
                        NumberOfPeriods = 6,
                        Period = period,
                        ProductIds = new List<int> { productId }
                    });

                    if (reportList.Count > 0)
                    {
                        model.x = reportList.Select(x => FormatPeriodStart(x.Key, period)).ToList<string>();
                        model.y = reportList.Select(x => (decimal) x.Value).ToList<decimal>();
                    }
                }


            }
            else if (request.ReportType == 2)
            {
                var reportList = SuperAdminService.GetSellerRatingReport(request);
                if (reportList.Count > 0)
                {
                    model.x = reportList.Select(x => x.key).ToList<string>();
                    model.y = reportList.Select(x => x.value).ToList<decimal>();
                }
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private string FormatPeriodStart(DateTime dt, ReportPeriod period)
        {

            switch (period)
            {
                case ReportPeriod.Week:
                    return dt.ToString("dd MMM");
                case ReportPeriod.Month:
                    return dt.ToString("MMM yyyy");
                case ReportPeriod.Year:
                    return dt.ToString("yyyy");
                default:
                    return dt.ToString();
            }

        }

        [HttpPost]
        public ActionResult LoadChartData(ChartRequestModel request)
        {

            //var service = new SuperAdminService();

            List<NameYPair<decimal>> list = SuperAdminService.GetOrderStatusReport().Select(r => new NameYPair<decimal>
            {
                name = HttpContext.GetText(r.key, ""),
                y = r.value
            }).ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadUserStats(ChartRequestModel request)
        {

            //var service = new SuperAdminService();


            HighChartBar<decimal> list = new HighChartBar<decimal>();
            list = SuperAdminService.GetRegisteredUserByMonth(4);



            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadTotalUserStats(ChartRequestModel request)
        {
            //var service = new SuperAdminService();

            int usersCount = SuperAdminService.GetRegisteredUserTotal();

            var resp = new
            {
                total = usersCount
            };


            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Logout()
        {
            try
            {

                //sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = SessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;

                if (adminSessionInfo != null)
                {
                    adminSessionInfo = null;
                    Session.Clear();
                    Session.Abandon();
                    //Response.Cookies.Clear();
                    var adminCookie = new HttpCookie("bhssession", "");
                    adminCookie.Expires = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    Response.Cookies.Add(adminCookie);

                    
                    return RedirectToAction("Index", "Login");
                    return Json(new { status = "done" });
                }
                else
                {
                    return Json(new { status = "fail" });
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.Log(ex);
                return Json(new { status = "error" });
            }
        }
        
        //[HttpPost]
        public ActionResult GetSession()
        {

            var culture = new System.Globalization.CultureInfo("id-ID");
            var day = culture.DateTimeFormat.GetDayName(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").DayOfWeek);
            if (day != "")
            {
                Session["LoginDate"] = day + ", " + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
            }
            else
            {
                Session["LoginDate"] = "";
            }

            //sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = SessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {
                Session["UserName"] = adminSessionInfo.FullName;
                Session["UserRole"] = adminSessionInfo.UserRole;

                string originalPath = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                var docPath = "";
                if (adminSessionInfo.imgProfile != "")
                {
                    docPath = adminSessionInfo.imgProfile.Trim(); //originalPath + 
                }
                Session["UserProfile"] = docPath;
                return Json(new { username = Session["UserName"], userrole = Session["UserRole"], profileimg = Session["UserProfile"], logindate = Session["LoginDate"] });
            }
            return Json(new { username = "", userrole = "", profileimg = "", logindate = "" });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RefreshAgency(int regionId)
        {
            DashboardModel model = new DashboardModel
            {
                Companies = AgencyService.ListComapanyGetbyRegion(regionId)
            };
            var result = (from s in model.Companies
                          select new
                          {
                              id = s.CompanyId,
                              name = s.CompanyName
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public FileResult Export()
        {
            
            lpgdevEntities entities = new lpgdevEntities();
            entities.Database.CommandTimeout = 120;

            DataTable dt = new DataTable("MasterData");
            dt.Columns.AddRange(new DataColumn[] {
                                            new DataColumn("[[[OrderID]]]"),
                                            new DataColumn("[[[Date]]]"),
                                            new DataColumn("[[[Nama Pelanggan]]]"),
                                            new DataColumn("[[[No Handphone Pelanggan]]]"),
                                            new DataColumn("[[[No Invoice]]]"),
                                            new DataColumn("[[[Region]]]"),
                                            new DataColumn("[[[Kota]]]"),
                                            new DataColumn("[[[Alamat]]]"),
                                            new DataColumn("[[[Produk]]]"),
                                            new DataColumn("[[[Jumlah]]]"),
                                            new DataColumn("[[[Tanggal & Waktu Pemesanan]]]"),
                                            new DataColumn("[[[Tanggal & Waktu Pengiriman]]]"),
                                            new DataColumn("[[[Waktu Selesai Pengiriman]]]"),
                                            new DataColumn("[[[Metode Beli]]]"),
                                            new DataColumn("[[[Status]]]"),
                                            new DataColumn("[[[Category]]]"),
                                            new DataColumn("[[[Total Harga]]]"),
                                            new DataColumn("[[[Total KG]]]"),
                                            new DataColumn("[[[Sum of KG]]]"),
                                            new DataColumn("[[[Rating]]]"),
                                            new DataColumn("[[[Reason]]]"),
                                            new DataColumn("[[[Note]]]"),
                                            new DataColumn("[[[Agen]]]"),
                                            new DataColumn("[[[Outlet]]]"),
                                            new DataColumn("[[[Admin]]]"),
                                            new DataColumn("[[[Hp Admin]]]")
            });

            string dateStart = Request.Form.Get("dtpStart").ToString();
            string dateEnd = Request.Form.Get("dtpEnd").ToString();
            
            var customers = from customer in entities.MasterDataExtractionWithPeriode(Convert.ToDateTime(dateStart), Convert.ToDateTime(dateEnd))
                            select customer;
            string orderStatus;
            foreach (var customer in customers)
            {
                //buying status 
                if (customer.BuyingStatus == 1)
                {
                    orderStatus = "Order App";
                }
                else if (customer.BuyingStatus == 2)
                {
                    orderStatus = "Order Tele";
                }
                else
                {
                    orderStatus = "Order Pick up";
                }
                dt.Rows.Add(
                customer.OrdrID,
                (DateTime.Parse(customer.OrderDate.ToString()).ToString("dd MMM yyyy")),
                customer.Name,
                customer.CustMobileNumber,
                customer.InvoiceNumber,
                customer.RegionName,
                customer.CityName,
                customer.Address,
                customer.ProductList,
                customer.QUANTITY,
                customer.OrderTime != null ? customer.OrderTime.Value.ToString("dd/MM/yyyy HH:mm"):"",
                (customer.DeliveryDate != null ? customer.DeliveryDate.Value.ToString("dd/MM/yyyy"):"" ) + " " + customer.SlotName,
                customer.DeliveredAt != null ? customer.DeliveredAt.Value.ToString("dd/MM/yyyy HH:mm") : "",
                orderStatus,
                ((OrderStatus) Convert.ToInt32(customer.StatusID)).ToString().Replace("_", " "),
                customer.Category,
                customer.GrandTotal,
                customer.Weight,
                customer.QUANTITY * customer.Weight,
                customer.Rating,
                customer.Comments,
                customer.ReasonText,
                customer.AgencyName,
                customer.Outlet,
                customer.AgentAdminName,
                customer.MobileNumber
               );
            }

            entities.Database.CommandTimeout = null;

            var style = XLWorkbook.DefaultStyle;
            using (XLWorkbook wb = new XLWorkbook())
            {


                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Masterdata.xlsx");
                }
            }
        }
    }
}
