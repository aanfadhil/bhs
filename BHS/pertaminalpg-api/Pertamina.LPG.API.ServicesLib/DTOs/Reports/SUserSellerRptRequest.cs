﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Reports
{
    public class SUserSellerRptRequest : AuthBase
    {

        public int total_type { get; set; }
        public int periodical_data { get; set; }
        public List<ProductInputDto> products { get; set; }
        public int number_of_products { get; set; }
        public int agency_id { get; set; }

    }
}
