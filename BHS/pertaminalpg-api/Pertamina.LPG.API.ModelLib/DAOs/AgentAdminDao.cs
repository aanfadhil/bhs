﻿using System;
using System.Collections.Generic;
using System.Linq;
using Pertamina.LPG.API.DAOs;
using Pertamina.LPG.API.Models;

namespace Pertamina.LPG.API.Services
{
    public class AgentAdminDao : DAO
    {
        public AgentAdmin FindByMobileNumber(string mobileNumber)
        {
            var admins = _context.AgentAdmins.Where(a => a.StatusId && a.MobileNumber.Replace("+62", "") == mobileNumber.Replace("+62", ""));
            if (admins.Count() > 0)
            {
                return admins.Single();
            }
            return null;
        }

        public void Update(AgentAdmin admin)
        {
            _context.Entry(admin).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }

        public AgentAdmin FindById(int userId)
        {
            return _context.AgentAdmins.Find(userId);
        }

        public AgentAdmin FindActiveById(int userId)
        {
            return _context.AgentAdmins.Where(t => t.AgadmID == userId && t.StatusId).FirstOrDefault();
        }

        public List<GetUserNotificationHistory_Result> GetNotificationList(int userId, string search, int pagelength, int pagenumber)
        {
            return _context.GetUserNotificationHistory(pagelength, pagenumber, search, userId, "admin agen").ToList();
        }

        public int GetNotificationCount(int userId, string search, int pagelength, int pagenumber)
        {
            var count = _context.GetUserNotificationHistoryCount(pagelength, pagenumber, search, userId, "admin agen").FirstOrDefault();
            if (count == null)
            {
                return 0;
            }
            return count.Total.Value;
        }

        public int GetNewNotificationCount(int userId)
        {
            var count = _context.GetUserNewNotificationCount(userId, "admin agen").FirstOrDefault();
            if (count == null)
            {
                if(count.Total == null)
                    return 0;   
            }
            return count.Total.Value;
        }
    }
}
