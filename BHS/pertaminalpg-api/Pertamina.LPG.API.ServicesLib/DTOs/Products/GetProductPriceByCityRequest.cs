﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Product
{
    public class GetProductPriceByCityRequest
    {
        public int user_id { get; set; }
        public string auth_token { get; set; }
        public bool is_admin { get; set; }
        public int row_per_page { get; set; }
        public int page_number { get; set; }
        public int city_id { get; set; }
        public string city_code { get; set; }
        public string city_name { get; set; }

    }
}
