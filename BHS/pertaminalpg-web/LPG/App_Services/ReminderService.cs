﻿using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Configuration;
using System.IO;
using Microsoft.ApplicationInsights;

namespace LPG.App_Services
{

    public class ReminderService : ServiceBase, IReminderService
    {
        public ReminderService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<ReminderModel> ListReminder()
        {
            var reminderList = new List<ReminderModel>();
            string filepath = ConfigurationManager.AppSettings["DefaultPath_reminder"].ToString();
            using (var context = new lpgdevEntities())
            {
                var data = from reminder in context.Reminders
                           join user in context.SuperAdmins
                           on reminder.UpdatedBy equals user.SAdminID
                           orderby reminder.RmdrID descending, reminder.CreatedDate descending
                           select new ReminderModel
                           {
                               ReminderId = reminder.RmdrID,
                               Description = reminder.Description,
                               UserType = reminder.UserType,
                               BannerImage = (reminder.ReminderImage != "") ? (filepath + reminder.ReminderImage) : "",
                               LastModifiedDate = reminder.UpdatedDate,
                               IsActive = reminder.StatusId,
                               UpdatedUser = user.FullName

                           };
                reminderList = data.ToList();
            }

            return reminderList;
        }

        //public List<ReminderModel> getActiveReminder()
        //{
        //    var reminderList = new List<ReminderModel>();
        //    using (var context = new lpgdevEntities())
        //    {
        //        var data = from reminder in context.Reminders
        //                   //join driver in context.Drivers
        //                   //on reminder.UserType 
        //                   where reminder.RmdrID == reminder.RmdrID

        //                   select new ReminderModel
        //                   {
        //                       ReminderId = reminder.RmdrID,
        //                       Description = reminder.Description,
        //                       BannerImage = reminder.ReminderImage,
        //                       //Drivername = -reminder.driver
        //                       IsActive = reminder.StatusId
        //                   };
        //        if (data.Count() > 0)
        //            reminderList = data.ToList();
        //    }
        //    return reminderList;
        //}

        public ReminderModel saveReminderDetails(ReminderModel entity, string Cmd, int loggedBy)
        {

            try
            {

                using (var context = new lpgdevEntities())
                {
                    bool Status = false;
                    if (Cmd == "btnSaveAndPublish")
                    {
                        Status = true;
                    }
                    else if (Cmd == "btnSaveUnpublish")
                    {
                        Status = false;
                    }
                    var Data = context.Reminders.Where(r => r.RmdrID == entity.ReminderId).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.RmdrID = entity.ReminderId;
                        Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        Data.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                        Data.CreatedBy = base.UserId.ToShort();
                        Data.UpdatedBy = base.UserId.ToShort();
                        Data.Description = entity.Description;
                        Data.ReminderImage = (entity.BannerImage != "") ? Path.GetFileName(entity.BannerImage) : "";
                        Data.UserType = entity.UserType;
                        Data.StatusId = Status;
                        //Data.ProdID = 2;
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggedBy);
                    }
                    else
                    {
                        Reminder objLocal = new Reminder();
                        objLocal.RmdrID = entity.ReminderId;
                        objLocal.ReminderImage = (entity.BannerImage != "") ? Path.GetFileName(entity.BannerImage) : "";
                        objLocal.UserType = entity.UserType;
                        objLocal.Description = entity.Description;
                        objLocal.StatusId = Status;
                        objLocal.CreatedBy = base.UserId.ToShort();
                        objLocal.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        objLocal.UpdatedBy = base.UserId.ToShort();
                        objLocal.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        //Data.ProdID = 2;
                        context.Reminders.Add(objLocal);
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggedBy);
                    }
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public ReminderModel getReminderDetails(int reminderId)
        {
            string filepath = ConfigurationManager.AppSettings["DefaultPath_reminder"].ToString();
            using (var context = new lpgdevEntities())
            {
                var rtns = (from reminder in context.Reminders
                            where reminder.RmdrID == reminderId
                            select new ReminderModel()
                            {
                                ReminderId = reminder.RmdrID,
                                Description = reminder.Description,
                                BannerImage = (reminder.ReminderImage != "") ? (filepath + reminder.ReminderImage) : "",
                                IsActive = reminder.StatusId,
                                UserType = reminder.UserType


                            }).FirstOrDefault();

                if (rtns == null)
                {
                    rtns = (from reminder in context.Reminders
                            where reminder.RmdrID == reminderId
                            select new ReminderModel()
                            {
                                ReminderId = reminder.RmdrID,
                                Description = reminder.Description,
                                BannerImage = string.Empty,
                                IsActive = reminder.StatusId,
                                UserType = reminder.UserType

                            }).FirstOrDefault();
                }

                return rtns;
            }
        }

        public bool deleteReminder(int id, int loggeduserId)
        {
            bool status = false;
            using (var context = new lpgdevEntities())
            {
                var data = context.Reminders.Where(r => r.RmdrID == id).FirstOrDefault();
                if (data != null)
                {
                    context.Reminders.Remove(data);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", loggeduserId);
                    status = true;
                }
            }
            return status;
        }
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = "Reminder";
            _activityService.AddActivityLog(actModel, "Reminder");
        }
    }
}
