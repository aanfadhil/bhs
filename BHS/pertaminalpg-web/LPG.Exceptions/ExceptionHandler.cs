﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace GBG.Shared.Exceptions
{
    /// <summary>
    /// Class to handle exceptions
    /// </summary>
    public class ExceptionHandler
    {
        private string _applicationName;
        private BaseException _exception;
        private List<ExceptionPublishMode> _publisModes;

        static ExceptionHandler()
        {
            var configSource = ConfigurationSourceFactory.Create();
            Logger.SetLogWriter(new LogWriterFactory(configSource).Create());
        }

        /// <summary>
        /// Initializes the handler for the given exception
        /// </summary>
        /// <param name="applicationName">Name of the application</param>
        /// <param name="ex">Instance of the Exception</param>
        public ExceptionHandler(string applicationName, BaseException ex)
        {
            this._applicationName = applicationName;
            this._exception = ex;

            this._publisModes = new List<ExceptionPublishMode>();
            this._publisModes.Add(ExceptionPublishMode.File);
        }

        /// <summary>
        /// Initializes the handler with the exception and publish mode
        /// </summary>
        /// <param name="applicationName">Name of the application</param>
        /// <param name="ex">Instance of the exception</param>
        /// <param name="publishModes">List of publish modes</param>
        public ExceptionHandler(string applicationName, BaseException ex, params ExceptionPublishMode[] publishModes)
        {
            this._applicationName = applicationName;
            this._exception = ex;

            this._publisModes = new List<ExceptionPublishMode>();
            if (publishModes != null && publishModes.Length > 0)
                this._publisModes.AddRange(publishModes);
            else
                this._publisModes.Add(ExceptionPublishMode.File);
        }

        /// <summary>
        /// Publish the exception in the current instance
        /// </summary>
        /// <param name="additionalInfo">Additional info, if any, to be included in the publish</param>
        public void Publish(string additionalInfo = "")
        {
            LogEntry logEntry = GetLogEntry(additionalInfo);

            foreach (ExceptionPublishMode mode in _publisModes)
            {
                switch (mode)
                {
                    case ExceptionPublishMode.File:
                        Logger.Write(logEntry, "TextLog");
                        break;
                    case ExceptionPublishMode.Email:
                        //Logger.Write(logEntry, "EmailLogging");
                        break;
                    case ExceptionPublishMode.Database:
                        //Logger.Write(logEntry, "DatabaseLogging");
                        break;
                }
            }
        }

        /// <summary>
        /// Get the string message associated with the current exception
        /// </summary>
        /// <param name="additionalInfo">Additional information, if any, to be included in the message</param>
        /// <returns>String representation of the exception</returns>
        public string GetExceptionMessage(string additionalInfo = "")
        {
            StringBuilder message = new StringBuilder();
            message.AppendLine("Message :> " + _exception.Message);
            if (_exception.InnerException != null)
                message.AppendLine("Inner Exception :> " + _exception.InnerException);
            if (!string.IsNullOrWhiteSpace(additionalInfo))
                message.AppendLine("Addition Info :> " + additionalInfo);
            message.AppendLine("Trace :> " + _exception.ToString());

            return message.ToString();
        }

        /// <summary>
        /// Get the LogEntry associated with the current instance
        /// </summary>
        /// <param name="additionalInfo">Additiona Infomation, if any, to be included in the log</param>
        /// <returns>Instanc eof the LogEntry to be logged</returns>
        private LogEntry GetLogEntry(string additionalInfo = "")
        {
            // TODO :: Change the hard-coding using the exception level severity
            TraceEventType severityType = TraceEventType.Error;
            LogEntry logDetails = new LogEntry()
            {
                Title = this._exception.Message,
                Message = GetExceptionMessage(additionalInfo),
                TimeStamp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"),
                ProcessName = this._applicationName,
                Severity = severityType
            };

            return logDetails;
        }

        /// <summary>
        /// Publish the given exception
        /// </summary>
        /// <param name="applicationName">Name of the application</param>
        /// <param name="ex">Instance of Exception to be published</param>
        /// <param name="publishModes">List of publish modes</param>
        public static void PublishException(string applicationName, BaseException ex, string additionalInfo, params ExceptionPublishMode[] publishModes)
        {
            new ExceptionHandler(applicationName, ex, publishModes).Publish(additionalInfo);
        }

        /// <summary>
        /// Publish the information
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="applicationName"></param>
        public static void PublishLog(string title,string message, string applicationName)
        {
            LogEntry logEntry = new LogEntry()
            {
                Title = title,
                Message = message,
                TimeStamp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"),
                ProcessName = applicationName,
                Severity = TraceEventType.Information
            };

            Logger.Write(logEntry, "TextLog");
        }
    }
}
