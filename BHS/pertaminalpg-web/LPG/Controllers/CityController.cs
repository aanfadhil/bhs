﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace LPG.Controllers
{
    public class CityController : AbstractController<ICityService>
    {
        SessionManagement sessionManagement;
        private IProvinceService ProvinceService;

        public CityController(
            ICityService _serviceObject,
            IProvinceService _provinceService
            ) : base(_serviceObject)
        {
            ProvinceService = _provinceService;
        }
       

        // GET: Kota
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListCity()
        {
            var vm = new CityModel();
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var temp = serviceObject.ListCity();
                    return View(temp);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "City List" });
            }
        }

        public ActionResult AddCity()
        {   
            CityAndProvinceModel model = new CityAndProvinceModel();
            CityModel city = new CityModel();
            List<ProvinceModel> provinceList = new List<ProvinceModel>();
            provinceList = ProvinceService.ListProvince();
            model.DDList = provinceList.Select(c => new SelectListItem { Text = c.ProvName.ToString(), Value = c.ID.ToString() }).ToList();
            model.city = city;
            sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }

        [HttpPost]
        public ActionResult AddCity(CityAndProvinceModel _dataModel)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    var reply = serviceObject.SaveCity(_dataModel.city);

                    SetFlash(FlashMessageType.Success, "Input kota berhasil");

                    return RedirectToAction("ListCity");
                }

                return RedirectToAction("ListCity");
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Price Add/Update" });
            }

        }

        public ActionResult EditCity(int? cityId)
        {
            try
            {
                CityAndProvinceModel model = new CityAndProvinceModel();
                CityModel city = new CityModel();
                List<ProvinceModel> provinceList = new List<ProvinceModel>();
                provinceList = ProvinceService.ListProvince();

                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var convid = cityId ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.GetCity(convid);
                        model.DDList = provinceList.Select(c => new SelectListItem { Selected = (c.ID == vm.ProvID), Text = c.ProvName.ToString(), Value = c.ID.ToString() }).ToList();
                        model.city = city;
                        if (vm != null)
                        {
                            model.city = vm;
                            //BindPositions(vm.ID);
                            return View(model);
                        }
                        else
                        {
                            //BindPositions(null);
                            return View(model);
                        }
                    }
                    else
                    {
                        //BindPositions(null);
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Detail Price - Get" });
            }
        }

        [HttpPost]
        public ActionResult EditCity(CityAndProvinceModel _dataModel)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    var reply = serviceObject.SaveCity(_dataModel.city);

                    SetFlash(FlashMessageType.Success, "Input kota berhasil");

                    return RedirectToAction("ListCity");
                }

                return RedirectToAction("ListCity");
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Price Add/Update" });
            }

        }
    }
}