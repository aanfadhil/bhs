﻿using System.Collections.Generic;

namespace LPG.Models
{
    public class CustomerModel
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string SecondAddress { get; set; }
        public string MainAddress { get; set; }
        public string city { get; set; }
        public string Region { get; set; }
        public string ProfileImage { get; set; }
        public string Code { get; set; }
        public string MobileNo { get; set; }
        public bool IsExists { get; set; }
        public string AdditionalInfo { get; set; }
        public List<CustomerModel> CustomerList { get; set; }

    }
}