﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Pertamina.LPG.API.DTOs;
using Pertamina.LPG.API.DTOs.AgentBoss;
using Pertamina.LPG.API.Services;
using Pertamina.LPG.API.DTOs.Gen;

namespace Pertamina.LPG.API.Controllers
{
    public class AgentBossController : ApiController
    {
        [HttpPost]
        [ActionName("login")]
        public NegotiatedContentResult<LoginResponse> PostLogin([FromBody]LoginRequest request)
        {
            LoginResponse resp = AgentBossServices.Login(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("forgot_password")]
        public NegotiatedContentResult<ForgotPasswordResponse> PostForgotPassword([FromBody]ForgotPasswordRequest request)
        {
            ForgotPasswordResponse resp = AgentBossServices.ForgotPassword(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("reset_password")]
        public NegotiatedContentResult<ResponseDto> PostResetPassword([FromBody]ResetPasswordRequest request)
        {
            ResponseDto resp = AgentBossServices.ResetPassword(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("change_password_agent_boss")]
        public NegotiatedContentResult<ResponseDto> PostChangePassword([FromBody]ChangePasswordAgentBossRequest request)
        {
            ResponseDto resp = AgentBossServices.ChangePassword(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("change_profile_agent_boss")]
        public NegotiatedContentResult<ResponseDto> PostChangeProfile([FromBody]ChangeProfileAgentBossRequest request)
        {
            ResponseDto resp = AgentBossServices.ChangeProfile(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("get_agent_boss_details")]
        public NegotiatedContentResult<GetAgentBossDetailsResponse> PostGetDetails([FromBody]GetAgentBossDetailsRequest request)
        {
            GetAgentBossDetailsResponse resp = AgentBossServices.GetDetails(request);
            return Content(HttpStatusCode.OK, resp);
        }
        [HttpPost]
        [ActionName("check_otp")]
        public NegotiatedContentResult<ResponseDto> PostCheckOTP([FromBody]CheckOtpRequest request)
        {
            ResponseDto resp = AgentBossServices.CheckOTP(request);
            return Content(HttpStatusCode.OK, resp);
        }
        [HttpPost]
        [ActionName("resend_otp")]
        public NegotiatedContentResult<ResendOtpResponse> PostResendOtp([FromBody]ResendOtpRequest request)
        {
            ResendOtpResponse resp = AgentBossServices.ResendOtp(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("change_profilephoto_agent_boss")]
        public NegotiatedContentResult<ResponseDto> PostChangeProfilePhoto([FromBody]ChangeProfilePhotoRequest request)
        {
            ResponseDto resp = AgentBossServices.ChangeProfilePhoto(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("get_latest_version")]
        public NegotiatedContentResult<GetLatestVersionResponse> PostGetLatestApkVersion([FromBody]GetLatestVersionRequest request)
        {
            AgentAdminServices _adminServices = new AgentAdminServices();

            GetLatestVersionResponse resp = new GetLatestVersionResponse();
            resp.has_resource = 1;
            switch ((request.app_os ?? "").ToUpper())
            {
                case "ANDROID":
                    var version = (new UserServices()).GetAppVersion(VersionAppType.Android, VersionUserType.ABoss).Split(';');

                    resp.version = version[0];
                    resp.version_code = version[1];
                    resp.app_id = version[2];
                    break;
                case "IOS":
                    var iosversion = (new UserServices()).GetAppVersion(VersionAppType.IOS, VersionUserType.ABoss).Split(';');


                    resp.version = iosversion[0];
                    resp.version_code = iosversion[1];
                    resp.app_id = iosversion[2];
                    break;

                default:
                    resp.code = 1;
                    resp.message = "jenis app tidak ditemukan";
                    resp.has_resource = 0;
                    break;
            }

            return Content(HttpStatusCode.OK, resp);
        }
    }
}
