﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pertamina.LPG.API.Models;

namespace Pertamina.LPG.API.DTOs.Notifications
{
    public class NotificationDto : ResponseDto
    {
        public Nullable<int> LogId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string MobileNumber { get; set; }
        public Nullable<int> UserID { get; set; }
        public string UserName { get; set; }
        public string UserRole { get; set; }
        public int order_id { get; set; }
        public string CreatedDate { get; set; }
        public Nullable<short> IsRead { get; set; }


        public NotificationDto():base()
        {

        }

        public NotificationDto(GetUserNotificationHistory_Result result):base()
        {
            LogId = result.LogId;
            Title = result.Title;
            this.Message = result.Message;
            this.MobileNumber = result.MobileNumber;
            this.UserID = result.UserID;
            this.UserName = result.UserName;
            this.UserRole = result.UserRole;
            this.order_id = result.OrdrID??0;
            if(result.CreatedDate != null)
            {
                this.CreatedDate = result.CreatedDate.Value.ToString("dd MMM yyyy HH:mm");
            }

            this.IsRead = result.IsRead;
        }
    }
}
