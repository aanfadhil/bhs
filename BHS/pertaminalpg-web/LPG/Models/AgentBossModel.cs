﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LPG.Models
{
    public class AgentBossModel
    {
        public int AgentBossId { get; set; }
        public string OwnerName { get; set; }
        public string CompanyName { get; set; }

        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid Mobile Number.")]
        public string MobileNo { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public List<AgentBossModel> AgentBossList { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string AgentBossEmail { get; set; }

        public int AgenDriverAgenId { get; set; }

        public List<SelectListItem> AgencyList { get; set; }
        public string errorMessage { get; set; }

        public string CountryCode { get { return Constants.APPSETTING_COUNTRY_CODE; } }
    }
}