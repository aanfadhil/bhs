﻿using Microsoft.ApplicationInsights;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LPG.Controllers
{
    public class AbstractController<TServiceType> : Controller
        where TServiceType : class
    {
        
        
        protected TServiceType serviceObject;
        protected TelemetryClient telemectryClient = new TelemetryClient();

        public AbstractController(TServiceType _serviceObject) : base()
        {
            serviceObject = _serviceObject;
        }

        public AbstractController() : base()
        {

        }

        protected CloudBlobContainer GetCloudBlobContainer()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                    CloudConfigurationManager.GetSetting("pertaminalpg_AzureStorageConnectionString"));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("lpg-cms-static-assets");
            container.CreateIfNotExists();
            return container;
        }

        protected string UploadToAzure(string localPathToFile, string filename)
        {

            var container = GetCloudBlobContainer();
            var blobRef = container.GetBlockBlobReference(filename);
            using (var fileStream = System.IO.File.OpenRead(localPathToFile))
            {
                blobRef.UploadFromStream(fileStream);
            }
            return "https://pertaminalpg.blob.core.windows.net/lpg-cms-static-assets/" + filename;
        }

        public void SetFlash(FlashMessageType type, string text)
        {
            TempData["FlashMessage.Type"] = type;
            TempData["FlashMessage.Text"] = text;
        }
    }


    public enum FlashMessageType
    {
        Success,
        Warning,
        Error
    }

}
