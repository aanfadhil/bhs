﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using LPG.Reporting.DTOs.Activities;
using LPG.Reporting.DTOs.Agencies;
using LPG.Reporting.DTOs.SystemActors;
using LPG.Reporting.DTOs.DistributionPoints;
using LPG.Reporting.DTOs.Products;
using LPG.Reporting.DTOs.Reminders;

namespace LPG.Reporting.DTOs.Promo
{
    [DataContract]
    public class PromoInfoDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public int InfoID { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string Caption { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string InfoImage { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public short StatusID { get; set; }
        [DataMember]
        public short CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public short UpdatedBy { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public short SuperAdminSAdminID { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string SuperAdminFullName { get; set; }
        [DataMember]
        [Required]
        [StringLength(20)]
        public string SuperAdminMobileNum { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string SuperAdminPassword { get; set; }
        [DataMember]
        [StringLength(255)]
        public string SuperAdminProfileImage { get; set; }
        [DataMember]
        public string SuperAdminAppToken { get; set; }
        [DataMember]
        public DateTime? SuperAdminLastLogin { get; set; }
        [DataMember]
        public short SuperAdminUserTypeID { get; set; }
        [DataMember]
        public bool SuperAdminStatusID { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string SuperAdminEmail { get; set; }
        [DataMember]
        [StringLength(64)]
        public string SuperAdminAccToken { get; set; }
        [DataMember]
        [StringLength(255)]
        public string SuperAdminAppID { get; set; }
        [DataMember]
        public int? SuperAdminCreatedBy { get; set; }
        [DataMember]
        public DateTime? SuperAdminCreatedDate { get; set; }
        [DataMember]
        public int? SuperAdminUpdatedBy { get; set; }
        [DataMember]
        public DateTime? SuperAdminUpdatedDate { get; set; }
        [DataMember]
        public IEnumerable<ActivityLogDTO> SuperAdminActivityLogs { get; set; }
        [DataMember]
        public IEnumerable<AgencyDTO> SuperAdminAgencies { get; set; }
        [DataMember]
        public IEnumerable<AgencyDTO> SuperAdminAgencies1 { get; set; }
        [DataMember]
        public IEnumerable<AgentAdminDTO> SuperAdminAgentAdmins { get; set; }
        [DataMember]
        public IEnumerable<AgentBossDTO> SuperAdminAgentBosses { get; set; }
        [DataMember]
        public IEnumerable<ContactInfoDTO> SuperAdminContactInfoes { get; set; }
        [DataMember]
        public IEnumerable<ContactInfoDTO> SuperAdminContactInfoes1 { get; set; }
        [DataMember]
        public IEnumerable<DistributionPointDTO> SuperAdminDistributionPoints { get; set; }
        [DataMember]
        public IEnumerable<DistributionPointDTO> SuperAdminDistributionPoints1 { get; set; }
        [DataMember]
        public IEnumerable<DriverDTO> SuperAdminDrivers { get; set; }
        [DataMember]
        public IEnumerable<DriverDTO> SuperAdminDrivers1 { get; set; }
        [DataMember]
        public IEnumerable<ProductDTO> SuperAdminProducts { get; set; }
        [DataMember]
        public IEnumerable<ProductDTO> SuperAdminProducts1 { get; set; }
        [DataMember]
        public IEnumerable<ProductExchangeDTO> SuperAdminProductExchanges { get; set; }
        [DataMember]
        public IEnumerable<ProductExchangeDTO> SuperAdminProductExchanges1 { get; set; }
        [DataMember]
        public IEnumerable<PromoBannerDTO> SuperAdminPromoBanners { get; set; }
        [DataMember]
        public IEnumerable<PromoBannerDTO> SuperAdminPromoBanners1 { get; set; }
        [DataMember]
        public IEnumerable<PromoInfoDTO> SuperAdminPromoInfoes { get; set; }
        [DataMember]
        public IEnumerable<PromoInfoDTO> SuperAdminPromoInfoes1 { get; set; }
        [DataMember]
        public IEnumerable<ReminderDTO> SuperAdminReminders { get; set; }
        [DataMember]
        public IEnumerable<ReminderDTO> SuperAdminReminders1 { get; set; }
    }

    public class PromoInfoMapper : MapperBase<PromoInfo, PromoInfoDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<PromoInfo, PromoInfoDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<PromoInfo, PromoInfoDTO>>) (p => new PromoInfoDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    InfoID = p.InfoID,
                    Caption = p.Caption,
                    InfoImage = p.InfoImage,
                    Position = p.Position,
                    StatusID = p.StatusID,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate,
                    SuperAdminSAdminID = p.SuperAdmin != null ? p.SuperAdmin.SAdminID : default(short),
                    SuperAdminFullName = p.SuperAdmin != null ? p.SuperAdmin.FullName : default(string),
                    SuperAdminMobileNum = p.SuperAdmin != null ? p.SuperAdmin.MobileNum : default(string),
                    SuperAdminPassword = p.SuperAdmin != null ? p.SuperAdmin.Password : default(string),
                    SuperAdminProfileImage = p.SuperAdmin != null ? p.SuperAdmin.ProfileImage : default(string),
                    SuperAdminAppToken = p.SuperAdmin != null ? p.SuperAdmin.AppToken : default(string),
                    SuperAdminLastLogin = p.SuperAdmin != null ? p.SuperAdmin.LastLogin : default(DateTime?),
                    SuperAdminUserTypeID = p.SuperAdmin != null ? p.SuperAdmin.UserTypeID : default(short),
                    SuperAdminStatusID = p.SuperAdmin != null ? p.SuperAdmin.StatusID : default(bool),
                    SuperAdminEmail = p.SuperAdmin != null ? p.SuperAdmin.Email : default(string),
                    SuperAdminAccToken = p.SuperAdmin != null ? p.SuperAdmin.AccToken : default(string),
                    SuperAdminAppID = p.SuperAdmin != null ? p.SuperAdmin.AppID : default(string),
                    SuperAdminCreatedBy = p.SuperAdmin != null ? p.SuperAdmin.CreatedBy : default(int?),
                    SuperAdminCreatedDate = p.SuperAdmin != null ? p.SuperAdmin.CreatedDate : default(DateTime?),
                    SuperAdminUpdatedBy = p.SuperAdmin != null ? p.SuperAdmin.UpdatedBy : default(int?),
                    SuperAdminUpdatedDate = p.SuperAdmin != null ? p.SuperAdmin.UpdatedDate : default(DateTime?),
                    //ActivityLogs = p.SuperAdmin.ActivityLogs.AsQueryable().Select(this._activityLogMapper.SelectorExpression),
                    //Agencies = p.SuperAdmin.Agencies.AsQueryable().Select(this._agencyMapper.SelectorExpression),
                    //Agencies1 = p.SuperAdmin.Agencies1.AsQueryable().Select(this._agencyMapper.SelectorExpression),
                    //AgentAdmins = p.SuperAdmin.AgentAdmins.AsQueryable().Select(this._agentAdminMapper.SelectorExpression),
                    //AgentBosses = p.SuperAdmin.AgentBosses.AsQueryable().Select(this._agentBossMapper.SelectorExpression),
                    //ContactInfoes = p.SuperAdmin.ContactInfoes.AsQueryable().Select(this._contactInfoMapper.SelectorExpression),
                    //ContactInfoes1 = p.SuperAdmin.ContactInfoes1.AsQueryable().Select(this._contactInfoMapper.SelectorExpression),
                    //DistributionPoints = p.SuperAdmin.DistributionPoints.AsQueryable().Select(this._distributionPointMapper.SelectorExpression),
                    //DistributionPoints1 = p.SuperAdmin.DistributionPoints1.AsQueryable().Select(this._distributionPointMapper.SelectorExpression),
                    //Drivers = p.SuperAdmin.Drivers.AsQueryable().Select(this._driverMapper.SelectorExpression),
                    //Drivers1 = p.SuperAdmin.Drivers1.AsQueryable().Select(this._driverMapper.SelectorExpression),
                    //Products = p.SuperAdmin.Products.AsQueryable().Select(this._productMapper.SelectorExpression),
                    //Products1 = p.SuperAdmin.Products1.AsQueryable().Select(this._productMapper.SelectorExpression),
                    //ProductExchanges = p.SuperAdmin.ProductExchanges.AsQueryable().Select(this._productExchangeMapper.SelectorExpression),
                    //ProductExchanges1 = p.SuperAdmin.ProductExchanges1.AsQueryable().Select(this._productExchangeMapper.SelectorExpression),
                    //PromoBanners = p.SuperAdmin.PromoBanners.AsQueryable().Select(this._promoBannerMapper.SelectorExpression),
                    //PromoBanners1 = p.SuperAdmin.PromoBanners1.AsQueryable().Select(this._promoBannerMapper.SelectorExpression),
                    //PromoInfoes = p.SuperAdmin.PromoInfoes.AsQueryable().Select(this._promoInfoMapper.SelectorExpression),
                    //PromoInfoes1 = p.SuperAdmin.PromoInfoes1.AsQueryable().Select(this._promoInfoMapper.SelectorExpression),
                    //Reminders = p.SuperAdmin.Reminders.AsQueryable().Select(this._reminderMapper.SelectorExpression),
                    //Reminders1 = p.SuperAdmin.Reminders1.AsQueryable().Select(this._reminderMapper.SelectorExpression)
                }));
            }
        }

        public override void MapToModel(PromoInfoDTO dto, PromoInfo model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.InfoID = dto.InfoID;
            model.Caption = dto.Caption;
            model.InfoImage = dto.InfoImage;
            model.Position = dto.Position;
            model.StatusID = dto.StatusID;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;

        }
    }
}
