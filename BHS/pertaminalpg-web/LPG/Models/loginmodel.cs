﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LPG.Models
{
    public class LoginModel
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        [RegularExpression(@"^\S*$", ErrorMessage = "No white space allowed")]
        public string new_password { get; set; }
        [Compare("new_password")]
        [RegularExpression(@"^\S*$", ErrorMessage = "No white space allowed")]
        public string confirm_password { get; set; }
        public string ProfileImage { get; set; }
        public int UserType { get; set; }
        public DateTime LastLogin { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsRememberMe { get; set; }

    }
}