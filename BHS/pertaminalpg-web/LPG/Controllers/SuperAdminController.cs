﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Script.Serialization;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class SuperAdminController : AbstractController<ISuperAdminService>
    {
        // GET: Super Admin List
        SessionManagement sessionManagement;

        public SuperAdminController(ISuperAdminService _serviceObject) : base(_serviceObject)
        {
        }

        //private SuperAdminService serviceObject = new SuperAdminService();

        public ActionResult ListSuperAdmin()
        {

            var vm = new SuperAdminModel();
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.SuperAdminModelList = serviceObject.ListSuperAdminUser();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "SuperAdmin List" });
            }
            return View(vm);
        }

        // ADD : SuperAdmin details to DB       
        [HttpPost]
        public ActionResult AddSuperAdmin(SuperAdminModel vm)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View(vm);
            //}
            try
            {
                //throw new Exception("dsfdsf");
                sessionManagement = new SessionManagement();
                string filepath = ConfigurationManager.AppSettings["DefaultPath_Superadmin"].ToString();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var ObjList = serviceObject.ListSuperUserType();
                    var ProvList = serviceObject.ListProvince();
                    var CityList = serviceObject.ListCities();


                    if (ModelState.IsValid)
                    {
                        if (serviceObject.IsExistsEmailID(vm.email, vm.sAdminId))
                        {
                            vm.DDList = ObjList.Select(c => new SelectListItem { Text = c.type_name.ToString(), Value = c.userTypeId.ToString() }).ToList();
                            vm.ProvList = ProvList.Select(c => new SelectListItem { Text = c.ProvName.ToString(), Value = c.ID.ToString() }).ToList();
                            vm.CityList = CityList.Select(c => new SelectListItem { Text = c.CityName.ToString(), Value = c.ID.ToString() }).ToList();

                            vm.errorMessage = MessageSource.GetMessage("Email.exists");
                            return View(vm);
                        }
                        else if (serviceObject.IsExistsMobileNo(vm.mobile_number, vm.sAdminId))
                        {
                            vm.DDList = ObjList.Select(c => new SelectListItem { Text = c.type_name.ToString(), Value = c.userTypeId.ToString() }).ToList();
                            vm.ProvList = ProvList.Select(c => new SelectListItem { Text = c.ProvName.ToString(), Value = c.ID.ToString() }).ToList();
                            vm.CityList = CityList.Select(c => new SelectListItem { Text = c.CityName.ToString(), Value = c.ID.ToString() }).ToList();

                            vm.errorMessage = MessageSource.GetMessage("Mobile.exists");
                            return View(vm);
                        }
                        else
                        {
                            serviceObject.SaveSuperAdminUser(vm, adminSessionInfo.UserId);
                            if (vm.sAdminId == adminSessionInfo.UserId && vm.profile_image != "")
                            {

                                adminSessionInfo.imgProfile = vm.profile_image;
                            }
                            return RedirectToAction("ListSuperAdmin");
                        }

                    }
                    else
                    {
                        vm.DDList = ObjList.Select(c => new SelectListItem { Text = c.type_name.ToString(), Value = c.userTypeId.ToString() }).ToList();
                        vm.ProvList = ProvList.Select(c => new SelectListItem { Text = c.ProvName.ToString(), Value = c.ID.ToString() }).ToList();
                        vm.CityList = CityList.Select(c => new SelectListItem { Text = c.CityName.ToString(), Value = c.ID.ToString() }).ToList();

                        return View(vm);
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            catch (Exception ex)
            {
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "SuperAdmin Add/Update" });
            }
        }

        public ActionResult AddSuperAdmin(int? id)
        {
            try
            {
                SuperAdminModel model = new SuperAdminModel();
                //SuperAdminService serviceObject = new SuperAdminService();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var ObjList = serviceObject.ListSuperUserType();
                    var ProvList = serviceObject.ListProvince();
                    var CityList = serviceObject.ListCities();

                    var convid = id ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.GetSuperAdminUser(convid);
                        if (vm != null)
                        {
                            vm.DDList = ObjList.Select(c => new SelectListItem { Text = c.type_name.ToString(), Value = c.userTypeId.ToString() }).ToList();
                            vm.ProvList = ProvList.Select(c => new SelectListItem { Text = c.ProvName.ToString(), Value = c.ID.ToString() }).ToList();
                            vm.CityList = CityList.Select(c => new SelectListItem { Text = c.CityName.ToString(), Value = c.ID.ToString() }).ToList();

                            return View(vm);
                        }
                        else
                        {
                            return View(model);
                        }
                    }
                    else
                    {
                        model.DDList = ObjList.Select(c => new SelectListItem { Text = c.type_name.ToString(), Value = c.userTypeId.ToString() }).ToList();
                        model.ProvList = ProvList.Select(c => new SelectListItem { Text = c.ProvName.ToString(), Value = c.ID.ToString() }).ToList();
                        model.CityList = CityList.Select(c => new SelectListItem { Text = c.CityName.ToString(), Value = c.ID.ToString() }).ToList();

                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "SuperAdmin Edit" });
            }
        }

        //EDIT : Edit Super Admin Password from DB        
        public ActionResult EditSuperAdminPassword()
        {
            try
            {
                SuperAdminModel model = new SuperAdminModel();
                //SuperAdminService serviceObject = new SuperAdminService();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var convid = adminSessionInfo.UserId;
                    var ProvList = serviceObject.ListProvince();
                    var CityList = serviceObject.ListCities();


                    if (convid > 0)
                    {
                        var vm = serviceObject.GetSuperAdminUser(convid);
                        if (vm != null)
                        {
                            if(vm.provinceId != 0)
                            {
                                CityList = CityList.Where(c => c.ProvID == vm.provinceId).ToList();
                            }


                            vm.ProvList = ProvList.Select(c => new SelectListItem { Text = c.ProvName.ToString(), Value = c.ID.ToString() }).ToList();
                            vm.CityList = CityList.Select(c => new SelectListItem { Text = c.CityName.ToString(), Value = c.ID.ToString() }).ToList();

                            return View(vm);
                        }
                        else
                        {
                            model.ProvList = ProvList.Select(c => new SelectListItem { Text = c.ProvName.ToString(), Value = c.ID.ToString() }).ToList();
                            model.CityList = CityList.Select(c => new SelectListItem { Text = c.CityName.ToString(), Value = c.ID.ToString() }).ToList();

                            return View(model);
                        }
                    }
                    else
                    {
                        model.ProvList = ProvList.Select(c => new SelectListItem { Text = c.ProvName.ToString(), Value = c.ID.ToString() }).ToList();
                        model.CityList = CityList.Select(c => new SelectListItem { Text = c.CityName.ToString(), Value = c.ID.ToString() }).ToList();

                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Change Password - View" });
            }
        }

        [HttpPost]
        public ActionResult EditSuperAdminPassword(SuperAdminModel vm)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                serviceObject.SaveSuperAdminPassword(vm);
                if (vm.sAdminId == adminSessionInfo.UserId)
                {
                    adminSessionInfo.CityId = vm.cityId;
                    if (vm.profile_image != "")
                    {
                        adminSessionInfo.imgProfile = vm.profile_image;
                    }
                }
                return RedirectToAction("ListSuperAdmin");
            }
            catch (Exception ex)
            {
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Change Password - Update" });
            }
        }

        // DELETE : Edit Super Admin  
        //[HttpPost]
        public ActionResult DeleteSuperAdmin(int id)
        {

            sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {
                if (serviceObject.DeleteSuperAdminUser(id, adminSessionInfo.UserId))
                {
                    //return View();
                    return RedirectToAction("ListSuperAdmin", "SuperAdmin");
                }

            }
            return RedirectToAction("Index", "Error", new { area = "SuperAdmin - Delete" });
        }

        ////UPLOAD : Profile Image 
        //[AcceptVerbs(HttpVerbs.Post)]
        //public JsonResult UploadProfileImage()
        //{
        //    string _imgname = string.Empty;
        //    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
        //    {
        //        var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
        //        if (pic.ContentLength > 0)
        //        {
        //            var fileName = Path.GetFileName(pic.FileName);
        //            var _ext = Path.GetExtension(pic.FileName);

        //            _imgname = Guid.NewGuid().ToString();
        //            var _comPath = Server.MapPath("/Content/Upload/ProfileImg/ProfileImg_") + _imgname + _ext;
        //            _imgname = "ProfileImg_" + _imgname + _ext;

        //            ViewBag.Msg = _comPath;
        //            var path = _comPath;

        //            // Saving Image in Original Mode
        //            pic.SaveAs(path);

        //            // resizing image
        //            MemoryStream ms = new MemoryStream();
        //            WebImage img = new WebImage(_comPath);

        //            if (img.Width > 200)
        //                img.Resize(1500, 577);
        //            img.Save(_comPath);
        //            // end resize
        //        }
        //    }
        //    return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        //}  

        [HttpPost]
        public JsonResult getCityByIdProv(int provinceId)
        {
            List<CityModel> cityList = serviceObject.ListCities(provinceId);

            SelectList obgcity = new SelectList(cityList, "Id", "CityName", 0);
            return Json(obgcity);
        }

    }
}
