﻿using System.Collections.Generic;
using i18n;

namespace LPG
{
    /// <summary>
    /// Gets / returns a textual message for a given key string.
    /// This is also used as a class for common/reused methods
    /// </summary>
    public static class MessageSource
    {
        // TODO : This mapping may be moved to a database table and GetMessage method modified accordingly.
        private static Dictionary<string, string> _Messages = new Dictionary<string, string>
        {
            {"product.add", "[[[Product added successfully]]]"},
            {"product.update", "[[[Product updated successfully]]]"},
            {"product.delete", "[[[Product deleted successfully]]]"},
            {"invalid.email", "[[[Invalid email. Please enter valid email address.]]]" },
            {"invalid.mobile", "[[[Invalid mobile. Please enter valid mobile.]]]" },
            {"required.data", "[[[Please enter required data.]]]" },
            {"sadmin.add", "[[[Super Admin added successfully]]]"},
            {"sadmin.update", "[[[Super Admin updated successfully]]]"},
            {"sadmin.delete", "[[[Super Admin deleted successfully]]]"},
            {"reset.invalid", "[[[Invalid email]]]"},
            {"reset.password", "[[[Super Admin password reset successfully]]]"},
            {"reset.mismatch", "[[[New password and confirm password is mismatched]]]"},
            {"Email.exists", "[[[Email already exists]]]"},
            {"Mobile.exists", "[[[Mobile number already exists]]]"},
            {"Exchange.Promo", "[[[Exchange promo price must be greater than or equals to exchange price]]]"},
            {"banner.InfoPromo","[[[Info Promo updated]]]"},
            {"required.Agent.Admin", "[[[Agent Admin is required]]]" },
            {"required.Distribution", "[[[Distribution point is required]]]" },
            {"order_status.received", "[[[Received]]]" },
            {"order_status.Processed", "[[[Processed]]]" },
            {"order_status.OutForDelivery", "[[[OutForDelivery]]]" },
            {"order_status.Delivered", "[[[Delivered]]]" },
            {"order_status.CancelledByConsumer", "[[[CancelledByConsumer]]]" },
            {"order_status.CancelledBySystem", "[[[CancelledBySystem]]]" },
            {"order_status.CancelledBySuperUser", "[[[CancelledBySuperUser]]]" },
            {"order_status.CancelledByHQSuperUser", "[[[CancelledByHQSuperUser]]]" },
            {"order_status.Processedbutnotdelivered", "[[[Processed but not delivered]]]" },
            {"delivery_status.Assigned", "[[[Assigned]]]" },
            {"delivery_status.OutForDelivery", "[[[OutForDelivery]]]" },
            {"delivery_status.Cancelled", "[[[Cancelled]]]" },
            {"delivery_status.OnTimeDelivery", "[[[OnTimeDelivery]]]" },
            {"delivery_status.LateDelivery", "[[[LateDelivery]]]" },
            {"delivery_status.EarlyDelivery", "[[[EarlyDelivery]]]" },
            { "", ""},


        };
        /// <summary>
        /// Get the message to display for the given key.
        /// </summary>
        /// <param name="key">The key to the message.</param>
        /// <returns>The display text for the key. If the key is not found, returns "!".</returns>
        public static string GetMessage(string key)
        {
            if (_Messages.ContainsKey(key))
            {
                return _Messages[key];
            }
            return "!";
        }
        //public static string GetStandardMobileNumber(string mobileNumber)
        //{
        //    if (mobileNumber.StartsWith("+62"))
        //        return mobileNumber;
        //    else
        //        return mobileNumber.Insert(0, "+62");
        //}

        public static string ToStandardPhone(this string mobileNumber)
        {
            if (mobileNumber.StartsWith("+62"))
                return mobileNumber;
            else
                return mobileNumber.Insert(0, "+62");
        }
        public static string ToTrimCode(this string mobileNumber)
        {
            if (mobileNumber.StartsWith("+62"))
                return mobileNumber?.Replace("+62", "");
            else
                return mobileNumber;
        }
    }
}
