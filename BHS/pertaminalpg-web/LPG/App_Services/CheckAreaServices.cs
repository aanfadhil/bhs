﻿using LPG.App_Classes;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using Microsoft.ApplicationInsights;


namespace LPG.App_Services
{
    public class CheckAreaServices : ServiceBase, ICheckAreaServices
    {

        public CheckAreaServices(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {

        }


        public List<CheckAreaModel> GetFilteredAddress(CheckAreaFilter request)
        {
            var result = context.CheckAreaFilter(request.consumer_id, request.region_id).Select(t => new CheckAreaModel()
            {
                addr_id = t.AddrID,
                consumer_id = t.ConsID,
                consumer_name = t.Name,
                mobile_number = t.PhoneNumber,
                consumer_addr_id = t.AddrID,
                latitude = t.Latitude,
                longitude = t.Longitude,
                address = t.Address,
                postal_code = t.PostalCode,
                is_default = t.IsDefault,
                region = t.RegionName,
                city_id = t.CityID,
                more_info = t.AdditionalInfo
            }).ToList();

            return result;
        }
    }
}