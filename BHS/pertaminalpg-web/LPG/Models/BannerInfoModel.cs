﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LPG.Models
{
    public class BannerInfoModel
    {
        public int BannerInfoId { get; set; }
        //[Required]
        [Required(ErrorMessage = "Image required")]
        public string BannerImage { get; set; }
        [Required]
        public string Caption { get; set; }
        [Required]
        public int Position { get; set; }

        public string SyaratKetentuan { get; set; }

        public int Status { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public List<BannerInfoModel> BannerInfoList { get; set; }

        public string UpdatedUser { get; set; }

        public string ErrorMessage { get; set; }

        public bool IsBannerInfoExists { get; set; }
        public List<int> Positions { get; set; }

        public PengaturanPromoModel PengaturanPromo { get; set; }
    }
}