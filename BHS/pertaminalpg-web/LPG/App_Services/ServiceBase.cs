﻿using LPG.App_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LPG.Models;
using Microsoft.ApplicationInsights;

namespace LPG.App_Services
{
    public abstract class ServiceBase : AbstractAppService, IBaseAppService, IServiceBase
    {
        public ServiceBase(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public int? UserId { get { return new SessionManagement().getAdminSession()?.UserId; } }

    }
}
