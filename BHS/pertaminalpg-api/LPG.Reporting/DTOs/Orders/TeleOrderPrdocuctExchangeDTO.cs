﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.Orders
{
    public class TeleOrderPrdocuctExchangeDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int TelOrdPrdExID { get; set; }
        public int TeleOrdID { get; set; }
        public int ProdID { get; set; }
        public string ExchangeWith { get; set; }
        public int ExchangeQuantity { get; set; }
        public decimal ExchangePrice { get; set; }
        public decimal ExchangePromoPrice { get; set; }
        public bool StatusId { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TotalAmount { get; set; }
    }

    public class TeleOrderPrdocuctExchangeMapper : MapperBase<TeleOrderPrdocuctExchange, TeleOrderPrdocuctExchangeDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<TeleOrderPrdocuctExchange, TeleOrderPrdocuctExchangeDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<TeleOrderPrdocuctExchange, TeleOrderPrdocuctExchangeDTO>>) (p => new TeleOrderPrdocuctExchangeDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    TelOrdPrdExID = p.TelOrdPrdExID,
                    TeleOrdID = p.TeleOrdID,
                    ProdID = p.ProdID,
                    ExchangeWith = p.ExchangeWith,
                    ExchangeQuantity = p.ExchangeQuantity,
                    ExchangePrice = p.ExchangePrice,
                    ExchangePromoPrice = p.ExchangePromoPrice,
                    StatusId = p.StatusId,
                    CreatedDate = p.CreatedDate,
                    SubTotal = p.SubTotal,
                    TotalAmount = p.TotalAmount
                }));
            }
        }

        public override void MapToModel(TeleOrderPrdocuctExchangeDTO dto, TeleOrderPrdocuctExchange model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.TelOrdPrdExID = dto.TelOrdPrdExID;
            model.TeleOrdID = dto.TeleOrdID;
            model.ProdID = dto.ProdID;
            model.ExchangeWith = dto.ExchangeWith;
            model.ExchangeQuantity = dto.ExchangeQuantity;
            model.ExchangePrice = dto.ExchangePrice;
            model.ExchangePromoPrice = dto.ExchangePromoPrice;
            model.StatusId = dto.StatusId;
            model.CreatedDate = dto.CreatedDate;
            model.SubTotal = dto.SubTotal;
            model.TotalAmount = dto.TotalAmount;

        }
    }
}
