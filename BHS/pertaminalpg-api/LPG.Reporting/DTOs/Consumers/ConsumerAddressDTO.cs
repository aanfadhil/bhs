﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using LPG.Reporting.DTOs.Orders;

namespace LPG.Reporting.DTOs.Consumers
{
    public class ConsumerAddressDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int AddrID { get; set; }
        public int ConsID { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string AdditionalInfo { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool IsDefault { get; set; }
        public int StatusID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string RegionName { get; set; }
        public IEnumerable<OrderDTO> Orders { get; set; }
    }

    public class ConsumerAddressMapper : MapperBase<ConsumerAddress, ConsumerAddressDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private OrderMapper _orderMapper = new OrderMapper();
        public override Expression<Func<ConsumerAddress, ConsumerAddressDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<ConsumerAddress, ConsumerAddressDTO>>) (p => new ConsumerAddressDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    AddrID = p.AddrID,
                    ConsID = p.ConsID,
                    Address = p.Address,
                    PostalCode = p.PostalCode,
                    AdditionalInfo = p.AdditionalInfo,
                    Latitude = p.Latitude,
                    Longitude = p.Longitude,
                    IsDefault = p.IsDefault,
                    StatusID = p.StatusID,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate,
                    RegionName = p.RegionName,
                    Orders = p.Orders.AsQueryable().Select(this._orderMapper.SelectorExpression)
                }));
            }
        }

        public override void MapToModel(ConsumerAddressDTO dto, ConsumerAddress model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.AddrID = dto.AddrID;
            model.ConsID = dto.ConsID;
            model.Address = dto.Address;
            model.PostalCode = dto.PostalCode;
            model.AdditionalInfo = dto.AdditionalInfo;
            model.Latitude = dto.Latitude;
            model.Longitude = dto.Longitude;
            model.IsDefault = dto.IsDefault;
            model.StatusID = dto.StatusID;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;
            model.RegionName = dto.RegionName;

        }
    }
}
