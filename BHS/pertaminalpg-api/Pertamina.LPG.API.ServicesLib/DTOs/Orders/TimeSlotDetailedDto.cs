﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pertamina.LPG.API.DTOs.Orders
{
    public class TimeSlotDetailedDto
    {
        public short SlotID { get; set; }
        public string SlotName { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTine { get; set; }
        public bool StatusId { get; set; }

        //public virtual ICollection<Order> Orders { get; set; }
        //public virtual ICollection<TeleOrder> TeleOrders { get; set; }



        //// override object.Equals
        //public override bool Equals(object obj)
        //{
        //    if (obj == null || GetType() != obj.GetType())
        //    {
        //        return false;
        //    }
        //    TimeSlotDto other = (TimeSlotDto)obj;
        //    if (this.time_slot_id != other.time_slot_id)
        //    {
        //        return false;
        //    }
        //    if (this.date != other.date)
        //    {
        //        return false;
        //    }
        //    return true;
        //}

        //// override object.GetHashCode
        //public override int GetHashCode()
        //{
        //    int hash = 29;
        //    hash = hash * this.time_slot_id * 7;
        //    if (this.date != null)
        //    {
        //        hash = hash * this.date.GetHashCode() * 11;
        //    }
        //    return hash;
        //}
    }
}