namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MRegion")]
    public partial class MRegion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MRegion()
        {
            Agencies = new HashSet<Agency>();
        }

        [Key]
        public int RegionID { get; set; }

        [Required]
        [StringLength(200)]
        public string RegionName { get; set; }

        [Required]
        [StringLength(10)]
        public string RegionCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Agency> Agencies { get; set; }
    }
}
