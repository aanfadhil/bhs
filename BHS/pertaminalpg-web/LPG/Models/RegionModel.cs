﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class RegionModel
    {
        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public string RegionCode { get; set; }
        public string RegionCodeName { get; set; }
    }
}