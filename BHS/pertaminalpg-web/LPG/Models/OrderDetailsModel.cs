﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class OrderDetailsModel
    {
        public int OrderId { get; set; }
        public String ProductName { get; set; }
        public decimal Amount { get; set; }
        public decimal UNitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal PromoPrice { get; set; }
        public decimal PostageFee { get; set; }
        public decimal PromoPostage { get; set; }
    }
}