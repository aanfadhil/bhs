﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LPG.Models
{
    public class CompanyModel
    {
        public int CompanyId{ get; set; }
        [Required]
        public string Soldtono { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string Region { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public bool IsCompanyExists { get; set; }
        public List<CompanyModel> CompanyList { get; set; }

        public int RegionId { get; set; }
        public List<SelectListItem> RegionList { get; set; }
        public string errorMessage { get; set; }
    }
}