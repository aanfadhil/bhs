﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using LPG.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using ClosedXML.Excel;
using System.Globalization;
using i18n;


namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class CheckAreaController : AbstractController<ICheckAreaServices>
    {
        SessionManagement sessionManagement;
        private IRegionServices RegionServices;
        private ICustomerService CustomerService;
        private IDistributionService DistributionService;
        private ISuperAdminService SuperAdminService;

        private const string SESSION_TIME_OUT = "SESSION_TIMEOUT";

        public CheckAreaController(
            ICheckAreaServices _serviceObject,
            IRegionServices _regionServices,
            IProductService _productService,
            ICustomerService _customerService,
            IDistributionService _distributionService,
            ISuperAdminService _superAdminService
            ) : base(_serviceObject)
        {
            this.RegionServices = _regionServices;
            this.CustomerService = _customerService;
            this.DistributionService = _distributionService;
            this.SuperAdminService = _superAdminService;
        }

        // GET: CheckArea
        public ActionResult CheckArea()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var vm = new CheckAreaViewModel();
                    vm.Regions = RegionServices.GetList();
                    vm.DefaultRegion = SuperAdminService.GetSuperAdminDefaultRegion(adminSessionInfo.UserId);

                    return View(vm);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products List" });
            }

            return View();
        }
        public ActionResult Index()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var vm = new CheckAreaViewModel();
                    vm.Regions = RegionServices.GetList();
                    vm.DefaultRegion = SuperAdminService.GetSuperAdminDefaultRegion(adminSessionInfo.UserId);

                    return View(vm);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products List" });
            }

            return View();
        }

        [HttpPost]
        public ActionResult GetCustomer(string name)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    name = name.ToLower();
                    var customer = CustomerService.ListCustomers().Where(t => t.Name.ToLower().Contains(name) || t.MobileNo.ToLower().Contains(name)).OrderBy(o => o.Name).ToList();
                    return Json(customer);
                }
                else
                {
                    return Json(SESSION_TIME_OUT);
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult GetAddresses(CheckAreaFilter request)
        {
            try
            {
                var result = serviceObject.GetFilteredAddress(request);
                return Json(result);
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(ex);
            }
        }

        [HttpPost]
        public ActionResult GetDistributionPoints()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var result = DistributionService.ListDistribution();
                    return Json(result);
                }
                else
                {
                    return Json(SESSION_TIME_OUT);
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(null);
            }
        }
    }
}