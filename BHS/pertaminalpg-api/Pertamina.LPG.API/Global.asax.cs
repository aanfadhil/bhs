﻿using Pertamina.LPG.API.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Pertamina.LPG.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            I18NConfig.InitializeLocalization("id");
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //GlobalConfiguration.Configure(Startup.ConfigureHandfire);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);


        }

        /// <summary>
        /// Handles the ReleaseRequestState event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Application_ReleaseRequestState(object sender, EventArgs e)
        {
            //HttpContextBase context = Request.GetOwinContext().Get<HttpContextBase>(typeof(HttpContextBase).FullName);
            //i18n.LocalizedApplication.InstallResponseFilter(context);
        }
    }
}
