﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPG.App_Services
{
    public interface IBaseAppService
    {
        int? UserId { get; }
    }
}
