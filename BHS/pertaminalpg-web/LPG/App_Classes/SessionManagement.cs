﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Helpers;
using System.Threading;
using System.Globalization;
using LPG.Models;
using LPG.App_Classes;

namespace LPG.App_Classes
{

    public class SessionManagement : ISessionManagement
    {

        #region RememberMeSession

        public void setRememberMe(LoginModel adminSessionInfo)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            string adminsession_json = serializer.Serialize(adminSessionInfo).EncryptString();

            var adminCookie = new HttpCookie("RememberMeCookie", adminsession_json);
            adminCookie.Expires = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").AddDays(7);
            HttpContext.Current.Response.Cookies.Add(adminCookie);

        }

        public LoginModel getRememberMe()
        {
            LoginModel adminSessionInfo = null;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            var httpCookie = HttpContext.Current.Request.Cookies["RememberMeCookie"];
            if (httpCookie != null)
            {
                adminSessionInfo = new LoginModel();

                string adminsession_json = httpCookie.Value.DecryptString();
                adminSessionInfo = serializer.Deserialize<LoginModel>(adminsession_json);
            }
            return adminSessionInfo;
        }


        #endregion

        //#region LanguageSession


        //public void setLanguageSession(LanguageEnum enumLanguage)
        //{
        //    try
        //    {
        //        HttpContext.Current.Session["Language"] = enumLanguage;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //}

        //public LanguageEnum getLanguageSession()
        //{
        //    try
        //    {
        //        if (HttpContext.Current.Session["Language"] == null)
        //        {
        //            HttpContext.Current.Session["Language"] = LanguageEnum.English;
        //        }
        //        LanguageEnum enumLanguage = (LanguageEnum)HttpContext.Current.Session["Language"];

        //        return enumLanguage;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //public static LanguageEnum getlanguage_Static()
        //{
        //    try
        //    {

        //        LanguageEnum enumLanguage = (LanguageEnum)HttpContext.Current.Session["Language"];

        //        return enumLanguage;
        //    }
        //    catch (Exception)
        //    {
        //        return LanguageEnum.English;
        //    }
        //}

        //#endregion

        #region AdminSession

        public void setAdminSession(AdminSessionEntity adminSessionInfo)
        {
            try
            {
                HttpContext.Current.Session["AdminSession"] = adminSessionInfo;

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                
                string adminsession_json = serializer.Serialize(adminSessionInfo).EncryptString();

                var adminCookie = new HttpCookie("bhssession", adminsession_json);
                adminCookie.Expires = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").AddDays(7);
                HttpContext.Current.Response.Cookies.Add(adminCookie);

                //AdminSessionCookie admcookie = new AdminSessionCookie();
                //admcookie.UserId = adminSessionInfo.UserId;

                //HttpCookie cookie = new HttpCookie("adms");


            }
            catch (Exception)
            {

                throw;
            }

        }

        public AdminSessionEntity getAdminSession()
        {
            //try
            //{
            AdminSessionEntity adminSessionInfo = null;

            adminSessionInfo = (AdminSessionEntity) HttpContext.Current.Session["AdminSession"];

            return adminSessionInfo;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
        }


        #endregion

    }

}
