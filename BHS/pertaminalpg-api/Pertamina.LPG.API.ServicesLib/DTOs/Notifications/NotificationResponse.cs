﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Notifications
{
    public class NotificationResponse
    {
        public int? recordsTotal { get; set; }
        public int? recordsFiltered { get; set; }
        public List<NotificationDto> data { get; set; }
    }
}
