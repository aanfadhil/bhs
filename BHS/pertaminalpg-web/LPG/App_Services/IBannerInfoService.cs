﻿using System.Collections.Generic;
using System.Web.Mvc;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IBannerInfoService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        bool deleteBannerInfo(int id, int loggedBy);
        List<SelectListItem> GetAvailablePositions(int? InfoID);
        BannerInfoModel getBannerInfoDetails(int bannerInfoId);
        List<BannerInfoModel> ListBannerInfo();
        BannerInfoModel saveBannerDetails(BannerInfoModel entity, string Cmd, int loggedBy);
    }
}