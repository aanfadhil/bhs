﻿using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace LPG.App_Services
{
    public class DriverService : ServiceBase, IDriverService
    {
        public DriverService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<DriverModel> ListDriver()
        {
            var driverList = new List<DriverModel>();
            //using (var context = new lpgdevEntities())
            //{
            var data = from drivers in context.Drivers
                       join distributionpoin in context.DistributionPoints
                       on drivers.DbptID equals distributionpoin.DbptID
                       join company in context.Agencies
                       on drivers.AgenID equals company.AgenID
                       
                       where drivers.StatusId == true
                       orderby drivers.DrvrID descending, drivers.CreatedDate descending
                       select new DriverModel
                       {
                           DriverId = drivers.DrvrID,
                           DriverName = drivers.DriverName,
                           AgenDriverAgenId = company.AgenID,
                           AgenDriverDistId = distributionpoin.DbptID,
                           DistributionPoint = distributionpoin.DistributionPointName,
                           Company = company.AgencyName,
                           MobilePhone = drivers.MobileNumber,
                           CityId = distributionpoin.CityID,
                           CityName = distributionpoin.MCity == null ? null : distributionpoin.MCity.CityName

                       };
            driverList = data.ToList();
            //}

            return driverList;
        }

        public DriverModel getDriverDetails(int driverId)
        {
            DriverModel rtns = new DriverModel();
            //using (var context = new lpgdevEntities())
            //{
            rtns = (from drivers in context.Drivers

                    join distributionpoin in context.DistributionPoints
                    on drivers.DbptID equals distributionpoin.DbptID

                    join company in context.Agencies
                    on drivers.AgenID equals company.AgenID

                    where drivers.DrvrID == driverId

                    select new DriverModel()
                    {
                        DriverId = drivers.DrvrID,
                        DriverName = drivers.DriverName,
                        //DistributionPoint = distributionpoin.DistributionPointName,   
                        AgenDriverAgenId = company.AgenID,
                        AgenDriverDistId = distributionpoin.DbptID,
                        Company = company.AgencyName,
                        MobilePhone = drivers.MobileNumber


                    }).FirstOrDefault();
            rtns.MobilePhone = rtns.MobilePhone?.ToTrimCode();
            return rtns;
            //}
        }

        public DriverModel saveDriverDetails(DriverModel entity, int loggedBy)
        {

            try
            {

                //using (var context = new lpgdevEntities())
                //{

                var Data = context.Drivers.Where(r => r.DrvrID == entity.DriverId).FirstOrDefault();
                if (Data != null)
                {

                    Data.StatusId = true;
                    Data.UpdatedBy = base.UserId.ToShort();
                    Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    Data.DriverName = entity.DriverName;
                    Data.AgenID = entity.AgenDriverAgenId;
                    Data.DbptID = entity.AgenDriverDistId;
                    //Data.MobileNumber = MessageSource.GetStandardMobileNumber(entity.MobilePhone);
                    Data.MobileNumber = entity.CountryCode.ToDefaultString() + entity.MobilePhone;
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggedBy);


                }
                else
                {
                    Driver objLocal = new Driver();

                    objLocal.DrvrID = entity.DriverId;
                    objLocal.CreatedBy = base.UserId.ToShort();
                    objLocal.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    objLocal.UpdatedBy = base.UserId.ToShort();
                    objLocal.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    objLocal.DriverName = entity.DriverName;
                    objLocal.MobileNumber = entity.CountryCode.ToDefaultString() + entity.MobilePhone;
                    objLocal.MobileNumber = entity.MobilePhone;
                    Data.AgenID = entity.AgenDriverAgenId;
                    Data.DbptID = entity.AgenDriverDistId;

                    context.Drivers.Add(objLocal);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggedBy);
                }
                //}
                return entity;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool DeleteDriver(int Id, int loggedBy)
        {
            try
            {
                //using (var context = new lpgdevEntities())
                //{
                var Data = context.Drivers.Where(r => r.DrvrID == Id).FirstOrDefault();
                if (Data != null)
                {
                    Data.StatusId = false;
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", loggedBy);
                    return true;
                }
                return false;
                //}
            }
            catch (Exception)
            {
                throw;
            }

        }


        public bool IsExists(int id)
        {
            //using (var context = new lpgdevEntities())
            //{
            return context.Drivers.Where(c => c.DrvrID == id).Any();
            //}

        }
        public bool IsExistsMobileNo(string mobileNo, int userId)
        {

            //using (var context = new lpgdevEntities())
            //{
            if (userId > 0)
                return context.Drivers.Where(c => c.MobileNumber == mobileNo && c.StatusId == true && c.DrvrID != userId).Any();
            else
                return context.Drivers.Where(c => c.MobileNumber == mobileNo && c.StatusId == true).Any();

            //}

        }
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel
            {
                //ActivityService _activityService = new ActivityService();

                Activity = action,
                ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"),
                ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay,
                CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"),
                UserType = loggedBy,
                Item = "Driver"
            };
            _activityService.AddActivityLog(actModel, "Driver");
        }
    }
}
