﻿using Pertamina.LPG.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DAOs
{
    public class DetailPriceDao : DAO
    {
        public List<DetailPrice> GetProducts(int pageNumber, int rowsPerPage, int cityID, string cityCode, string cityName)
        {
            //page number starts with 0, as requested by mobile UI team
            return _context.DetailPrices.Where(x => x.Product.StatusId && x.Product.Published && ((x.CityID == cityID) || (x.MCity.CityCode == cityCode) || (x.MCity.CityName == (cityName??"").Trim().ToUpper()))).OrderBy(p => p.Product.Position).Skip(pageNumber * rowsPerPage).Take(rowsPerPage).ToList();
        }

        public Reminder GetRemindersForProducts()
        {
            var reminders = _context.Reminders.Where(x => !x.UserType && x.StatusId).OrderByDescending(x => x.UpdatedDate).ToList();
            if (reminders != null)
            {
                return reminders.FirstOrDefault();
            }
            return null;
        }

        public int GetTotalCount(int cityID, string cityCode, string cityName)
        {
            return _context.DetailPrices.Where(x => x.Product.StatusId && x.Product.Published && ((x.CityID == cityID) || (x.MCity.CityCode == cityCode) || (x.MCity.CityName == cityName))).Count();
        }
    }
}
