﻿using GBG.Shared.Exceptions;
using LPG.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPG
{
    internal static class LogHelper
    {
        static string instanceName = "LPGWEB";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="traceType"></param>
        internal static void WriteLog(string message, TraceEventType traceType = TraceEventType.Information)
        {
            ExceptionHandler.PublishLog(string.Empty, message, instanceName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="additionalInfo"></param>
        internal static void PublishException(Exception ex, string additionalInfo = "")
        {
            BaseException bex = (ex is BaseException) ? (BaseException)ex : new UnknownException(ex);
            ExceptionHandler.PublishException(instanceName, bex, additionalInfo, ExceptionPublishMode.File);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="additionalInfo"></param>
        /// <param name="publishModes"></param>
        internal static void PublishException(Exception ex, string additionalInfo, params ExceptionPublishMode[] publishModes)
        {
            BaseException bex = (ex is BaseException) ? (BaseException)ex : new UnknownException(ex);
            ExceptionHandler.PublishException(instanceName, bex, additionalInfo, publishModes);
        }

    }
}
