﻿using LPG.App_Classes;
using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace LPG.App_Services
{

    public class UserService : ServiceBase, IUserService
    {
        private IAgencyService _agencyService;
        //private ActivityService _activityService;

        public UserService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry, IAgencyService agencyService) : base(context, activityService, _telemetry)
        {
            _agencyService = agencyService;
            _activityService = activityService;
        }

        public UserRegistrtaionModel saveAgency(UserRegistrtaionModel entity)
        {
            try
            {
                //using (var context = new lpgdevEntities())
                //{
                var Data = context.Agencies.Where(r => r.AgenID == entity.AgencyId).FirstOrDefault();
                if (Data != null)
                {
                    Data.SoldToNumber = entity.SoldTono;
                    Data.AgencyName = entity.AgencyName;
                    Data.RegionId = entity.RegionId;
                    Data.StatusId = true;
                    Data.Region = "";
                    Data.UpdatedBy = base.UserId.ToShort();
                    Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "Agen Perusahaan", base.UserId.Value);
                }
                else
                {
                    int agencyId = context.Agencies.Max(d => d.AgenID);

                    Agency objLocal = new Agency
                    {
                        AgenID = agencyId,
                        SoldToNumber = entity.SoldTono,
                        AgencyName = entity.AgencyName,
                        RegionId = entity.RegionId,
                        Region = "",
                        StatusId = true,

                        CreatedBy = base.UserId.ToShort(),
                        CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"),

                        UpdatedBy = base.UserId.ToShort(),
                        UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time")
                    };

                    context.Agencies.Add(objLocal);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "Agen Perusahaan", base.UserId.Value);
                }
                //}
                return entity;
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                throw;
            }
        }

        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();
            // ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = action_item;
            _activityService.AddActivityLog(actModel, action_item);
        }

        public UserRegistrtaionModel saveDistribution(UserRegistrtaionModel entity)
        {
            try
            {
                entity.IsDistributionExixts = false;
                //using (var context = new lpgdevEntities())
                //{
                if (entity.AgenDistId > 0)
                {
                    var DataExists = (from distribution in context.DistributionPoints
                                      where distribution.DbptID != entity.AgenDistId &&
                                      distribution.AgenID == entity.AgenDistAgencyId
                                      select distribution
                                     ).FirstOrDefault();

                    if (DataExists != null)
                    {
                        entity.IsDistributionExixts = true;

                        entity.ErrorMessage = "Distribution Already Exists";
                        return entity;
                    }

                }
                else
                {
                    var DataExists = (from distribution in context.DistributionPoints
                                      where distribution.DistributionPointName == entity.AgenDistributionName
                                      && distribution.AgenID == entity.AgenDistAgencyId
                                      select distribution
                                      ).FirstOrDefault();

                    if (DataExists != null)
                    {
                        entity.IsDistributionExixts = true;
                        entity.ErrorMessage = "Distribution Name Already Exists";
                        return entity;
                    }
                }

                var Data = context.DistributionPoints.Where(r => r.DbptID == entity.AgenDistId).FirstOrDefault();
                if (Data != null)
                {
                    Data.AgenID = entity.AgenDistAgencyId;
                    Data.DistributionPointName = entity.AgenDistributionName;
                    Data.Latitude = entity.Lat;
                    Data.Longitude = entity.Long;
                    Data.Address = entity.AgenAddress;
                    Data.StatusId = true;

                    var city = context.MCities.Where(t => t.CityName.ToUpper().Replace("kota", "").Replace("kabupaten", "").Replace("city", "").Trim() == entity.DistCityName.ToUpper().Replace("kota", "").Replace("kabupaten", "").Replace("city", "").Trim()).FirstOrDefault();

                    if (city != null)
                    {
                        Data.CityID = city.ID;
                    }

                    Data.UpdatedBy = base.UserId.ToShort();
                    Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "Titik Distribusi", base.UserId.Value);
                }
                else
                {
                    int distId = context.DistributionPoints.Max(d => d.DbptID);

                    DistributionPoint objDist = new DistributionPoint();

                    objDist.DbptID = distId;
                    objDist.AgenID = entity.AgenDistAgencyId;
                    objDist.DistributionPointName = entity.AgenDistributionName;
                    objDist.Latitude = entity.Lat;
                    objDist.Longitude = entity.Long;
                    objDist.Address = entity.AgenAddress;
                    objDist.StatusId = true;

                    objDist.UpdatedBy = base.UserId.ToShort();
                    objDist.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                    objDist.CreatedBy = base.UserId.ToShort();
                    objDist.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                    var city = context.MCities.Where(t => t.CityName.ToUpper().Replace("kota", "").Replace("kabupaten", "").Replace("city", "").Trim() == entity.DistCityName.ToUpper().Replace("kota", "").Replace("kabupaten", "").Replace("city", "").Trim()).FirstOrDefault();

                    if (city != null)
                    {
                        Data.CityID = city.ID;
                    }

                    context.DistributionPoints.Add(objDist);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "Titik Distribusi", base.UserId.Value);
                }
                //}
                return entity;
            }
            catch (Exception ex)
            {
                string message = ex.Message.ToString();
                throw;
            }
        }
        public UserRegistrtaionModel saveAgentBoss(UserRegistrtaionModel entity)
        {
            try
            {


                entity.IsAgentBossExists = false;
                //using (var context = new lpgdevEntities())
                //{

                if (entity.AgencyBossId > 0)
                {
                    var DataExists = (from agentBoss in context.AgentBosses
                                      where agentBoss.AbosID != entity.AgencyBossId &&
                                      agentBoss.AgenID == entity.ABAgencyId
                                      select agentBoss
                                     ).FirstOrDefault();

                    if (DataExists != null)
                    {
                        entity.IsAgentBossExists = true;

                        entity.ErrorMessage = "Agent Already Exists";
                        return entity;
                    }

                }
                else
                {
                    var DataExists = (from agentBoss in context.AgentBosses
                                      where agentBoss.OwnerName == entity.AgencyOwnername
                                      && agentBoss.AgenID == entity.ABAgencyId
                                      select agentBoss
                                      ).FirstOrDefault();

                    if (DataExists != null)
                    {
                        entity.IsAgentBossExists = true;
                        entity.ErrorMessage = "Agent Name Already Exists";
                        return entity;
                    }
                }

                var Data = context.AgentBosses.Where(r => r.AbosID == entity.AgenDistId).FirstOrDefault();
                if (Data != null)
                {
                    Data.AgenID = entity.ABAgencyId;
                    Data.OwnerName = entity.AgencyOwnername;
                    Data.ProfileImage = entity.AgencyBossImage;
                    Data.StatusId = true;
                    Data.UpdatedBy = base.UserId.ToShort();
                    Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    Data.Email = entity.AgentBossEmail;

                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "Agen Bos", base.UserId.Value);
                }
                else
                {
                    int bossId = context.AgentBosses.Max(d => d.AbosID);

                    AgentBoss objDist = new AgentBoss();

                    objDist.AbosID = bossId;
                    objDist.AgenID = entity.ABAgencyId;
                    objDist.OwnerName = entity.AgencyOwnername;
                    objDist.MobileNumber = entity.CountryCode.ToDefaultString() + entity.AgencyBossMobile;

                    objDist.UpdatedBy = base.UserId.ToShort();
                    objDist.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                    objDist.CreatedBy = base.UserId.ToShort();
                    objDist.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    objDist.StatusId = true;
                    objDist.Email = entity.AgentBossEmail;
                    context.AgentBosses.Add(objDist);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "Agen Bos", base.UserId.Value);

                    try
                    {
                        if (!string.IsNullOrEmpty(entity.AgencyBossMobile))
                        {
                            string newPassword = SMSService.SendOTP(entity.AgencyBossMobile);
                            if (newPassword != "")
                            {
                                var updateData = context.AgentBosses.Where(r => r.AbosID == objDist.AbosID).FirstOrDefault();
                                if (updateData != null)
                                {
                                    updateData.Password = TokenGenerator.GetHashedPassword(newPassword, 49);
                                    context.SaveChanges();
                                }
                            }
                        }
                    }
                    catch { }
                }
                //}
                return entity;
            }
            catch (Exception ex)
            {
                string message = ex.Message.ToString();
                throw;
            }
        }

        public UserRegistrtaionModel saveAgentAdmin(UserRegistrtaionModel entity)
        {
            try
            {


                entity.IsAgentAdminExists = false;
                //using (var context = new lpgdevEntities())
                //{

                if (entity.AgenAdminId > 0)
                {
                    var DataExists = (from agentadmin in context.AgentAdmins
                                      where agentadmin.AgadmID != entity.AgenAdminId &&
                                      agentadmin.AgenID == entity.AgenAdAgenId &&
                                      agentadmin.DbptID == entity.AgentDistId
                                      select agentadmin
                                     ).FirstOrDefault();

                    if (DataExists != null)
                    {
                        entity.IsAgentAdminExists = true;

                        entity.ErrorMessage = "Agent admin Already Exists";
                        return entity;
                    }

                }
                else
                {
                    var DataExists = (from agentadmin in context.AgentAdmins
                                      where agentadmin.AgentAdminName == entity.AgenAdminName
                                      && agentadmin.AgenID == entity.AgenAdAgenId &&
                                      agentadmin.DbptID == entity.AgentDistId
                                      select agentadmin
                                      ).FirstOrDefault();

                    if (DataExists != null)
                    {
                        entity.IsAgentAdminExists = true;
                        entity.ErrorMessage = "Agent Name Already Exists";
                        return entity;
                    }
                }

                var Data = context.AgentAdmins.Where(r => r.AgadmID == entity.AgenAdminId).FirstOrDefault();
                if (Data != null)
                {
                    Data.AgenID = entity.AgenAdAgenId;
                    Data.DbptID = entity.AgentDistId;
                    Data.AgentAdminName = entity.AgenAdminName;
                    Data.MobileNumber = entity.CountryCode.ToDefaultString() + entity.AgnAdminMObile;
                    Data.LastLogin = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    Data.StatusId = true;
                    Data.email = entity.AgentAdminEmail;
                    Data.UpdatedBy = base.UserId.ToShort();
                    Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "Agen Admin", base.UserId.Value);
                }
                else
                {
                    int agentAdId = context.AgentAdmins.Max(d => d.AgadmID);

                    AgentAdmin objAdmin = new AgentAdmin();

                    objAdmin.AgadmID = agentAdId;
                    objAdmin.AgenID = entity.AgenAdAgenId;
                    objAdmin.DbptID = entity.AgentDistId;
                    objAdmin.AgentAdminName = entity.AgenAdminName;
                    objAdmin.MobileNumber = entity.CountryCode.ToDefaultString() + entity.AgnAdminMObile;
                    objAdmin.StatusId = true;
                    objAdmin.email = entity.AgentAdminEmail;
                    objAdmin.UpdatedBy = base.UserId.ToShort();
                    objAdmin.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                    objAdmin.CreatedBy = base.UserId.ToShort();
                    objAdmin.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");


                    context.AgentAdmins.Add(objAdmin);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "Agen Admin", base.UserId.Value);

                    try
                    {
                        if (!string.IsNullOrEmpty(entity.AgnAdminMObile))
                        {
                            string newPassword = SMSService.SendOTP(entity.AgnAdminMObile);
                            if (newPassword != "")
                            {
                                var updateData = context.AgentAdmins.Where(r => r.AgadmID == objAdmin.AgadmID).FirstOrDefault();
                                if (updateData != null)
                                {
                                    updateData.Password = TokenGenerator.GetHashedPassword(newPassword, 49);
                                    context.SaveChanges();
                                }
                            }
                        }
                    }
                    catch { }
                }
                //}
                return entity;
            }
            catch (Exception ex)
            {
                string message = ex.Message.ToString();
                throw;
            }

        }

        public UserRegistrtaionModel saveDriver(UserRegistrtaionModel entity)
        {
            try
            {
                entity.IsAgentDriverExixts = false;
                //using (var context = new lpgdevEntities())
                //{

                //if (entity.AgenAdminId > 0)
                //{
                //    var DataExists = (from driver in context.Drivers
                //                      where driver.DrvrID != entity.AgendriverId &&
                //                      driver.AgenID == entity.AgenDriverAgenId &&
                //                      driver.DbptID == entity.AgenDriverDistId &&
                //                      driver.StatusId == true
                //                      select driver
                //                     ).FirstOrDefault();

                //    if (DataExists != null)
                //    {
                //        entity.IsAgentDriverExixts = true;

                //        entity.ErrorMessage = "Driver Already Exists";
                //        return entity;
                //    }

                //}
                //else
                //{
                //    var DataExists = (from driver in context.Drivers
                //                      where driver.DriverName == entity.AgenDriverName
                //                      && driver.AgenID == entity.AgenDriverAgenId &&
                //                      driver.DbptID == entity.AgenDriverDistId &&
                //                      driver.StatusId == true
                //                      select driver
                //                      ).FirstOrDefault();

                //    if (DataExists != null)
                //    {
                //        entity.IsAgentDriverExixts = true;
                //        entity.ErrorMessage = "Agent driver Already Exists";
                //        return entity;
                //    }
                //}



                var Driver_Agency_Dist = (from driver in context.Drivers
                                          where driver.AgenID == entity.AgenDriverAgenId &&
                                          driver.DbptID == entity.AgenDriverDistId &&
                                          driver.StatusId == true
                                          select driver
                                      ).Count();

                if (Driver_Agency_Dist >= 0)
                {

                    var Data = context.Drivers.Where(r => r.DrvrID == entity.AgendriverId).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.AgenID = entity.AgenDriverAgenId;
                        Data.DbptID = entity.AgenDriverDistId;
                        Data.DriverName = entity.AgenDriverName;
                        Data.MobileNumber = entity.CountryCode.ToDefaultString() + entity.AgenDriverMobileNo;
                        Data.StatusId = true;

                        Data.UpdatedBy = base.UserId.ToShort();
                        Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "Agen Driver", base.UserId.Value);
                    }
                    else
                    {
                        int driverId = context.Drivers.Max(d => d.DrvrID);

                        Driver objdriver = new Driver();

                        objdriver.DrvrID = driverId;
                        objdriver.AgenID = entity.AgenDriverAgenId;
                        objdriver.DbptID = entity.AgenDriverDistId;
                        objdriver.DriverName = entity.AgenDriverName;
                        objdriver.MobileNumber = entity.CountryCode.ToDefaultString() + entity.AgenDriverMobileNo;
                        objdriver.StatusId = true;

                        objdriver.UpdatedBy = base.UserId.ToShort();
                        objdriver.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                        objdriver.CreatedBy = base.UserId.ToShort();
                        objdriver.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");


                        context.Drivers.Add(objdriver);
                        context.SaveChanges();

                        try
                        {
                            if (!string.IsNullOrEmpty(entity.AgenDriverMobileNo))
                            {
                                string newPassword = SMSService.SendOTP(entity.AgenDriverMobileNo);
                                if (newPassword != "")
                                {
                                    var updateData = context.Drivers.Where(r => r.DrvrID == objdriver.DrvrID).FirstOrDefault();
                                    if (updateData != null)
                                    {
                                        updateData.Password = TokenGenerator.GetHashedPassword(newPassword, 49);
                                        context.SaveChanges();
                                        CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "Agen Driver", base.UserId.Value);
                                    }
                                }
                            }
                        }
                        catch { }
                    }
                }
                else
                {
                    entity.IsAgentDriverExixts = true;
                    entity.ErrorMessage = "Driver Already Exist for Agency and Distribution Point";
                    return entity;
                }







                //}
                return entity;
            }
            catch (Exception ex)
            {
                string message = ex.Message.ToString();
                throw;
            }

        }

        public UserRegistrtaionModel getAgencyDetails(int distributionId)
        {
            //using (var context = new lpgdevEntities())
            //{
            var rtns = (from distribution in context.DistributionPoints
                        join agency in context.Agencies
                        on distribution.AgenID equals agency.AgenID
                        where agency.AgenID == distribution.AgenID

                        select new UserRegistrtaionModel()
                        {
                            AgenDistId = distribution.DbptID,
                            AgenDistributionName = distribution.DistributionPointName,
                            AgenDistAgencyId = agency.AgenID,
                            AgenDistAgencyName = agency.AgencyName

                        }).FirstOrDefault();

            if (rtns == null)
            {
                rtns = (from distribution in context.DistributionPoints
                        join agency in context.Agencies
                        on distribution.AgenID equals agency.AgenID
                        where agency.AgenID == distribution.AgenID
                        select new UserRegistrtaionModel()
                        {
                            AgenDistId = distribution.DbptID,
                            AgenDistributionName = distribution.DistributionPointName,
                            AgenDistAgencyId = agency.AgenID,
                            AgenDistAgencyName = agency.AgencyName

                        }).FirstOrDefault();
            }

            return rtns;
            //}
        }

        public bool IsExists(int id)
        {
            //using (var context = new lpgdevEntities())
            //{
            return context.Agencies.Where(c => c.AgenID == id).Any();
            //}

        }

        public List<RegionModel> ListRegion()
        {
            var regionList = new List<RegionModel>();
            //using (var context = new lpgdevEntities())
            //{
            var data = from region in context.MRegions
                       select new RegionModel
                       {
                           RegionId = region.RegionID,
                           RegionName = region.RegionName,
                           RegionCode = region.RegionCode,
                           RegionCodeName = region.RegionCode + " - " + region.RegionName

                       };
            regionList = data.ToList();
            //}

            return regionList;
        }

        public bool IsExistsMobileNo(string mobileNo, int userId)
        {
            mobileNo = MessageSource.ToStandardPhone(mobileNo);
            using (var context = new lpgdevEntities())
                //{
                return context.AgentAdmins.Where(c => c.MobileNumber == mobileNo && c.StatusId == true && c.AgadmID != userId).Any();
            //}

        }
        //For Agent Admin
        public bool IsExistsEmail(string email, int userId)
        {
            //using (var context = new lpgdevEntities())
            //{
            return context.AgentAdmins.Where(c => c.email == email && c.StatusId == true && c.AgadmID != userId).Any();
            //}

        }
    }
}
