﻿using System.Collections.Generic;
using System.Web.Mvc;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IPengaturanPromoServices 
    {
        string GetVoucher();
        PengaturanPromoModel AddVoucher(PengaturanPromoModel data,int userId);
        List<PengaturanPromoModel> ListVoucher();
        PengaturanPromoModel GetPromo(int promoId);
        bool CheckVoucher(string vouncher);
    }
}
