﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using LPG.Models;
using i18n;
using Hangfire;
using LPG.Jobs.Hangfire;
using LPG.Jobs.RecurringJobs;
using Hangfire.Console;
using Hangfire.Logging.LogProviders;

namespace LPG
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureHangfire(IAppBuilder app)
        {

            GlobalConfiguration.Configuration
                .UseLogProvider(new ColouredConsoleLogProvider())
                .UseSqlServerStorage("hangfireDbConnection")
                .UseConsole();


            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                ServerName = "LPG Jobs",
                Queues = new[] { "main", "orders" }
            });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new HangfireDashboardAuthFilter() }
            });


            ConfigureRecuringJobs();

        }

        private string CronDailyMidnight04 = "4 0 * * *"; //Actually 00:04 Every day 
        private string CronDailyMidnight12 = "12 0 * * *"; // 00:12 Every day
        private string CronEverySecondMinute = "*/2 * * * * "; // Every two minute
        private string CronEveryMinute = "*/1 * * * *"; //Every Minute

        //private RecurringJobManager manager = new RecurringJobManager();

        private void ConfigureRecuringJobs()
        {
#if RELEASE
            RecurringJob.AddOrUpdate<MarkNotDeliveredOrders>(c => c.RunJob(JobCancellationToken.Null, null), () => CronDailyMidnight04, TimeZoneInfo.Local, "main");
            //RecurringJob.AddOrUpdate<MarkNotDeliveredOrders>(c => c.RunJob(JobCancellationToken.Null, null), () => CronDailyMidnight12, TimeZoneInfo.Local, "main");
#endif
        }
    }
}
