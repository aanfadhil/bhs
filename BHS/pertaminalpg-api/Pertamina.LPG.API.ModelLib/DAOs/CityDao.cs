﻿using Pertamina.LPG.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DAOs
{
    public class CityDao : DAO
    { 
        public MCity FindCityByName(string cityName)
        {
            return _context.MCities.Include("MProvince").Include("MProvince.MRegion").Where(x => x.CityName.ToLower().Replace("kota", "").Replace("kabupaten", "").Replace("city", "") == cityName.ToLower().Replace("kota","").Replace("kabupaten","").Replace("city","")).FirstOrDefault();
        }

        public MCity GetById(int id)
        {
            return _context.MCities.Where(t => t.ID == id).FirstOrDefault();
        }
    }
}
