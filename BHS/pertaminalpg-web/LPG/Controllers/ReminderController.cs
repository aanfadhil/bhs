﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.IO;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class ReminderController : AbstractController<IReminderService>
    {
        // GET: Reminder
        SessionManagement sessionManagement;

        public ReminderController(IReminderService _serviceObject) : base(_serviceObject)
        {
        }

        //ReminderService serviceObject = new ReminderService();
        // GET: BannerPromo
        public ActionResult AddReminder(int? id)
        {
            try
            {
                ReminderModel model = new ReminderModel();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {


                    var convid = id ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.getReminderDetails(convid);
                        if (vm != null)
                        {
                            return View(vm);
                        }
                        else
                        {
                            return View(model);
                        }
                    }
                    else
                    {
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { area = "Admin" });
                }

            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }
        }

        public ActionResult ListReminder()
        {
            var vm = new ReminderModel();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.ReminderList = serviceObject.ListReminder();
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { area = "Admin" });
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult AddReminder(ReminderModel vm, string Cmd)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }
            try
            {

                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    serviceObject.saveReminderDetails(vm, Cmd, adminSessionInfo.UserId);

                    SetFlash(FlashMessageType.Success, "Input reminder berhasil");

                    return RedirectToAction("ListReminder", "Reminder");
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { area = "Admin" });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }
        }
        public ActionResult deleteReminder(int? id)
        {

            if (id != null & id > 0)
            {

                try
                {
                    sessionManagement = new SessionManagement();
                    AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                    ViewBag.Role = adminSessionInfo.UserRole;
                    if (adminSessionInfo != null)
                    {

                        serviceObject.deleteReminder(id.ToInt(), adminSessionInfo.UserId);

                    }

                }
                catch
                {

                }
            }
            return RedirectToAction("ListReminder", "Reminder");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            string _imgname = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    _imgname = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/extfiles/reminder/Reminder_") + _imgname + _ext;
                    _imgname = "Reminder_" + _imgname + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    if (!Directory.Exists(Path.GetDirectoryName(path)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(path));
                    }

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width > 200)
                        img.Resize(1000, 1000, true, true);
                    img.Save(_comPath);
                    // end resize
                }
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }
    }
}
