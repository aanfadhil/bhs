﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Agent
{
    public class GetNotificationHistoryRequest
    {
        public int user_id { get; set; }
        public string auth_token { get; set; }
        public int row_per_page { get; set; }
        public int page_number { get; set; }
        public string search { get; set; }
    }
}
