﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public abstract class BaseModel<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}