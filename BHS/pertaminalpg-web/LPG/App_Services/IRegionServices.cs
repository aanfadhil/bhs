﻿using System.Collections.Generic;
using System.Web.Mvc;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IRegionServices 
    {
        List<RegionModel> GetList();
    }
}
