﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using LPG.Utilities;
using Microsoft.ApplicationInsights;
using System;
using System.Web.Mvc;


namespace LPG.Controllers
{
    public class LoginController : AbstractController<ILoginService>
    {
        private TelemetryClient telemetry = new TelemetryClient();

        // GET: Login
        //LoginService serviceObject;
        SessionManagement sessionManagement;

        public LoginController(ILoginService _serviceObject) : base(_serviceObject)
        {
        }

        public ActionResult Index()
        {
            sessionManagement = new SessionManagement();
            LoginModel vm = new LoginModel();

            var rememberMe = sessionManagement.getRememberMe();

            if (rememberMe != null)
            {
                vm.Email = rememberMe.Email;
                vm.Password = rememberMe.Password;
                vm.IsRememberMe = true;
            }

            //To delete, this is just to test
            //SMSService.SendSMS("6281933863069", "Test message from CMS, please ignore. :)");
            return View(vm);
        }

        [HttpPost]
        public ActionResult Index(LoginModel vm)
        {
            try
            {
                //serviceObject = base.serviceObject; //new LoginService();
                sessionManagement = new SessionManagement();

                //AdminSessionEntity adminSessionInfo = loginService.adminLogin(vm.Email.Trim(), vm.Password.Trim());

                AdminSessionEntity adminSessionInfo = serviceObject.AdminLoginActiveDirectoryAsync(vm.Email.Trim(), vm.Password.Trim());
                if (adminSessionInfo != null)
                {
                    sessionManagement.setAdminSession(adminSessionInfo);

                    if (vm.IsRememberMe)
                    {
                        sessionManagement.setRememberMe(vm);
                    }
                    else
                    {
                        sessionManagement.setRememberMe(null);
                    }

                    return RedirectToAction("Index", "Home");
                }
                else
                {

                    vm.ErrorMessage = "[[[Username dan password salah]]]";
                    return View(vm);
                }
            }
            catch (Exception ex)
            {
                telemetry.TrackException(ex);
                ExceptionHelper.Log(ex);
                vm.ErrorMessage = "[[[Cannot connect to server]]]";
                return View(vm);
            }
        }

        // GET: Forgot Password
        public ActionResult ForgotPassword()
        {
            LoginModel vm = new LoginModel();
            return View(vm);
        }
        [HttpPost]
        public ActionResult ForgotPassword(LoginModel vm)
        {
            try
            {
                //serviceObject = base.serviceObject; //new LoginService();
                vm.ErrorMessage = serviceObject.ForgotPassword(vm.Email, vm.new_password, vm.confirm_password);
                if (vm.ErrorMessage == MessageSource.GetMessage("reset.password"))
                {
                    return RedirectToAction("Index", "Login");
                }
                return View(vm);
            }
            catch (Exception ex)
            {
                telemetry.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Forgot Password" });
            }
        }
    }
}
