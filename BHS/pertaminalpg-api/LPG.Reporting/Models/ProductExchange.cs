namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProductExchange")]
    public partial class ProductExchange
    {
        [Key]
        public int PrExID { get; set; }

        public int ProdID { get; set; }

        [Required]
        [StringLength(250)]
        public string ExchangeWith { get; set; }

        public int ExchangeQuantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ExchangePrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ExchangePromoPrice { get; set; }

        public bool StatusId { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public virtual Product Product { get; set; }

        public virtual SuperAdmin SuperAdmin { get; set; }

        public virtual SuperAdmin SuperAdmin1 { get; set; }
    }
}
