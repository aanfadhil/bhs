namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderAllocationLog")]
    public partial class OrderAllocationLog
    {
        [Key]
        public long AlogID { get; set; }

        public int OrdrID { get; set; }

        public int AgadmID { get; set; }

        public int DbptID { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Distance { get; set; }

        public bool TimeSlotAvailable { get; set; }

        public short AllocationStatus { get; set; }

        public short AssignmentType { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public virtual Order Order { get; set; }
    }
}
