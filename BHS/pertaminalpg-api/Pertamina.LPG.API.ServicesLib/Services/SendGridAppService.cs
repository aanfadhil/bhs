﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using System.IO;

namespace Pertamina.LPG.API.Services
{
    class SendGridAppService
    {
        private string ApiKey;
        private SendGridClient _client;
        private string From = "lpg@pertamina.com";

        public SendGridAppService()
        {

            ApiKey = Properties.Settings.Default.SendGridAPIKey;
            _client = new SendGridClient(ApiKey);

        }

        public async Task SendEmailWithAttachment(EmailAddress to, EmailAddress from, string subject, string body, string pathToFile, string fileNameToUse)
        {

            var msg = MailHelper.CreateSingleEmail(from, to, subject, body, "");

            var bytes = File.ReadAllBytes(pathToFile);
            var file = Convert.ToBase64String(bytes);
            msg.AddAttachment(fileNameToUse, file);
            var response = await _client.SendEmailAsync(msg);

        }

    }
}
