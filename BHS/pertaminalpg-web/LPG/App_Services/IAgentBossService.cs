﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IAgentBossService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        bool DeleteAgentBoss(int Id, int loggedBy);
        AgentBossModel getActiveAgentBoss(int agentBossId);
        bool IsExists(int id);
        bool IsExistsMobileNo(string mobileNo, int userId);
        List<AgentBossModel> ListAgentBoss();
        AgentBossModel saveAgentBossDetails(AgentBossModel entity, int loggedBy);
    }
}