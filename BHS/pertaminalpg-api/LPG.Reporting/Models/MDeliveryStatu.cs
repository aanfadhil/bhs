namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MDeliveryStatu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MDeliveryStatu()
        {
            OrderDeliveries = new HashSet<OrderDelivery>();
            TeleOrderDeliveries = new HashSet<TeleOrderDelivery>();
        }

        [Key]
        public short DlstID { get; set; }

        [Required]
        [StringLength(50)]
        public string DeliveryStatus { get; set; }

        public bool StatusId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderDelivery> OrderDeliveries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleOrderDelivery> TeleOrderDeliveries { get; set; }
    }
}
