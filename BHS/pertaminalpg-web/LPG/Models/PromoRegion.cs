//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LPG.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PromoRegion
    {
        public int IDPromoRegion { get; set; }
        public int PromoID { get; set; }
        public int RegionID { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
    
        public virtual MRegion MRegion { get; set; }
        public virtual PromoVoucher PromoVoucher { get; set; }
    }
}
