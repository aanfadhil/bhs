﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Product
{
    public class GetProductPriceByCityResponse : ResponseDto
    {
        public ProductPagenationDetailsDto product_details { get; set; }
        public DetailPriceDto[] products { get; set; }
        public int has_reminder { get; set; }
        public int reminder_id { get; set; }
        public string reminder_image { get; set; }
        public string reminder_description { get; set; }
    }
}
