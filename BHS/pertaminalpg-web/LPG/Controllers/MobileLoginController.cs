﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;

namespace LPG.Controllers
{
    public class MobileLoginController : ApiController, IMobileLoginController
    {
        private ILoginService LoginService;

        public MobileLoginController(ILoginService _LoginService) : base()
        {
            LoginService = _LoginService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<MobileLoginResponse> MobileLogin([FromBody] MobileLoginRequest request)
        {
            MobileLoginResponse response = new MobileLoginResponse();
            try
            {
                string mobile_number = GetStandardMobileNumber(request.mobile_number);
                string password = request.password;

                await Request.Content.ReadAsStringAsync();
                
                using (var context = new lpgdevEntities())
                {
                    SuperAdmin superuser = context.SuperAdmins.Where(a => a.StatusID && a.MobileNum.Replace("+62", "") == mobile_number.Replace("+62", "")).Single();
                    if (superuser != null)
                    {
                        MailAddress adminEmail = new MailAddress(superuser.Email);
                        AdminSessionEntity adminSessionInfo = LoginService.AdminLoginActiveDirectoryAsync(adminEmail.User.Trim(), password.Trim());
                        if (adminSessionInfo != null)
                        {
                            response.code = 1;
                            response.has_resource = 0;
                            superuser.AppToken = request.push_token;
                            superuser.AppID = request.app_id;
                            superuser.LastLogin = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                            string authToken = TokenGenerator.GenerateToken(superuser.FullName, superuser.Password, request.mobile_number);
                            superuser.AccToken = authToken;
                            context.SaveChanges();
                            response.code = 0;

                            SuperUserLoginDto dto = new SuperUserLoginDto();
                            dto.auth_token = superuser.AccToken;
                            dto.user_id = superuser.SAdminID;

                            SuperUserLoginDetailsDto dtoDetails = new SuperUserLoginDetailsDto();
                            dtoDetails.profile_image = GetAppSetting<string>("SuperAdminImagePath", string.Empty) + superuser.ProfileImage;
                            dtoDetails.super_user_name = superuser.FullName;
                            dtoDetails.city_name = superuser.CityId != null ? superuser.MCity.CityName : string.Empty;

                            response.user_login = dto;
                            response.super_user_details = dtoDetails;
                            response.has_resource = 1;
                            response.code = 0;
                            response.message = "[[[Successfully logged in to the system]]]";
                            return response;
                        }
                        else
                        {
                            response.code = 1;
                            response.has_resource = 0;
                            response.message = "[[[Login Failed]]]";
                        }
                    }
                    else
                    {
                        response.code = 1;
                        response.has_resource = 0;
                        response.message = "[[[Superuser not found]]]";
                    }
                }
            }
            catch (Exception ex)
            { 
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public static T GetAppSetting<T>(string key, T defaultValue)
        {
            if (!string.IsNullOrEmpty(key))
            {
                string value = ConfigurationManager.AppSettings[key];
                try
                {
                    if (value != null)
                    {
                        var theType = typeof(T);
                        if (theType.IsEnum)
                            return (T)Enum.Parse(theType, value.ToString(), true);

                        return (T)Convert.ChangeType(value, theType);
                    }

                    return default(T);
                }
                catch
                {
                    // ignored
                }
            }

            return defaultValue;
        }

        public static string GetStandardMobileNumber(string mobileNumber)
        {
            if (string.IsNullOrEmpty(mobileNumber))
            {
                return null;
            }
            if (mobileNumber.StartsWith("+62"))
                return mobileNumber;
            else
            {
                if (mobileNumber.StartsWith("0"))
                {
                    mobileNumber = mobileNumber.Remove(0, 1);
                }
                return mobileNumber.Insert(0, "+62");
            }

        }
    }
}
