﻿using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.App_Services.JsonConvertor
{
    public class ProductConvertor
    {
        public static List<ProductModel> ProductListJsonToModel(GetProductListResponse model)
        {
            var productListModel = new List<ProductModel>();

            foreach (var item in model.products)
            {
                var tempItem = new ProductModel();
                tempItem.ProdID = item.product_id;
                tempItem.ProductName = item.product_title;
                tempItem.ProductImage = item.product_image;
                tempItem.CreatedDate = item.product_createdon;
                tempItem.Published = item.product_published;
                tempItem.Position = item.product_position;


                productListModel.Add(tempItem);
            }

            return productListModel;
        }


        public static ProductModel ProductJsonToModel(GetProductDetailsResponse model)
        {
            var tempItem = new ProductModel();
            tempItem.ProdID = model.product_details.product_id;
            tempItem.ProductName = model.product_details.product_name;
            tempItem.ProductImage = model.product_details.product_image;
            //tempItem.CreatedDate = model.product_details.product_createdon;
            //tempItem.Published = model.product_details.product_published;
            tempItem.Position = model.product_details.position;
            tempItem.ExchangeProductLists = model.product_details.exchange.ToList<ProductExchangeList>();
            return tempItem;
        }
    }
}