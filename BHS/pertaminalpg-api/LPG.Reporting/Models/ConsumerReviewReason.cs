namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsumerReviewReason")]
    public partial class ConsumerReviewReason
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ConsumerReviewReason()
        {
            ConsumerReviews = new HashSet<ConsumerReview>();
        }

        [Key]
        public int ReasonID { get; set; }

        public string ReasonText { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsumerReview> ConsumerReviews { get; set; }
    }
}
