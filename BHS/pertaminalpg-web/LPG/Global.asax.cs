﻿using i18n;
using LPG.App_Start;
using LPG.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;
using Unity.AspNet.Mvc;

namespace LPG
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //Microsoft.Web.Infrastructure.DynamicModuleHelper.DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
            AreaRegistration.RegisterAllAreas();
            UnityConfig.RegisterComponents();


            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            I18NConfig.InitWithDefaultLanguage("id");
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));

        }

        protected void Application_Stop()
        {


        }

        protected void Application_End(object sender, EventArgs e)
        {
            //Log.Instance.Info("Application_End");
            //DelaySavingBySeconds(ConfigurationManager.AppSettings["DelaySavingBySeconds"]);

            //log4net.
        }


        protected void Application_Error()
        {
            HttpContext httpContext = HttpContext.Current;
            if (httpContext != null)
            {
                var exception = Server.GetLastError();
                //log the error!
                ExceptionHelper.Log(exception);


                var newline = "<br/>";
                string ErrorlineNo = exception.StackTrace.Substring(exception.StackTrace.Length - 7, 7);
                string Errormsg = exception.GetType().Name.ToString();
                string extype = exception.GetType().ToString();
                string exurl = HttpContext.Current.Request.Url.ToString();
                string ErrorLocation = exception.Message.ToString();
                string EmailHead = "<b>Dear Team,</b>" + "<br/>" + "An exception occurred in a Application Url" + " " + exurl + " " + "With following Details" + "<br/>" + "<br/>";
                string EmailSing = newline + "Thanks and Regards" + newline + "    " + "     " + "<b>Application Admin </b>" + "</br>";
                string Sub = "Exception occurred" + " " + "in Application" + " " + exurl;
                string HostAdd = "smtp.gmail.com";
                string errorString = EmailHead + "<b>Log Written Date: </b>" + " " + DateTime.Now.ToString() + newline + "<b>Error Line No :</b>" + " " + ErrorlineNo + "\t\n" + " " + newline + "<b>Error Message:</b>" + " " + Errormsg + newline + "<b>Exception Type:</b>" + " " + extype + newline + "<b> Error Details :</b>" + " " + ErrorLocation + newline + "<b>Error Page Url:</b>" + " " + exurl + newline + newline + newline + newline + EmailSing;


                RequestContext requestContext = ((MvcHandler) httpContext.CurrentHandler).RequestContext;
                /* When the request is ajax the system can automatically handle a mistake with a JSON response. 
                   Then overwrites the default response */
                if (requestContext.HttpContext.Request.IsAjaxRequest())
                {
                    httpContext.Response.Clear();
                    string controllerName = requestContext.RouteData.GetRequiredString("controller");
                    IControllerFactory factory = ControllerBuilder.Current.GetControllerFactory();
                    IController controller = factory.CreateController(requestContext, controllerName);
                    ControllerContext controllerContext = new ControllerContext(requestContext, (ControllerBase) controller);

                    JsonResult jsonResult = new JsonResult
                    {
                        Data = new { success = false, serverError = "500" },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    jsonResult.ExecuteResult(controllerContext);
                    httpContext.Response.End();
                }
                else
                {
                    Session["Exception"] = errorString;
                    httpContext.Response.Redirect("~/Error");
                }
            }
        }

        //protected void Application_EndRequest(object sender, EventArgs e)
        //{
        //    using (DependencyResolver.Current as IDisposable) ;
        //}




    }
}
