﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class NotificationHistory
    {
        public long LogId { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }
        public string Username { get; set; }
        public string MobileNumber { get; set; }
        public string Role { get; set; }
        public string NamaAgen { get; set; }
        public int? UserId { get; set; }
        public short? IsRead { get; set; }
        public int? Success { get; set; }
        public DateTime? ReadAt { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ReadAtFormated { get; set; }
        public string CreatedDateFormated { get; set; }
    }
}