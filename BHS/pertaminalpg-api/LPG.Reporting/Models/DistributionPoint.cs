namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DistributionPoint")]
    public partial class DistributionPoint
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DistributionPoint()
        {
            AgentAdmins = new HashSet<AgentAdmin>();
            Drivers = new HashSet<Driver>();
        }

        [Key]
        public int DbptID { get; set; }

        public int AgenID { get; set; }

        [Required]
        [StringLength(100)]
        public string DistributionPointName { get; set; }

        [Required]
        [StringLength(100)]
        public string Latitude { get; set; }

        [Required]
        [StringLength(100)]
        public string Longitude { get; set; }

        [Required]
        [StringLength(250)]
        public string Address { get; set; }

        public bool StatusId { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public virtual Agency Agency { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AgentAdmin> AgentAdmins { get; set; }

        public virtual SuperAdmin SuperAdmin { get; set; }

        public virtual SuperAdmin SuperAdmin1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Driver> Drivers { get; set; }
    }
}
