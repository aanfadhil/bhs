﻿using LPG.Utilities;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class UploadFileController : Controller
    {

        protected CloudBlobContainer GetCloudBlobContainer()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                    CloudConfigurationManager.GetSetting("pertaminalpg_AzureStorageConnectionString"));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("lpg-cms-static-assets");
            container.CreateIfNotExists();
            return container;
        }

        //UPLOAD : Profile Image 
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadProfileImage()
        {
            string _imgname = string.Empty;
            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    var pic = System.Web.HttpContext.Current.Request.Files["prfImage"];
                    HttpPostedFileBase file = new HttpPostedFileWrapper(pic);

                    if (file != null)
                    {

                        string path = Server.MapPath("~/extfiles/profile/superuser/");
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        LogHelper.WriteLog(string.Format("Path : {0}", path));

                        file.SaveAs(path + Path.GetFileName(file.FileName));
                        ViewBag.Message = "File uploaded successfully.";

                        if (pic.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(pic.FileName);
                            var _ext = Path.GetExtension(pic.FileName);

                            _imgname = string.Format("ProfileImg_{0}{1}", Guid.NewGuid().ToString(), _ext);
                            var _comPath = string.Format("{0}{1}", path, _imgname);

                            LogHelper.WriteLog(string.Format("Full Path : {0}", _comPath));

                            // Saving Image in Original Mode
                            pic.SaveAs(_comPath);

                            // resizing image
                            //MemoryStream ms = new MemoryStream();
                            //WebImage img = new WebImage(_comPath);

                            //if (img.Width > 200)
                            //    img.Resize(1500, 577);
                            //img.Save(_comPath);
                            // end resize

                            var _userId = System.Web.HttpContext.Current.Request.Form["userId"].ToDefaultString();
                            var _userType = System.Web.HttpContext.Current.Request.Form["userType"].ToDefaultString();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LogHelper.PublishException(ex);
                ExceptionHelper.Log(ex);
            }

            return Json(_imgname, JsonRequestBehavior.AllowGet);
        }
    }
}
