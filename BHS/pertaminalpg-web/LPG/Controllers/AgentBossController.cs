﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class AgentBossController : AbstractController<IAgentBossService>
    {
        // GET: AgentBoss
        SessionManagement sessionManagement;
        private AgencyService agencyObject;// = new AgencyService();
        private DistributionService distService;// = new DistributionService();

        public AgentBossController(IAgentBossService _serviceObject, AgencyService _agencyObject, DistributionService _distService) : base(_serviceObject)
        {
            agencyObject = _agencyObject;
            distService = _distService;
        }

        //AgentBossService serviceObject = new AgentBossService();

        public ActionResult ListAgentBoss()
        {
            var vm = new AgentBossModel();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.AgentBossList = serviceObject.ListAgentBoss();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(vm);
        }

        public ActionResult AddAgentBoss(int? id)
        {
            try
            {
                AgentBossModel model = new AgentBossModel();
                //AgencyService agencyObject = new AgencyService();
                //DistributionService distService = new DistributionService();

                sessionManagement = new SessionManagement();
                var agencyList = agencyObject.ListComapany();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    var convid = id ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.getActiveAgentBoss(convid);
                        if (vm != null)
                        {
                            vm.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                            return View(vm);
                        }
                        else
                        {
                            model.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                            return View(model);
                        }
                    }
                    else
                    {
                        model.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        return View(model);

                    }

                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public ActionResult AddAgentBoss(AgentBossModel vm)
        {
            var agencyList = agencyObject.ListComapany();
            vm.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();

            if (!ModelState.IsValid)
            {
                return View(vm);
            }
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    if (serviceObject.IsExistsMobileNo(vm.CountryCode + vm.MobileNo, vm.AgentBossId))
                    {
                        vm.errorMessage = MessageSource.GetMessage("Mobile.exists");
                        return View(vm);
                    }
                    else
                    {
                        serviceObject.saveAgentBossDetails(vm, adminSessionInfo.UserId);
                        SetFlash(FlashMessageType.Success, "Input agen bos berhasil");

                        return RedirectToAction("ListAgentBoss", "AgentBoss");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }
        }

        public ActionResult DeleteAgentBoss(int id)
        {
            sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {
                if (serviceObject.DeleteAgentBoss(id, adminSessionInfo.UserId))
                {
                    //return View();
                    return RedirectToAction("ListAgentBoss", "AgentBoss");
                }

            }
            return RedirectToAction("Index", "Error");
        }
    }
}
