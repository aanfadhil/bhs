﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using Microsoft.ApplicationInsights;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;

namespace LPG.Controllers
{
    public class DetailPriceController : AbstractController<IDetailPriceService>
    {
        SessionManagement sessionManagement;

        public DetailPriceController(IDetailPriceService _serviceObject) : base(_serviceObject)
        {
        }

        public ActionResult EditPrice(int? detailPriceId)
        {
            try
            {
                DetailPriceModel model = new DetailPriceModel();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var convid = detailPriceId ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.GetDetailPrice(convid);
                        if (vm != null)
                        {
                            model = vm;
                            return View(model);
                        }
                        else
                        {
                            return View(model);
                        }
                    }
                    else
                    {
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Detail Price - Get" });
            }
        }

        [HttpPost]
        public ActionResult EditPrice(DetailPriceModel _dataModel)
        {
            try
            {
                ProductModel prod = new ProductModel();
                ProductAndPriceList det = new ProductAndPriceList();
                det.product = prod;
                det.product.ProdID = _dataModel.ProductID;
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    var reply = serviceObject.SavePrice(_dataModel, (Int16)adminSessionInfo.UserId);
                    return RedirectToAction("DetailProduct", "Product", new { productId = _dataModel.ProductID });
                }

                return RedirectToAction("ListProduct");
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Price Add/Update" });
            }

        }
    }
}