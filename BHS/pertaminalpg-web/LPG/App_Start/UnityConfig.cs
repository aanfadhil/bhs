﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Controllers;
using LPG.Models;
using Microsoft.ApplicationInsights;
using System.Web.Http;
using System.Web.Mvc;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using Unity.Mvc5;

namespace LPG
{
    public static class UnityConfig
    {
        public static UnityContainer Container;

        public static void RegisterComponents()
        {
            var container = new UnityContainer();


            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IActivityService, ActivityService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(TelemetryClient)));
            container.RegisterType<IAgencyService, AgencyService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IAgentAdminService, AgentAdminService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IAgentBossService, AgentBossService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IBannerInfoService, BannerInfoService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IBannerPromoService, BannerPromoService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IContactService, ContactService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<ICustomerService, CustomerService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IDistributionService, DistributionService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IDriverService, DriverService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IFaqService, FaqService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IHomeBannerServices, HomeBannerServices>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<ILoginService, LoginService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient), typeof(ISuperAdminService)));
            container.RegisterType<IOrderService, OrderService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IProductService, ProductService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IPengaturanPromoServices, PengaturanPromoServices>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<ICheckAreaServices, CheckAreaServices>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<INotificationHistoryService, NotificationHistoryService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IRegionServices, RegionServices>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<ICityService, CityService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IProvinceService, ProvinceService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IDetailPriceService, DetailPriceService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            //container.RegisterType<PushMessagingService>();
            container.RegisterType<IReminderService, ReminderService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<ISuperAdminService, SuperAdminService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient)));
            container.RegisterType<IUserService, UserService>(new InjectionConstructor(typeof(lpgdevEntities), typeof(IActivityService), typeof(TelemetryClient), typeof(IAgencyService)));
            container.RegisterType<ISessionManagement, SessionManagement>(new InjectionConstructor());
            container.RegisterType<lpgdevEntities>(new InjectionConstructor());
            container.RegisterType<TelemetryClient>(new InjectionConstructor());

            container.RegisterType<IUploadController, UploadController>(new InjectionConstructor(typeof(ISuperAdminService), typeof(IAgentBossService), typeof(IAgentAdminService), typeof(IDriverService), typeof(ICustomerService)));
            container.RegisterType<IMobileLoginController, MobileLoginController>(new InjectionConstructor(typeof(ILoginService)));


            Container = container;

            DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));
            //Global
            //GlobalConfiguration.Configuration.ServiceResolver.SetResolver(new Unity.WebApi.UnityDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

            //GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}
