﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class AgentAdminController : AbstractController<IAgentAdminService>
    {
        SessionManagement sessionManagement;
        private IAgencyService agencyObject;// = new AgencyService();
        private IDistributionService distService;// = new DistributionService();


        public AgentAdminController(IAgentAdminService _serviceObject, IAgencyService _agencyObject, IDistributionService _distService) : base(_serviceObject)
        {
            agencyObject = _agencyObject;
            distService = _distService;
        }


        //GET: AgentAdmin
        public ActionResult ListAgentAdmin()
        {
            var vm = new AgentAdminModel();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.AgentAdminList = serviceObject.ListAgentAdmin();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult GetDist(int agency)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var products = distService.GetByAgency(agency);
                    return Json(products);
                }
                else
                {
                    return Json("SESSION TIMEOUT");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(null);
            }
        }

        public ActionResult AddAgentAdmin(int? id)
        {
            try
            {
                AgentAdminModel model = new AgentAdminModel();


                sessionManagement = new SessionManagement();
                var agencyList = agencyObject.ListComapany();
                var distList = distService.ListDistribution();


                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    var convid = id ?? 0;
                    model.AgentId = convid;
                    if (convid > 0)
                    {
                        var vm = serviceObject.getActiveAgentAdmin(convid);
                        if (vm != null)
                        {
                            vm.agencyAList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                            vm.agencyDList = distService.GetByAgency(vm.AgenadminAgenId).Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();
                            return View(vm);
                        }
                        else
                        {
                            model.agencyAList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                            model.agencyDList = new System.Collections.Generic.List<SelectListItem>(); //distList.Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();
                            return View(model);
                        }
                    }
                    else
                    {
                        model.agencyAList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        model.agencyDList = new System.Collections.Generic.List<SelectListItem>();//distList.Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();
                        return View(model);

                    }

                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public ActionResult AddAgentAdmin(AgentAdminModel vm)
        {
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    //AgentAdminService serviceObject = new AgentAdminService();
                    //AgencyService agencyObject = new AgencyService();
                    //DistributionService distService = new DistributionService();
                    if (!ModelState.IsValid)
                    {
                        var agencyList = agencyObject.ListComapany();
                        var distList = distService.ListDistribution();
                        vm.agencyAList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        vm.agencyDList = distList.Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();
                        vm.errorMessage = MessageSource.GetMessage("required.data");
                        return View(vm);
                    }
                    if (serviceObject.IsExistsMobileNo(vm.MobileNo, vm.AgentId))
                    {

                        vm.errorMessage = MessageSource.GetMessage("Mobile.exists");
                        var agencyList = agencyObject.ListComapany();
                        var distList = distService.ListDistribution();
                        vm.agencyAList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        vm.agencyDList = distList.Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();

                        return View(vm);
                    }
                    else if (serviceObject.IsExistsEmail(vm.email, vm.AgentId))
                    {

                        vm.errorMessage = MessageSource.GetMessage("Email.exists");
                        var agencyList = agencyObject.ListComapany();
                        var distList = distService.ListDistribution();
                        vm.agencyAList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        vm.agencyDList = distList.Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();
                        return View(vm);
                    }
                    else
                    {

                        serviceObject.saveAgentDetails(vm, adminSessionInfo.UserId);

                        SetFlash(FlashMessageType.Success, "Input admin agen berhasil");

                        return RedirectToAction("ListAgentAdmin", "AgentAdmin");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error");
            }
        }

        public ActionResult DeleteAgentAdmin(int id)
        {

            sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {
                if (serviceObject.DeleteAgentAdmin(id, adminSessionInfo.UserId))
                {
                    //return View();
                    return RedirectToAction("ListAgentAdmin", "AgentAdmin");
                }

            }
            return RedirectToAction("Index", "Error");
        }
    }
}
