﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LPG.Models
{
    public class DistributionModel
    {
        public int DistributionId { get; set; }
        public string CompanyAgent { get; set; }
        [Required]
        public string DistributionPoint{ get; set; }
        [Required]
        public string Lat { get; set; }
        [Required]
        public string Long { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string CityName { get; set; }
        
        public int CityId { get; set; }

        public bool IsActive { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public List<DistributionModel> DistributionList { get; set; }
        public bool IsExixts { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int AgenDriverAgenId { get; set; }
        public List<SelectListItem> AgencyList { get; set; }
        public string errorMessage { get; set; }
    }
}