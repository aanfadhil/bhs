namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TeleOrder")]
    public partial class TeleOrder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TeleOrder()
        {
            TeleCustomers = new HashSet<TeleCustomer>();
            TeleOrderDeliveries = new HashSet<TeleOrderDelivery>();
            TeleOrderDetails = new HashSet<TeleOrderDetail>();
            TeleOrderPrdocuctExchanges = new HashSet<TeleOrderPrdocuctExchange>();
        }

        [Key]
        public int TeleOrdID { get; set; }

        public int AgadmID { get; set; }

        public int? DrvrID { get; set; }

        [StringLength(100)]
        public string InvoiceNumber { get; set; }

        [Column(TypeName = "date")]
        public DateTime OrderDate { get; set; }

        public TimeSpan OrderTime { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeliveryDate { get; set; }

        public short? DeliverySlotID { get; set; }

        public int NumberOfProducts { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PromoProduct { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ShippingCharge { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PromoShipping { get; set; }

        [Column(TypeName = "numeric")]
        public decimal GrantTotal { get; set; }

        public bool DeliveryType { get; set; }

        public DateTime? DeliveryStartDate { get; set; }

        public DateTime? DeliveredDate { get; set; }

        public int Deviation { get; set; }

        public short StatusId { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal RefillSubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PromoRefill { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ExchangeSubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PromoExchange { get; set; }

        public virtual AgentAdmin AgentAdmin { get; set; }

        public virtual AgentAdmin AgentAdmin1 { get; set; }

        public virtual Driver Driver { get; set; }

        public virtual MDeliverySlot MDeliverySlot { get; set; }

        public virtual MOrderStatu MOrderStatu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleCustomer> TeleCustomers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleOrderDelivery> TeleOrderDeliveries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleOrderDetail> TeleOrderDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleOrderPrdocuctExchange> TeleOrderPrdocuctExchanges { get; set; }
    }
}
