﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.Consumers
{
    public class TeleCustomerDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int TeleCustID { get; set; }
        public int TeleOrdID { get; set; }
        public string CustomerName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Address { get; set; }
        public string MobileNumber { get; set; }
        public bool StatusId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class TeleCustomerMapper : MapperBase<TeleCustomer, TeleCustomerDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<TeleCustomer, TeleCustomerDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<TeleCustomer, TeleCustomerDTO>>) (p => new TeleCustomerDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    TeleCustID = p.TeleCustID,
                    TeleOrdID = p.TeleOrdID,
                    CustomerName = p.CustomerName,
                    Latitude = p.Latitude,
                    Longitude = p.Longitude,
                    Address = p.Address,
                    MobileNumber = p.MobileNumber,
                    StatusId = p.StatusId,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate
                }));
            }
        }

        public override void MapToModel(TeleCustomerDTO dto, TeleCustomer model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.TeleCustID = dto.TeleCustID;
            model.TeleOrdID = dto.TeleOrdID;
            model.CustomerName = dto.CustomerName;
            model.Latitude = dto.Latitude;
            model.Longitude = dto.Longitude;
            model.Address = dto.Address;
            model.MobileNumber = dto.MobileNumber;
            model.StatusId = dto.StatusId;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;

        }
    }
}
