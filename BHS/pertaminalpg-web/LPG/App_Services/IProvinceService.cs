﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IProvinceService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        List<ProvinceModel> ListProvince();
    }
}