﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using LPG.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using ClosedXML.Excel;
using System.Globalization;
using i18n;
using LPG.Reporting.ReportServices;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class CustomerController : AbstractController<ICustomerService>
    {
        SessionManagement sessionManagement;

        public CustomerController(ICustomerService _serviceObject) : base(_serviceObject)
        {
        }

        //CustomerService serviceObject = new CustomerService();

        // GET: Customer
        public ActionResult ListCustomer()
        {
            var vm = new CustomerModel();
            //CustomerService serviceObject = new CustomerService();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.CustomerList = serviceObject.ListCustomers();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult ListCustomer(DatatablesRequest model)
        {
            var result = new DataTableModel<GetCustomerPaging_Result>();
            //CustomerService serviceObject = new CustomerService();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    result = serviceObject.GetCustomerPaging(model);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Error");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult Export()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    lpgdevEntities entities = new lpgdevEntities();
                    DataTable dt = new DataTable("MasterData");
                    dt.Columns.AddRange(new DataColumn[] {
                        new DataColumn("[[[No]]]"),
                        new DataColumn("[[[ID]]]"),
                        new DataColumn("[[[Nama Pelanggan]]]"),
                        new DataColumn("[[[Alamat Utama]]]"),
                        new DataColumn("[[[Kota/Kabupaten]]]"),
                        new DataColumn("[[[Kode Pos]]]"),
                        new DataColumn("[[[No HandPhone]]]")
                    });

                    var consumers = serviceObject.GetCustomerPaging(new DatatablesRequest()
                    {
                        draw = 0,
                        length = null,
                        start = 1,
                        search = new List<string>() { "" },
                        searchtype = ""
                    });

                    int count = 0;
                    foreach (var consumer in consumers.data)
                    {
                        count++;
                        
                        dt.Rows.Add
                        (
                            count,
                            consumer.ConsID,
                        consumer.Name,
                        consumer.Address,
                        consumer.RegionName,
                        consumer.PostalCode,
                        consumer.PhoneNumber
                       );
                    }
                    var style = XLWorkbook.DefaultStyle;


                    using (XLWorkbook wb = new XLWorkbook())
                    {


                        wb.Worksheets.Add(dt);
                        using (MemoryStream stream = new MemoryStream())
                        {
                            wb.SaveAs(stream);
                            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Daftar Pelanggan.xlsx");
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Order List" });
            }

        }

        public ActionResult deleteCustomer(int? id)
        {
            if (id != null & id > 0)
            {

                try
                {
                    sessionManagement = new SessionManagement();
                    AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                    ViewBag.Role = adminSessionInfo.UserRole;
                    if (adminSessionInfo != null)
                    {
                        //CustomerService serviceObject = new CustomerService();

                        serviceObject.deleteCustomer(id.ToInt());

                    }
                }
                catch
                {

                }
            }
            return RedirectToAction("ListCustomer", "Customer");
        }

        public ActionResult CustomerDetail(int? id)
        {
            CustomerModel model = new CustomerModel();
            sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {

                var convid = id ?? 0;
                if (convid > 0)
                {
                    model = serviceObject.CustomerView(convid);
                    //var vm = orderObject.getOrderDetails(convid);
                    if (model != null)
                    {
                        return PartialView("CustomerPartial", model);
                    }
                    else
                    {

                        return PartialView("CustomerPartial", model);
                    }
                }
                else
                {
                    return View(model);
                }

            }
            else
            {
                return RedirectToAction("Index", "Login", new { area = "Admin" });
            }
        }
    }
}
