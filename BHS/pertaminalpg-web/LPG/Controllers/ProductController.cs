﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using Microsoft.ApplicationInsights;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class ProductController : AbstractController<IProductService>
    {
        SessionManagement sessionManagement;
        private IDetailPriceService detailPriceObject;

        public ProductController(
            IProductService _serviceObject
            ) : base(_serviceObject)
        {

        }

        //ProductService serviceObject = new ProductService();

        public ActionResult ListProduct()
        {
            var vm = new ProductModel();
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var temp = serviceObject.ListProducts().Where(f => f.StatusId).OrderBy(o => o.Position).ToList();
                    return View(temp);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products List" });
            }
        }

        public ActionResult AddProduct(int? productId)
        {
            try
            {
                ProductModel model = new ProductModel();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var convid = productId ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.GetProduct(convid);
                        if (vm != null)
                        {
                            //if(vm.ExchangeProductLists == null)
                            //{
                            //    vm.ExchangeProductLists = new List<ProductExchangeList>();
                            //    var productEx = new ProductExchangeList();
                            //    productEx.exchange_with = "";
                            //    vm.ExchangeProductLists.Add(productEx);
                            //}
                            BindPositions(vm.ProdID);
                            return View(vm);
                        }
                        else
                        {
                            BindPositions(null);
                            return View(model);
                        }
                    }
                    else
                    {
                        //model.ExchangeProductLists = new List<ProductExchangeList>();
                        //var productEx = new ProductExchangeList();
                        //productEx.exchange_with = "";
                        //model.ExchangeProductLists.Add(productEx); 
                        BindPositions(null);
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products - Get" });
            }
        }

        [HttpPost]
        public ActionResult AddProduct(ProductModel _dataModel)
        {
            try
            {
                var a = System.Web.HttpContext.Current.Request.Files.AllKeys.Any();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                //ModelState.Remove("ExchangeProductLists");
                if (adminSessionInfo != null)
                {
                    if (_dataModel.ExchangeProductLists != null && _dataModel.ExchangeProductLists.Count > 0)
                    {
                        foreach (ProductExchangeList prodExItem in _dataModel.ExchangeProductLists.ToList())
                        {
                            if (prodExItem.exchange_with == "" || prodExItem.exchange_quantity == 0 || prodExItem.exchange_price == 0)
                            {
                                BindPositions(null);
                                return View(_dataModel);
                            }
                            else
                            {
                                if (prodExItem.exchange_promo_price != 0 && prodExItem.exchange_promo_price > prodExItem.exchange_price)
                                {
                                    _dataModel.ErrorMessage = MessageSource.GetMessage("Exchange.Promo");
                                    BindPositions(null);
                                    return View(_dataModel);
                                }
                            }
                        }
                    }
                    var reply = serviceObject.SaveProduct(_dataModel, (Int16)adminSessionInfo.UserId);

                    SetFlash(FlashMessageType.Success, "Input produk berhasil");

                    //return RedirectToAction("ListProduct", "Product", this.AddToastMessage("Success", "Product Added Succesfuly", ToastType.Success));
                    return RedirectToAction("ListProduct", "Product");




                    //if(ModelState.IsValid)
                    //{                    
                    //    var reply = _service.SaveProduct(_dataModel, (Int16)adminSessionInfo.UserId);
                    //}  
                    //else
                    //{
                    //    if (_dataModel.ExchangeProductLists.Count > 0)
                    //    {
                    //        foreach (ProductExchangeList prodExItem in _dataModel.ExchangeProductLists.ToList())
                    //        {
                    //            if (prodExItem.exchange_with == "" || prodExItem.exchange_quantity == 0 || prodExItem.exchange_price == 0 || prodExItem.exchange_promo_price == 0)
                    //            {                               
                    //                return View(_dataModel);
                    //            }
                    //        }
                    //    }
                    //}
                }

                return RedirectToAction("ListProduct");
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products" });
            }

        }

        public ActionResult AddNewProductExchange()
        {
            ProductExchangeList prodEx = new ProductExchangeList();
            return PartialView("_ProductExchange", prodEx);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadProdImage()
        {
            string _imgname = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["ProdImg"];
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    var filePath = Server.MapPath("/extfiles/products/");

                    Directory.CreateDirectory(path: filePath);

                    _imgname = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/extfiles/products/Product_") + _imgname + _ext;
                    _imgname = "Product_" + _imgname + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width < 500)
                    {
                        img.Resize(250, 250);
                    }
                    else
                    {
                        img.Resize(500, 500);
                    }



                    img.Save(_comPath);

                    _imgname = UploadToAzure(_comPath, _imgname);

                    // end resize
                }
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadExcel()
        {
            string _excel = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var excel = System.Web.HttpContext.Current.Request.Files["ProdExcel"];
                if (excel.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(excel.FileName);
                    var _ext = Path.GetExtension(excel.FileName);

                    var filePath = Server.MapPath("/extfiles/products/");

                    Directory.CreateDirectory(path: filePath);

                    _excel = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/extfiles/products/Excel_") + _excel + _ext;
                    _excel = "Excel_" + _excel + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    excel.SaveAs(path);

                    // resizing image
                    _excel = UploadToAzure(_comPath, _excel);

                    // end resize
                }
            }
            return Json(Convert.ToString(_excel), JsonRequestBehavior.AllowGet);
        }




        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadProdDetImage()
        {
            string _imgname = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["ProdDetImg"];
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    var filePath = Server.MapPath("/extfiles/products/");

                    Directory.CreateDirectory(path: filePath);

                    _imgname = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/extfiles/products/ProductDetail_") + _imgname + _ext;
                    _imgname = "ProductDetail_" + _imgname + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width > 350)
                        img.Resize(360 * 2, 196 * 2);
                    img.Save(_comPath);

                    _imgname = UploadToAzure(_comPath, _imgname);
                    // end resize
                }
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteProductExchange(int id)
        {
            if (serviceObject.DeleteProductExchange(id))
            {
                return Json(true);
            }
            return Json(true);
        }

        [HttpPost]
        public JsonResult DeleteProduct(int id)
        {

            sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {
                if (serviceObject.DeleteProduct(id, adminSessionInfo.UserId))
                {
                    return Json(true);
                }
            }
            return Json(true);
        }

        public void BindPositions(int? ProdId)
        {
            var CurrentList = new List<SelectListItem>();
            var positionlist = new List<SelectListItem>();
            for (var i = 1; i < 100; i++)
                positionlist.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            CurrentList = serviceObject.GetAvailablePositions(ProdId);
            positionlist.RemoveAll(x => CurrentList.Exists(y => y.Value == x.Value));
            ViewBag.vbPositionlist = positionlist;
        }

        public ActionResult DetailProduct(int? productId)
        {
            try
            {
                ProductModel model = new ProductModel();
                ProductAndPriceList productAndPrice = new ProductAndPriceList();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var convid = productId ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.GetProduct(convid);
                        var vp = serviceObject.ListPrices(convid);
                        if (vm != null)
                        {
                            productAndPrice.product = vm;
                            productAndPrice.priceList = vp;
                            //List<DetailPrice> priceList = new List<DetailPrice>();
                            //priceList = detailPriceObject.ListPrices();

                            BindPositions(vm.ProdID);
                            return View(productAndPrice);
                        }
                        else
                        {
                            BindPositions(null);
                            return View(model);
                        }
                    }
                    else
                    {
                        BindPositions(null);
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products - Get" });
            }
        }

        public ActionResult DownloadTemplate()
        {
            String fileName = "Template Pengisian Harga.xlsx";
            ExcelPackage pck = new ExcelPackage(new System.IO.FileInfo(fileName));

            var ws = pck.Workbook.Worksheets.Add("List Kota");
            ws.Cells[1, 1].Value = "ID";
            ws.Cells[1, 2].Value = "ID KOTA";
            ws.Cells[1, 3].Value = "NAMA KOTA";
            ws.Cells[1, 4].Value = "HARGA NORMAL";
            ws.Cells[1, 5].Value = "HARGA PROMO";
            ws.Cells[1, 6].Value = "HARGA REFILL";
            ws.Cells[1, 7].Value = "HARGA REFILL PROMO";


            int startRow = 2;

            var db = new lpgdevEntities();
            var cities = db.MCities.ToList();

            foreach (var item in cities)
            {
                ws.Cells[startRow, 1].Value = item.ID;
                ws.Cells[startRow, 2].Value = item.CityCode;
                ws.Cells[startRow, 3].Value = item.CityName;
                ws.Cells[startRow, 4].Value = 0;
                ws.Cells[startRow, 5].Value = 0;
                ws.Cells[startRow, 6].Value = 0;
                ws.Cells[startRow, 7].Value = 0;
                startRow++;
            }

            this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            this.Response.AddHeader(
                      "content-disposition",
                      string.Format("attachment;  filename={0}", fileName));
            this.Response.BinaryWrite(pck.GetAsByteArray());
            //pck.Save();

            return null;
        }


        public ActionResult DownloadTemplateEdit(int? productID)
        {
            String fileName = "Template Pengisian Harga.xlsx";
            ExcelPackage pck = new ExcelPackage(new System.IO.FileInfo(fileName));

            var ws = pck.Workbook.Worksheets.Add("List Kota");
            ws.Cells[1, 1].Value = "ID";
            ws.Cells[1, 2].Value = "ID KOTA";
            ws.Cells[1, 3].Value = "NAMA KOTA";
            ws.Cells[1, 4].Value = "HARGA NORMAL";
            ws.Cells[1, 5].Value = "HARGA PROMO";
            ws.Cells[1, 6].Value = "HARGA REFILL";
            ws.Cells[1, 7].Value = "HARGA REFILL PROMO";


            int startRow = 2;

            var db = new lpgdevEntities();
            List<DetailPriceModel> priceList = new List<DetailPriceModel>();
            var cities = db.MCities.ToList();
            var convid = productID ?? 0;
            var vp = serviceObject.ListPrices(convid);

            Dictionary<int, DetailPriceModel> hashMap = new Dictionary<int, DetailPriceModel>();

            foreach (var v in vp)
            {
                hashMap[v.CityID] = v;
            }

            foreach (var city in cities)
            {
                MCity objectCity = new MCity();
                objectCity.CityCode = city.CityCode;
                objectCity.CityName = city.CityName;

                if (!hashMap.ContainsKey(city.ID))
                {
                    DetailPriceModel newmod = new DetailPriceModel();
                    newmod.CityID = city.ID;
                    newmod.TubeNormalPrice = 0;
                    newmod.TubePromoPrice = 0;
                    newmod.RefillNormalPrice = 0;
                    newmod.RefillPromoPrice = 0;
                    newmod.MCity = objectCity;
                    hashMap[city.ID] = newmod;
                }
                else
                {
                    hashMap[city.ID].MCity = objectCity; 
                }
            }

            foreach(KeyValuePair<int,DetailPriceModel> item in hashMap)
            {
                ws.Cells[startRow, 1].Value = item.Value.CityID;
                ws.Cells[startRow, 2].Value = item.Value.MCity.CityCode;
                ws.Cells[startRow, 3].Value = item.Value.MCity.CityName;
                ws.Cells[startRow, 4].Value = item.Value.TubeNormalPrice;
                ws.Cells[startRow, 5].Value = item.Value.TubePromoPrice;
                ws.Cells[startRow, 6].Value = item.Value.RefillNormalPrice;
                ws.Cells[startRow, 7].Value = item.Value.RefillPromoPrice;
                startRow++;
            }

            //foreach (var item in hashMap)
            //{
            //    ws.Cells[startRow, 1].Value = item.ID;
            //    ws.Cells[startRow, 2].Value = item.CityCode;
            //    ws.Cells[startRow, 3].Value = item.CityName;
            //    ws.Cells[startRow, 4].Value = 0;
            //    ws.Cells[startRow, 5].Value = 0;
            //    ws.Cells[startRow, 6].Value = 0;
            //    ws.Cells[startRow, 7].Value = 0;
            //    startRow++;
            //}

            this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            this.Response.AddHeader(
                      "content-disposition",
                      string.Format("attachment;  filename={0}", fileName));
            this.Response.BinaryWrite(pck.GetAsByteArray());
            //pck.Save();

            return null;
        }

        public ActionResult EditPrice(int? detailPriceId)
        {
            try
            {
                DetailPriceModel model = new DetailPriceModel();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var convid = detailPriceId ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.GetDetailPrice(convid);
                        if (vm != null)
                        {
                            model = vm;
                            BindPositions(vm.ID);
                            return View(model);
                        }
                        else
                        {
                            BindPositions(null);
                            return View(model);
                        }
                    }
                    else
                    {
                        BindPositions(null);
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Detail Price - Get" });
            }
        }

        [HttpPost]
        public ActionResult EditPrice(DetailPriceModel _dataModel)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    var reply = serviceObject.SavePrice(_dataModel, (Int16)adminSessionInfo.UserId);
                    return RedirectToAction("DetailProduct", new { productId = _dataModel.ProductID });
                }

                return RedirectToAction("ListProduct");
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Price Add/Update" });
            }

        }
    }


}
