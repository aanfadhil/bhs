﻿namespace LPG.Models
{
    public class MSuperUserTypeModel
    {
        public short userTypeId { get; set; }
        public string type_name { get; set; }
    }
}