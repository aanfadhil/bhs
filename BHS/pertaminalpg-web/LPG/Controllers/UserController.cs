﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class UserController : AbstractController<IUserService>
    {
        SessionManagement sessionManagement;
        //UserService serviceObject = new UserService();
        private IAgencyService agencyObject;// = new AgencyService();
        private IDistributionService distObject;// = new DistributionService();
        private IDriverService driverObject;
        private IAgentBossService bossObject;

        //public UserController() : base()
        //{

        //}

        public UserController(IUserService _serviceObject,
            IAgencyService _agencyObject,
            IDistributionService _distObject,
            IDriverService _driverObject,
            IAgentBossService _bossObject) : base(_serviceObject)
        {
            agencyObject = _agencyObject;
            distObject = _distObject;
            driverObject = _driverObject;
            bossObject = _bossObject;
        }

        // GET: User
        public ActionResult AddUser(int? id)
        {
            try
            {
                UserRegistrtaionModel model = new UserRegistrtaionModel();

                sessionManagement = new SessionManagement();
                var agencyList = agencyObject.ListComapany();
                var distList = distObject.ListDistribution();
                var regionList = serviceObject.ListRegion();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var convid = id ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.getAgencyDetails(convid);
                        if (vm != null)
                        {
                            vm.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                            vm.DistributionList = distList.Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();
                            vm.RegionList = regionList.Select(x => new SelectListItem { Text = x.RegionCodeName.ToString(), Value = x.RegionId.ToString() }).ToList();
                            return View(vm);
                        }
                        else
                        {
                            model.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                            model.DistributionList = new List<SelectListItem>();// distList.Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();
                            model.RegionList = regionList.Select(x => new SelectListItem { Text = x.RegionName.ToString(), Value = x.RegionId.ToString() }).ToList();
                            return View(model);
                        }
                    }
                    else
                    {
                        model.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        model.DistributionList = new List<SelectListItem>();//distList.Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();
                        model.RegionList = regionList.Select(x => new SelectListItem { Text = x.RegionCodeName.ToString(), Value = x.RegionId.ToString() }).ToList();
                        return View(model);

                    }
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { area = "Admin" });
                }

            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }
        }
        [HttpPost]
        public ActionResult AddUser(string Cmd, UserRegistrtaionModel vm)
        {

            try
            {
                //UserService serviceObject = new UserService();
                //AgencyService agencyObject = new AgencyService();
                //DistributionService distObject = new DistributionService();
                //DriverService driverObject = new DriverService();

                /*Bind DropDown*/
                var agencyBossList = agencyObject.ListComapany();
                vm.AgencyList = agencyBossList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();

                var agencyAdminList = distObject.ListDistribution();
                // vm.DistributionList = agencyAdminList.Select(c => new SelectListItem { Text = c.CompanyAgent.ToString(), Value = c.DistributionId.ToString() }).ToList();
                vm.DistributionList = agencyAdminList.Select(c => new SelectListItem { Text = c.DistributionPoint.ToString(), Value = c.DistributionId.ToString() }).ToList();

                var agentDriverList = driverObject.ListDriver();
                vm.DriverList = agentDriverList.Select(c => new SelectListItem { Text = c.DriverName.ToString(), Value = c.DriverId.ToString() }).ToList();

                var regionList = this.serviceObject.ListRegion();
                vm.RegionList = regionList.Select(c => new SelectListItem { Text = c.RegionName.ToString() + c.RegionCode.ToString(), Value = c.RegionId.ToString() }).ToList();

                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    if (Cmd == "btnAgency")
                    {
                        ViewBag.SelTab = 0;

                        if (ModelState["SoldTono"].Errors.Count == 0 && ModelState["AgencyName"].Errors.Count == 0 && ModelState["RegionId"].Errors.Count == 0)
                        {
                            foreach (ModelState modelState in ViewData.ModelState.Values)
                            {
                                if (modelState.Errors.Count > 0)
                                {
                                    modelState.Errors.Clear();
                                }
                            }

                        }
                        if (ModelState.IsValid)
                        {
                            var returnData = serviceObject.saveAgency(vm);
                            if (returnData.IsAgencyExists)
                            {
                                var regionsList = this.serviceObject.ListRegion();
                                vm.RegionList = regionList.Select(c => new SelectListItem { Text = c.RegionName.ToString(), Value = c.RegionId.ToString() }).ToList();
                                return View(returnData);
                            }
                            else
                            {
                                SetFlash(FlashMessageType.Success, "Input agen berhasil");
                                return RedirectToAction("ListCompany", "Company");
                            }
                        }

                        return View(vm);
                    }
                    else if (Cmd == "btnDistribution")
                    {
                        ViewBag.SelTab = 1;
                        if (ModelState["AgenDistAgencyId"].Errors.Count == 0)
                        {
                            foreach (ModelState modelState in ViewData.ModelState.Values)
                            {
                                if (modelState.Errors.Count > 0)
                                {
                                    modelState.Errors.Clear();
                                }
                            }

                        }
                        if (ModelState.IsValid)
                        {
                            string[] cord = vm.Lat?.Split(',');
                            if (cord.Count() == 2)
                            {
                                vm.Lat = cord[0];
                                vm.Long = cord[1];
                            }

                            var returnData = serviceObject.saveDistribution(vm);
                            if (returnData.IsDistributionExixts)
                            {
                                return View(returnData);
                            }
                            else
                            {
                                SetFlash(FlashMessageType.Success, "Input titik distribusi berhasil");
                                return RedirectToAction("ListDistribution", "Distribution");
                            }
                        }
                        else
                        {

                            return View(vm);
                        }
                    }
                    else if (Cmd == "btnAgentboss")
                    {

                        ViewBag.SelTab = 2;

                        if (ModelState["ABAgencyId"].Errors.Count == 0 && ModelState["AgencyBossMobile"].Errors.Count == 0)
                        {
                            foreach (ModelState modelState in ViewData.ModelState.Values)
                            {
                                if (modelState.Errors.Count > 0)
                                {
                                    modelState.Errors.Clear();
                                }
                            }

                        }

                        if (ModelState.IsValid)
                        {

                            if (bossObject.IsExistsMobileNo(vm.CountryCode + vm.AgencyBossMobile, 0))
                            {
                                vm.ErrorMessage = MessageSource.GetMessage("Mobile.exists");

                                return View(vm);
                            }
                            else
                            {
                                var returnData = serviceObject.saveAgentBoss(vm);
                                if (returnData.IsAgentBossExists)
                                {
                                    return View(returnData);
                                }
                                else
                                {
                                    SetFlash(FlashMessageType.Success, "Input agen bos berhasil");
                                    return RedirectToAction("ListAgentBoss", "AgentBoss");
                                }
                            }
                        }
                        else
                        {

                            return View(vm);
                        }
                    }
                    else if (Cmd == "btnAgentAdmin")
                    {

                        if (serviceObject.IsExistsMobileNo(vm.AgnAdminMObile, 0))
                        {

                            vm.ErrorMessage = MessageSource.GetMessage("Mobile.exists");
                            ViewBag.SelTab = 3;
                            return View(vm);
                        }

                        if (!string.IsNullOrEmpty(vm.AgentAdminEmail))
                        {
                            string emailRegex = @"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";
                            Regex re = new Regex(emailRegex);
                            if (!re.IsMatch(vm.AgentAdminEmail))
                            {
                                ViewBag.SelTab = 3;
                                return View(vm);
                            }
                        }
                        if (vm.AgenAdAgenId == 0)
                        {
                            //vm.ErrorMessage = MessageSource.GetMessage("Agent.Admin.Required");
                            ViewBag.SelTab = 3;
                            return View(vm);
                        }
                        if (vm.AgentDistId == 0)
                        {
                            //vm.ErrorMessage = MessageSource.GetMessage("required.Distribution");
                            ViewBag.SelTab = 3;
                            return View(vm);
                        }


                        if (serviceObject.IsExistsEmail(vm.AgentAdminEmail, 0))
                        {

                            vm.ErrorMessage = MessageSource.GetMessage("Email.exists");
                            ViewBag.SelTab = 3;
                            return View(vm);
                        }
                        else
                        {
                            ViewBag.SelTab = 3;
                            var returnData = serviceObject.saveAgentAdmin(vm);
                            if (returnData.IsAgentAdminExists)
                            {

                                return View(returnData);
                            }
                            else
                            {

                                SetFlash(FlashMessageType.Success, "Input admin agen berhasil");
                                return RedirectToAction("ListAgentAdmin", "AgentAdmin");
                            }
                        }


                    }
                    else if (Cmd == "btnDriver")
                    {
                        ViewBag.SelTab = 4;
                        if (ModelState["AgenDriverDistId"].Errors.Count == 0 && ModelState["AgenDriverAgenId"].Errors.Count == 0)
                        {
                            foreach (ModelState modelState in ViewData.ModelState.Values)
                            {
                                if (modelState.Errors.Count > 0)
                                {
                                    modelState.Errors.Clear();
                                }
                            }

                        }

                        if (ModelState.IsValid)
                        {

                            if (driverObject.IsExistsMobileNo(vm.CountryCode + vm.AgenDriverMobileNo, 0))
                            {

                                vm.ErrorMessage = MessageSource.GetMessage("Mobile.exists");
                                ViewBag.SelTab = 4;
                                return View(vm);
                            }
                            else
                            {
                                ViewBag.SelTab = 4;
                                var returnData = serviceObject.saveDriver(vm);
                                if (returnData.IsAgentDriverExixts)
                                {
                                    return View(returnData);
                                }
                                else
                                {

                                    SetFlash(FlashMessageType.Success, "Input driver berhasil");

                                    return RedirectToAction("ListDriver", "Driver");
                                }
                            }

                        }
                        else
                        {

                            return View(vm);
                        }

                    }
                    return RedirectToAction("AddUser", "User");
                }

                return View(vm);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }
        }

        //[HttpPost]
        //public ActionResult AddMultiDriver(UserRegistrtaionModel vm)
        //{
        //    ViewBag.SelTab = 4;
        //    if (ModelState["AgenDriverDistId"].Errors.Count == 0 && ModelState["AgenDriverAgenId"].Errors.Count == 0)
        //    {
        //        foreach (ModelState modelState in ViewData.ModelState.Values)
        //        {
        //            if (modelState.Errors.Count > 0)
        //            {
        //                modelState.Errors.Clear();
        //            }
        //        }

        //    }

        //    if (ModelState.IsValid)
        //    {

        //        if (driverObject.IsExistsMobileNo(vm.CountryCode + vm.AgenDriverMobileNo, 0))
        //        {

        //            vm.ErrorMessage = MessageSource.GetMessage("Mobile.exists");
        //            ViewBag.SelTab = 4;
        //            return View(vm);
        //        }
        //        else
        //        {
        //            ViewBag.SelTab = 4;
        //            var returnData = serviceObject.saveDriver(vm);
        //            if (returnData.IsAgentDriverExixts)
        //            {
        //                return View(returnData);
        //            }
        //            else
        //            {
        //                return RedirectToAction("ListDriver", "Driver");
        //            }
        //        }

        //    }
        //}
    }
}
