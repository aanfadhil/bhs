﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class CompanyController : AbstractController<IAgencyService>
    {
        SessionManagement sessionManagement;
        private UserService userObject;

        public CompanyController(IAgencyService _serviceObject, UserService _userObject) : base(_serviceObject)
        {
            userObject = _userObject;
        }

        //AgencyService serviceObject = new AgencyService();
        // GET: Company
        public ActionResult ListCompany()
        {
            var vm = new CompanyModel();
            //AgencyService serviceObject = new AgencyService();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.CompanyList = serviceObject.ListComapany();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(vm);
        }

        public ActionResult AddCompany(int? id)
        {
            try
            {
                CompanyModel model = new CompanyModel();


                sessionManagement = new SessionManagement();
                var userList = userObject.ListRegion();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var convid = id ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.getCompanyDetails(convid);
                        if (vm != null)
                        {
                            vm.RegionList = userList.Select(c => new SelectListItem { Text = c.RegionName.ToString(), Value = c.RegionId.ToString() }).ToList();
                            return View(vm);
                        }
                        else
                        {
                            model.RegionList = userList.Select(c => new SelectListItem { Text = c.RegionName.ToString(), Value = c.RegionId.ToString() }).ToList();
                            return View(model);
                        }
                    }
                    else
                    {
                        model.RegionList = userList.Select(c => new SelectListItem { Text = c.RegionName.ToString(), Value = c.RegionId.ToString() }).ToList();
                        return View(model);

                    }

                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public ActionResult AddCompany(CompanyModel vm)
        {

            try
            {
                sessionManagement = new SessionManagement();
                //UserService userObject = new UserService();
                var userList = userObject.ListRegion();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    if (vm.RegionId == 0)
                    {
                        vm.RegionList = userList.Select(c => new SelectListItem { Text = c.RegionName.ToString(), Value = c.RegionId.ToString() }).ToList();
                        vm.errorMessage = MessageSource.GetMessage("required.data");
                        return View(vm);
                    }

                    serviceObject.saveCompanyDetails(vm, adminSessionInfo.UserId);

                    SetFlash(FlashMessageType.Success, "Input agen berhasil");

                    return RedirectToAction("ListCompany", "Company");

                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }
        }

        public ActionResult DeleteCompany(int id)
        {
            sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {
                if (serviceObject.deleteAgency(id, adminSessionInfo.UserId))
                {
                    //return View();
                    return RedirectToAction("ListCompany", "Company");
                }

            }
            return RedirectToAction("Index", "Error");
        }

    }
}
