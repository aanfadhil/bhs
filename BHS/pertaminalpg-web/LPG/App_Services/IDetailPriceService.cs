﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IDetailPriceService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        List<DetailPrice> ListPrices();
        DetailPriceModel GetDetailPrice(int detailPriceId);
        DetailPriceModel SavePrice(DetailPriceModel model, short loggeduserId);
    }
}