﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LPG.Models
{
    public class ProductExchangeList
    {
        public int exchanhe_id { get; set; }
        [Required(ErrorMessage = "Exchange with required")]
        public string exchange_with { get; set; }
        [Required(ErrorMessage = "Exchange quantity required")]
       
        public int exchange_quantity { get; set; }
        [Required(ErrorMessage = "Exchange price required")]
        [RegularExpression(@"^(0|-?\d{0,18}(\.\d{0,3})?)$")]
        public decimal exchange_price { get; set; }
        [Required(ErrorMessage = "Exchange promo price required")]
        [RegularExpression(@"^(0|-?\d{0,18}(\.\d{0,3})?)$")]
        public decimal exchange_promo_price { get; set; }
        //public short CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public short UpdatedBy { get; set; }
        //public DateTime UpdatedDate { get; set; }
    }
}