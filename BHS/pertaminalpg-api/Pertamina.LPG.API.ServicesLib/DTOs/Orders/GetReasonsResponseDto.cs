﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Orders
{
    public class GetReasonsResponseDto
    {
        public List<ReasonDto> service_reason_rating { get; set; }
    }

    public class ReasonDto
    {
        public int? reason_id { get; set; }
        public string reason { get; set; }
    }
}
