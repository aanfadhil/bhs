﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using LPG.Reporting.DTOs.Consumers;

namespace LPG.Reporting.DTOs.Orders
{
    public class TeleOrderDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int TeleOrdID { get; set; }
        public int AgadmID { get; set; }
        public int? DrvrID { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public TimeSpan OrderTime { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public short? DeliverySlotID { get; set; }
        public int NumberOfProducts { get; set; }
        public decimal SubTotal { get; set; }
        public decimal? PromoProduct { get; set; }
        public decimal? ShippingCharge { get; set; }
        public decimal? PromoShipping { get; set; }
        public decimal GrantTotal { get; set; }
        public bool DeliveryType { get; set; }
        public DateTime? DeliveryStartDate { get; set; }
        public DateTime? DeliveredDate { get; set; }
        public int Deviation { get; set; }
        public short StatusId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public decimal RefillSubTotal { get; set; }
        public decimal PromoRefill { get; set; }
        public decimal ExchangeSubTotal { get; set; }
        public decimal PromoExchange { get; set; }
        public IEnumerable<TeleCustomerDTO> TeleCustomers { get; set; }
        public IEnumerable<TeleOrderDeliveryDTO> TeleOrderDeliveries { get; set; }
        public IEnumerable<TeleOrderDetailDTO> TeleOrderDetails { get; set; }
        public IEnumerable<TeleOrderPrdocuctExchangeDTO> TeleOrderPrdocuctExchanges { get; set; }
    }

    public class TeleOrderMapper : MapperBase<TeleOrder, TeleOrderDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private TeleCustomerMapper _teleCustomerMapper = new TeleCustomerMapper();
        private TeleOrderDeliveryMapper _teleOrderDeliveryMapper = new TeleOrderDeliveryMapper();
        private TeleOrderDetailMapper _teleOrderDetailMapper = new TeleOrderDetailMapper();
        private TeleOrderPrdocuctExchangeMapper _teleOrderPrdocuctExchangeMapper = new TeleOrderPrdocuctExchangeMapper();
        public override Expression<Func<TeleOrder, TeleOrderDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<TeleOrder, TeleOrderDTO>>) (p => new TeleOrderDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    TeleOrdID = p.TeleOrdID,
                    AgadmID = p.AgadmID,
                    DrvrID = p.DrvrID,
                    InvoiceNumber = p.InvoiceNumber,
                    OrderDate = p.OrderDate,
                    OrderTime = p.OrderTime,
                    DeliveryDate = p.DeliveryDate,
                    DeliverySlotID = p.DeliverySlotID,
                    NumberOfProducts = p.NumberOfProducts,
                    SubTotal = p.SubTotal,
                    PromoProduct = p.PromoProduct,
                    ShippingCharge = p.ShippingCharge,
                    PromoShipping = p.PromoShipping,
                    GrantTotal = p.GrantTotal,
                    DeliveryType = p.DeliveryType,
                    DeliveryStartDate = p.DeliveryStartDate,
                    DeliveredDate = p.DeliveredDate,
                    Deviation = p.Deviation,
                    StatusId = p.StatusId,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate,
                    RefillSubTotal = p.RefillSubTotal,
                    PromoRefill = p.PromoRefill,
                    ExchangeSubTotal = p.ExchangeSubTotal,
                    PromoExchange = p.PromoExchange,
                    TeleCustomers = p.TeleCustomers.AsQueryable().Select(this._teleCustomerMapper.SelectorExpression),
                    TeleOrderDeliveries = p.TeleOrderDeliveries.AsQueryable().Select(this._teleOrderDeliveryMapper.SelectorExpression),
                    TeleOrderDetails = p.TeleOrderDetails.AsQueryable().Select(this._teleOrderDetailMapper.SelectorExpression),
                    TeleOrderPrdocuctExchanges = p.TeleOrderPrdocuctExchanges.AsQueryable().Select(this._teleOrderPrdocuctExchangeMapper.SelectorExpression)
                }));
            }
        }

        public override void MapToModel(TeleOrderDTO dto, TeleOrder model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.TeleOrdID = dto.TeleOrdID;
            model.AgadmID = dto.AgadmID;
            model.DrvrID = dto.DrvrID;
            model.InvoiceNumber = dto.InvoiceNumber;
            model.OrderDate = dto.OrderDate;
            model.OrderTime = dto.OrderTime;
            model.DeliveryDate = dto.DeliveryDate;
            model.DeliverySlotID = dto.DeliverySlotID;
            model.NumberOfProducts = dto.NumberOfProducts;
            model.SubTotal = dto.SubTotal;
            model.PromoProduct = dto.PromoProduct;
            model.ShippingCharge = dto.ShippingCharge;
            model.PromoShipping = dto.PromoShipping;
            model.GrantTotal = dto.GrantTotal;
            model.DeliveryType = dto.DeliveryType;
            model.DeliveryStartDate = dto.DeliveryStartDate;
            model.DeliveredDate = dto.DeliveredDate;
            model.Deviation = dto.Deviation;
            model.StatusId = dto.StatusId;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;
            model.RefillSubTotal = dto.RefillSubTotal;
            model.PromoRefill = dto.PromoRefill;
            model.ExchangeSubTotal = dto.ExchangeSubTotal;
            model.PromoExchange = dto.PromoExchange;

        }
    }
}
