﻿namespace LPG.App_Services
{
    public interface IServiceBase
    {
        int? UserId { get; }
    }
}