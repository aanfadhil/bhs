﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace LPG.Reporting.DTOs.SystemActors
{
    [DataContract]
    public class AgentAdminDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public int AgadmID { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string AgentAdminName { get; set; }
        [DataMember]
        public int AgenID { get; set; }
        [DataMember]
        public int DbptID { get; set; }
        [DataMember]
        [Required]
        [StringLength(20)]
        public string MobileNumber { get; set; }
        [DataMember]
        [StringLength(255)]
        public string ProfileImage { get; set; }
        [DataMember]
        public string AppToken { get; set; }
        [DataMember]
        public DateTime? LastLogin { get; set; }
        [DataMember]
        [StringLength(50)]
        public string Password { get; set; }
        [DataMember]
        public bool StatusId { get; set; }
        [DataMember]
        public short CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public short UpdatedBy { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        [StringLength(64)]
        public string AccToken { get; set; }
        [DataMember]
        [StringLength(255)]
        public string AppID { get; set; }
        [DataMember]
        [StringLength(64)]
        public string email { get; set; }
    }

    public class AgentAdminMapper : MapperBase<AgentAdmin, AgentAdminDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<AgentAdmin, AgentAdminDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<AgentAdmin, AgentAdminDTO>>) (p => new AgentAdminDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    AgadmID = p.AgadmID,
                    AgentAdminName = p.AgentAdminName,
                    AgenID = p.AgenID,
                    DbptID = p.DbptID,
                    MobileNumber = p.MobileNumber,
                    ProfileImage = p.ProfileImage,
                    AppToken = p.AppToken,
                    LastLogin = p.LastLogin,
                    Password = p.Password,
                    StatusId = p.StatusId,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate,
                    AccToken = p.AccToken,
                    AppID = p.AppID,
                    email = p.email
                }));
            }
        }

        public override void MapToModel(AgentAdminDTO dto, AgentAdmin model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.AgadmID = dto.AgadmID;
            model.AgentAdminName = dto.AgentAdminName;
            model.AgenID = dto.AgenID;
            model.DbptID = dto.DbptID;
            model.MobileNumber = dto.MobileNumber;
            model.ProfileImage = dto.ProfileImage;
            model.AppToken = dto.AppToken;
            model.LastLogin = dto.LastLogin;
            model.Password = dto.Password;
            model.StatusId = dto.StatusId;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;
            model.AccToken = dto.AccToken;
            model.AppID = dto.AppID;
            model.email = dto.email;

        }
    }
}
