﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace LPG.Reporting.DTOs.SystemActors
{
    [DataContract]
    public class AgentBossDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public int AbosID { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string OwnerName { get; set; }
        [DataMember]
        public int AgenID { get; set; }
        [DataMember]
        [Required]
        [StringLength(20)]
        public string MobileNumber { get; set; }
        [DataMember]
        [StringLength(255)]
        public string ProfileImage { get; set; }
        [DataMember]
        public string AppToken { get; set; }
        [DataMember]
        [StringLength(50)]
        public string Password { get; set; }
        [DataMember]
        public DateTime? LastLogin { get; set; }
        [DataMember]
        public bool StatusId { get; set; }
        [DataMember]
        public short CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public short UpdatedBy { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        [StringLength(64)]
        public string AccToken { get; set; }
        [DataMember]
        [StringLength(255)]
        public string AppID { get; set; }
        [DataMember]
        [StringLength(255)]
        public string Email { get; set; }
    }

    public class AgentBossMapper : MapperBase<AgentBoss, AgentBossDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<AgentBoss, AgentBossDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<AgentBoss, AgentBossDTO>>) (p => new AgentBossDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    AbosID = p.AbosID,
                    OwnerName = p.OwnerName,
                    AgenID = p.AgenID,
                    MobileNumber = p.MobileNumber,
                    ProfileImage = p.ProfileImage,
                    AppToken = p.AppToken,
                    Password = p.Password,
                    LastLogin = p.LastLogin,
                    StatusId = p.StatusId,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate,
                    AccToken = p.AccToken,
                    AppID = p.AppID,
                    Email = p.Email
                }));
            }
        }

        public override void MapToModel(AgentBossDTO dto, AgentBoss model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.AbosID = dto.AbosID;
            model.OwnerName = dto.OwnerName;
            model.AgenID = dto.AgenID;
            model.MobileNumber = dto.MobileNumber;
            model.ProfileImage = dto.ProfileImage;
            model.AppToken = dto.AppToken;
            model.Password = dto.Password;
            model.LastLogin = dto.LastLogin;
            model.StatusId = dto.StatusId;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;
            model.AccToken = dto.AccToken;
            model.AppID = dto.AppID;
            model.Email = dto.Email;

        }
    }
}
