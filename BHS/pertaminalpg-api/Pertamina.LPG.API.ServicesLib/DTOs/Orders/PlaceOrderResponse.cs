﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pertamina.LPG.API.DTOs.Orders
{
    public class PlaceOrderResponse : ResponseDto
    {
        public PlaceOrderRespOrderDto order_details { get; set; }
        public string keterangan_voucher { get; set; }
        public decimal potongan { get; set; }
        public bool is_voucher_valid { get; set; }
    }
}