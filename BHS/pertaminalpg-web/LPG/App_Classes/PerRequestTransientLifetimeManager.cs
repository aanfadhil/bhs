﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unity.AspNet.Mvc;
using Unity.Lifetime;

namespace LPG.App_Classes
{
    public class PerRequestTransientLifetimeManager : ILifetimePolicy
    {
        public object GetValue(ILifetimeContainer container = null)
        {
            // will always create a new object (Transient)
            return null;
        }

        public void SetValue(object newValue, ILifetimeContainer container = null)
        {
            // No point in saving to http context if not disposable
            if (newValue is IDisposable)
            {
                var perRequestLifetimeManager = new PerRequestLifetimeManager();
                perRequestLifetimeManager.SetValue(newValue);
            }
        }

        public void RemoveValue(ILifetimeContainer container = null)
        {
            //throw new NotImplementedException();
        }
    }
}
