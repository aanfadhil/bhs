﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBG.Shared.Exceptions
{
    internal class PublishConfiguration
    {
        public string AppName { get; set; }

        public FileModeConfiguration FileMode { get; set; }

        public EmailModeConfiguration EmailMode { get; set; }

        public DbModeConfiguration DbMode { get; set; }
    }

    internal class FileModeConfiguration
    {
        public string DirectoryPath { get; set; }

        public string FileName { get; set; }
    }

    internal class EmailModeConfiguration
    {
        public string[] EmailId { get; set; }

        public string FromAddress { get; set; }

        public SMTPConfiguration SMTP { get; set; }
    }

    internal class SMTPConfiguration
    {
        public string Url { get; set; }

        public int Port { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }

    internal class DbModeConfiguration
    {
        public string ConnectionString { get; set; }
    }
}
