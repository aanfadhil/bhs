﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IDistributionService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        bool DeleteDistribution(int Id, int loggedBy);
        DistributionModel getDistributionDetails(int distributionId);
        List<DistributionModel> GetByAgency(int agenId);
        List<DistributionModel> ListDistribution();
        DistributionModel saveDistributionDetails(DistributionModel entity, int loggedBy);
    }
}