﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.Orders
{
    public class InvoiceNumberDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int InvnrID { get; set; }
        public int InvoiceNumber1 { get; set; }
        public int AgenID { get; set; }
    }

    public class InvoiceNumberMapper : MapperBase<InvoiceNumber, InvoiceNumberDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<InvoiceNumber, InvoiceNumberDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<InvoiceNumber, InvoiceNumberDTO>>) (p => new InvoiceNumberDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    InvnrID = p.InvnrID,
                    InvoiceNumber1 = p.InvoiceNumber1,
                    AgenID = p.AgenID
                }));
            }
        }

        public override void MapToModel(InvoiceNumberDTO dto, InvoiceNumber model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.InvnrID = dto.InvnrID;
            model.InvoiceNumber1 = dto.InvoiceNumber1;
            model.AgenID = dto.AgenID;

        }
    }
}
