﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace LPG.Reporting.DTOs.SystemActors
{
    [DataContract]
    public class SuperAdminDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public short SAdminID { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string FullName { get; set; }
        [DataMember]
        [Required]
        [StringLength(20)]
        public string MobileNum { get; set; }
        [DataMember]
        [Required]
        [StringLength(50)]
        public string Password { get; set; }
        [DataMember]
        [StringLength(255)]
        public string ProfileImage { get; set; }
        [DataMember]
        public string AppToken { get; set; }
        [DataMember]
        public DateTime? LastLogin { get; set; }
        [DataMember]
        public short UserTypeID { get; set; }
        [DataMember]
        public bool StatusID { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string Email { get; set; }
        [DataMember]
        [StringLength(64)]
        public string AccToken { get; set; }
        [DataMember]
        [StringLength(255)]
        public string AppID { get; set; }
        [DataMember]
        public int? CreatedBy { get; set; }
        [DataMember]
        public DateTime? CreatedDate { get; set; }
        [DataMember]
        public int? UpdatedBy { get; set; }
        [DataMember]
        public DateTime? UpdatedDate { get; set; }
    }

    public class SuperAdminMapper : MapperBase<SuperAdmin, SuperAdminDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<SuperAdmin, SuperAdminDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<SuperAdmin, SuperAdminDTO>>) (p => new SuperAdminDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    SAdminID = p.SAdminID,
                    FullName = p.FullName,
                    MobileNum = p.MobileNum,
                    Password = p.Password,
                    ProfileImage = p.ProfileImage,
                    AppToken = p.AppToken,
                    LastLogin = p.LastLogin,
                    UserTypeID = p.UserTypeID,
                    StatusID = p.StatusID,
                    Email = p.Email,
                    AccToken = p.AccToken,
                    AppID = p.AppID,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate
                }));
            }
        }

        public override void MapToModel(SuperAdminDTO dto, SuperAdmin model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.SAdminID = dto.SAdminID;
            model.FullName = dto.FullName;
            model.MobileNum = dto.MobileNum;
            model.Password = dto.Password;
            model.ProfileImage = dto.ProfileImage;
            model.AppToken = dto.AppToken;
            model.LastLogin = dto.LastLogin;
            model.UserTypeID = dto.UserTypeID;
            model.StatusID = dto.StatusID;
            model.Email = dto.Email;
            model.AccToken = dto.AccToken;
            model.AppID = dto.AppID;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;

        }
    }
}
