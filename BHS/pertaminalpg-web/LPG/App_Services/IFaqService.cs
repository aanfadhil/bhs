﻿using System.Collections.Generic;
using System.Web.Mvc;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IFaqService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        bool deleteFaqDetails(int faqId, int loggedBy);
        List<FaqModel> getActiveFAQ();
        List<SelectListItem> GetAvailablePositions(int? FaqId);
        FaqModel getFaqDetails(int faqId);
        List<FaqModel> ListFaq();
        FaqModel saveFaqDetails(FaqModel entity, int loggedBy);
    }
}