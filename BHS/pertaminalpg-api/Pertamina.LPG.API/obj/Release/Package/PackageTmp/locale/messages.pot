msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2018-08-27 19:04+07:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: i18n.POTGenerator\n"

#: Disabled references:1
msgid "Active Orders exist"
msgstr ""

#: Disabled references:1
msgid "Address Deleted Successfully"
msgstr ""

#: Disabled references:1
msgid "Address details found"
msgstr ""

#: Disabled references:1
msgid "Address not found"
msgstr ""

#: Disabled references:1
msgid "Address Updated Successfully"
msgstr ""

#: Disabled references:1
msgid "Admin credentials invalid"
msgstr ""

#: Disabled references:1
msgid "Admin details"
msgstr ""

#: Disabled references:1
msgid "Agen"
msgstr ""

#: Disabled references:1
msgid "Agencies Listed."
msgstr ""

#: Disabled references:1
msgid "Agent Admin record not found"
msgstr ""

#: Disabled references:1
msgid "Agent Boss credentials invalid"
msgstr ""

#: Disabled references:1
msgid "Agent Boss record not found"
msgstr ""

#: Disabled references:1
msgid "An error occurred"
msgstr ""

#: Disabled references:1
msgid "Assigned order count"
msgstr ""

#: Disabled references:1
msgid "Blocked User"
msgstr ""

#: Disabled references:1
msgid "Category"
msgstr ""

#: Disabled references:1
msgid "City Updated Successfully"
msgstr ""

#: Disabled references:1
msgid "Contact found"
msgstr ""

#: Disabled references:1
msgid "Could not change password"
msgstr ""

#: Disabled references:1
msgid "Date"
msgstr ""

#: Disabled references:1
msgid "Day after tomorrow"
msgstr ""

#: Disabled references:1
msgid "Details not found"
msgstr ""

#: Disabled references:1
msgid "District List Obtained"
msgstr ""

#: Disabled references:1
msgid "Driver Details"
msgstr ""

#: Disabled references:1
msgid "Driver details obtained"
msgstr ""

#: Disabled references:1
msgid "Drivers listed"
msgstr ""

#: Disabled references:1
msgid "Drivers not found"
msgstr ""

#: Disabled references:1
msgid "Email has been sent"
msgstr ""

#: Disabled references:1
msgid "Email id not found for this user"
msgstr ""

#: Disabled references:1
msgid "E-Receipt found"
msgstr ""

#: Disabled references:1
msgid "e-Reciept sent successfully"
msgstr ""

#: Disabled references:1
msgid "Exchange input not valid"
msgstr ""

#: Disabled references:1
msgid "FAQs listed"
msgstr ""

#: Disabled references:1
msgid "Get full details of a user"
msgstr ""

#: Disabled references:1
msgid "Invalid delivery date. Date should be in yyyy-mm-dd format."
msgstr ""

#: Disabled references:1
msgid "Invalid driver credentials"
msgstr ""

#: Disabled references:1
msgid "Invalid is_agent_admin input. Please provide 1 for agent admin, 0 for agent boss."
msgstr ""

#: Disabled references:1
msgid "Invalid order type. Please enter order type as OrderApp or OrderTelp."
msgstr ""

#: Disabled references:1
msgid "Invalid OTP"
msgstr ""

#: Disabled references:1
msgid "Invalid Super User"
msgstr ""

#: Disabled references:1
msgid "Invalid user type. Please enter 1- Super User, 2 - Agent Boss,3 - Agent Admin,4 - Driver,5 - Consumer"
msgstr ""

#: Disabled references:1
msgid "Invoice Details"
msgstr ""

#: Disabled references:1
msgid "Issue Details Listed"
msgstr ""

#: Disabled references:1
msgid "Jumlah"
msgstr ""

#: Disabled references:1
msgid "Kota"
msgstr ""

#: Disabled references:1
msgid "Login Failed"
msgstr ""

#: Disabled references:1
msgid "Metode Beli"
msgstr ""

#: Disabled references:1
msgid "Monthly Sales Report"
msgstr ""

#: Disabled references:1
msgid "Nama Pelanggan"
msgstr ""

#: Disabled references:1
msgid "New Address Added Successfully"
msgstr ""

#: Disabled references:1
msgid "No Active Order exists"
msgstr ""

#: Disabled references:1
msgid "No contact found"
msgstr ""

#: Disabled references:1
msgid "No distribution near you"
msgstr ""

#: Disabled references:1
msgid "No Invoice"
msgstr ""

#: Disabled references:1
msgid "No order found"
msgstr ""

#: Disabled references:1
msgid "No tele order found"
msgstr ""

#: Disabled references:1
msgid "Note"
msgstr ""

#: Disabled references:1
msgid "Order Cancelled"
msgstr ""

#: Disabled references:1
msgid "Order can't cancel"
msgstr ""

#: Disabled references:1
msgid "Order details"
msgstr ""

#: Disabled references:1
msgid "Order Details"
msgstr ""

#: Disabled references:1
msgid "Order details found."
msgstr ""

#: Disabled references:1
msgid "Order diluar jangkauan"
msgstr ""

#: Disabled references:1
msgid "Order has been delivered"
msgstr ""

#: Disabled references:1
msgid "Order is invalid"
msgstr ""

#: Disabled references:1
msgid "Order placed"
msgstr ""

#: Disabled references:1
msgid "Orders counted"
msgstr ""

#: Disabled references:1
msgid "Orders listed"
msgstr ""

#: Disabled references:1
msgid "Orders Listed"
msgstr ""

#: Disabled references:1
msgid "OTP not sent"
msgstr ""

#: Disabled references:1
msgid "OTP Resent Successfully"
msgstr ""

#: Disabled references:1
msgid "OTP sent successfully"
msgstr ""

#: Disabled references:1
msgid "Password Reset Successfully"
msgstr ""

#: Disabled references:1
msgid "Password Updated Successfully"
msgstr ""

#: Disabled references:1
msgid "Pickup order confirmed"
msgstr ""

#: Disabled references:1
msgid "Pickup order placed"
msgstr ""

#: Disabled references:1
msgid "Product details found"
msgstr ""

#: Disabled references:1
msgid "Products listed"
msgstr ""

#: Disabled references:1
msgid "Produk"
msgstr ""

#: Disabled references:1
msgid "Profile Details Updated Successfully"
msgstr ""

#: Disabled references:1
msgid "Profile is updated"
msgstr ""

#: Disabled references:1
msgid "Promo banner found"
msgstr ""

#: Disabled references:1
msgid "Promo banner not  found"
msgstr ""

#: Disabled references:1
msgid "Promo info found"
msgstr ""

#: Disabled references:1
msgid "Promo info not found"
msgstr ""

#: Disabled references:1
msgid "Rating"
msgstr ""

#: Disabled references:1
msgid "Rating Reason Report"
msgstr ""

#: Disabled references:1
msgid "Reason"
msgstr ""

#: Disabled references:1
msgid "Region"
msgstr ""

#: Disabled references:1
msgid "Registration failed"
msgstr ""

#: Disabled references:1
msgid "Resetting password failed"
msgstr ""

#: Disabled references:1
msgid "Review Posted"
msgstr ""

#: Disabled references:1
msgid "Sales Report"
msgstr ""

#: Disabled references:1
msgid "Status"
msgstr ""

#: Disabled references:1
msgid "Successfully logged in to the system"
msgstr ""

#: Disabled references:1
msgid "Sum of KG"
msgstr ""

#: Disabled references:1
msgid "Superuser credentials invalid"
msgstr ""

#: Disabled references:1
msgid "Superuser details"
msgstr ""

#: Disabled references:1
msgid "Superuser not found"
msgstr ""

#: Disabled references:1
msgid "Tanggal & Waktu Pengiriman"
msgstr ""

#: Disabled references:1
msgid "Tele order detalis not found"
msgstr ""

#: Disabled references:1
msgid "Tele-order confirmed"
msgstr ""

#: Disabled references:1
msgid "Tele-order placed"
msgstr ""

#: Disabled references:1
msgid "There is a problem with your address - coordinates might be empty."
msgstr ""

#: Disabled references:1
msgid "This is your default address. Please try again after updating new default address."
msgstr ""

#: Disabled references:1
msgid "This phone number is already registered"
msgstr ""

#: Disabled references:1
msgid "This user is not registered or active"
msgstr ""

#: Disabled references:1
msgid "Timeslot you've selected has become unavailable. Please retry."
msgstr ""

#: Disabled references:1
msgid "Timeslots Listed"
msgstr ""

#: Disabled references:1
msgid "Today"
msgstr ""

#: Disabled references:1
msgid "Tomorrow"
msgstr ""

#: Disabled references:1
msgid "Total Harga"
msgstr ""

#: Disabled references:1
msgid "Total KG"
msgstr ""

#: Disabled references:1
msgid "User Activated Successfully"
msgstr ""

#: Disabled references:1
msgid "User Address Details"
msgstr ""

#: Disabled references:1
msgid "User Details Found"
msgstr ""

#: Disabled references:1
msgid "User Exists"
msgstr ""

#: Disabled references:1
msgid "User is already Activated"
msgstr ""

#: Disabled references:1
msgid "User not activated"
msgstr ""

#: Disabled references:1
msgid "User Registered Successfully"
msgstr ""

#: Disabled references:1
msgid "Valid OTP"
msgstr ""

#: Disabled references:1
msgid "Value for {0} must be between {1} and {2}"
msgstr ""

#: Disabled references:1
msgid "Voucher tidak valid."
msgstr ""

#: Disabled references:1
msgid "Voucher valid."
msgstr ""

#: Disabled references:1
msgid "Wrong period. Only accept 1 for Month and 2 for Week"
msgstr ""

#: Disabled references:1
msgid "Wrong role"
msgstr ""

#: Disabled references:1
msgid "Your address is not on our coverage area."
msgstr ""

#: Disabled references:1
msgid "Your address is on our coverage area."
msgstr ""

