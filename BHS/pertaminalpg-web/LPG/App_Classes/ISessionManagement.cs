﻿using LPG.Models;

namespace LPG.App_Classes
{
    public interface ISessionManagement
    {
        AdminSessionEntity getAdminSession();
        LoginModel getRememberMe();
        void setAdminSession(AdminSessionEntity adminSessionInfo);
        void setRememberMe(LoginModel adminSessionInfo);
    }
}