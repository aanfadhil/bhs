namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AgentBoss")]
    public partial class AgentBoss
    {
        [Key]
        public int AbosID { get; set; }

        [Required]
        [StringLength(100)]
        public string OwnerName { get; set; }

        public int AgenID { get; set; }

        [Required]
        [StringLength(20)]
        public string MobileNumber { get; set; }

        [StringLength(255)]
        public string ProfileImage { get; set; }

        public string AppToken { get; set; }

        [StringLength(50)]
        public string Password { get; set; }

        public DateTime? LastLogin { get; set; }

        public bool StatusId { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [StringLength(64)]
        public string AccToken { get; set; }

        [StringLength(255)]
        public string AppID { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        public virtual Agency Agency { get; set; }

        public virtual SuperAdmin SuperAdmin { get; set; }
    }
}
