namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            OrderDetails = new HashSet<OrderDetail>();
            OrderPrdocuctExchanges = new HashSet<OrderPrdocuctExchange>();
            ProductExchanges = new HashSet<ProductExchange>();
            Reminders = new HashSet<Reminder>();
            TeleOrderDetails = new HashSet<TeleOrderDetail>();
            TeleOrderPrdocuctExchanges = new HashSet<TeleOrderPrdocuctExchange>();
        }

        [Key]
        public int ProdID { get; set; }

        [Required]
        [StringLength(150)]
        public string ProductName { get; set; }

        public int Position { get; set; }

        [StringLength(100)]
        public string ProductImage { get; set; }

        [StringLength(100)]
        public string ProductImageDetails { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TubePrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TubePromoPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal RefillPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal RefillPromoPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ShippingPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ShippingPromoPrice { get; set; }

        public bool Published { get; set; }

        public bool StatusId { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [StringLength(25)]
        public string ProductCode { get; set; }

        public double? Weight { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderPrdocuctExchange> OrderPrdocuctExchanges { get; set; }

        public virtual SuperAdmin SuperAdmin { get; set; }

        public virtual SuperAdmin SuperAdmin1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductExchange> ProductExchanges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reminder> Reminders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleOrderDetail> TeleOrderDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleOrderPrdocuctExchange> TeleOrderPrdocuctExchanges { get; set; }
    }
}
