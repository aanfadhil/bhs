﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace LPG.Utilities
{
    public static class Common
    {
        /// <summary>
        /// Get value from Configuration file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static T GetAppSetting<T>(string key, T defaultValue)
        {
            if (!string.IsNullOrEmpty(key))
            {
                string value = ConfigurationManager.AppSettings[key];
                try
                {
                    if (value != null)
                    {
                        var theType = typeof(T);
                        if (theType.IsEnum)
                            return (T)Enum.Parse(theType, value.ToString(), true);

                        return (T)Convert.ChangeType(value, theType);
                    }

                    return default(T);
                }
                catch
                {
                    // ignored
                }
            }

            return defaultValue;
        }

        public static void SendMail(string content)
        {
            try
            {
                var fromAddress = new MailAddress("linson.kj@eraminfotech.in", "Admin Eram");
                var toAddress = new MailAddress("linsonkj@gmail.com", "Linson");
                const string fromPassword = "Tessa2015@";
                const string subject = "Exception";
                string body = content;

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
            }
            catch
            {

            }
        }

    
    }
}
