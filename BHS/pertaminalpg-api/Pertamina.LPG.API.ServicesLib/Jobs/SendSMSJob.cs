﻿using Hangfire;
using Hangfire.Server;
using Hangfire.Console;
using Pertamina.LPG.API.DTOs.Gen;
using Pertamina.LPG.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pertamina.LPG.API.ServicesLib.Jobs
{
    public class SendSMSJob
    {
        [Queue("sms")]
        [AutomaticRetry(Attempts = 0, LogEvents = true, OnAttemptsExceeded = AttemptsExceededAction.Fail)]
        public static void SendOTPForForgotPassword(ForgotPasswordResponse response, string mobileNumber, int userId, string userType, PerformContext context)
        {
            context.WriteLine("Sending SMS");
            var resp = OTPServices.SendOTPForForgotPassword(response, mobileNumber, userId, userType);
            context.WriteLine("Got response: " + resp.message);





        }
    }
}
