﻿using LPG.App_Classes;
using LPG.Models;
using LPG.Utilities;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.App_Services
{
    public class OrderService : AbstractAppService, IOrderService
    {
        public const short ID_ORDER_RECEIVED = 1,
             ID_ORDER_PROCESSED = 2,
             ID_ORDER_OUT_FOR_DELIVERY = 3,
             ID_ORDER_DELIVERED = 4,
             ID_ORDER_CANCELLED_BY_CONSUMER = 5,
             ID_ORDER_CANCELLED_BY_SYSTEM = 6,
             ID_ORDER_CANCELLED_BY_SUSER = 7,
             ID_ORDER_CANCELLED_BY_HQ_SUSER = 8,
             ID_ORDER_PROCESSED_BUT_NOT_DELIVERED = 9
            ;

        public const short DELIVERY_STATUS_ASSIGNED = 1,
            DELIVERY_STATUS_OUTFORDELIVERY = 2,
            DELIVERY_STATUS_CANCELLED = 3,
            DELIVERY_STATUS_ONTIMEDELIVERY = 4,
            DELIVERY_STATUS_LATEDELIVERY = 5,
            DELIVERY_STATUS_EARLYDELIVERY = 6;

        public const string APPSETTING_MSG_TO_CONSUMER_FOR_RATING = "MsgToConsumerForRating";
        public const string APPSETTING_USER_ROLE_PELANGGAN = "pelanggan";
        public const string APPSETTING_TITLE_FOR_CONSUMER_TO_RATE = "TitleForConsumerToRate";
        public const string APPSETTING_MSG_FOR_CONSUMER_AT_SADMIN_CANCEL = "MsgToConsumerAtSAdminCancel";
        public const string APPSETTING_TITLE_FOR_CONSUMER_AT_SADMIN_CANCEL = "TitleForConsumerAtSAdminCancel";

        public OrderService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<GetOrderList_Result> GetAllOrders()
        {
            List<GetOrderList_Result> result = new List<GetOrderList_Result>();
            AdminSessionEntity adminSession = (AdminSessionEntity)HttpContext.Current.Session["AdminSession"];
            int cityId = adminSession.CityId;
            using (var context = new lpgdevEntities())
            {
                var orderList = context.GetOrderList();
                if(cityId != 0)
                {
                    result = orderList.Where(order => order.CityID == cityId).ToList();
                }
                else
                {
                    result = orderList.ToList();
                }
            }
            return result;
        }

        public DataTableModel<OrderListPagingModel> GetAllOrdersPaging(DatatablesRequest model)
        {
            DataTableModel<OrderListPagingModel> result = new DataTableModel<OrderListPagingModel>();
            AdminSessionEntity adminSession = (AdminSessionEntity)HttpContext.Current.Session["AdminSession"];
            int cityId = adminSession.CityId;
            using (var context = new lpgdevEntities())
            {

                var citiesObj = context.SACities.Where(t => t.SAdminID == adminSession.UserId).Select(x => x.CityID.ToString()).ToArray();
                string cities = null;
                if(citiesObj.Length > 0)
                {
                    cities = string.Join(",", citiesObj);
                }

                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)context).ObjectContext.CommandTimeout = 1000;

                DateTime? startFilter = null;
                DateTime? endFilter = null;

                try
                {
                    startFilter = DateTime.ParseExact(model.start_date, "yyyy-MM-dd", null);
                }
                catch (Exception)
                {
                    
                }

                try
                {
                    endFilter = DateTime.ParseExact(model.end_date, "yyyy-MM-dd", null);
                }
                catch (Exception)
                {

                }
                
                var orderList = context.GetOrderlistPaging(model.length,model.start,model.search[0],model.searchtype??"",cities,startFilter,endFilter);
                result.data = orderList.Select(t => new OrderListPagingModel()
                    {
                        Address = t.Address,
                        Agen = t.Agen,
                        AgenID = t.AgenID,
                        AgentAdminName = t.AgentAdminName,
                        AgentAdminPhone = t.AgentAdminPhone,
                        BuyingStatus = t.BuyingStatus,
                        CityID = t.CityID,
                        CityName = t.CityName,
                        DeliveryDate = t.DeliveryDate,
                        Driver = t.Driver,
                        DriverID = t.DriverID,
                        GrandTotal = t.GrandTotal,
                        InvoiceNumber = t.InvoiceNumber,
                        MobileNumber = t.MobileNumber,
                        Name = t.Name,
                        NumberOfProducts = t.NumberOfProducts,
                        OrderDate = t.OrderDate,
                        OrderDateFormated = t.OrderDate != null ?t.OrderDate.Value.ToString("dd/MM/yyyy HH:mm"):"",
                        OrdrID = t.OrdrID,
                        PotonganVoucher = t.PotonganVoucher,
                        ProductList = t.ProductList,
                        PromoID = t.PromoID,
                        Quantity = t.Quantity,
                        Rating = t.Rating,
                        RegionCode = t.RegionCode,
                        RegionName = t.RegionName,
                        SlotName = t.SlotName,
                        sort = t.sort,
                        StatusID = t.StatusID,
                        Total = t.Total,
                        Voucher = t.Voucher,
                        DbptID = t.DbptID,
                        DistributionPointName = t.DistributionPointName,
                        DeliveredAtFormated = t.DeliveredAt != null ? t.DeliveredAt.Value.ToString("dd/MM/yyyy HH:mm") : ""

                }
                ).ToList();
                
                if(result.data.Count > 0)
                {
                    result.recordsFiltered = result.data[0].Total;
                    result.recordsTotal = result.data[0].Total;
                }
                else
                {
                    result.recordsFiltered = 0;
                    result.recordsTotal = 0;
                }
            }
            
            return result;
        }

        private bool ListOrderQuery(GetOrderList_Result data, DatatablesRequest model)
        {
            return true;
        }



        //public List<OrderModel> ListOrder()
        //{
        //    List<OrderModel> orderList = new List<OrderModel>();
        //    List<OrderModel> teleorderList = new List<OrderModel>();
        //    List<OrderModel> mergedList = new List<OrderModel>();
        //    using (var context = new lpgdevEntities())
        //    {
        //        var data = from o in context.Orders
        //                   join od in context.OrderDetails
        //                   on o.OrdrID equals od.OrdrID
        //                   join product in context.Products
        //                   on od.ProdID equals product.ProdID                         
        //                   join con in context.Consumers
        //                   on o.ConsID equals con.ConsID 
        //                   join conAdr in context.ConsumerAddresses
        //                   on o.AddrID equals conAdr.AddrID 
        //                   //into ConsumerAdr
        //                   //from consumerAdr in ConsumerAdr.DefaultIfEmpty()//Consumer Address

        //                   join agentAd in context.AgentAdmins
        //                   on o.AgadmID equals agentAd.AgadmID into AgentAdmin
        //                   from agentAdmin in AgentAdmin.DefaultIfEmpty()//Agent admins 

        //                   join agentAgency in context.Agencies
        //                   on agentAdmin.AgenID equals agentAgency.AgenID into AgencyNames
        //                   from agency in AgencyNames.DefaultIfEmpty()//Agency

        //                   join agentRegion in context.MRegions
        //                   on agency.RegionId equals agentRegion.RegionID into Regions
        //                   from reg in Regions.DefaultIfEmpty()//Agent Region

        //                   join timeslot in context.MDeliverySlots
        //                   on o.DeliverySlotID equals timeslot.SlotID into OrderTimeslots
        //                   from OrderTimeslot in OrderTimeslots.DefaultIfEmpty()//Timeslot

        //                   select new OrderModel
        //                   {
        //                       //order
        //                       OrderId = o.OrdrID,
        //                       InvoiceNo = o.InvoiceNumber,
        //                       BuyingStatus = 1,
        //                       Status = o.StatusID,
        //                       TotalTotalEnd = o.GrandTotal,
        //                       TimeslotDate = o.DeliveryDate,

        //                       //consumer
        //                       ConsumerName = (con.Name==null) ? "tidak ada data" : con.Name,

        //                       //consumer Adress
        //                       ConsumerAddress = (conAdr.Address==null)? "tidak ada data": conAdr.Address,
        //                       ConsumerRegion = (conAdr.RegionName == null) ? "tidak ada data" : conAdr.RegionName,

        //                       //agent admin 
        //                       AgentName = (agentAdmin.AgentAdminName==null)? "tidak ada data": agentAdmin.AgentAdminName,
        //                       AdminMobileNo = (agentAdmin.MobileNumber == null) ? "tidak ada data" : agentAdmin.MobileNumber,


        //                       ProductName = (product.ProductName==null)? "tidak ada data": product.ProductName,
        //                       //ProductAmount =(products.TubePrice==null)? "tidak ada data":products.TubePrice,
        //                       ProductQuantity = o.NumberOfProducts,
        //                       OrderedProductType = (od.RefillQuantity != 0) ? 2 : (od.Quantity != 0) ? 1 :3,
        //                       SlotName = OrderTimeslot.SlotName,
        //                       OrderedDate = o.OrderDate,
        //                       AgentRegion = reg.RegionCode,
        //                       AgentAddress = reg.RegionName

        //                   };

        //        var teleOrderdata = from otele in context.TeleOrders
        //                            join odtele in context.TeleOrderDetails
        //                            on otele.TeleOrdID equals odtele.TeleOrdID                                 

        //                            join con in context.TeleCustomers
        //                            on otele.TeleOrdID equals con.TeleOrdID into Consumer
        //                            from consumers in Consumer.DefaultIfEmpty()//Consumer Details

        //                                //join conAdr in context.ConsumerAddresses
        //                                //on o.AddrID equals conAdr.AddrID into ConsumerAdr
        //                                //from consumerAdr in ConsumerAdr.DefaultIfEmpty()//Consumer Address

        //                            join agentAd in context.AgentAdmins
        //                            on otele.AgadmID equals agentAd.AgadmID into AgentAdmin
        //                            from agentAdmin in AgentAdmin.DefaultIfEmpty()//Agent admins

        //                            join agentAgency in context.Agencies
        //                            on agentAdmin.AgenID equals agentAgency.AgenID into AgencyNames
        //                            from agency in AgencyNames.DefaultIfEmpty()//Agency

        //                            join agentRegion in context.MRegions
        //                            on agency.RegionId equals agentRegion.RegionID into Regions
        //                            from reg in Regions.DefaultIfEmpty()//Agent Region

        //                            join product in context.Products
        //                            on odtele.ProdID equals product.ProdID
        //                            //into Products
        //                            //from products in Products.DefaultIfEmpty()//products

        //                            join timeslot in context.MDeliverySlots
        //                            on otele.DeliverySlotID equals timeslot.SlotID into OrderTimeslots
        //                            from OrderTimeslot in OrderTimeslots.DefaultIfEmpty()//Timeslot

        //                            select new OrderModel
        //                            {
        //                                //order
        //                                OrderId = otele.TeleOrdID,
        //                                InvoiceNo = otele.InvoiceNumber,
        //                                BuyingStatus = (otele.DeliveryType.ToString() == "1") ? (2) : 3,
        //                                Status = otele.StatusId,
        //                                TotalTotalEnd = otele.GrantTotal,
        //                                TimeslotDate = otele.DeliveryDate,

        //                                //consumer
        //                                ConsumerName = (consumers.CustomerName == null) ? "tidak ada data" : consumers.CustomerName,

        //                                //consumer Adress
        //                                ConsumerAddress = (consumers.Address == null) ? "tidak ada data" : consumers.Address,
        //                                ConsumerRegion = (consumers.Address == null) ? "tidak ada data" : consumers.Address,

        //                                //agent admin 
        //                                AgentName = (agentAdmin.AgentAdminName == null) ? "tidak ada data" : agentAdmin.AgentAdminName,
        //                                AdminMobileNo = (agentAdmin.MobileNumber == null) ? "tidak ada data" : agentAdmin.MobileNumber,


        //                                ProductName = (product.ProductName == null) ? "tidak ada data" : product.ProductName,
        //                                ProductQuantity = otele.NumberOfProducts,
        //                                OrderedProductType = (odtele.RefillQuantity != 0) ? 2 : (odtele.Quantity != 0) ? 1 : 3,
        //                                SlotName = OrderTimeslot.SlotName,
        //                                OrderedDate = otele.OrderDate,
        //                                AgentRegion = reg.RegionCode,
        //                                AgentAddress = reg.RegionName
        //                            };
        //        orderList = data.Distinct().ToList();
        //        teleorderList = teleOrderdata.Distinct().ToList();
        //        mergedList = orderList.Union(teleorderList).ToList();

        //    }

        //    return mergedList;
        //}

        public OrderModel getOrderDetails(int orderId, int buyType)
        {
            List<OrderModel> orderModel = new List<OrderModel>();
            if (buyType == 1) // OrderApp then fetching Orders, OrderDetails tables
            {
                OrderPrdocuctExchange orderExchange = new OrderPrdocuctExchange();
                using (var context = new lpgdevEntities())
                {
                    orderExchange = context.OrderPrdocuctExchanges.Where(r => r.OrdrID == orderId).FirstOrDefault();
                }

                if (orderExchange == null) // OrderExchange is null then fetching OrderDetails table
                {
                    using (var context = new lpgdevEntities())
                    {
                        var rtns = (from order in context.Orders
                                    join consumer in context.Consumers
                                    on order.ConsID equals consumer.ConsID
                                    join consumerars in context.ConsumerAddresses
                                    on order.AddrID equals consumerars.AddrID
                                    join agentAd in context.AgentAdmins
                                    on order.AgadmID equals agentAd.AgadmID into AgentAdmin
                                    
                                    from agencyAdmin in AgentAdmin.DefaultIfEmpty()//Agent admins

                                    join agentAgency in context.Agencies
                                    on agencyAdmin.AgenID equals agentAgency.AgenID into AgencyNames
                                    from agency in AgencyNames.DefaultIfEmpty()//Agency

                                    join agencyDP in context.DistributionPoints
                                    on agency.AgenID equals agencyDP.AgenID into Agencydistpoint
                                    from distpoint in Agencydistpoint.DefaultIfEmpty()//Agency Distripution Point

                                    join agencydriver in context.Drivers
                                    on order.DrvrID equals agencydriver.DrvrID into drivers
                                    from driver in drivers.DefaultIfEmpty()//Agency Driver

                                        //join orderdetails in context.OrderDetails
                                        //on order.OrdrID equals orderdetails.OrdrID
                                        //join product in context.Products
                                        //on orderdetails.ProdID equals product.ProdID
                                    where order.OrdrID == orderId

                                    select new OrderModel()
                                    {
                                        OrderId = order.OrdrID,
                                        ConsumerName = consumer.Name,
                                        ConsumerAddress = consumerars.Address,
                                        ConsumerRegion = consumerars.RegionName,
                                        AgentName = agency.AgencyName,
                                        AgentRegion = agency.Region,
                                        AgentAddress = distpoint.Address,
                                        AdminMobileNo = agencyAdmin.MobileNumber,
                                        InvoiceNo = order.InvoiceNumber,
                                        //ProductName = product.ProductName,
                                        //ProductAmount = product.TubePrice,
                                        LastModifiedDate = order.UpdatedDate,
                                        BuyingStatus = 1, //Ordered Mode App/Tele/Pickup ,
                                        Status = order.StatusID,
                                        TotalEarlyCount = order.SubTotal + order.ShippingCharge,
                                        TotalPromoTotal = order.PromoProduct + order.PromoShipping,
                                        TotalTotalEnd = order.GrandTotal,
                                        PotonganVoucher = order.PotonganVoucher,
                                        Voucher = order.Voucher,
                                        DeliveryDate = order.MDeliverySlot.SlotName,
                                        TimeslotDate = order.DeliveryDate,
                                        ProductList = (from orderdetails in context.OrderDetails
                                                       join product in context.Products
                                                       on orderdetails.ProdID equals product.ProdID
                                                       where orderdetails.OrdrID == orderId

                                                       select new ProductModel
                                                       {
                                                           ProductName = (orderdetails.Quantity != 0) ? product.ProductName + " - Tukar Tambah" : "",
                                                           ProductRefillName = (orderdetails.RefillQuantity != 0) ? product.ProductName + " - Refill" : "",
                                                           Quantity = orderdetails.Quantity,
                                                           RefillQuantity = orderdetails.RefillQuantity,
                                                           // Total Product amount refill/new
                                                           TotalAmount = orderdetails.SubTotal,
                                                           TubePrice = orderdetails.UnitPrice,

                                                           TotalPromoProduct = orderdetails.PromoProduct,
                                                           PromoPrice = (orderdetails.Quantity != 0) ? Math.Truncate((orderdetails.PromoProduct / orderdetails.Quantity) * 1000) / 1000 : 0,

                                                           //TotalRefillAmount = orderdetails.RefillPrice,
                                                           //RefillPrice = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.RefillPrice / orderdetails.RefillQuantity) * 1000) / 1000 : 0,
                                                           TotalRefillAmount = orderdetails.RefillSubTotal,
                                                           RefillPrice = orderdetails.RefillPrice,

                                                           TotalPromoRefillAmount = orderdetails.PromoRefill,
                                                           PromoRefillAmount = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.PromoRefill / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalShippingAmount = 0,
                                                           ShippingAmount = orderdetails.ShippingCharge,

                                                           TotalPromoShippingAmount = 0,
                                                           PromoShippingAmount = orderdetails.PromoShipping,

                                                       }).ToList()


                                    }).FirstOrDefault();

                        return rtns;
                    }
                }
                else // OrderExchange is not null then fetching OrderProductExchange table
                {
                    using (var context = new lpgdevEntities())
                    {
                        var rtns = (from order in context.Orders
                                    join consumer in context.Consumers
                                    on order.ConsID equals consumer.ConsID
                                    join consumerars in context.ConsumerAddresses
                                    on order.AddrID equals consumerars.AddrID
                                    join agentAd in context.AgentAdmins
                                    on order.AgadmID equals agentAd.AgadmID into AgentAdmin
                                    
                                    from agencyAdmin in AgentAdmin.DefaultIfEmpty()//Agent admins
                                    join agentAgency in context.Agencies
                                    on agencyAdmin.AgenID equals agentAgency.AgenID into AgencyNames
                                    from agency in AgencyNames.DefaultIfEmpty()//Agency
                                    join agencyDP in context.DistributionPoints
                                    on agency.AgenID equals agencyDP.AgenID into Agencydistpoint
                                    from distpoint in Agencydistpoint.DefaultIfEmpty()//Agency Distripution Point
                                    join agencydriver in context.Drivers
                                    on order.DrvrID equals agencydriver.DrvrID into drivers
                                    from driver in drivers.DefaultIfEmpty()//Agency Driver

                                        //join orderdetails in context.OrderDetails
                                        //on order.OrdrID equals orderdetails.OrdrID
                                        //join product in context.Products
                                        //on orderdetails.ProdID equals product.ProdID
                                    where order.OrdrID == orderId

                                    select new OrderModel()
                                    {
                                        OrderId = order.OrdrID,
                                        ConsumerName = consumer.Name,
                                        ConsumerAddress = consumerars.Address,
                                        ConsumerRegion = consumerars.RegionName,
                                        AgentName = agency.AgencyName,
                                        AgentRegion = agency.Region,
                                        AgentAddress = distpoint.Address,
                                        AdminMobileNo = agencyAdmin.MobileNumber,
                                        InvoiceNo = order.InvoiceNumber,
                                        //ProductName = product.ProductName,
                                        //ProductAmount = product.TubePrice,
                                        LastModifiedDate = order.UpdatedDate,
                                        BuyingStatus = 1, //order.DeliverySlotID,
                                        Status = order.StatusID,
                                        TotalEarlyCount = order.SubTotal + order.ShippingCharge,
                                        TotalPromoTotal = order.PromoProduct + order.PromoShipping,
                                        TotalTotalEnd = order.GrandTotal,
                                        PotonganVoucher = order.PotonganVoucher,
                                        Voucher = order.Voucher,
                                        DeliveryDate = order.MDeliverySlot.SlotName,
                                        TimeslotDate = order.DeliveryDate,
                                        ProductList = (from orderdetails in context.OrderDetails
                                                       join product in context.Products
                                                       on orderdetails.ProdID equals product.ProdID
                                                       where orderdetails.OrdrID == orderId
                                                       select new ProductModel
                                                       {
                                                           //ProductName = product.ProductName + " - Tabung + Isi",
                                                           ProductName = (orderdetails.Quantity != 0) ? product.ProductName + " - Tukar Tambah" : "",
                                                           Quantity = orderdetails.Quantity,

                                                           ProductRefillName = (orderdetails.RefillQuantity != 0) ? product.ProductName + " - Refill" : "",
                                                           RefillQuantity = orderdetails.RefillQuantity,

                                                           TotalAmount = orderdetails.SubTotal,
                                                           TubePrice = orderdetails.UnitPrice,

                                                           TotalPromoProduct = orderdetails.PromoProduct,
                                                           PromoPrice = (orderdetails.Quantity != 0) ? Math.Truncate((orderdetails.PromoProduct / orderdetails.Quantity) * 1000) / 1000 : 0,

                                                           //TotalRefillAmount = orderdetails.RefillPrice,
                                                           //RefillPrice = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.RefillPrice / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalRefillAmount = orderdetails.RefillSubTotal,
                                                           RefillPrice = orderdetails.RefillPrice,

                                                           TotalPromoRefillAmount = orderdetails.PromoRefill,
                                                           PromoRefillAmount = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.PromoRefill / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalShippingAmount = 0,
                                                           ShippingAmount = orderdetails.ShippingCharge,

                                                           TotalPromoShippingAmount = 0,
                                                           PromoShippingAmount = orderdetails.PromoShipping,

                                                       })
                                                       .Union
                                                       (
                                                       from orderex in context.OrderPrdocuctExchanges
                                                       join productEx in context.Products
                                                       on orderex.ProdID equals productEx.ProdID
                                                       join orderdetails in context.OrderDetails
                                                       on order.OrdrID equals orderdetails.OrdrID
                                                       where orderex.OrdrID == orderId
                                                       && orderdetails.OrdrID == orderId && orderdetails.ProdID == orderex.ProdID
                                                       select new ProductModel
                                                       {

                                                           //ProductName = product.ProductName + " - Tabung + Isi",
                                                           ProductName = productEx.ProductName + " dengan " + orderex.ExchangeWith,
                                                           Quantity = orderex.ExchangeQuantity,

                                                           ProductRefillName = "",
                                                           RefillQuantity = 0,

                                                           //TotalAmount = orderex.ExchangePrice,
                                                           //TubePrice = (orderex.ExchangeQuantity != 0) ? Math.Truncate((orderex.ExchangePrice / orderex.ExchangeQuantity) * 1000 / 1000) : 0,

                                                           TotalAmount = orderex.SubTotal,
                                                           TubePrice = orderex.ExchangePrice,

                                                           TotalPromoProduct = orderex.ExchangePromoPrice * orderex.ExchangeQuantity,
                                                           PromoPrice = orderex.ExchangePromoPrice,

                                                           TotalRefillAmount = 0,
                                                           RefillPrice = 0,

                                                           TotalPromoRefillAmount = 0,
                                                           PromoRefillAmount = 0,

                                                           TotalShippingAmount = 0,
                                                           ShippingAmount = orderdetails.ShippingCharge,

                                                           TotalPromoShippingAmount = 0,
                                                           PromoShippingAmount = orderdetails.PromoShipping,

                                                       }).ToList()


                                    }).FirstOrDefault();

                        return rtns;
                    }
                }

            }
            else if (buyType == 2) // TeleOrder then fetching TeleOrders, TeleOrderDetails tables
            {

                TeleOrderPrdocuctExchange teleorderExchange = new TeleOrderPrdocuctExchange();
                using (var context = new lpgdevEntities())
                {
                    teleorderExchange = context.TeleOrderPrdocuctExchanges.Where(r => r.TeleOrdID == orderId).FirstOrDefault();
                }

                if (teleorderExchange == null) // TeleOrderExchange is null then fetching TeleOrderDetails table
                {
                    using (var context = new lpgdevEntities())
                    {
                        var rtns = (from order in context.TeleOrders
                                    join consumer in context.TeleCustomers
                                    on order.TeleOrdID equals consumer.TeleOrdID
                                    join agentAd in context.AgentAdmins
                                    on order.AgadmID equals agentAd.AgadmID into AgentAdmin
                                    from agencyAdmin in AgentAdmin.DefaultIfEmpty()//Agent admins                                    
                                    join agentAgency in context.Agencies
                                    on agencyAdmin.AgenID equals agentAgency.AgenID into AgencyNames
                                    from agency in AgencyNames.DefaultIfEmpty()//Agency
                                    join agencyDP in context.DistributionPoints
                                    on agency.AgenID equals agencyDP.AgenID into Agencydistpoint
                                    from distpoint in Agencydistpoint.DefaultIfEmpty()//Agency Distripution Point
                                    join agencydriver in context.Drivers
                                    on order.DrvrID equals agencydriver.DrvrID into drivers
                                    from driver in drivers.DefaultIfEmpty()//Agency Driver

                                        //join orderdetails in context.TeleOrderDetails
                                        //on order.TeleOrdID equals orderdetails.TeleOrdID
                                        //join product in context.Products
                                        //on orderdetails.ProdID equals product.ProdID
                                    where order.TeleOrdID == orderId

                                    select new OrderModel()
                                    {
                                        OrderId = order.TeleOrdID,
                                        ConsumerName = consumer.CustomerName,
                                        ConsumerAddress = consumer.Address,
                                        ConsumerRegion = consumer.Address,
                                        AgentName = agency.AgencyName,
                                        AgentRegion = agency.Region,
                                        AgentAddress = distpoint.Address,
                                        AdminMobileNo = agencyAdmin.MobileNumber,
                                        InvoiceNo = order.InvoiceNumber,
                                        //ProductName = product.ProductName,
                                        //ProductAmount = product.TubePrice,
                                        LastModifiedDate = order.UpdatedDate,
                                        BuyingStatus = buyType,
                                        Status = order.StatusId,
                                        TotalEarlyCount = order.SubTotal + order.ShippingCharge,
                                        TotalPromoTotal = order.PromoProduct + order.PromoShipping,
                                        TotalTotalEnd = order.GrantTotal,

                                        ProductList = (from orderdetails in context.TeleOrderDetails
                                                       join product in context.Products
                                                       on orderdetails.ProdID equals product.ProdID
                                                       where orderdetails.TeleOrdID == orderId
                                                       select new ProductModel
                                                       {
                                                           //ProductName = product.ProductName + " - Tabung + Isi",
                                                           ProductName = (orderdetails.Quantity != 0) ? product.ProductName + " - Tukar Tambah" : "",
                                                           Quantity = orderdetails.Quantity,

                                                           ProductRefillName = (orderdetails.RefillQuantity != 0) ? product.ProductName + " - Refill" : "",
                                                           RefillQuantity = orderdetails.RefillQuantity,

                                                           TotalAmount = orderdetails.SubTotal,
                                                           TubePrice = orderdetails.UnitPrice,

                                                           TotalPromoProduct = orderdetails.PromoProduct,
                                                           PromoPrice = (orderdetails.Quantity != 0) ? Math.Truncate((orderdetails.PromoProduct ?? 0 / orderdetails.Quantity) * 1000) / 1000 : 0,

                                                           //TotalRefillAmount = orderdetails.RefillPrice,
                                                           //RefillPrice = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.RefillPrice / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalRefillAmount = orderdetails.RefillSubTotal,
                                                           RefillPrice = orderdetails.RefillPrice,

                                                           TotalPromoRefillAmount = orderdetails.PromoRefill,
                                                           PromoRefillAmount = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.PromoRefill / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalShippingAmount = 0,
                                                           ShippingAmount = orderdetails.ShippingCharge,

                                                           TotalPromoShippingAmount = 0,
                                                           PromoShippingAmount = orderdetails.PromoShipping,

                                                       })
                                                       .ToList()


                                    }).FirstOrDefault();

                        return rtns;
                    }
                }
                else  // TeleOrderExchange is not null then fetching TeleOrderExchangeProduct table
                {
                    using (var context = new lpgdevEntities())
                    {
                        var rtns = (from order in context.TeleOrders
                                    join consumer in context.TeleCustomers
                                    on order.TeleOrdID equals consumer.TeleOrdID
                                    join agentAd in context.AgentAdmins
                                    on order.AgadmID equals agentAd.AgadmID into AgentAdmin
                                    from agencyAdmin in AgentAdmin.DefaultIfEmpty()//Agent admins
                                    join agentAgency in context.Agencies
                                    on agencyAdmin.AgenID equals agentAgency.AgenID into AgencyNames
                                    from agency in AgencyNames.DefaultIfEmpty()//Agency                                    
                                    join agencyDP in context.DistributionPoints
                                    on agency.AgenID equals agencyDP.AgenID into Agencydistpoint
                                    from distpoint in Agencydistpoint.DefaultIfEmpty()//Agency Distripution Point
                                    join agencydriver in context.Drivers
                                    on order.DrvrID equals agencydriver.DrvrID into drivers
                                    from driver in drivers.DefaultIfEmpty()//Agency Driver

                                        //join orderdetails in context.TeleOrderDetails
                                        //on order.TeleOrdID equals orderdetails.TeleOrdID
                                        //join product in context.Products
                                        //on orderdetails.ProdID equals product.ProdID
                                    where order.TeleOrdID == orderId

                                    select new OrderModel()
                                    {
                                        OrderId = order.TeleOrdID,
                                        ConsumerName = consumer.CustomerName,
                                        ConsumerAddress = consumer.Address,
                                        ConsumerRegion = consumer.Address,
                                        AgentName = agency.AgencyName,
                                        AgentRegion = agency.Region,
                                        AgentAddress = distpoint.Address,
                                        AdminMobileNo = agencyAdmin.MobileNumber,
                                        InvoiceNo = order.InvoiceNumber,
                                        //ProductName = product.ProductName,
                                        //ProductAmount = product.TubePrice,
                                        LastModifiedDate = order.UpdatedDate,
                                        BuyingStatus = buyType,
                                        Status = order.StatusId,
                                        TotalEarlyCount = order.SubTotal + order.ShippingCharge,
                                        TotalPromoTotal = order.PromoProduct + order.PromoShipping,
                                        TotalTotalEnd = order.GrantTotal,

                                        ProductList = (from orderdetails in context.TeleOrderDetails
                                                       join product in context.Products
                                                       on orderdetails.ProdID equals product.ProdID
                                                       where orderdetails.TeleOrdID == orderId
                                                       select new ProductModel
                                                       {
                                                           //ProductName = product.ProductName + " - Tabung + Isi",
                                                           ProductName = (orderdetails.Quantity != 0) ? product.ProductName + " - Tukar Tambah" : "",
                                                           Quantity = orderdetails.Quantity,

                                                           ProductRefillName = (orderdetails.RefillQuantity != 0) ? product.ProductName + " - Refill" : "",
                                                           RefillQuantity = orderdetails.RefillQuantity,

                                                           TotalAmount = orderdetails.SubTotal,
                                                           TubePrice = orderdetails.UnitPrice,

                                                           TotalPromoProduct = orderdetails.PromoProduct,
                                                           PromoPrice = (orderdetails.Quantity != 0) ? Math.Truncate((orderdetails.PromoProduct ?? 0 / orderdetails.Quantity) * 1000) / 1000 : 0,

                                                           //TotalRefillAmount = orderdetails.RefillPrice,
                                                           //RefillPrice = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.RefillPrice / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalRefillAmount = orderdetails.RefillSubTotal,
                                                           RefillPrice = orderdetails.RefillPrice,

                                                           TotalPromoRefillAmount = orderdetails.PromoRefill,
                                                           PromoRefillAmount = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.PromoRefill / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalShippingAmount = 0,
                                                           ShippingAmount = orderdetails.ShippingCharge,

                                                           TotalPromoShippingAmount = 0,
                                                           PromoShippingAmount = orderdetails.PromoShipping,

                                                       })
                                                       .Union
                                                       (from orderex in context.TeleOrderPrdocuctExchanges
                                                        join productEx in context.Products
                                                        on orderex.ProdID equals productEx.ProdID
                                                        join orderdetails in context.TeleOrderDetails
                                                       on order.TeleOrdID equals orderdetails.TeleOrdID
                                                        where orderex.TeleOrdID == orderId
                                                         && orderdetails.TeleOrdID == orderId
                                                        select new ProductModel
                                                        {
                                                            ProductName = productEx.ProductName + " dengan " + orderex.ExchangeWith,
                                                            Quantity = orderex.ExchangeQuantity,

                                                            ProductRefillName = "",
                                                            RefillQuantity = 0,

                                                            //TotalAmount = orderex.ExchangePrice,
                                                            //TubePrice = (orderex.ExchangeQuantity != 0) ? Math.Truncate((orderex.ExchangePrice / orderex.ExchangeQuantity) * 1000 / 1000) : 0,

                                                            TotalAmount = orderex.SubTotal,
                                                            TubePrice = orderex.ExchangePrice,

                                                            TotalPromoProduct = orderex.ExchangePromoPrice,
                                                            PromoPrice = orderex.ExchangePromoPrice * orderex.ExchangeQuantity,

                                                            TotalRefillAmount = 0,
                                                            RefillPrice = 0,

                                                            TotalPromoRefillAmount = 0,
                                                            PromoRefillAmount = 0,

                                                            TotalShippingAmount = 0,
                                                            ShippingAmount = orderdetails.ShippingCharge,

                                                            TotalPromoShippingAmount = 0,
                                                            PromoShippingAmount = orderdetails.PromoShipping,
                                                        })
                                                       .ToList()


                                    }).FirstOrDefault();

                        return rtns;
                    }
                }




            }
            else if (buyType == 3) // PickupOrder then fetching TeleOrders, TeleOrderDetails tables
            {
                TeleOrderPrdocuctExchange teleorderExchange = new TeleOrderPrdocuctExchange();
                using (var context = new lpgdevEntities())
                {
                    teleorderExchange = context.TeleOrderPrdocuctExchanges.Where(r => r.TeleOrdID == orderId).FirstOrDefault();
                }

                if (teleorderExchange == null) // TeleOrderExchange is null then fetching TeleOrderDetails table
                {
                    using (var context = new lpgdevEntities())
                    {
                        var rtns = (from order in context.TeleOrders
                                    join agentAd in context.AgentAdmins
                                    on order.AgadmID equals agentAd.AgadmID into AgentAdmin
                                    from agencyAdmin in AgentAdmin.DefaultIfEmpty()//Agent admins                                    
                                    join agentAgency in context.Agencies
                                    on agencyAdmin.AgenID equals agentAgency.AgenID into AgencyNames
                                    from agency in AgencyNames.DefaultIfEmpty()//Agency
                                    join agencyDP in context.DistributionPoints
                                    on agency.AgenID equals agencyDP.AgenID into Agencydistpoint
                                    from distpoint in Agencydistpoint.DefaultIfEmpty()//Agency Distripution Point
                                    join agencydriver in context.Drivers
                                    on order.DrvrID equals agencydriver.DrvrID into drivers
                                    from driver in drivers.DefaultIfEmpty()//Agency Driver

                                        //join orderdetails in context.TeleOrderDetails
                                        //on order.TeleOrdID equals orderdetails.TeleOrdID
                                        //join product in context.Products
                                        //on orderdetails.ProdID equals product.ProdID
                                    where order.TeleOrdID == orderId

                                    select new OrderModel()
                                    {
                                        OrderId = order.TeleOrdID,
                                        ConsumerName = string.Empty,
                                        ConsumerAddress = string.Empty,
                                        ConsumerRegion = string.Empty,
                                        AgentName = agency.AgencyName,
                                        AgentRegion = agency.Region,
                                        AgentAddress = distpoint.Address,
                                        AdminMobileNo = agencyAdmin.MobileNumber,
                                        InvoiceNo = order.InvoiceNumber,
                                        //ProductName = product.ProductName,
                                        //ProductAmount = product.TubePrice,
                                        LastModifiedDate = order.UpdatedDate,
                                        BuyingStatus = buyType,
                                        Status = order.StatusId,
                                        TotalEarlyCount = order.SubTotal + order.ShippingCharge,
                                        TotalPromoTotal = order.PromoProduct + order.PromoShipping,
                                        TotalTotalEnd = order.GrantTotal,

                                        ProductList = (from orderdetails in context.TeleOrderDetails
                                                       join product in context.Products
                                                       on orderdetails.ProdID equals product.ProdID
                                                       where orderdetails.TeleOrdID == orderId
                                                       select new ProductModel
                                                       {
                                                           //ProductName = product.ProductName + " - Tabung + Isi",
                                                           ProductName = (orderdetails.Quantity != 0) ? product.ProductName + " - Tukar Tambah" : "",
                                                           Quantity = orderdetails.Quantity,

                                                           ProductRefillName = (orderdetails.RefillQuantity != 0) ? product.ProductName + " - Refill" : "",
                                                           RefillQuantity = orderdetails.RefillQuantity,

                                                           TotalAmount = orderdetails.SubTotal,
                                                           TubePrice = orderdetails.UnitPrice,

                                                           TotalPromoProduct = orderdetails.PromoProduct,
                                                           PromoPrice = (orderdetails.Quantity != 0) ? Math.Truncate((orderdetails.PromoProduct ?? 0 / orderdetails.Quantity) * 1000) / 1000 : 0,

                                                           //TotalRefillAmount = orderdetails.RefillPrice,
                                                           //RefillPrice = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.RefillPrice / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalRefillAmount = orderdetails.RefillSubTotal,
                                                           RefillPrice = orderdetails.RefillPrice,

                                                           TotalPromoRefillAmount = orderdetails.PromoRefill,
                                                           PromoRefillAmount = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.PromoRefill / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalShippingAmount = 0,
                                                           ShippingAmount = orderdetails.ShippingCharge,

                                                           TotalPromoShippingAmount = 0,
                                                           PromoShippingAmount = orderdetails.PromoShipping,

                                                       })
                                                       .ToList()


                                    }).FirstOrDefault();

                        return rtns;
                    }
                }
                else  // TeleOrderExchange is not null then fetching TeleOrderExchangeProduct table
                {
                    using (var context = new lpgdevEntities())
                    {
                        var rtns = (from order in context.TeleOrders
                                    join agentAd in context.AgentAdmins
                                    on order.AgadmID equals agentAd.AgadmID into AgentAdmin
                                    from agencyAdmin in AgentAdmin.DefaultIfEmpty()//Agent admins
                                    join agentAgency in context.Agencies
                                    on agencyAdmin.AgenID equals agentAgency.AgenID into AgencyNames
                                    from agency in AgencyNames.DefaultIfEmpty()//Agency                                    
                                    join agencyDP in context.DistributionPoints
                                    on agency.AgenID equals agencyDP.AgenID into Agencydistpoint
                                    from distpoint in Agencydistpoint.DefaultIfEmpty()//Agency Distripution Point
                                    join agencydriver in context.Drivers
                                    on order.DrvrID equals agencydriver.DrvrID into drivers
                                    from driver in drivers.DefaultIfEmpty()//Agency Driver

                                        //join orderdetails in context.TeleOrderDetails
                                        //on order.TeleOrdID equals orderdetails.TeleOrdID
                                        //join product in context.Products
                                        //on orderdetails.ProdID equals product.ProdID
                                    where order.TeleOrdID == orderId

                                    select new OrderModel()
                                    {
                                        OrderId = order.TeleOrdID,
                                        ConsumerName = string.Empty,
                                        ConsumerAddress = string.Empty,
                                        ConsumerRegion = string.Empty,
                                        AgentName = agency.AgencyName,
                                        AgentRegion = agency.Region,
                                        AgentAddress = distpoint.Address,
                                        AdminMobileNo = agencyAdmin.MobileNumber,
                                        InvoiceNo = order.InvoiceNumber,
                                        //ProductName = product.ProductName,
                                        //ProductAmount = product.TubePrice,
                                        LastModifiedDate = order.UpdatedDate,
                                        BuyingStatus = buyType,
                                        Status = order.StatusId,
                                        TotalEarlyCount = order.SubTotal + order.ShippingCharge,
                                        TotalPromoTotal = order.PromoProduct + order.PromoShipping,
                                        TotalTotalEnd = order.GrantTotal,

                                        ProductList = (from orderdetails in context.TeleOrderDetails
                                                       join product in context.Products
                                                       on orderdetails.ProdID equals product.ProdID
                                                       where orderdetails.TeleOrdID == orderId
                                                       select new ProductModel
                                                       {
                                                           //ProductName = product.ProductName + " - Tabung + Isi",
                                                           ProductName = (orderdetails.Quantity != 0) ? product.ProductName + " - Tukar Tambah" : "",
                                                           Quantity = orderdetails.Quantity,

                                                           ProductRefillName = (orderdetails.RefillQuantity != 0) ? product.ProductName + " - Refill" : "",
                                                           RefillQuantity = orderdetails.RefillQuantity,

                                                           TotalAmount = orderdetails.SubTotal,
                                                           TubePrice = orderdetails.UnitPrice,

                                                           TotalPromoProduct = orderdetails.PromoProduct,
                                                           PromoPrice = (orderdetails.Quantity != 0) ? Math.Truncate((orderdetails.PromoProduct ?? 0 / orderdetails.Quantity) * 1000) / 1000 : 0,

                                                           //TotalRefillAmount = orderdetails.RefillPrice,
                                                           //RefillPrice = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.RefillPrice / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalRefillAmount = orderdetails.RefillSubTotal,
                                                           RefillPrice = orderdetails.RefillPrice,

                                                           TotalPromoRefillAmount = orderdetails.PromoRefill,
                                                           PromoRefillAmount = (orderdetails.RefillQuantity != 0) ? Math.Truncate((orderdetails.PromoRefill / orderdetails.RefillQuantity) * 1000) / 1000 : 0,

                                                           TotalShippingAmount = 0,
                                                           ShippingAmount = orderdetails.ShippingCharge,

                                                           TotalPromoShippingAmount = 0,
                                                           PromoShippingAmount = orderdetails.PromoShipping,

                                                       })
                                                       .Union
                                                       (from orderex in context.TeleOrderPrdocuctExchanges
                                                        join productEx in context.Products
                                                        on orderex.ProdID equals productEx.ProdID
                                                        join orderdetails in context.TeleOrderDetails
                                                       on order.TeleOrdID equals orderdetails.TeleOrdID
                                                        where orderex.TeleOrdID == orderId
                                                         && orderdetails.TeleOrdID == orderId
                                                        select new ProductModel
                                                        {
                                                            ProductName = productEx.ProductName + " dengan " + orderex.ExchangeWith,
                                                            Quantity = orderex.ExchangeQuantity,

                                                            ProductRefillName = "",
                                                            RefillQuantity = 0,

                                                            //TotalAmount = orderex.ExchangePrice,
                                                            //TubePrice = (orderex.ExchangeQuantity != 0) ? Math.Truncate((orderex.ExchangePrice / orderex.ExchangeQuantity) * 1000 / 1000) : 0,

                                                            TotalAmount = orderex.SubTotal,
                                                            TubePrice = orderex.ExchangePrice,

                                                            TotalPromoProduct = orderex.ExchangePromoPrice,
                                                            PromoPrice = orderex.ExchangePromoPrice * orderex.ExchangeQuantity,

                                                            TotalRefillAmount = 0,
                                                            RefillPrice = 0,

                                                            TotalPromoRefillAmount = 0,
                                                            PromoRefillAmount = 0,

                                                            TotalShippingAmount = 0,
                                                            ShippingAmount = orderdetails.ShippingCharge,

                                                            TotalPromoShippingAmount = 0,
                                                            PromoShippingAmount = orderdetails.PromoShipping,
                                                        })
                                                       .ToList()


                                    }).FirstOrDefault();

                        return rtns;
                    }
                }
            }
            else
            {
                return new OrderModel();
            }
        }


        public List<ProductModel> GetProductListForOrder(OrderModel model, int orderId)
        {
            List<ProductModel> prodList = new List<ProductModel>();
            using (var context = new lpgdevEntities())
            {
                var prodQuery = (from orderdetails in context.OrderDetails
                                 join product in context.Products
                                 on orderdetails.ProdID equals product.ProdID
                                 where orderdetails.OrdrID == orderId
                                 select new ProductModel
                                 {
                                     ProductName = product.ProductName,
                                     TubePrice = (orderdetails.RefillQuantity != 0) ? orderdetails.RefillPrice : orderdetails.UnitPrice,
                                     Quantity = (orderdetails.RefillQuantity != 0) ? orderdetails.RefillQuantity : orderdetails.Quantity,
                                     TotalAmount = (orderdetails.RefillQuantity != 0) ? (orderdetails.RefillPrice * orderdetails.RefillQuantity) : orderdetails.UnitPrice * orderdetails.Quantity,

                                     PromoPrice = (orderdetails.RefillQuantity != 0) ? orderdetails.PromoRefill : orderdetails.PromoProduct,
                                     PostalFee = orderdetails.ShippingCharge,
                                     PromoPostage = orderdetails.PromoShipping,
                                     TotalIAmount = (orderdetails.RefillQuantity != 0) ? (orderdetails.ShippingCharge * orderdetails.RefillQuantity) : (orderdetails.ShippingCharge * orderdetails.Quantity),
                                     TotalPAmount = (orderdetails.RefillQuantity != 0) ? (orderdetails.PromoRefill * orderdetails.RefillQuantity) : (orderdetails.PromoRefill * orderdetails.Quantity),
                                     TotalNEnds = (orderdetails.RefillQuantity != 0) ? (orderdetails.PromoShipping * orderdetails.RefillQuantity) : (orderdetails.PromoShipping * orderdetails.Quantity)

                                 });
                prodList = prodQuery.ToList();
            }


            return prodList;
        }

        public bool deleteOrder(int id)
        {
            bool status = false;
            using (var context = new lpgdevEntities())
            {
                var data = context.Orders.Where(r => r.OrdrID == id).FirstOrDefault();
                if (data != null)
                {
                    context.Orders.Remove(data);
                    context.SaveChanges();
                    status = true;
                }
            }
            return status;
        }


        public bool UpdateOrderStatus(int id, int loggedBy, short orderStatusID)
        {
            bool status = false;
            using (var context = new lpgdevEntities())
            {
                var data = context.Orders.Where(r => r.OrdrID == id).FirstOrDefault();
                if (data != null)
                {

                    data.StatusID = orderStatusID;
                    data.UpdatedBy = loggedBy;
                    data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");


                    var orderDelivery = data.OrderDeliveries.FirstOrDefault();

                    orderDelivery.StartDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");



                    if (orderStatusID == ID_ORDER_DELIVERED) //Delivered
                    {
                        var driverDeliveredOn = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        var slotStartDate = data.DeliveryDate + data.MDeliverySlot.StartTime;
                        var slotEndDate = data.DeliveryDate + data.MDeliverySlot.EndTine;

                        if (driverDeliveredOn >= slotStartDate && driverDeliveredOn <= slotEndDate) //Delivered On Time - Within TimeSlot
                        {
                            orderDelivery.deviation = 0;
                            orderDelivery.StatusId = DELIVERY_STATUS_ONTIMEDELIVERY;
                        }
                        else if (driverDeliveredOn < slotStartDate) //
                        {
                            orderDelivery.deviation = (driverDeliveredOn - slotStartDate).TotalMinutes.ToInt();
                            orderDelivery.StatusId = DELIVERY_STATUS_EARLYDELIVERY;
                        }
                        else if (driverDeliveredOn > slotEndDate)
                        {
                            orderDelivery.deviation = (driverDeliveredOn - slotEndDate).TotalMinutes.ToInt();
                            orderDelivery.StatusId = DELIVERY_STATUS_LATEDELIVERY;
                        }



                        ReadAndSendPushNotification(APPSETTING_MSG_TO_CONSUMER_FOR_RATING, APPSETTING_TITLE_FOR_CONSUMER_TO_RATE, data.Consumer.AppToken, data.OrdrID, data.DrvrID.HasValue ? data.DrvrID.Value : 0, 0, PushMessagingService.APPSETTING_APPLICATION_ID_CONSUMER, PushMessagingService.APPSETTING_SENDER_ID_CONSUMER, (int) PushMessagingService.PushType.TypeTwo,data.Consumer.ConsID,data.Consumer.Name,data.Consumer.PhoneNumber, APPSETTING_USER_ROLE_PELANGGAN);
                    }
                    if (orderStatusID == ID_ORDER_CANCELLED_BY_HQ_SUSER) // Cancelled
                    {
                        orderDelivery.StatusId = DELIVERY_STATUS_CANCELLED; //Cancelled

                        ReadAndSendPushNotification(APPSETTING_MSG_FOR_CONSUMER_AT_SADMIN_CANCEL, APPSETTING_TITLE_FOR_CONSUMER_AT_SADMIN_CANCEL, data.Consumer.AppToken, data.OrdrID, 0, 0, PushMessagingService.APPSETTING_APPLICATION_ID_CONSUMER, PushMessagingService.APPSETTING_SENDER_ID_CONSUMER, (int)PushMessagingService.PushType.TypeThree, data.Consumer.ConsID, data.Consumer.Name, data.Consumer.PhoneNumber, APPSETTING_USER_ROLE_PELANGGAN);
                    }
                    status = true;

                    context.SaveChanges();

                }
                else
                {
                    var dataTele = context.TeleOrders.Where(r => r.TeleOrdID == id).FirstOrDefault();
                    if (dataTele != null)
                    {

                        dataTele.StatusId = orderStatusID;
                        dataTele.UpdatedBy = loggedBy;
                        dataTele.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        context.SaveChanges();
                        status = true;
                    }
                }
            }
            return status;
        }

        public void ReadAndSendPushNotification(string messageTypeKey, string msgTitleKey, string appToken, int orderId, int driverId, int orderCount, string applicationId, string senderId, int type, int userId, string userName, string mobile, string role)
        {
            string msgContent = Common.GetAppSetting<string>(messageTypeKey, string.Empty);
            msgContent = msgContent.Replace("{order_id}", orderId.ToString());
            msgContent = msgContent.Replace("{order_count}", orderCount > 0 ? orderCount.ToString() : string.Empty);
            msgContent = msgContent.Replace("{driver_id}", driverId.ToString());
            string msgTitle = Common.GetAppSetting<string>(msgTitleKey, string.Empty);
            PushMessagingService.SendPushNotification(appToken, msgContent, msgTitle, applicationId, senderId, orderId, driverId, type,userId,userName,mobile,role);
        }
    }
}
