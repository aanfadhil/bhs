﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.Orders
{
    public class TeleOrderDeliveryDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int TelDelvID { get; set; }
        public int TeleOrdID { get; set; }
        public int DrvrID { get; set; }
        public int AgadmID { get; set; }
        public DateTime AcceptedDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public short StatusId { get; set; }
        public int deviation { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class TeleOrderDeliveryMapper : MapperBase<TeleOrderDelivery, TeleOrderDeliveryDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<TeleOrderDelivery, TeleOrderDeliveryDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<TeleOrderDelivery, TeleOrderDeliveryDTO>>) (p => new TeleOrderDeliveryDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    TelDelvID = p.TelDelvID,
                    TeleOrdID = p.TeleOrdID,
                    DrvrID = p.DrvrID,
                    AgadmID = p.AgadmID,
                    AcceptedDate = p.AcceptedDate,
                    StartDate = p.StartDate,
                    DeliveryDate = p.DeliveryDate,
                    StatusId = p.StatusId,
                    deviation = p.deviation,
                    CreatedDate = p.CreatedDate
                }));
            }
        }

        public override void MapToModel(TeleOrderDeliveryDTO dto, TeleOrderDelivery model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.TelDelvID = dto.TelDelvID;
            model.TeleOrdID = dto.TeleOrdID;
            model.DrvrID = dto.DrvrID;
            model.AgadmID = dto.AgadmID;
            model.AcceptedDate = dto.AcceptedDate;
            model.StartDate = dto.StartDate;
            model.DeliveryDate = dto.DeliveryDate;
            model.StatusId = dto.StatusId;
            model.deviation = dto.deviation;
            model.CreatedDate = dto.CreatedDate;

        }
    }
}
