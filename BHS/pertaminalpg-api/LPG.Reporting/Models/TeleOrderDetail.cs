namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TeleOrderDetail
    {
        [Key]
        public int TeleOrdDetID { get; set; }

        public int TeleOrdID { get; set; }

        public int ProdID { get; set; }

        public int Quantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal UnitPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PromoProduct { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ShippingCharge { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PromoShipping { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TotalAmount { get; set; }

        public DateTime CreatedDate { get; set; }

        public int RefillQuantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal RefillPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PromoRefill { get; set; }

        [Column(TypeName = "numeric")]
        public decimal RefillSubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal RefillTotalAmount { get; set; }

        public virtual Product Product { get; set; }

        public virtual TeleOrder TeleOrder { get; set; }
    }
}
