﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace LPG.Models
{
    public class SuperAdminModel
    {
        public short sAdminId { get; set; }
        [Required(ErrorMessage = "Full Name Required")]
        public string full_name { get; set; }
        [Required(ErrorMessage = "Invalid mobile number")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid Mobile Number.")]
        public string mobile_number { get; set; }
        public string password { get; set; }
        public string new_password { get; set; }
        public string confirm_password { get; set; }
        public string profile_image { get; set; }
        public string appToken { get; set; }
        public Nullable<System.DateTime> last_login { get; set; }
        [Required(ErrorMessage = "User Role Required")]
        public short userTypeId { get; set; }        
        public string type_name { get; set; }
        public bool statusId { get; set; }
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string email { get; set; }
        [Required(ErrorMessage = "Your city is required")]
        public int cityId { get; set; }
        public string city_name { get; set; }
        public int provinceId { get; set; }
        public string province_name { get; set; }
        public string errorMessage { get; set; }
        public string accToken { get; set; }
        public string appId { get; set; }
        public List<SuperAdminModel> SuperAdminModelList { get; set; }
        public List<MSuperUserTypeModel> userTypeList { get; set; }
        public List<SelectListItem> DDList { get; set; }
        public List<SelectListItem> ProvList { get; set; }
        public List<SelectListItem> CityList { get; set; }
        

        public string CountryCode { get { return Constants.APPSETTING_COUNTRY_CODE; } }
    }
}