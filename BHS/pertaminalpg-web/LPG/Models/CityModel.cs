﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class CityModel
    {
        public int ID { get; set; }
        public int ProvID { get; set; }
        public String CityCode { get; set; }
        public String CityName { get; set; }
    }
}