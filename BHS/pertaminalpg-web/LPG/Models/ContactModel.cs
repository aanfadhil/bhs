﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LPG.Models
{
    public class ContactModel
    {
        public int ContactId { get; set; }
        [Required(ErrorMessage = "Description required")]
        public string Description { get; set; }         
        [Required(ErrorMessage = "Image required")]
        public string BannerImage { get; set; }
        public bool IsActive { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public List<ContactModel> ContactList { get; set; }
        public bool IsExists { get; set; }
    }
}