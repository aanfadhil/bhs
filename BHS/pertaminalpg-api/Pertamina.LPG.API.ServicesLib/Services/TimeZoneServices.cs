﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace Pertamina.LPG.API.Services
{
    public class TimeZoneServices
    {
        public static DateTime GetLocalDateTime(double latitude, double longitude, DateTime utcDate)
        {
            var client = new RestClient("https://maps.googleapis.com/maps/api/timezone/json?location="+ latitude.ToString().Replace(',', '.') + "," + longitude.ToString().Replace(',', '.')+ "&timestamp="+utcDate.ToTimestamp()+"&key="+ Common.GetAppSetting<string>("TimezoneApiKey", string.Empty));
            var request = new RestRequest(Method.GET);
            //request.AddParameter("location", latitude.ToString().Replace(',','.') + "," + longitude.ToString().Replace(',', '.'));
            //request.AddParameter("timestamp", utcDate.ToTimestamp());
            //request.AddParameter("key ", Common.GetAppSetting<string>("TimezoneApiKey", string.Empty));
            var response = client.Execute<GoogleTimeZone>(request);

            return utcDate.AddSeconds(response.Data.rawOffset + response.Data.dstOffset);
        }
    }

    public class GoogleTimeZone
    {
        public double dstOffset { get; set; }
        public double rawOffset { get; set; }
        public string status { get; set; }
        public string timeZoneId { get; set; }
        public string timeZoneName { get; set; }
    }

    public static class ExtensionMethods
    {
        public static double ToTimestamp(this DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }
    }
}
