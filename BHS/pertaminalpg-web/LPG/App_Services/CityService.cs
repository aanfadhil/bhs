﻿using LPG.App_Classes;
using LPG.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Configuration;
using System.Web.Configuration;
using System.IO;
using System.Web.Mvc;
using Microsoft.ApplicationInsights;
using OfficeOpenXml;
using System.Web.UI.WebControls;

namespace LPG.App_Services
{
    public class CityService : AbstractAppService, ICityService
    {
        public CityService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            throw new NotImplementedException();
        }

        public CityModel SaveCity(CityModel model)
        {   
            try
            {
                var Data = context.MCities.Where(r => r.ID == model.ID).FirstOrDefault();
                if(Data != null)
                {
                    Data.CityCode = model.CityCode;
                    Data.CityName = model.CityName;
                    
                    Data.ProvID = model.ProvID;
                    context.SaveChanges();
                }
                else
                {
                    MCity city = new MCity();
                    city.CityCode = model.CityCode;
                    city.CityName = model.CityName;
                    city.ProvID = model.ProvID;
                    context.MCities.Add(city);
                    context.SaveChanges();
                }
                return model;
            }
            catch(Exception ex)
            {
                string msg = ex.Message.ToString();
                throw;
            }
        }

        public CityModel GetCity(int cityID)
        {
            var data = (from city in context.MCities
                        where city.ID == cityID
                        select new CityModel
                        {
                            CityCode = city.CityCode,
                            CityName = city.CityName,
                            ProvID = city.ProvID,
                            ID = city.ID
                        }).FirstOrDefault();
            return data;
        }

        public List<CityModel> ListCity()
        {
            var cityList = new List<CityModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from city in context.MCities
                           select new CityModel
                           {
                               ID = city.ID,
                               CityCode = city.CityCode,
                               CityName = city.CityName,
                               ProvID = city.ProvID
                           };

                cityList = data.ToList();
            }
            return cityList;
        }
    }
}