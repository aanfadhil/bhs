﻿using LPG.App_Classes;

namespace LPG.App_Services
{
    public interface ILoginService
    {
        AdminSessionEntity adminLogin(string email, string Password);
        AdminSessionEntity AdminLoginActiveDirectoryAsync(string username, string password);
        void ChangePassword(int user_id, string new_password, string email);
        string ForgotPassword(string EmailId, string new_password, string confirm_password);
    }
}