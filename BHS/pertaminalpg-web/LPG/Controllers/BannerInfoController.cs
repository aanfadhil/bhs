﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;
using i18n;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class BannerInfoController : AbstractController<IBannerInfoService>
    {
        SessionManagement sessionManagement;
        private IPengaturanPromoServices PengaturanPromoServices;

        public BannerInfoController(IBannerInfoService _serviceObject,IPengaturanPromoServices _pengaturanPromoServices) : base(_serviceObject)
        {
            PengaturanPromoServices = _pengaturanPromoServices;
        }

        //BannerInfoService serviceObject = new BannerInfoService();
        // GET: BannerPromo
        public ActionResult AddBanner(int? id,int? promoId)
        {

            try
            {
                BannerInfoModel model = new BannerInfoModel();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var convid = id ?? 0;
                    if (convid > 0)
                    {

                        var vm = serviceObject.getBannerInfoDetails(convid);
                        if (vm != null)
                        {
                            BindPositions(vm.BannerInfoId);
                            return View(vm);
                        }
                        else
                        {
                            if (promoId != null && promoId.HasValue) {
                                model.PengaturanPromo = PengaturanPromoServices.GetPromo(promoId.Value);
                                model.Caption = model.PengaturanPromo.Title;
                                model.SyaratKetentuan = BuildTermCondition(model.PengaturanPromo);
                            }

                            BindPositions(null);
                            return View(model);
                        }
                    }
                    else
                    {
                        BindPositions(null);
                        if (promoId != null && promoId.HasValue)
                        {
                            model.PengaturanPromo = PengaturanPromoServices.GetPromo(promoId.Value);
                            model.Caption = model.PengaturanPromo.Title;
                            model.SyaratKetentuan = BuildTermCondition(model.PengaturanPromo);
                        }
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }

            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
        }

        private string BuildTermCondition(PengaturanPromoModel data)
        {
            var result = "";

            result += "Kode Voucher : " + data.Voucher;
            result += Environment.NewLine+"Potongan :"+ string.Format("{0:n0}", data.Potongan);
            result += Environment.NewLine + "Quota Per User :" + string.Format("{0:n0}", data.UserQuota);
            if (data.Quota.HasValue) result += Environment.NewLine + "Quota keseluruhan :" + string.Format("{0:n0}", data.Quota);
            if (data.DStartDate.HasValue) result += Environment.NewLine + "Mulai :" + data.DStartDate.Value.ToString("dd MM yyyy HH:mm");
            if (data.DEndDate.HasValue) result += Environment.NewLine + "Berakhir :" + data.DEndDate.Value.ToString("dd MM yyyy HH:mm");

            if (data.Products.Count > 0) result += Environment.NewLine + "Untuk Produk :" + string.Join(",", data.Products.Select(x => x.ProductName.ToString()).ToArray());
            if (data.Regions.Count > 0) result += Environment.NewLine + "Di Region :" + string.Join(",", data.Regions.Select(x => x.RegionName.ToString()).ToArray());
            if(data.Customers.Count > 0) result += Environment.NewLine + "Untuk Pelanggan :" + string.Join("\n", data.Customers.Select(x => x.Name.ToString()).ToArray());

            return result;
        }

        public ActionResult ListBanner()
        {

            var vm = new BannerInfoModel();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.BannerInfoList = serviceObject.ListBannerInfo();
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult AddBanner(BannerInfoModel vm, string Cmd,int? promoId)
        {
            if (!ModelState.IsValid)
            {
                BindPositions(null);
                return View(vm);
            }
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    if (vm.PengaturanPromo == null && promoId.HasValue) {
                        vm.PengaturanPromo = new PengaturanPromoModel()
                        {
                            PromoID = promoId.Value
                        };
                    }
                    serviceObject.saveBannerDetails(vm, Cmd, adminSessionInfo.UserId);

                    SetFlash(FlashMessageType.Success, "Input info banner berhasil");

                    if (promoId != null)
                        return RedirectToAction("ListPromo", "PengaturanPromo");
                    else
                        return RedirectToAction("ListBanner", "BannerInfo");

                }
                else
                {
                    return RedirectToAction("Index", "Login", new { area = "Admin" });
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message.ToString();
                return RedirectToAction("Index", "Error", this.AddToastMessage("Error", "[[[Error while updating promo info banner]]]", ToastType.Error));
            }
        }

        public ActionResult deleteBannerInfo(int? id)
        {

            if (id != null & id > 0)
            {

                try
                {
                    sessionManagement = new SessionManagement();
                    AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                    ViewBag.Role = adminSessionInfo.UserRole;
                    if (adminSessionInfo != null)
                    {

                        serviceObject.deleteBannerInfo(id.ToInt(), adminSessionInfo.UserId);

                    }

                }
                catch
                {

                }
            }

            SetFlash(FlashMessageType.Success, "Delete banner info berhasil");

            return RedirectToAction("ListBanner", "BannerInfo");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            string _imgname = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    _imgname = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/extfiles/banners/BannerInfo_") + _imgname + _ext;
                    _imgname = "BannerInfo_" + _imgname + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    if (!Directory.Exists(Path.GetDirectoryName(path)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(path));
                    }

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width > 200)
                        img.Resize(1000, 1000, true, true);
                    img.Save(_comPath);
                    // end resize
                }
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }


        public void BindPositions(int? InfoID)
        {
            var CurrentList = new List<SelectListItem>();
            var positionlist = new List<SelectListItem>();
            for (var i = 1; i < 100; i++)
                positionlist.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            CurrentList = serviceObject.GetAvailablePositions(InfoID);
            positionlist.RemoveAll(x => CurrentList.Exists(y => y.Value == x.Value));
            ViewBag.vbPositionlist = positionlist;
        }


    }
}
