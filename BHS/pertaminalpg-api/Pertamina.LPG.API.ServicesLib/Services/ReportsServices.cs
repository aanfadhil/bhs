﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pertamina.LPG.API.DTOs;
using Pertamina.LPG.API.DAOs;
using Pertamina.LPG.API.DTOs.Reports;
using Pertamina.LPG.API.Models;
using Pertamina.LPG.API.Services.Helpers;
using System.Globalization;
using Pertamina.LPG.API.Utils;
using System.Data;
using LPG.Reporting.ReportServices;

namespace Pertamina.LPG.API.Services
{


    public class ReportsServices
    {
        public const string APPSETTING_REPORTPERIOD_RANGE = "ReportPeriodRange";

        private SalesReportService _salesReportService;

        public ReportsServices()
        {
            _salesReportService = new SalesReportService();
        }

        public static void SendMasterDataFile()
        {
            //var context = new PertaminaLpgDbEntities();

            DataTable dt = new DataTable("MasterData");

            dt.Columns.AddRange(new DataColumn[18] { new DataColumn("[[[Date]]]"),
                                            new DataColumn("[[[Nama Pelanggan]]]"),
                                            new DataColumn("[[[No Invoice]]]"),
                                            new DataColumn("[[[Agen]]]"),
                                            new DataColumn("[[[Region]]]"),
                                            new DataColumn("[[[Kota]]]"),
                                            new DataColumn("[[[Produk]]]"),
                                            new DataColumn("[[[Jumlah]]]"),
                                            new DataColumn("[[[Tanggal & Waktu Pengiriman]]]"),
                                            new DataColumn("[[[Metode Beli]]]"),
                                            new DataColumn("[[[Status]]]"),
                                            new DataColumn("[[[Category]]]"),
                                            new DataColumn("[[[Total Harga]]]"),
                                            new DataColumn("[[[Total KG]]]"),
                                            new DataColumn("[[[Sum of KG]]]"),
                                            new DataColumn("[[[Rating]]]"),
                                            new DataColumn("[[[Reason]]]"),
                                            new DataColumn("[[[Note]]]"),
            });

            using (var dao = new SuperUserDao())
            {
                //MasterDataExtractionResult
                //var customerData = dao.
            }

        }

        public static GetProductResponse GetProductsByAgentBoss(GetProductRequest request)
        {
            GetProductResponse response = new GetProductResponse();
            try
            {
                if (request.is_boss)
                {
                    if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
                    {
                        return response;
                    }
                }
                else
                {
                    if (!SuperUserServices.CheckSuperUser(request.user_id, request.auth_token, response))
                    {
                        return response;
                    }
                }
                response.product_names = new List<GetProductsDto>();
                using (OrderDao dao = new OrderDao())
                {
                    var productList = dao.GetProductsByAgentBoss(request.is_boss ? request.user_id : 0);
                    if (productList != null && productList.Count > 0)
                    {
                        response.product_names = productList.Select(r => new GetProductsDto
                        {
                            product_id = r.ProdID,
                            product_name = r.ProductName
                        }).ToList();
                    }
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("boss.prdt.listed");
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                return response;
            }
        }

        public static GetDriverNameResponse GetDriversByAgentBoss(GetDriverNameRequest request)
        {
            GetDriverNameResponse response = new GetDriverNameResponse();
            try
            {
                if (request.is_boss)
                {
                    if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
                    {
                        return response;
                    }
                }
                else
                {
                    if (!SuperUserServices.CheckSuperUser(request.user_id, request.auth_token, response))
                    {
                        return response;
                    }
                }
                response.driver_names = new List<GetDriverNameDto>();
                using (OrderDao dao = new OrderDao())
                {
                    var driverList = dao.GetDriversByAgentBoss(request.is_boss ? request.user_id : 0);
                    if (driverList != null && driverList.Count > 0)
                    {
                        response.driver_names = driverList.Select(r => new GetDriverNameDto
                        {
                            driver_id = r.DrvrID,
                            driver_name = r.DriverName
                        }).ToList();
                    }
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("boss.drv.listed");
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                return response;
            }
        }
        /// <summary>
        /// Sales report for Agent Boss
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static ABossSellerRptResponse GetSellerReportByAgentBoss(ABossSellerRptRequest request)
        {
            var productIdList = request.products;

            var _service = new SalesReportService();

            ABossSellerRptResponse response = new ABossSellerRptResponse();

            switch (request.total_type)
            {
                case 1: //Amount

                    response.sales_details = _service.GetSalesReportInUnits(new SalesReportRequestDTO()
                    {
                        //AgencyIDs = new List<int> { request.agency_id },
                        Period = ToReportPeriod(request.periodical_data),
                        ProductIds = request.products,
                        NumberOfPeriods = request.number_of_periods
                    }).Select(x => new ABossSellerRptDto()
                    {
                        key = x.Key.ToString(ToReportPeriodStringFormat(request.periodical_data)),
                        value = x.Value
                    }).ToList();

                    break;


                case 2: //Cash

                    response.sales_details = _service.GetSalesReportInCash(new SalesReportRequestDTO()
                    {
                        //AgencyIDs = new List<int> { request.agency_id },
                        Period = ToReportPeriod(request.periodical_data),
                        ProductIds = request.products,
                        NumberOfPeriods = request.number_of_periods
                    }).Select(x => new ABossSellerRptDto()
                    {
                        key = x.Key.ToString(ToReportPeriodStringFormat(request.periodical_data)),
                        value = (decimal) x.Value
                    }).ToList();

                    break;

                default:

                    break;
            }


            response.code = 0;
            response.has_resource = 1;
            response.message = MessagesSource.GetMessage("boss.sales.report");
            return response;

            //try
            //{
            //    if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
            //    {
            //        return response;
            //    }
            //    response.sales_details = new List<ABossSellerRptDto>();
            //    using (OrderDao dao = new OrderDao())
            //    {
            //        int periodRange = Common.GetAppSetting<int>(APPSETTING_REPORTPERIOD_RANGE, 6);

            //        if (productIdList != null && productIdList.Count > 0)
            //        {
            //            string productIds = string.Join(",", productIdList.Select(n => n.ToString()).ToArray());

            //            var sellerRpt = dao.GetSellerReportByAgentBoss(request.user_id, request.total_type, request.periodical_data, periodRange, productIds);
            //            if (sellerRpt != null && sellerRpt.Count > 0)
            //            {
            //                response.sales_details = sellerRpt.Select(r => new ABossSellerRptDto
            //                {
            //                    key = r.Period,
            //                    value = r.Value.ToDecimal()
            //                }).ToList();
            //            }
            //        }
            //        response.code = 0;
            //        response.has_resource = 1;
            //        response.message = MessagesSource.GetMessage("boss.sales.report");
            //        return response;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    response.MakeExceptionResponse(ex);
            //    return response;
            //}
        }



        public static ABossReviewReportResponse GetReviewReportByAgentBoss(ABossReviewReportRequest request)
        {
            ABossReviewReportResponse response = new ABossReviewReportResponse();
            try
            {
                if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                response.service_rating = new List<ABossReviewReportDto>();
                using (OrderDao dao = new OrderDao())
                {
                    int periodRange = Common.GetAppSetting<int>(APPSETTING_REPORTPERIOD_RANGE, 6);
                    var reportDetails = dao.GetReviewReportByAgentBoss(request.user_id, request.driver_id, request.periodical_data, periodRange);
                    if (reportDetails != null && reportDetails.Count > 0)
                    {
                        response.service_rating = reportDetails.Select(r => new ABossReviewReportDto
                        {
                            key = r.Period,
                            value = r.Value.ToDecimal()
                        }).ToList();
                    }
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("boss.sales.report");
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                return response;
            }

        }

        public static ABossReviewReasonResponse GetReviewReasonByAgentBoss(ABossReviewReasonRequest request)
        {
            ABossReviewReasonResponse response = new ABossReviewReasonResponse();
            try
            {
                if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                response.service_reason_rating = new List<ABossReviewReasonDto>();
                using (OrderDao dao = new OrderDao())
                {
                    int periodRange = Common.GetAppSetting<int>(APPSETTING_REPORTPERIOD_RANGE, 6);
                    var reportDetails = dao.GetReviewReasonByAgentBoss(request.user_id, request.driver_id, request.periodical_data, periodRange);
                    if (reportDetails != null && reportDetails.Count > 0)
                    {
                        response.service_reason_rating = reportDetails.Select(r => new ABossReviewReasonDto
                        {
                            key = r.ReasonText,
                            value = r.Value.ToDecimal()
                        }).ToList();
                    }
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("boss.rating.report");
                    return response;

                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                return response;
            }

        }

        public static GetAgencyNameResponse GetAgencyNames(GetAgencyNameRequest request)
        {

            GetAgencyNameResponse response = new GetAgencyNameResponse();
            try
            {
                if (!SuperUserServices.CheckSuperUser(request.user_id, request.auth_token, response))
                {
                    response.message = MessagesSource.GetMessage("invalid.super.user");
                    return response;
                }



                response.agency_names = new List<GetAgencyNameDto>();
                using (AgencyDao dao = new AgencyDao())
                {
                    SuperAdmin superAdmin = null;

                    using (SuperUserDao suDao = new SuperUserDao())
                    {
                        superAdmin = suDao.FindByIdWithDetails(request.user_id);
                    }

                    if(superAdmin == null)
                    {
                        response.message = MessagesSource.GetMessage("invalid.super.user");
                        return response;
                    }

                    var agencyList = dao.GetAgenciesBySE(request.user_id);
                    if (agencyList != null && agencyList.Count > 0)
                    {
                        response.agency_names = agencyList.Select(r => new GetAgencyNameDto
                        {
                            agency_id = r.AgenID,
                            agency_name = r.AgencyName
                        }).ToList();
                    }

                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("agencies.listed");
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                return response;
            }
        }

        public static SUserReviewReportResponse GetSUserReviewReport(SUserReviewReportRequest request)
        {
            SUserReviewReportResponse response = new SUserReviewReportResponse();
            try
            {
                if (!SuperUserServices.CheckSuperUser(request.user_id, request.auth_token, response))
                {
                    response.message = MessagesSource.GetMessage("invalid.super.user");
                    return response;
                }
                response.service_rating = new List<SUserReviewReportDto>();
                using (OrderDao dao = new OrderDao())
                {
                    int periodRange = Common.GetAppSetting<int>(APPSETTING_REPORTPERIOD_RANGE, 4);
                    var reportDetails = dao.GetReviewReportBySUser(request.user_id, request.agency_id, request.periodical_data, periodRange);
                    if (reportDetails != null && reportDetails.Count > 0)
                    {
                        response.service_rating = reportDetails.Select(r => new SUserReviewReportDto
                        {
                            key = r.Period,
                            value = r.Value.ToDecimal()
                        }).ToList();
                        
                        //if(request.periodical_data == 1)
                            response.service_rating.Reverse();
                    }

                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("suser.sales.report");
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                return response;
            }

        }
        public static SUserReviewReasonResponse GetSUserReviewReasonReport(SUserReviewReasonRequest request)
        {
            SUserReviewReasonResponse response = new SUserReviewReasonResponse();
            try
            {
                if (!SuperUserServices.CheckSuperUser(request.user_id, request.auth_token, response))
                {
                    response.message = MessagesSource.GetMessage("invalid.super.user");
                    return response;
                }
                response.service_reason_rating = new List<SUserReviewReasonDto>();
                using (OrderDao dao = new OrderDao())
                {
                    int periodRange = Common.GetAppSetting<int>(APPSETTING_REPORTPERIOD_RANGE, 6);
                    var reportDetails = dao.GetReviewReasonBySUser(request.user_id, request.agency_id, request.periodical_data, periodRange);
                    if (reportDetails != null && reportDetails.Count > 0)
                    {
                        response.service_reason_rating = reportDetails.Select(r => new SUserReviewReasonDto
                        {
                            key = r.ReasonText,
                            value = r.Value.ToDecimal()
                        }).ToList();
                    }
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("suser.rating.report");
                    return response;

                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                return response;
            }

        }

        private static ReportPeriod ToReportPeriod(int periodicData)
        {

            switch (periodicData)
            {
                case 1: //Month
                    return ReportPeriod.Month;

                case 2: //Week
                    return ReportPeriod.Week;

                case 3: //Year
                    return ReportPeriod.Year;
                default:
                    return ReportPeriod.Week;

            }

        }

        private static string ToReportPeriodStringFormat(int periodicData)
        {

            switch (periodicData)
            {
                case 1: //Month
                    return "MMM yyy";

                case 2: //Week
                    return "dd MMM";
                case 3: //Year
                    return "yyyy";
                default:
                    return "dd MMM";
            }

        }
        
        public static SUserSellerRptResponse GetSellerReportBySuperUser(SUserSellerRptRequest request)
        {

            SUserSellerRptResponse response = new SUserSellerRptResponse();

            var _service = new SalesReportService();

            var allowedIds = (new OrderDao()).GetSaCities(request.user_id);

            switch (request.total_type)
            {
                case 1: //Amount

                    response.sales_details = _service.GetSalesReportInUnitss(new SalesReportRequestDTO()
                    {
                        AgencyIDs = new List<int> { request.agency_id },
                        Period = ToReportPeriod(request.periodical_data),
                        ProductIds = request.products.Select(x => x.product_id).ToList(),
                        NumberOfPeriods = 4
                    },allowedIds).Select(x => new SUserSellerRptDto()
                    {
                        key = x.Key.ToString(ToReportPeriodStringFormat(request.periodical_data)),
                        value = (decimal) x.Value
                    }).ToList();

                    break;


                case 2: //Cash

                    response.sales_details = _service.GetSalesReportInCashs(new SalesReportRequestDTO()
                    {
                        AgencyIDs = new List<int> { request.agency_id },
                        Period = ToReportPeriod(request.periodical_data),
                        ProductIds = request.products.Select(x => x.product_id).ToList(),
                        NumberOfPeriods = 4
                    },allowedIds).Select(x => new SUserSellerRptDto()
                    {
                        key = x.Key.ToString(ToReportPeriodStringFormat(request.periodical_data)),
                        value = (decimal) x.Value
                    }).ToList();

                    break;

                default:

                    break;
            }


            response.code = 0;
            response.has_resource = 1;
            response.message = MessagesSource.GetMessage("suser.sales.report");

            return response;


            //try
            //{
            //    var productIdList = request.products.Select(x => x.product_id).ToList();
            //    if (!SuperUserServices.CheckSuperUser(request.user_id, request.auth_token, response))
            //    {
            //        response.message = MessagesSource.GetMessage("invalid.super.user");
            //        return response;
            //    }
            //    response.sales_details = new List<SUserSellerRptDto>();
            //    using (OrderDao dao = new OrderDao())
            //    {
            //        int periodRange = Common.GetAppSetting<int>(APPSETTING_REPORTPERIOD_RANGE, 6);

            //        if (productIdList != null && productIdList.Count > 0)
            //        {
            //            string productIds = string.Join(",", productIdList.Select(n => n.ToString()).ToArray());

            //            var sellerRpt = dao.GetSellerReportBySUser(request.user_id, request.total_type, request.periodical_data, periodRange, request.number_of_products, productIds, request.agency_id);
            //            if (sellerRpt != null && sellerRpt.Count > 0)
            //            {
            //                response.sales_details = sellerRpt.Select(r => new SUserSellerRptDto
            //                {
            //                    key = r.Period,
            //                    value = r.Value.ToDecimal()
            //                }).ToList();
            //            }
            //        }
            //        response.code = 0;
            //        response.has_resource = 1;
            //        response.message = MessagesSource.GetMessage("suser.sales.report");
            //        return response;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    response.MakeExceptionResponse(ex);
            //    return response;
            //}
        }




        //AgentBoss
        public static ReportKeyValueListResponseFloatDto GetAgentBossReportSellerOnTimeRequest(AgentBossReportSellerOnTimeRequest request)
        {

            ReportKeyValueListResponseFloatDto response = new ReportKeyValueListResponseFloatDto();

            try
            {
                if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
                {
                    response.message = MessagesSource.GetMessage("invalid.agentboss");
                    return response;
                }

                SellerReportOnTimeRequest req = new SellerReportOnTimeRequest
                {
                    entity_id = request.driver_id,
                    periodical_data = request.periodical_data,
                    for_role = UserType.AgentBoss

                };

                return ReportsServices.GetSellerReportOnTime(req);
            }
            catch (Exception e)
            {

                response.MakeExceptionResponse(e);
                return response;

            }
        }


        public static ReportKeyValueListResponseFloatDto GetAgentBossSellerReportDelired(AgentBossReportSellerDeliveredRequest request)
        {

            ReportKeyValueListResponseFloatDto response = new ReportKeyValueListResponseFloatDto();

            try
            {
                if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
                {
                    response.message = MessagesSource.GetMessage("invalid.agentboss");
                    return response;
                }

                SellerReportDeliveredRequest req = new SellerReportDeliveredRequest
                {
                    entity_id = request.driver_id,
                    periodical_data = request.periodical_data,
                    for_role = UserType.AgentBoss
                };

                return ReportsServices.GetSellerReportSellerReportDelivered(req);
            }
            catch (Exception e)
            {

                response.MakeExceptionResponse(e);
                return response;

            }

        }



        //Super User
        public static ReportKeyValueListResponseFloatDto GetSuperUserReportSellerOnTime(SuperUserReportSellerOnTimeRequest request)
        {
            ReportKeyValueListResponseFloatDto response = new ReportKeyValueListResponseFloatDto();

            try
            {
                if (!SuperUserServices.CheckSuperUser(request.user_id, request.auth_token, response))
                {
                    response.message = MessagesSource.GetMessage("invalid.super.user");
                    return response;
                }

                SellerReportOnTimeRequest req = new SellerReportOnTimeRequest
                {
                    entity_id = request.agency_id,
                    periodical_data = request.periodical_data,
                    for_role = UserType.SuperUser
                };

                return ReportsServices.GetSellerReportOnTime(req,request.user_id);
            }
            catch (Exception e)
            {

                response.MakeExceptionResponse(e);
                return response;

            }
        }


        public static ReportKeyValueListResponseFloatDto GetSuperUserSellerReportDelivered(SuperUserReportSellerDeliveredRequest request)
        {

            ReportKeyValueListResponseFloatDto response = new ReportKeyValueListResponseFloatDto();

            try
            {
                if (!SuperUserServices.CheckSuperUser(request.user_id, request.auth_token, response))
                {
                    response.message = MessagesSource.GetMessage("invalid.super.user");
                    return response;
                }

                SellerReportDeliveredRequest req = new SellerReportDeliveredRequest
                {
                    entity_id = request.agency_id,
                    periodical_data = request.periodical_data,
                    for_role = UserType.SuperUser
                };

                return ReportsServices.GetSellerReportSellerReportDelivered(req,request.user_id);
            }
            catch (Exception e)
            {

                response.MakeExceptionResponse(e);
                return response;

            }

        }


        //Common methods
        public static ReportKeyValueListResponseFloatDto GetSellerReportSellerReportDelivered(SellerReportDeliveredRequest request)
        {

            ReportKeyValueListResponseFloatDto resp = new ReportKeyValueListResponseFloatDto
            {
                report = new List<ReportKeyValuePairDoubleDto>()
            };

            switch (request.for_role)
            {
                case UserType.SuperUser:

                    resp.code = 0;
                    resp.has_resource = 1;
                    resp.httpCode = System.Net.HttpStatusCode.OK;

                    switch (request.periodical_data)
                    {
                        case 1: //Month
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersDeliveredForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = DateTime.Parse(r.Key).ToString("MMM yyyy"),
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }

                            break;
                        case 2: //Week
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersDeliveredForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = r.Key,
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }

                            break;

                        default:

                            resp.message = "Wrong period. Only accept 1 for Month and 2 for Week" + request.periodical_data;
                            resp.code = 0;
                            resp.has_resource = 0;
                            resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                            break;
                    }

                    break;
                case UserType.AgentBoss:

                    resp.code = 0;
                    resp.has_resource = 1;
                    resp.httpCode = System.Net.HttpStatusCode.OK;

                    switch (request.periodical_data)
                    {
                        case 1: //Month
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersDeliveredForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = DateTime.Parse(r.Key).ToString("MMM yyyy"),
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }
                            break;

                        case 2: //Week
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersDeliveredForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = r.Key,
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }

                            break;

                        default:

                            resp.message = "Wrong period. Only accept 1 for Month and 2 for Week" + request.periodical_data;
                            resp.code = 0;
                            resp.has_resource = 0;
                            resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                            break;
                    }

                    break;
                case UserType.AgentAdmin:
                    resp.message = "Wrong role " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case UserType.Driver:
                    resp.message = "Wrong role " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case UserType.Consumer:
                    resp.message = "Wrong role " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                default:
                    resp.message = "Wrong role " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
            }



            return resp;
        }


        public static ReportKeyValueListResponseFloatDto GetSellerReportSellerReportDelivered(SellerReportDeliveredRequest request,int suId)
        {

            ReportKeyValueListResponseFloatDto resp = new ReportKeyValueListResponseFloatDto
            {
                report = new List<ReportKeyValuePairDoubleDto>()
            };

            switch (request.for_role)
            {
                case UserType.SuperUser:

                    resp.code = 0;
                    resp.has_resource = 1;
                    resp.httpCode = System.Net.HttpStatusCode.OK;

                    switch (request.periodical_data)
                    {
                        case 1: //Month
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersDeliveredForRole(request.entity_id, request.for_role, request.periodical_data,suId);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = DateTime.Parse(r.Key).ToString("MMM yyyy"),
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }

                            break;
                        case 2: //Week
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersDeliveredForRole(request.entity_id, request.for_role, request.periodical_data,suId);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = r.Key,
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }

                            break;

                        default:

                            resp.message = "Wrong period. Only accept 1 for Month and 2 for Week" + request.periodical_data;
                            resp.code = 0;
                            resp.has_resource = 0;
                            resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                            break;
                    }

                    break;
                case UserType.AgentBoss:

                    resp.code = 0;
                    resp.has_resource = 1;
                    resp.httpCode = System.Net.HttpStatusCode.OK;

                    switch (request.periodical_data)
                    {
                        case 1: //Month
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersDeliveredForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = DateTime.Parse(r.Key).ToString("MMM yyyy"),
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }
                            break;

                        case 2: //Week
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersDeliveredForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = r.Key,
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }

                            break;

                        default:

                            resp.message = "Wrong period. Only accept 1 for Month and 2 for Week" + request.periodical_data;
                            resp.code = 0;
                            resp.has_resource = 0;
                            resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                            break;
                    }

                    break;
                case UserType.AgentAdmin:
                    resp.message = "Wrong role " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case UserType.Driver:
                    resp.message = "Wrong role " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case UserType.Consumer:
                    resp.message = "Wrong role " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                default:
                    resp.message = "Wrong role " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
            }



            return resp;
        }


        public static ReportKeyValueListResponseFloatDto GetSellerReportOnTime(SellerReportOnTimeRequest request)
        {

            ReportKeyValueListResponseFloatDto resp = new ReportKeyValueListResponseFloatDto
            {
                report = new List<ReportKeyValuePairDoubleDto>()
            };

            switch (request.for_role)
            {
                case UserType.SuperUser:

                    resp.code = 0;
                    resp.has_resource = 1;
                    resp.httpCode = System.Net.HttpStatusCode.OK;

                    switch (request.periodical_data)
                    {
                        case 1: //Month
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersOnTimeForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = DateTime.Parse(r.Key).ToString("MMM yyyy"),
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }

                            break;

                        case 2: //Week
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersOnTimeForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = r.Key,
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }

                            break;

                        default:

                            resp.message = "[[[Wrong period. Only accept 1 for Month and 2 for Week]]] " + request.periodical_data;
                            resp.code = 1;
                            resp.has_resource = 0;
                            resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                            break;
                    }

                    break;
                case UserType.AgentBoss:

                    resp.code = 0;
                    resp.has_resource = 1;
                    resp.httpCode = System.Net.HttpStatusCode.OK;

                    switch (request.periodical_data)
                    {
                        case 1: //Month
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersOnTimeForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = DateTime.Parse(r.Key).ToString("MMM yyyy"),
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }
                            break;

                        case 2: //Week
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersOnTimeForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = r.Key,
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }
                            break;

                        default:

                            resp.message = "[[[Wrong period. Only accept 1 for Month and 2 for Week]]] " + request.periodical_data;
                            resp.code = 0;
                            resp.has_resource = 0;
                            resp.httpCode = System.Net.HttpStatusCode.BadRequest;

                            break;
                    }

                    break;
                case UserType.AgentAdmin:
                    resp.message = "[[[Wrong role]]] " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case UserType.Driver:
                    resp.message = "[[[Wrong role]]] " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case UserType.Consumer:
                    resp.message = "[[[Wrong role]]] " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                default:
                    resp.message = "[[[Wrong role]]] " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
            }

            return resp;

        }

        public static ReportKeyValueListResponseFloatDto GetSellerReportOnTime(SellerReportOnTimeRequest request,int suid)
        {

            ReportKeyValueListResponseFloatDto resp = new ReportKeyValueListResponseFloatDto
            {
                report = new List<ReportKeyValuePairDoubleDto>()
            };

            switch (request.for_role)
            {
                case UserType.SuperUser:

                    resp.code = 0;
                    resp.has_resource = 1;
                    resp.httpCode = System.Net.HttpStatusCode.OK;

                    switch (request.periodical_data)
                    {
                        case 1: //Month
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersOnTimeForRole(request.entity_id, request.for_role, request.periodical_data,suid);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = DateTime.Parse(r.Key).ToString("MMM yyyy"),
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }

                            break;

                        case 2: //Week
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersOnTimeForRole(request.entity_id, request.for_role, request.periodical_data,suid);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = r.Key,
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }

                            break;

                        default:

                            resp.message = "[[[Wrong period. Only accept 1 for Month and 2 for Week]]] " + request.periodical_data;
                            resp.code = 1;
                            resp.has_resource = 0;
                            resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                            break;
                    }

                    break;
                case UserType.AgentBoss:

                    resp.code = 0;
                    resp.has_resource = 1;
                    resp.httpCode = System.Net.HttpStatusCode.OK;

                    switch (request.periodical_data)
                    {
                        case 1: //Month
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersOnTimeForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = DateTime.Parse(r.Key).ToString("MMM yyyy"),
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }
                            break;

                        case 2: //Week
                            using (OrderDao dao = new OrderDao())
                            {

                                List<KeyValuePair<string, double>> rep = dao.GetOrdersOnTimeForRole(request.entity_id, request.for_role, request.periodical_data);
                                resp.report = rep.Select(r => new ReportKeyValuePairDoubleDto()
                                {
                                    key = r.Key,
                                    value = r.Value
                                }).OrderBy(o => DateTime.Parse(o.key)).ToList();
                            }
                            break;

                        default:

                            resp.message = "[[[Wrong period. Only accept 1 for Month and 2 for Week]]] " + request.periodical_data;
                            resp.code = 0;
                            resp.has_resource = 0;
                            resp.httpCode = System.Net.HttpStatusCode.BadRequest;

                            break;
                    }

                    break;
                case UserType.AgentAdmin:
                    resp.message = "[[[Wrong role]]] " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case UserType.Driver:
                    resp.message = "[[[Wrong role]]] " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                case UserType.Consumer:
                    resp.message = "[[[Wrong role]]] " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
                default:
                    resp.message = "[[[Wrong role]]] " + request.for_role.ToCleanString();
                    resp.code = 1;
                    resp.has_resource = 0;
                    resp.httpCode = System.Net.HttpStatusCode.BadRequest;
                    break;
            }

            return resp;

        }






        /// <summary>
        /// Helper method for fake data
        /// </summary>
        /// <param name="months">Number of month from the current month and back</param>
        /// <returns>List of strings in format fx 'Nov 2017'</returns>
        private static List<string> GetLastMonths(int months)
        {
            List<string> resp = new List<string>();
            var today = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            var year = today.Year;

            for (int i = 0; i < months; i++)
            {
                var monthNr = today.Month - i;

                if (monthNr < 1)
                {
                    year = year - 1;
                    monthNr = 12 + monthNr;
                }

                var month = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(monthNr) + " " + year;
                resp.Add(month);
            }


            return resp;
        }

    }



}
