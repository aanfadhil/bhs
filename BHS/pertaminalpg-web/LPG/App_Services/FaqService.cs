﻿using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace LPG.App_Services
{
    public class FaqService : ServiceBase, IFaqService
    {
        public FaqService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<FaqModel> ListFaq()
        {
            var faqList = new List<FaqModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from faq in context.MFaqs
                           orderby faq.Position
                           select new FaqModel
                           {
                               FaqId = faq.FaqID,
                               Question = faq.Question,
                               Answer = faq.Answer,
                               Position = faq.Position,
                               StatusId = faq.StatusID
                           };

                faqList = data.ToList();
            }
            return faqList;
        }

        public List<FaqModel> getActiveFAQ()
        {
            var faqList = new List<FaqModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from faq in context.MFaqs
                           where faq.StatusID == true
                           orderby faq.Position

                           select new FaqModel
                           {
                               FaqId = faq.FaqID,
                               Question = faq.Question,
                               Answer = faq.Answer,
                               Position = faq.Position,
                               StatusId = faq.StatusID
                           };

                faqList = data.ToList();
            }

            return faqList;
        }

        public FaqModel saveFaqDetails(FaqModel entity, int loggedBy)
        {

            try
            {
                entity.IsFaqExists = false;
                using (var context = new lpgdevEntities())
                {

                    var Data = context.MFaqs.Where(r => r.FaqID == entity.FaqId).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.Question = entity.Question;
                        Data.Answer = entity.Answer;
                        Data.Position = entity.Position;
                        Data.StatusID = entity.StatusId;
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggedBy); //TO DO: Pass Loggedin user and map it in MFaq table

                    }
                    else
                    {
                        MFaq objContactModel = new MFaq();
                        objContactModel.Question = entity.Question;
                        objContactModel.Answer = entity.Answer;
                        objContactModel.Position = entity.Position;
                        objContactModel.StatusID = entity.StatusId;

                        context.MFaqs.Add(objContactModel);
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggedBy);//TO DO: Pass Loggedin user and map it in MFaq table
                    }
                }
                return entity;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public FaqModel getFaqDetails(int faqId)
        {
            using (var context = new lpgdevEntities())
            {
                var rtns = (from faq in context.MFaqs
                            where faq.FaqID == faqId
                            select new FaqModel()
                            {
                                FaqId = faq.FaqID,
                                Question = faq.Question,
                                Answer = faq.Answer,
                                Position = faq.Position,
                                StatusId = faq.StatusID

                            }).FirstOrDefault();

                return rtns;
            }
        }


        public bool deleteFaqDetails(int faqId, int loggedBy)
        {
            bool status = false;
            using (var context = new lpgdevEntities())
            {
                var data = context.MFaqs.Where(r => r.FaqID == faqId).FirstOrDefault();
                if (data != null)
                {
                    if (data.StatusID)
                        data.StatusID = false;
                    else
                        data.StatusID = true;

                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", loggedBy); //TO DO: Pass Loggedin user and map it in MFaq table
                    status = true;
                }
            }
            return status;
        }

        /// <summary>
        /// ADD : Activity Log table
        /// </summary>
        /// <param name="action">Add/Update/Delete</param> 
        /// <param name="action_item">Page Name</param>
        /// <param name="loggedBy">Loggedin UserID</param>
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = "FAQ";
            _activityService.AddActivityLog(actModel, "FAQ");
        }

        public List<SelectListItem> GetAvailablePositions(int? FaqId)
        {
            var PositionList = new List<SelectListItem>();
            using (var context = new lpgdevEntities())
            {
                if (FaqId == null)
                {
                    var data = from faq in context.MFaqs
                               where faq.FaqID == faq.FaqID & faq.StatusID == true

                               select new SelectListItem
                               {

                                   Value = faq.Position.ToString(),
                                   Text = faq.Position.ToString(),

                               };
                    if (data.Count() > 0)
                        PositionList = data.ToList();
                }
                else
                {
                    var data = from faq in context.MFaqs
                               where faq.FaqID != FaqId & faq.StatusID == true

                               select new SelectListItem
                               {

                                   Value = faq.Position.ToString(),
                                   Text = faq.Position.ToString(),

                               };
                    if (data.Count() > 0)
                        PositionList = data.ToList();
                }


            }

            return PositionList;
        }
    }
}
