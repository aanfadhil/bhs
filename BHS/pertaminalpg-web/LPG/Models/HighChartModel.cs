﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class HighChartMultiple<T>
    {
        public string name { get; set; }
        public List<T> data { get; set; }
    }

    public class HighChart<T>
    {
        public string name { get; set; }
        public T y { get; set; }
    }

    public class HighChartStacked<T>
    {
        public List<string> x { get; set; }
        public List<HighChartMultiple<T>> MultipleSeries { get; set; }

    }

    public class HighChartBar<T>
    {
        public List<string> x { get; set; }
        public List<T> y { get; set; }
    }

    public class NameYPair<T>
    {
        public string name { get; set; }
        public T y { get; set; }
    }

    

}