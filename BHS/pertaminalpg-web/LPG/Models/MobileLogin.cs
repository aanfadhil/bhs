﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace LPG.Models
{
    public class MobileLoginRequest
    {
        public string mobile_number { get; set; }
        public string password { get; set; }
        public string app_id { get; set; }
        public string push_token { get; set; }
    }

    public class MobileLoginResponse : ResponseDto
    {
        public SuperUserLoginDto user_login { get; set; }
        public SuperUserLoginDetailsDto super_user_details { get; set; }
    }

    public class ResponseDto
    {
        public int code { get; set; }
        public string message { get; set; }
        public int has_resource { get; set; }
        public HttpStatusCode httpCode { get; set; }
        public List<string> errors { get; set; }

        public void MakeExceptionResponse(Exception exception = null, string methodName = "")
        {
            this.code = 1;
            this.has_resource = 0;
            this.httpCode = HttpStatusCode.InternalServerError;

            string msg = "";
            Exception ex = exception;
            while (ex != null)
            {
                msg += ex.Message + "\r\n";
                ex = ex.InnerException;
            }
            this.message = ("exception: " + msg + "\r\n" + exception.StackTrace);
            //Util.Logger.Log(LoggerLevel.ERROR, methodName, MethodFormat.ERROR, exception);
            //this.message = MessagesSource.GetMessage("exception: " + msg + "\r\n"+ exception.StackTrace);
        }
    }

    public class SuperUserLoginDto
    {
        public int user_id { get; set; }
        public string auth_token { get; set; }
    }

    public class SuperUserLoginDetailsDto
    {

        public string profile_image { get; set; }
        public string city_name { get; set; }
        public string super_user_name { get; set; }

    }
}