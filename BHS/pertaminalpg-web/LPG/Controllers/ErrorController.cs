﻿using System.Web.Mvc;

namespace LPG.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            if (Session["Exception"] != null)
            {
                @ViewBag.ErrorMessage = Session["Exception"].ToDefaultString();
            }

            return View();
        }
    }
}
