﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace LPG.Reporting.DTOs.Products
{
    [DataContract]
    public class ProductDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public int ProdID { get; set; }
        [DataMember]
        [Required]
        [StringLength(150)]
        public string ProductName { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        [StringLength(100)]
        public string ProductImage { get; set; }
        [DataMember]
        [StringLength(100)]
        public string ProductImageDetails { get; set; }
        [DataMember]
        public decimal TubePrice { get; set; }
        [DataMember]
        public decimal TubePromoPrice { get; set; }
        [DataMember]
        public decimal RefillPrice { get; set; }
        [DataMember]
        public decimal RefillPromoPrice { get; set; }
        [DataMember]
        public decimal ShippingPrice { get; set; }
        [DataMember]
        public decimal ShippingPromoPrice { get; set; }
        [DataMember]
        public bool Published { get; set; }
        [DataMember]
        public bool StatusId { get; set; }
        [DataMember]
        public short CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public short UpdatedBy { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        [StringLength(25)]
        public string ProductCode { get; set; }
        [DataMember]
        public double? Weight { get; set; }
    }

    public class ProductMapper : MapperBase<Product, ProductDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<Product, ProductDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Product, ProductDTO>>) (p => new ProductDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    ProdID = p.ProdID,
                    ProductName = p.ProductName,
                    Position = p.Position,
                    ProductImage = p.ProductImage,
                    ProductImageDetails = p.ProductImageDetails,
                    TubePrice = p.TubePrice,
                    TubePromoPrice = p.TubePromoPrice,
                    RefillPrice = p.RefillPrice,
                    RefillPromoPrice = p.RefillPromoPrice,
                    ShippingPrice = p.ShippingPrice,
                    ShippingPromoPrice = p.ShippingPromoPrice,
                    Published = p.Published,
                    StatusId = p.StatusId,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate,
                    ProductCode = p.ProductCode,
                    Weight = p.Weight
                }));
            }
        }

        public override void MapToModel(ProductDTO dto, Product model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.ProdID = dto.ProdID;
            model.ProductName = dto.ProductName;
            model.Position = dto.Position;
            model.ProductImage = dto.ProductImage;
            model.ProductImageDetails = dto.ProductImageDetails;
            model.TubePrice = dto.TubePrice;
            model.TubePromoPrice = dto.TubePromoPrice;
            model.RefillPrice = dto.RefillPrice;
            model.RefillPromoPrice = dto.RefillPromoPrice;
            model.ShippingPrice = dto.ShippingPrice;
            model.ShippingPromoPrice = dto.ShippingPromoPrice;
            model.Published = dto.Published;
            model.StatusId = dto.StatusId;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;
            model.ProductCode = dto.ProductCode;
            model.Weight = dto.Weight;

        }
    }
}
