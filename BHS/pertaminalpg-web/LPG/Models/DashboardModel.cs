﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class DashboardModel
    {
        public DashboardModel()
        {
            Companies = new List<CompanyModel>();
            Products = new List<ProductChartModel>();
        }
        public List<CompanyModel> Companies { get; set; }
        public List<ProductChartModel> Products { get; set; }
        public List<RegionModel> RegionList { get; set; }
    }
}