﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Driver
{
    public class UpdatePositionRequest : AuthBase
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
        public int driverId { get; set; }
    }
}
