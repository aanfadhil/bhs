﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class ProfilePhotoChangeRequest
    {
        public int user_id { get; set; }
        public string auth_token { get; set; }
        public string profile_image { get; set; }
    }
}