﻿using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace LPG.App_Services
{
    public class DistributionService : AbstractAppService, IDistributionService
    {
        public DistributionService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<DistributionModel> ListDistribution()
        {
            var distributionList = new List<DistributionModel>();
            //using (var context = new lpgdevEntities())
            //{
            var data = from distribution in context.DistributionPoints
                       join agent in context.Agencies
                       on distribution.AgenID equals agent.AgenID
                       where distribution.StatusId == true

                       where distribution.StatusId == true
                       orderby distribution.DbptID descending, distribution.UpdatedDate descending, distribution.CreatedDate descending
                       select new DistributionModel
                       {
                           DistributionId = distribution.DbptID,
                           CompanyAgent = agent.AgencyName,
                           DistributionPoint = distribution.DistributionPointName,
                           Lat = distribution.Latitude,
                           Long = distribution.Longitude,
                           Address = distribution.Address

                       };
            distributionList = data.ToList();
            //}

            return distributionList;
        }

        public List<DistributionModel> GetByAgency(int agenId)
        {
            var distributionList = new List<DistributionModel>();
            //using (var context = new lpgdevEntities())
            //{
            var data = from distribution in context.DistributionPoints
                       join agent in context.Agencies
                       on distribution.AgenID equals agent.AgenID
                       where distribution.StatusId == true && distribution.AgenID == agenId

                       where distribution.StatusId == true
                       orderby distribution.DbptID descending, distribution.UpdatedDate descending, distribution.CreatedDate descending
                       select new DistributionModel
                       {
                           DistributionId = distribution.DbptID,
                           CompanyAgent = agent.AgencyName,
                           DistributionPoint = distribution.DistributionPointName,
                           Lat = distribution.Latitude,
                           Long = distribution.Longitude,
                           Address = distribution.Address
                       };
            distributionList = data.ToList();
            //}

            return distributionList;
        }

        public DistributionModel getDistributionDetails(int distributionId)
        {
            //using (var context = new lpgdevEntities())
            //{
            var rtns = (from dist in context.DistributionPoints
                        join agency in context.Agencies
                        on dist.AgenID equals agency.AgenID
                        where dist.DbptID == distributionId
                        select new DistributionModel()
                        {
                            DistributionId = dist.DbptID,
                            DistributionPoint = dist.DistributionPointName,
                            AgenDriverAgenId = agency.AgenID,
                            Address = dist.Address,
                            Lat = dist.Latitude + "," + dist.Longitude,

                        }).FirstOrDefault();

            return rtns;
            //}
        }

        public DistributionModel saveDistributionDetails(DistributionModel entity, int loggedBy)
        {

            try
            {

                //using (var context = new lpgdevEntities())
                //{

                var Data = context.DistributionPoints.Where(r => r.DbptID == entity.DistributionId).FirstOrDefault();

                if (Data != null)
                {

                    Data.StatusId = true;
                    Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    Data.Latitude = entity.Lat;
                    Data.Longitude = entity.Long;
                    Data.DistributionPointName = entity.DistributionPoint;
                    Data.AgenID = entity.AgenDriverAgenId;
                    Data.Address = entity.Address;

                    var city = context.MCities.Where(t => t.CityName.ToUpper().Replace("kota", "").Replace("kabupaten", "").Replace("city", "") == entity.CityName.ToUpper().Replace("kota", "").Replace("kabupaten", "").Replace("city", "")).FirstOrDefault();

                    if(city != null)
                    {
                        Data.CityID = city.ID;
                    }

                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggedBy);

                }
                else
                {
                    DistributionPoint objLocal = new DistributionPoint();

                    objLocal.DbptID = entity.DistributionId;
                    objLocal.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    objLocal.Latitude = entity.Lat;
                    objLocal.Longitude = entity.Long;
                    objLocal.Address = entity.Address;
                    objLocal.AgenID = entity.AgenDriverAgenId;


                    var city = context.MCities.Where(t => t.CityName.ToUpper().Replace("kota", "").Replace("kabupaten", "").Replace("city", "").Trim() == entity.CityName.ToUpper().Replace("kota", "").Replace("kabupaten", "").Replace("city", "").Trim()).FirstOrDefault();

                    if (city != null)
                    {
                        Data.CityID = city.ID;
                    }

                    context.DistributionPoints.Add(objLocal);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggedBy);
                }
                //}
                return entity;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool DeleteDistribution(int Id, int loggedBy)
        {
            try
            {
                //using (var context = new lpgdevEntities())
                //{
                var Data = context.DistributionPoints.Where(r => r.DbptID == Id).FirstOrDefault();
                if (Data != null)
                {
                    Data.StatusId = false;
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", loggedBy);
                    return true;
                }
                return false;
                //}
            }
            catch (Exception)
            {
                throw;
            }

        }

        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = "Distribution";
            _activityService.AddActivityLog(actModel, "Distribution");
        }
    }
}
