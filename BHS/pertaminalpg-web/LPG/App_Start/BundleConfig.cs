﻿using System.Web;
using System.Web.Optimization;

namespace LPG
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap/css/bootstrap.min.css",
                      "~/Content/font-awesome/css/font-awesome.min.css",
                      //
                      "~/Content/datatables/v2/datatables.min.css",
                      //"~/Content/datatables/dataTables.bootstrap4.css",
                      //"~/Content/datatables/buttons.dataTables.min.css",
                      "~/Content/css/sb-admin.css",
                      "~/Content/css/toastr.css"
                      ));

            //bundles.Add(new ScriptBundle("~/bundles/datatables_id", "//cdn.datatables.net/plug-ins/1.10.16/i18n/Indonesian.json").Include("~/Content/datatables/indonesia.json"));

            bundles.Add(new ScriptBundle("~/bundles/LPGjquery").Include(
                "~/Content/popper/popper.min.js",
                "~/Content/bootstrap/js/bootstrap.min.js",
                 "~/Content/jquery-easing/jquery.easing.min.js",
                 //"~/Content/chart.js/Chart.min.js",
                 //"~/Content/datatables/jquery.dataTables.js",
                 "~/Content/datatables/v2/datatables.min.js",
                 //"~/Content/datatables/dataTables.buttons.min.js",
                 // "~/Content/datatables/buttons.flash.min.js",
                 //"~/Content/datatables/jszip.min.js",
                 // "~/Content/datatables/vfs_fonts.js",
                 //"~/Content/datatables/buttons.html5.min",
                 "~/Content/js/sb-admin.js"

                ));



            bundles.Add(new StyleBundle("~/content/toastr", "http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css")
                .Include("~/Content/toastr.css"));

            bundles.Add(new ScriptBundle("~/bundles/toastr", "http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js")
                            .Include("~/Scripts/toastr.js"));

            //#if DEBUG          
            BundleTable.EnableOptimizations = false;
            //#else
            ///            BundleTable.EnableOptimizations = true;
            //#endif        

        }
    }
}
