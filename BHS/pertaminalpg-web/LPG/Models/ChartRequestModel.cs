﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class ChartRequestModel
    {
        public int ReportType { get; set; }
        public int ProductId { get; set; }
        public int Period { get; set; }
        public int TotalType { get; set; }
        public List<int> AgencyId { get; set; }
    }
}