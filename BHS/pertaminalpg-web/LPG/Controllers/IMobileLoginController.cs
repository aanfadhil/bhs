﻿using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace LPG.Controllers
{
    interface IMobileLoginController
    {
        Task<MobileLoginResponse> MobileLogin([FromBody] MobileLoginRequest request);
    }
}
