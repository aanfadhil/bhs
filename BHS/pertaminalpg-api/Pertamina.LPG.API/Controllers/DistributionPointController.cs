﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Pertamina.LPG.API.DTOs.DistributionPoint;
using Pertamina.LPG.API.Services;

namespace Pertamina.LPG.API.Controllers
{
    public class DistributionPointController : ApiController
    {
        private DistributionPointService _distPointServices = new DistributionPointService();

        [HttpPost]
        [ActionName("check_is_coverage_area")]
        public NegotiatedContentResult<CheckCoverageAreaResponse> PostGetProductPriceDetails([FromBody]CheckCoverageAreaRequest request)
        {
            CheckCoverageAreaResponse resp = _distPointServices.CheckOnCoverageArea(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("update_city")]
        public NegotiatedContentResult<CheckCoverageAreaResponse> PostUpdateCity()
        {
            _distPointServices.UpdateCity();
            return Content(HttpStatusCode.OK, new CheckCoverageAreaResponse());
        }

        
    }
}
