﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace LPG.Reporting.DTOs.Products
{
    [DataContract]
    public class ProductExchangeDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public int PrExID { get; set; }
        [DataMember]
        public int ProdID { get; set; }
        [DataMember]
        [Required]
        [StringLength(250)]
        public string ExchangeWith { get; set; }
        [DataMember]
        public int ExchangeQuantity { get; set; }
        [DataMember]
        public decimal? ExchangePrice { get; set; }
        [DataMember]
        public decimal ExchangePromoPrice { get; set; }
        [DataMember]
        public bool StatusId { get; set; }
        [DataMember]
        public short CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public short UpdatedBy { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
    }

    public class ProductExchangeMapper : MapperBase<ProductExchange, ProductExchangeDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<ProductExchange, ProductExchangeDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<ProductExchange, ProductExchangeDTO>>) (p => new ProductExchangeDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    PrExID = p.PrExID,
                    ProdID = p.ProdID,
                    ExchangeWith = p.ExchangeWith,
                    ExchangeQuantity = p.ExchangeQuantity,
                    ExchangePrice = p.ExchangePrice,
                    ExchangePromoPrice = p.ExchangePromoPrice,
                    StatusId = p.StatusId,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate
                }));
            }
        }

        public override void MapToModel(ProductExchangeDTO dto, ProductExchange model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.PrExID = dto.PrExID;
            model.ProdID = dto.ProdID;
            model.ExchangeWith = dto.ExchangeWith;
            model.ExchangeQuantity = dto.ExchangeQuantity;
            model.ExchangePrice = dto.ExchangePrice;
            model.ExchangePromoPrice = dto.ExchangePromoPrice;
            model.StatusId = dto.StatusId;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;

        }
    }
}
