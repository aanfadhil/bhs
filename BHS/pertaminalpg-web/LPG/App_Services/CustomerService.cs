﻿using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.App_Services
{
    public class CustomerService : AbstractAppService, ICustomerService
    {
        public CustomerService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<CustomerModel> ListCustomers(Boolean withDefaultAddress = false)
        {
            var customerList = new List<CustomerModel>();
            using (var context = new lpgdevEntities())
            {
                IQueryable<CustomerModel> data;

                if (withDefaultAddress)
                {
                    data = from customer in context.Consumers
                           join customeraddress in context.ConsumerAddresses
                           on customer.ConsID equals customeraddress.ConsID into ConsumerInfo
                           from customeraddress in ConsumerInfo.DefaultIfEmpty()

                           where customer.StatusID == 1 && customeraddress.IsDefault == true
                           select new CustomerModel
                           {
                               CustomerId = customer.ConsID,
                               Name = customer.Name,
                               MobileNo = customer.PhoneNumber,
                               Address = (customeraddress.Address == null) ? "tidak ada data" : customeraddress.Address,
                               Code = (customeraddress.PostalCode == null) ? "tidak ada data" : customeraddress.PostalCode,
                               Region = (customeraddress.RegionName == null) ? "tidak ada data" : customeraddress.RegionName,
                           };
                    customerList = data.Distinct().ToList();
                }
                else
                {
                    data = from customer in context.Consumers
                               //join customeraddress in context.ConsumerAddresses
                               //on customer.ConsID equals customeraddress.ConsID into ConsumerInfo
                               //from customeraddress in ConsumerInfo.DefaultIfEmpty()

                           where customer.StatusID == 1 //&& customeraddress.IsDefault == true
                           select new CustomerModel
                           {
                               CustomerId = customer.ConsID,
                               Name = customer.Name,
                               MobileNo = customer.PhoneNumber,
                               Address = customer.ConsumerAddresses.FirstOrDefault().Address ?? "tidak ada data",
                               Code = customer.ConsumerAddresses.FirstOrDefault().PostalCode ?? "tidak ada data",
                               Region = customer.ConsumerAddresses.FirstOrDefault().RegionName ?? "tidak ada data",
                           };
                    customerList = data.ToList();

                }


            }

            return customerList;
        }


        public DataTableModel<GetCustomerPaging_Result> GetCustomerPaging(DatatablesRequest data)
        {
            DataTableModel<GetCustomerPaging_Result> result = new DataTableModel<GetCustomerPaging_Result>();
            
            using (var context = new lpgdevEntities())
            {
                {
                    result.draw = data.draw;

                    result.data = context.GetCustomerPaging(data.length, data.start, data.search[0]).ToList();
                    
                    if(result.data.Count > 0)
                    {
                        result.recordsTotal = result.data[0].Total;
                        result.recordsFiltered = result.data[0].Total;
                    }
                    
                }


            }

            return result;
        }

        public bool deleteCustomer(int id)
        {
            bool status = false;
            using (var context = new lpgdevEntities())
            {
                var data = context.Consumers.Where(r => r.ConsID == id).FirstOrDefault();
                if (data != null)
                {
                    data.StatusID = 0;
                    data.UpdatedBy = SessionHelper.UserId;
                    data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    context.SaveChanges();
                    status = true;
                }
            }

            return status;
        }

        public CustomerModel CustomerView(int CustomerId)
        {
            using (var context = new lpgdevEntities())
            {

                var rtns = (from con in context.Consumers
                            join conAdr in context.ConsumerAddresses
                            on con.ConsID equals conAdr.ConsID into ConsumerInfo
                            from conAdres in ConsumerInfo.DefaultIfEmpty()
                            where con.ConsID == CustomerId

                            select new CustomerModel
                            {
                                Name = con.Name,
                                MobileNo = con.PhoneNumber,
                                ProfileImage = con.ProfileImage,
                                SecondAddress = (conAdres.Address == null) ? "[[[tidak ada data]]]" : conAdres.Address,
                                MainAddress = (conAdres.Address == null) ? "[[[tidak ada data]]]" : conAdres.Address,
                                Code = (conAdres.PostalCode == null) ? "[[[tidak ada data]]]" : conAdres.PostalCode,
                                AdditionalInfo = (conAdres.AdditionalInfo == null) ? "[[[tidak ada data]]]" : conAdres.AdditionalInfo,
                                Region = (conAdres.RegionName == null) ? "[[[tidak ada data]]]" : conAdres.RegionName,



                            }).FirstOrDefault();

                return rtns;
            }

        }
        public bool IsExists(int id)
        {
            using (var context = new lpgdevEntities())
            {
                return context.Consumers.Where(c => c.ConsID == id).Any();
            }

        }

    }
}
