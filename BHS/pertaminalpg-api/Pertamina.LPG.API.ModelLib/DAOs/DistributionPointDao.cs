﻿using Pertamina.LPG.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DAOs
{
    public class DistributionPointDao : DAO
    {
        //public List<DistributionPoint> GetDistributionPoint()
        //{
        //    return _context.DistributionPoints.ToList();
        //}

        public List<DistributionPoint> GetDistributionPoint()
        {
            
            var data = (from distribution in _context.DistributionPoints
                       join agent in _context.Agencies
                       on distribution.AgenID equals agent.AgenID
                       where distribution.StatusId == true

                        where distribution.StatusId == true
                        select distribution
                       ).ToList();
            return data;
        }

        public List<DistributionPoint> GetAllDistributionPoint()
        {

            var data = (from distribution in _context.DistributionPoints
                        join agent in _context.Agencies
                        on distribution.AgenID equals agent.AgenID
                        select distribution
                       ).ToList();
            return data;
        }

        public void UpdateCity(int id,string cityName)
        {

            var data = _context.DistributionPoints.Where(t => t.DbptID == id).FirstOrDefault();

            var city = _context.MCities.Where(t => t.CityName.ToUpper().Contains(cityName.ToUpper())).FirstOrDefault();

            if (city != null)
            {

                data.CityID = city.ID;

                _context.SaveChanges();
            }
            
        }

        public DistributionPoint GetDistributionPointById(int id)
        {
            return _context.DistributionPoints.Include("AgentAdmins").Include("Drivers").Include("Drivers.OrderDeliveries").Where(t => t.DbptID == id).FirstOrDefault();
        }
    }
}
