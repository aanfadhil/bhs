﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.SuperUser
{
    public class SendPostNotificationResponse : ResponseDto
    {
        public List<NotificationStatus> send_results { get; set; }
    }

    public class NotificationStatus
    {
        public int target { get; set; }
        public bool is_success { get; set; }
        public string message { get; set; }
    }
}
