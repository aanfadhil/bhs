﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LPG.Models
{
    public class AgentAdminModel
    {
        public int AgentAdminId { get; set; }
        public int AgentId { get; set; }
        public string AdminName { get; set; }
        public string CompanyName { get; set; }
        public string DistributionPointName { get; set; }

        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid Mobile Number.")]
        public string MobileNo { get; set; }
        public bool IsActive { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public List<AgentAdminModel> AgentAdminList { get; set; }

        public int AgenadminAgenId { get; set; }
        public int AgenadminDistId { get; set; }

        public string CityName { get; set; }
        public int? CityId { get; set; }

        public List<SelectListItem> agencyAList { get; set; }
        public List<SelectListItem> agencyDList { get; set; }
        public string errorMessage { get; set; }

        public string CountryCode { get { return Constants.APPSETTING_COUNTRY_CODE; } }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string email { get; set; }
    }
}