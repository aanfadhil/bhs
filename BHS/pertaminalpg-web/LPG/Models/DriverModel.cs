﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LPG.Models
{
    public class DriverModel
    {
        public int DriverId { get; set; }
        public string DriverName { get; set; }
        public string Company { get; set; }
        public string DistributionPoint { get; set; }

        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid Mobile Number.")]
        public string MobilePhone { get; set; }
        public bool IsActive { get; set; }

        public string CityName { get; set; }
        public int? CityId { get; set; }

        public int AgenDriverAgenId { get; set; }
        public int AgenDriverDistId { get; set; }
        public string ErrorMessage { get; set; }
        public List<SelectListItem> AgencyList { get; set; }
        public List<SelectListItem> DistributionList { get; set; }

        public DateTime LastModifiedDate { get; set; }
        public List<DriverModel> DriverModelList { get; set; }

        public string CountryCode { get { return Constants.APPSETTING_COUNTRY_CODE; } }
    }
}