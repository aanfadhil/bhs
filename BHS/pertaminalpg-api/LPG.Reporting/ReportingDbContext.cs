﻿namespace LPG.Reporting
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Common;

    //[DbConfigurationType(typeof(LPG.Reporting.Config.ReportingDatabaseConfiguration))]
    public partial class ReportingDbContext : DbContext
    {
        public ReportingDbContext()
            : base("name=LpgDataModel")
            //: base("name=lpgdevEntities")
        {
        }

        //public ReportingDbContext(DbConnection dbConnection, bool contextOwnsConnection)  // THIS ONE
        //: base(dbConnection, contextOwnsConnection) { }

        public virtual DbSet<ActivityLog> ActivityLogs { get; set; }
        public virtual DbSet<Agency> Agencies { get; set; }
        public virtual DbSet<AgentAdmin> AgentAdmins { get; set; }
        public virtual DbSet<AgentBoss> AgentBosses { get; set; }
        public virtual DbSet<Consumer> Consumers { get; set; }
        public virtual DbSet<ConsumerAddress> ConsumerAddresses { get; set; }
        public virtual DbSet<ConsumerReview> ConsumerReviews { get; set; }
        public virtual DbSet<ConsumerReviewReason> ConsumerReviewReasons { get; set; }
        public virtual DbSet<ContactInfo> ContactInfoes { get; set; }
        public virtual DbSet<DistributionPoint> DistributionPoints { get; set; }
        public virtual DbSet<Driver> Drivers { get; set; }
        public virtual DbSet<DriverNotificationLog> DriverNotificationLogs { get; set; }
        public virtual DbSet<InvoiceNumber> InvoiceNumbers { get; set; }
        public virtual DbSet<MDeliverySlot> MDeliverySlots { get; set; }
        public virtual DbSet<MDeliveryStatu> MDeliveryStatus { get; set; }
        public virtual DbSet<MFaq> MFaqs { get; set; }
        public virtual DbSet<MFunctionality> MFunctionalities { get; set; }
        public virtual DbSet<MModule> MModules { get; set; }
        public virtual DbSet<MOrderStatu> MOrderStatus { get; set; }
        public virtual DbSet<MRegion> MRegions { get; set; }
        public virtual DbSet<MSuperUserType> MSuperUserTypes { get; set; }
        public virtual DbSet<NotificationLog> NotificationLogs { get; set; }
        public virtual DbSet<OneTimePwd> OneTimePwds { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderAllocationLog> OrderAllocationLogs { get; set; }
        public virtual DbSet<OrderDelivery> OrderDeliveries { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<OrderPrdocuctExchange> OrderPrdocuctExchanges { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductExchange> ProductExchanges { get; set; }
        public virtual DbSet<PromoBanner> PromoBanners { get; set; }
        public virtual DbSet<PromoInfo> PromoInfoes { get; set; }
        public virtual DbSet<Reminder> Reminders { get; set; }
        public virtual DbSet<SuperAdmin> SuperAdmins { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<TeleCustomer> TeleCustomers { get; set; }
        public virtual DbSet<TeleOrder> TeleOrders { get; set; }
        public virtual DbSet<TeleOrderDelivery> TeleOrderDeliveries { get; set; }
        public virtual DbSet<TeleOrderDetail> TeleOrderDetails { get; set; }
        public virtual DbSet<TeleOrderPrdocuctExchange> TeleOrderPrdocuctExchanges { get; set; }
        public virtual DbSet<AccessControl> AccessControls { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActivityLog>()
                .Property(e => e.Item)
                .IsUnicode(false);

            modelBuilder.Entity<ActivityLog>()
                .Property(e => e.Activity)
                .IsUnicode(false);

            modelBuilder.Entity<Agency>()
                .Property(e => e.SoldToNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Agency>()
                .Property(e => e.AgencyName)
                .IsUnicode(false);

            modelBuilder.Entity<Agency>()
                .Property(e => e.Region)
                .IsUnicode(false);

            modelBuilder.Entity<Agency>()
                .HasMany(e => e.AgentAdmins)
                .WithRequired(e => e.Agency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Agency>()
                .HasMany(e => e.AgentBosses)
                .WithRequired(e => e.Agency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Agency>()
                .HasMany(e => e.DistributionPoints)
                .WithRequired(e => e.Agency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Agency>()
                .HasMany(e => e.Drivers)
                .WithRequired(e => e.Agency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Agency>()
                .HasMany(e => e.InvoiceNumbers)
                .WithRequired(e => e.Agency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgentAdmin>()
                .Property(e => e.AgentAdminName)
                .IsUnicode(false);

            modelBuilder.Entity<AgentAdmin>()
                .Property(e => e.MobileNumber)
                .IsUnicode(false);

            modelBuilder.Entity<AgentAdmin>()
                .Property(e => e.ProfileImage)
                .IsUnicode(false);

            modelBuilder.Entity<AgentAdmin>()
                .Property(e => e.AppToken)
                .IsUnicode(false);

            modelBuilder.Entity<AgentAdmin>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<AgentAdmin>()
                .Property(e => e.AccToken)
                .IsUnicode(false);

            modelBuilder.Entity<AgentAdmin>()
                .Property(e => e.AppID)
                .IsUnicode(false);

            modelBuilder.Entity<AgentAdmin>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<AgentAdmin>()
                .HasMany(e => e.OrderDeliveries)
                .WithRequired(e => e.AgentAdmin)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgentAdmin>()
                .HasMany(e => e.TeleCustomers)
                .WithRequired(e => e.AgentAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgentAdmin>()
                .HasMany(e => e.TeleOrders)
                .WithRequired(e => e.AgentAdmin)
                .HasForeignKey(e => e.AgadmID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgentAdmin>()
                .HasMany(e => e.TeleOrders1)
                .WithRequired(e => e.AgentAdmin1)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgentAdmin>()
                .HasMany(e => e.TeleOrderDeliveries)
                .WithRequired(e => e.AgentAdmin)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgentBoss>()
                .Property(e => e.OwnerName)
                .IsUnicode(false);

            modelBuilder.Entity<AgentBoss>()
                .Property(e => e.MobileNumber)
                .IsUnicode(false);

            modelBuilder.Entity<AgentBoss>()
                .Property(e => e.ProfileImage)
                .IsUnicode(false);

            modelBuilder.Entity<AgentBoss>()
                .Property(e => e.AppToken)
                .IsUnicode(false);

            modelBuilder.Entity<AgentBoss>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<AgentBoss>()
                .Property(e => e.AccToken)
                .IsUnicode(false);

            modelBuilder.Entity<AgentBoss>()
                .Property(e => e.AppID)
                .IsUnicode(false);

            modelBuilder.Entity<AgentBoss>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Consumer>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Consumer>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Consumer>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<Consumer>()
                .Property(e => e.IMEI)
                .IsUnicode(false);

            modelBuilder.Entity<Consumer>()
                .Property(e => e.AccToken)
                .IsUnicode(false);

            modelBuilder.Entity<Consumer>()
                .Property(e => e.AppID)
                .IsUnicode(false);

            modelBuilder.Entity<Consumer>()
                .Property(e => e.AppToken)
                .IsUnicode(false);

            modelBuilder.Entity<Consumer>()
                .Property(e => e.ProfileImage)
                .IsUnicode(false);

            modelBuilder.Entity<Consumer>()
                .HasMany(e => e.ConsumerAddresses)
                .WithRequired(e => e.Consumer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Consumer>()
                .HasMany(e => e.ConsumerReviews)
                .WithRequired(e => e.Consumer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Consumer>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.Consumer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConsumerAddress>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<ConsumerAddress>()
                .Property(e => e.PostalCode)
                .IsUnicode(false);

            modelBuilder.Entity<ConsumerAddress>()
                .Property(e => e.AdditionalInfo)
                .IsUnicode(false);

            modelBuilder.Entity<ConsumerAddress>()
                .Property(e => e.Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<ConsumerAddress>()
                .Property(e => e.Longitude)
                .IsUnicode(false);

            modelBuilder.Entity<ConsumerAddress>()
                .Property(e => e.RegionName)
                .IsUnicode(false);

            modelBuilder.Entity<ConsumerAddress>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.ConsumerAddress)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConsumerReview>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<ConsumerReviewReason>()
                .Property(e => e.ReasonText)
                .IsUnicode(false);

            modelBuilder.Entity<ContactInfo>()
                .Property(e => e.ContactInfoImage)
                .IsUnicode(false);

            modelBuilder.Entity<ContactInfo>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<DistributionPoint>()
                .Property(e => e.DistributionPointName)
                .IsUnicode(false);

            modelBuilder.Entity<DistributionPoint>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<DistributionPoint>()
                .HasMany(e => e.AgentAdmins)
                .WithRequired(e => e.DistributionPoint)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DistributionPoint>()
                .HasMany(e => e.Drivers)
                .WithRequired(e => e.DistributionPoint)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Driver>()
                .Property(e => e.DriverName)
                .IsUnicode(false);

            modelBuilder.Entity<Driver>()
                .Property(e => e.MobileNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Driver>()
                .Property(e => e.AccToken)
                .IsUnicode(false);

            modelBuilder.Entity<Driver>()
                .Property(e => e.AppID)
                .IsUnicode(false);

            modelBuilder.Entity<Driver>()
                .Property(e => e.AppToken)
                .IsUnicode(false);

            modelBuilder.Entity<Driver>()
                .Property(e => e.ProfileImage)
                .IsUnicode(false);

            modelBuilder.Entity<Driver>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<Driver>()
                .HasMany(e => e.ConsumerReviews)
                .WithRequired(e => e.Driver)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Driver>()
                .HasMany(e => e.OrderDeliveries)
                .WithRequired(e => e.Driver)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Driver>()
                .HasMany(e => e.TeleOrderDeliveries)
                .WithRequired(e => e.Driver)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MDeliverySlot>()
                .Property(e => e.SlotName)
                .IsUnicode(false);

            modelBuilder.Entity<MDeliverySlot>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.MDeliverySlot)
                .HasForeignKey(e => e.DeliverySlotID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MDeliverySlot>()
                .HasMany(e => e.TeleOrders)
                .WithOptional(e => e.MDeliverySlot)
                .HasForeignKey(e => e.DeliverySlotID);

            modelBuilder.Entity<MDeliveryStatu>()
                .Property(e => e.DeliveryStatus)
                .IsUnicode(false);

            modelBuilder.Entity<MDeliveryStatu>()
                .HasMany(e => e.OrderDeliveries)
                .WithRequired(e => e.MDeliveryStatu)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MDeliveryStatu>()
                .HasMany(e => e.TeleOrderDeliveries)
                .WithRequired(e => e.MDeliveryStatu)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MFaq>()
                .Property(e => e.Question)
                .IsUnicode(false);

            modelBuilder.Entity<MFaq>()
                .Property(e => e.Answer)
                .IsUnicode(false);

            modelBuilder.Entity<MFunctionality>()
                .Property(e => e.FunctionalityName)
                .IsUnicode(false);

            modelBuilder.Entity<MFunctionality>()
                .HasMany(e => e.AccessControls)
                .WithRequired(e => e.MFunctionality)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MModule>()
                .Property(e => e.ModuleName)
                .IsUnicode(false);

            modelBuilder.Entity<MModule>()
                .HasMany(e => e.MFunctionalities)
                .WithRequired(e => e.MModule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MModule>()
                .HasMany(e => e.AccessControls)
                .WithRequired(e => e.MModule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MOrderStatu>()
                .Property(e => e.OrderStatus)
                .IsUnicode(false);

            modelBuilder.Entity<MOrderStatu>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.MOrderStatu)
                .HasForeignKey(e => e.StatusID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MOrderStatu>()
                .HasMany(e => e.TeleOrders)
                .WithRequired(e => e.MOrderStatu)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MRegion>()
                .Property(e => e.RegionName)
                .IsUnicode(false);

            modelBuilder.Entity<MRegion>()
                .Property(e => e.RegionCode)
                .IsUnicode(false);

            modelBuilder.Entity<MRegion>()
                .HasMany(e => e.Agencies)
                .WithRequired(e => e.MRegion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MSuperUserType>()
                .Property(e => e.TypeName)
                .IsUnicode(false);

            modelBuilder.Entity<MSuperUserType>()
                .HasMany(e => e.AccessControls)
                .WithRequired(e => e.MSuperUserType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<NotificationLog>()
                .Property(e => e.ReceiverId)
                .IsUnicode(false);

            modelBuilder.Entity<NotificationLog>()
                .Property(e => e.JSONMessage)
                .IsUnicode(false);

            modelBuilder.Entity<NotificationLog>()
                .Property(e => e.FCMResponse)
                .IsUnicode(false);

            modelBuilder.Entity<OneTimePwd>()
                .Property(e => e.UserType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<OneTimePwd>()
                .Property(e => e.Otg)
                .IsUnicode(false);

            modelBuilder.Entity<Order>()
                .Property(e => e.InvoiceNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Order>()
                .Property(e => e.SubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order>()
                .Property(e => e.PromoProduct)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order>()
                .Property(e => e.ShippingCharge)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order>()
                .Property(e => e.PromoShipping)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order>()
                .Property(e => e.GrandTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order>()
                .Property(e => e.RefillSubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order>()
                .Property(e => e.PromoRefill)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order>()
                .Property(e => e.ExchangeSubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order>()
                .Property(e => e.PromoExchange)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.ConsumerReviews)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderAllocationLogs)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderDeliveries)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderDetails)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderPrdocuctExchanges)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderAllocationLog>()
                .Property(e => e.Distance)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.UnitPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.SubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.PromoProduct)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.ShippingCharge)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.PromoShipping)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.TotamAmount)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.RefillPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.PromoRefill)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.RefillSubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.RefillTotalAmount)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderPrdocuctExchange>()
                .Property(e => e.ExchangeWith)
                .IsUnicode(false);

            modelBuilder.Entity<OrderPrdocuctExchange>()
                .Property(e => e.ExchangePrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderPrdocuctExchange>()
                .Property(e => e.ExchangePromoPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderPrdocuctExchange>()
                .Property(e => e.SubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<OrderPrdocuctExchange>()
                .Property(e => e.TotalAmount)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Product>()
                .Property(e => e.ProductName)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.ProductImage)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.ProductImageDetails)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.TubePrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Product>()
                .Property(e => e.TubePromoPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Product>()
                .Property(e => e.RefillPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Product>()
                .Property(e => e.RefillPromoPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Product>()
                .Property(e => e.ShippingPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Product>()
                .Property(e => e.ShippingPromoPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<Product>()
                .Property(e => e.ProductCode)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.OrderDetails)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.OrderPrdocuctExchanges)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.ProductExchanges)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.TeleOrderDetails)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasMany(e => e.TeleOrderPrdocuctExchanges)
                .WithRequired(e => e.Product)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProductExchange>()
                .Property(e => e.ExchangeWith)
                .IsUnicode(false);

            modelBuilder.Entity<ProductExchange>()
                .Property(e => e.ExchangePrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<ProductExchange>()
                .Property(e => e.ExchangePromoPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<PromoBanner>()
                .Property(e => e.Caption)
                .IsUnicode(false);

            modelBuilder.Entity<PromoBanner>()
                .Property(e => e.BannerImage)
                .IsUnicode(false);

            modelBuilder.Entity<PromoInfo>()
                .Property(e => e.Caption)
                .IsUnicode(false);

            modelBuilder.Entity<PromoInfo>()
                .Property(e => e.InfoImage)
                .IsUnicode(false);

            modelBuilder.Entity<Reminder>()
                .Property(e => e.ReminderImage)
                .IsUnicode(false);

            modelBuilder.Entity<Reminder>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<SuperAdmin>()
                .Property(e => e.FullName)
                .IsUnicode(false);

            modelBuilder.Entity<SuperAdmin>()
                .Property(e => e.MobileNum)
                .IsUnicode(false);

            modelBuilder.Entity<SuperAdmin>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<SuperAdmin>()
                .Property(e => e.ProfileImage)
                .IsUnicode(false);

            modelBuilder.Entity<SuperAdmin>()
                .Property(e => e.AppToken)
                .IsUnicode(false);

            modelBuilder.Entity<SuperAdmin>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<SuperAdmin>()
                .Property(e => e.AccToken)
                .IsUnicode(false);

            modelBuilder.Entity<SuperAdmin>()
                .Property(e => e.AppID)
                .IsUnicode(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.ActivityLogs)
                .WithRequired(e => e.SuperAdmin)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.Agencies)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.Agencies1)
                .WithRequired(e => e.SuperAdmin1)
                .HasForeignKey(e => e.UpdatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.AgentAdmins)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.AgentBosses)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.ContactInfoes)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.ContactInfoes1)
                .WithRequired(e => e.SuperAdmin1)
                .HasForeignKey(e => e.UpdatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.DistributionPoints)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.DistributionPoints1)
                .WithRequired(e => e.SuperAdmin1)
                .HasForeignKey(e => e.UpdatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.Drivers)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.Drivers1)
                .WithRequired(e => e.SuperAdmin1)
                .HasForeignKey(e => e.UpdatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.Products)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.Products1)
                .WithRequired(e => e.SuperAdmin1)
                .HasForeignKey(e => e.UpdatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.ProductExchanges)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.ProductExchanges1)
                .WithRequired(e => e.SuperAdmin1)
                .HasForeignKey(e => e.UpdatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.PromoBanners)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.UpdatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.PromoBanners1)
                .WithRequired(e => e.SuperAdmin1)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.PromoInfoes)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.PromoInfoes1)
                .WithRequired(e => e.SuperAdmin1)
                .HasForeignKey(e => e.UpdatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.Reminders)
                .WithRequired(e => e.SuperAdmin)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SuperAdmin>()
                .HasMany(e => e.Reminders1)
                .WithRequired(e => e.SuperAdmin1)
                .HasForeignKey(e => e.UpdatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeleCustomer>()
                .Property(e => e.CustomerName)
                .IsUnicode(false);

            modelBuilder.Entity<TeleCustomer>()
                .Property(e => e.Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<TeleCustomer>()
                .Property(e => e.Longitude)
                .IsUnicode(false);

            modelBuilder.Entity<TeleCustomer>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<TeleCustomer>()
                .Property(e => e.MobileNumber)
                .IsUnicode(false);

            modelBuilder.Entity<TeleOrder>()
                .Property(e => e.InvoiceNumber)
                .IsUnicode(false);

            modelBuilder.Entity<TeleOrder>()
                .Property(e => e.SubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrder>()
                .Property(e => e.PromoProduct)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrder>()
                .Property(e => e.ShippingCharge)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrder>()
                .Property(e => e.PromoShipping)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrder>()
                .Property(e => e.GrantTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrder>()
                .Property(e => e.RefillSubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrder>()
                .Property(e => e.PromoRefill)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrder>()
                .Property(e => e.ExchangeSubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrder>()
                .Property(e => e.PromoExchange)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrder>()
                .HasMany(e => e.TeleCustomers)
                .WithRequired(e => e.TeleOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeleOrder>()
                .HasMany(e => e.TeleOrderDeliveries)
                .WithRequired(e => e.TeleOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeleOrder>()
                .HasMany(e => e.TeleOrderDetails)
                .WithRequired(e => e.TeleOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeleOrder>()
                .HasMany(e => e.TeleOrderPrdocuctExchanges)
                .WithRequired(e => e.TeleOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TeleOrderDetail>()
                .Property(e => e.UnitPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderDetail>()
                .Property(e => e.SubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderDetail>()
                .Property(e => e.PromoProduct)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderDetail>()
                .Property(e => e.ShippingCharge)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderDetail>()
                .Property(e => e.PromoShipping)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderDetail>()
                .Property(e => e.TotalAmount)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderDetail>()
                .Property(e => e.RefillPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderDetail>()
                .Property(e => e.PromoRefill)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderDetail>()
                .Property(e => e.RefillSubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderDetail>()
                .Property(e => e.RefillTotalAmount)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderPrdocuctExchange>()
                .Property(e => e.ExchangeWith)
                .IsUnicode(false);

            modelBuilder.Entity<TeleOrderPrdocuctExchange>()
                .Property(e => e.ExchangePrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderPrdocuctExchange>()
                .Property(e => e.ExchangePromoPrice)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderPrdocuctExchange>()
                .Property(e => e.SubTotal)
                .HasPrecision(18, 3);

            modelBuilder.Entity<TeleOrderPrdocuctExchange>()
                .Property(e => e.TotalAmount)
                .HasPrecision(18, 3);
        }
    }
}
