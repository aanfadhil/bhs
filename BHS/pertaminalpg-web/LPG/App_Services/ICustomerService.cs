﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface ICustomerService
    {
        CustomerModel CustomerView(int CustomerId);
        bool deleteCustomer(int id);
        bool IsExists(int id);
        DataTableModel<GetCustomerPaging_Result> GetCustomerPaging(DatatablesRequest model);
        List<CustomerModel> ListCustomers(bool withDefaultAddress = false);
    }
}