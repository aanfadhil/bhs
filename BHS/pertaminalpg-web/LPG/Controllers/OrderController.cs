﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using LPG.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using ClosedXML.Excel;
using System.Globalization;
using i18n;
using LPG.Reporting.ReportServices;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class OrderController : AbstractController<IOrderService>
    {
        SessionManagement sessionManagement;

        //public IOrderService orderService;

        public OrderController(IOrderService _serviceObject) : base(_serviceObject)
        {
        }

        //OrderService serviceObject = new OrderService();
        // GET: Order
        public ActionResult ListOrder()
        {
            var vm = new OrderModel();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    //vm.OrderModelList = serviceObject.ListOrder();
                    //vm.GetOrderList = serviceObject.GetAllOrders();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                throw ex;
                return RedirectToAction("Index", "Error");
            }

            return View(vm);
        }

        [HttpPost]
        public ActionResult ListOrderPaging(DatatablesRequest model)
        {
            var vm = new DataTableModel<OrderListPagingModel>();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    //vm.OrderModelList = serviceObject.ListOrder
                    var res = serviceObject.GetAllOrdersPaging(model);
                    vm = res;
                }
                else
                {
                    return Json(vm);
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                throw ex;
                return RedirectToAction("Index", "Error");
            }
            var jsonResult = Json(vm, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return Json(vm);
        }

        public ActionResult OrderDetails(int? id, int? buyType)
        {
            OrderModel model = new OrderModel();
            sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {


                var convid = id ?? 0;
                var conbuyType = buyType ?? 0;
                if (convid > 0)
                {
                    //model = serviceObject.getOrderDetails(convid);
                    model.OrderView = serviceObject.getOrderDetails(convid, conbuyType);
                    //var vm = orderObject.getOrderDetails(convid);
                    model = model ?? new OrderModel();
                    model = model.OrderView;
                    if (model != null)
                    {
                        return PartialView("Details", model);
                    }
                    else
                    {

                        //return PartialView("Details", model);
                        return View(model);
                    }
                }
                else
                {
                    return View(model);
                }

            }
            else
            {
                return RedirectToAction("Index", "Login", new { area = "Admin" });
            }
        }

        /// <summary>
        /// UPDATE : Order Status = 4 (Delivered)
        /// To Close Orders
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult deleteOrder(int? id)
        {
            if (id != null & id > 0)
            {

                try
                {
                    sessionManagement = new SessionManagement();
                    AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                    ViewBag.Role = adminSessionInfo.UserRole;

                    if (adminSessionInfo != null)
                    {

                        serviceObject.UpdateOrderStatus(id.ToInt(), adminSessionInfo.UserId, 4);

                    }
                }
                catch (Exception ex)
                {
                    telemectryClient.TrackException(ex);
                    throw ex;
                }
            }
            return RedirectToAction("ListOrder", "Order");
        }

        /// <summary>
        /// UPDATE : Order Status = 8 (CancelledByHQSuperUser)
        /// To Cancel Orders
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CancelOrder(int? id)
        {
            if (id != null & id > 0)
            {

                try
                {
                    sessionManagement = new SessionManagement();
                    AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                    ViewBag.Role = adminSessionInfo.UserRole;
                    if (adminSessionInfo != null)
                    {

                        serviceObject.UpdateOrderStatus(id.ToInt(), adminSessionInfo.UserId, 8);

                    }
                }
                catch (Exception ex)
                {
                    telemectryClient.TrackException(ex);
                }
            }
            return RedirectToAction("ListOrder", "Order");
        }

        [HttpPost]
        public ActionResult Export(DatatablesRequest request)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    lpgdevEntities entities = new lpgdevEntities();
                    DataTable dt = new DataTable("MasterData");
                    dt.Columns.AddRange(new DataColumn[] {
                        new DataColumn("[[[No]]]"),
                        new DataColumn("[[[OrderID]]]"),
                        new DataColumn("[[[Nama Pelanggan]]]"),
                        new DataColumn("[[[No Handphone]]]"),
                        new DataColumn("[[[No.Invoice]]]"),
                        new DataColumn("[[[Region]]]"),
                        new DataColumn("[[[Kota]]]"),
                        new DataColumn("[[[Produk]]]"),
                        new DataColumn("[[[Jumlah]]]"),
                        new DataColumn("[[[Waktu Pesan]]]"),
                        new DataColumn("[[[Tanggal & Waktu Pengiriman]]]"),
                        new DataColumn("[[[Tanggal & Waktu Selesai Pengiriman]]]"),
                        new DataColumn("[[[Metode Beli]]]"),
                        new DataColumn("[[[Voucher]]]"),
                        new DataColumn("[[[Status]]]"),
                        new DataColumn("[[[Total Harga(Rp)]]]"),
                        new DataColumn("[[[Alamat Deliv]]]"),
                        new DataColumn("[[[Agen]]]"),
                        new DataColumn("[[[Outlet]]]"),
                        new DataColumn("[[[Admin]]]"),
                        new DataColumn("[[[Hp Admin]]]")
                    });

                    var orders = serviceObject.GetAllOrdersPaging(new DatatablesRequest()
                    {
                        draw = 0,
                        length = null,
                        start = 1,
                        search = new List<string>() { "" },
                        searchtype = "",
                        end_date = request.end_date,
                        start_date = request.start_date
                    });

                    int count = 0;
                    foreach (var order in orders.data)
                    {
                        count++;

                        var orderdate = string.Empty;
                        var delivdate = string.Empty;

                        if (order.DeliveryDate != null)
                        {
                            delivdate = order.DeliveryDate.Value.ToString("dd/MM/yyyy") + " " + order.SlotName;
                        }

                        if (order.OrderDate != null)
                        {
                            orderdate = order.OrderDate.Value.ToString("dd/MM/yyyy HH:mm");
                        }

                        var buyingstatus = "";
                        if (order.BuyingStatus != null)
                        {
                            if (order.BuyingStatus == 1)
                            {
                                buyingstatus = "Delivery (Aplikasi)";
                            }
                            else if (order.BuyingStatus == 2)
                            {
                                buyingstatus = "Delivery (Telepon)";
                            }
                            else
                            {
                                buyingstatus = "Delivery (Pickup)";
                            }
                        }

                        var statusorder = "";

                        if (order.StatusID == 1)
                        {
                            statusorder = "Belum diterima";
                        }
                        else if (order.StatusID == 2)
                        {
                            statusorder = "Diterima";
                        }
                        else if (order.StatusID == 3)
                        {
                            statusorder = "Dikirim";
                        }
                        else if (order.StatusID == 4)
                        {
                            statusorder = "Berhasil";
                        }
                        else if (order.StatusID == 5)
                        {
                            statusorder = "Dibatalkanoleh Consumer";
                        }
                        else if (order.StatusID == 6)
                        {
                            statusorder = "Dibatalkanoleh System";
                        }
                        else if (order.StatusID == 7)
                        {
                            statusorder = "Dibatalkanoleh Super User";
                        }
                        else if (order.StatusID == 8)
                        {
                            statusorder = "Dibatalkanoleh HQ Super User";
                        }
                        else if (order.StatusID == 9)
                        {
                            statusorder = "Belum dikirim";
                        }
                        else
                        {
                            statusorder = "";
                        }

                        dt.Rows.Add
                        (
                        count,
                        order.OrdrID,
                        order.Name,
                        order.MobileNumber,
                        order.InvoiceNumber,
                        (order.StatusID != 5) ? order.RegionName : "",
                        order.CityName,
                        order.ProductList,
                        order.Quantity,
                        orderdate,
                        delivdate,
                        order.DeliveredAtFormated,
                        buyingstatus,
                        order.Voucher,
                        statusorder,
                        order.GrandTotal,
                        order.Address,
                        order.Agen,
                        order.DistributionPointName,
                        order.AgentAdminName,
                        order.AgentAdminPhone
                       );
                    }
                    var style = XLWorkbook.DefaultStyle;


                    using (XLWorkbook wb = new XLWorkbook())
                    {


                        wb.Worksheets.Add(dt);
                        using (MemoryStream stream = new MemoryStream())
                        {
                            wb.SaveAs(stream);
                            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Daftar Pesanan.xlsx");
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Order List" });
            }

        }

    }
}
