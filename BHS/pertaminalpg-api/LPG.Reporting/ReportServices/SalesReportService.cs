﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ExtensionMethods
{
    public static class MyDateTimeExtension
    {
        public static DateTime NextDayOfWeek(this DateTime dt, DayOfWeek dayOfWeek)
        {
            int offsetDays = dayOfWeek - dt.DayOfWeek;
            return dt.AddDays(offsetDays > 0 ? offsetDays : offsetDays + 7).Date;
        }



        public static DateTime PreviousOfWeek(this DateTime dt, DayOfWeek dayOfWeek)
        {
            int offsetDays = -(dt.DayOfWeek - dayOfWeek);
            return dt.AddDays(offsetDays).Date;
        }

        public static DateTime PreviousDayOfMonth(this DateTime dt)
        {
            return new DateTime(dt.Year, 1, 1);
        }

        public static DateTime PreviousDayOfYear(this DateTime dt)
        {
            return new DateTime(dt.Year, 1, 1);
        }
    }
}

namespace LPG.Reporting.ReportServices
{
    using ExtensionMethods;
    using NodaTime;
    using System.Linq.Expressions;

    public enum SKUs
    {
        BG12,
        BG55,
        ELPG,
        ALL
    }

    public enum ReportPeriod
    {
        Week,
        Month,
        Year
    }

    public enum Unit
    {
        Currency,
        Volume
    }

    public class SalesReportRequestDTO
    {
        //public List<SKUs> SKUs { get; set; }
        public List<int> ProductIds { get; set; }
        public ReportPeriod Period { get; set; }
        //public string UnitDisplayName { get; set; }
        public int NumberOfPeriods { get; set; }
        public List<int> AgencyIDs { get; set; }

    }

    public class SalesReportGroupingDTO
    {
        public int Year { get; set; }
        public int PeriodNum { get; set; }
        //public DateTime StartDate { get; set; }
    }

    public class SalesReportService
    {

        //private LpgDataDbContext _context;

        public SalesReportService()
        {
            //_context = new LpgDataDbContext();

        }


        public List<KeyValuePair<DateTime, double>> GetSalesReportInCash(SalesReportRequestDTO request)
        {
            var resp = new List<KeyValuePair<DateTime, double>>();

            using (var _context = new ReportingDbContext())
            {

                var result = GetOrderDetailsForRequest(request, _context).AsEnumerable()
                            .Select(d => new
                            {
                                Period = d.Key,
                                TotalRevenue = d.Sum(x => x.SubTotal + x.RefillSubTotal)
                            })
                            .Select(r =>
                                    new KeyValuePair<DateTime, double>(GetStartOfPeriod(r.Period.PeriodNum, r.Period.Year, request.Period), (double) r.TotalRevenue)
                            ).ToList();


                var dateRanges = BuildDataRangeForPeriods(request.NumberOfPeriods, request.Period, TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time")).OrderBy(o => o).ToList();

                var output = new List<KeyValuePair<DateTime, double>>();

                for (int i = 0; i < dateRanges.Count; i++)
                {
                    var curPeriodStart = dateRanges.ElementAt(i);

                    var dbRes = result.Where(x => x.Key.Date == curPeriodStart.Date);

                    if (request.Period == ReportPeriod.Year)
                    {
                        dbRes = result.Where(x => new DateTime(x.Key.Year, 1, 1) == new DateTime(curPeriodStart.Year, 1, 1)).ToList();
                    }

                    if (dbRes.Count() != 0)
                    {
                        output.Add(dbRes.First());
                    }
                    else
                    {
                        output.Add(new KeyValuePair<DateTime, double>(curPeriodStart, 0));
                    }

                }

                resp.AddRange(output);
            }


            return resp;
        }

        public List<KeyValuePair<DateTime, double>> GetSalesReportInCashs(SalesReportRequestDTO request,List<int?> suOrders)
        {
            var resp = new List<KeyValuePair<DateTime, double>>();

            using (var _context = new ReportingDbContext())
            {

                var result = GetOrderDetailsForRequest(request, _context,suOrders).AsEnumerable()
                            .Select(d => new
                            {
                                Period = d.Key,
                                TotalRevenue = d.Sum(x => x.SubTotal + x.RefillSubTotal)
                            })
                            .Select(r =>
                                    new KeyValuePair<DateTime, double>(GetStartOfPeriod(r.Period.PeriodNum, r.Period.Year, request.Period), (double)r.TotalRevenue)
                            ).ToList();


                var dateRanges = BuildDataRangeForPeriods(request.NumberOfPeriods, request.Period, TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time")).OrderBy(o => o).ToList();

                var output = new List<KeyValuePair<DateTime, double>>();

                for (int i = 0; i < dateRanges.Count; i++)
                {
                    var curPeriodStart = dateRanges.ElementAt(i);

                    var dbRes = result.Where(x => x.Key.Date == curPeriodStart.Date);

                    if (request.Period == ReportPeriod.Year)
                    {
                        dbRes = result.Where(x => new DateTime(x.Key.Year, 1, 1) == new DateTime(curPeriodStart.Year, 1, 1)).ToList();
                    }

                    if (dbRes.Count() != 0)
                    {
                        output.Add(dbRes.First());
                    }
                    else
                    {
                        output.Add(new KeyValuePair<DateTime, double>(curPeriodStart, 0));
                    }

                }

                resp.AddRange(output);
            }


            return resp;
        }


        public List<KeyValuePair<DateTime, int>> GetSalesReportInUnits(SalesReportRequestDTO request)
        {
            var resp = new List<KeyValuePair<DateTime, int>>();

            //Define periods for which we are going to do grouping
            var periods = request.NumberOfPeriods;


            using (var _context = new ReportingDbContext())
            {

                var result = GetOrderDetailsForRequest(request, _context).AsEnumerable()
                            .Select(d => new
                            {
                                Period = d.Key,
                                TotalAmount = d.Sum(x => x.Quantity + x.RefillQuantity)
                            })
                            .Select(r =>
                                    new KeyValuePair<DateTime, int>(GetStartOfPeriod(r.Period.PeriodNum, r.Period.Year, request.Period), r.TotalAmount)
                            ).ToList();

                var dateRanges = BuildDataRangeForPeriods(request.NumberOfPeriods, request.Period, TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time")).OrderBy(o => o).ToList();

                var output = new List<KeyValuePair<DateTime, int>>();

                for (int i = 0; i < dateRanges.Count; i++)
                {
                    var curPeriodStart = dateRanges.ElementAt(i);

                    var dbRes = result.Where(x => x.Key.Date == curPeriodStart.Date).ToList();

                    if (request.Period == ReportPeriod.Year)
                    {
                        dbRes = result.Where(x => new DateTime(x.Key.Year, 1, 1) == new DateTime(curPeriodStart.Year, 1, 1)).ToList();
                    }


                    if (dbRes.Count() != 0)
                    {
                        output.Add(dbRes.First());
                    }
                    else
                    {
                        output.Add(new KeyValuePair<DateTime, int>(curPeriodStart, 0));
                    }

                }

                resp.AddRange(output);

            }


            return resp;
        }

        public List<KeyValuePair<DateTime, int>> GetSalesReportInUnitss(SalesReportRequestDTO request,List<int?> suOrders)
        {
            var resp = new List<KeyValuePair<DateTime, int>>();

            //Define periods for which we are going to do grouping
            var periods = request.NumberOfPeriods;


            //using (var _context = new ReportingDbContext())
            using (var _context = new ReportingDbContext())
            {

                
                var result = GetOrderDetailsForRequest(request, _context,suOrders).AsEnumerable()
                            .Select(d => new
                            {
                                Period = d.Key,
                                TotalAmount = d.Sum(x => x.Quantity + x.RefillQuantity)
                            })
                            .Select(r =>
                                    new KeyValuePair<DateTime, int>(GetStartOfPeriod(r.Period.PeriodNum, r.Period.Year, request.Period), r.TotalAmount)
                            ).ToList();

                var dateRanges = BuildDataRangeForPeriods(request.NumberOfPeriods, request.Period, TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time")).OrderBy(o => o).ToList();

                var output = new List<KeyValuePair<DateTime, int>>();

                for (int i = 0; i < dateRanges.Count; i++)
                {
                    var curPeriodStart = dateRanges.ElementAt(i);

                    var dbRes = result.Where(x => x.Key.Date == curPeriodStart.Date).ToList();

                    if (request.Period == ReportPeriod.Year)
                    {
                        dbRes = result.Where(x => new DateTime(x.Key.Year, 1, 1) == new DateTime(curPeriodStart.Year, 1, 1)).ToList();
                    }


                    if (dbRes.Count() != 0)
                    {
                        output.Add(dbRes.First());
                    }
                    else
                    {
                        output.Add(new KeyValuePair<DateTime, int>(curPeriodStart, 0));
                    }

                }

                resp.AddRange(output);

            }


            return resp;
        }


        private List<DateTime> BuildDataRangeForPeriods(int numberOdPeriods, ReportPeriod period, DateTime todayDate)
        {
            var dateRanges = new List<DateTime>();
            //var today = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");


            for (int i = 0; i < numberOdPeriods; i++)
            {

                switch (period)
                {
                    case ReportPeriod.Week:
                        DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                        Calendar cal = dfi.Calendar;
                        DateTime date = cal.AddWeeks(new DateTime(todayDate.Year, 1, 1), GetWeekNumberForDate(todayDate) - (i + 1));//.AddDays(1); idk wtf is this add days 1
                        dateRanges.Add(date); //-(GetWeekNumberForDate(today)) * (i + 1)));

                        break;
                    case ReportPeriod.Month:
                        dateRanges.Add(new DateTime(todayDate.Year, todayDate.Month, 1).AddMonths(-i));
                        break;
                    case ReportPeriod.Year:
                        dateRanges.Add(todayDate.AddYears(-i));
                        break;
                    default:
                        break;
                }


            }

            return dateRanges;
        }

        private int GetWeekNumberForDate(DateTime date)
        {
            CultureInfo cul = CultureInfo.CurrentCulture;


            return cul.Calendar.GetWeekOfYear(
                    date,
                    CalendarWeekRule.FirstDay,
                    DayOfWeek.Monday);
        }

        private IQueryable<IGrouping<SalesReportGroupingDTO, OrderDetail>> GetOrderDetailsForRequest(SalesReportRequestDTO request, ReportingDbContext context)
        {
            var orderDeliveries = context.OrderDeliveries.AsQueryable();

            var dateRanges = BuildDataRangeForPeriods(request.NumberOfPeriods, request.Period, TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"));

            var statsStartDate = dateRanges.Last();


            if (request.AgencyIDs != null && request.AgencyIDs.Count > 0)
            {
                orderDeliveries = orderDeliveries.Where(BuildOrExpression<OrderDelivery, int>(p => p.AgentAdmin.AgenID, request.AgencyIDs.AsEnumerable()));
            }



            var whereOD = orderDeliveries
                .Where(od => od.CreatedDate >= statsStartDate)
                .Where(od => (od.StatusId == 4 || od.StatusId == 5 || od.StatusId == 6)) // && request.AgencyIDs.Contains(od.AgentAdmin.AgenID)
                                                                                         //.OrderByDescending(od => od.DeliveryDate)
                .SelectMany(od =>
                    od.Order.OrderDetails.AsQueryable().Where(BuildOrExpression<OrderDetail, int>(p => p.ProdID, request.ProductIds.AsEnumerable())) //det => request.ProductId == det.ProdID)
                );



            var group = whereOD.
                AsEnumerable()
                .GroupBy(orderDetail =>
               new SalesReportGroupingDTO
               {
                   Year = orderDetail.CreatedDate.Year,
                   //StartDate = SqlFunctions.DateAdd("week", SqlFunctions.DatePart("week", orderDetail.CreatedDate).Value, ((DateTime?) new DateTime(orderDetail.CreatedDate.Year, 1, 1)) ?? DateTime.MinValue) //dateRanges.Select(x => dateRanges.Select(o => o.))
                   PeriodNum = SqlFunctions.DatePart("week", orderDetail.CreatedDate).Value,
                   //    PeriodNum = //BuildGreaterThanExpression<OrderDetail, DateTime>(p => p.CreatedDate, dateRanges.AsEnumerable()).Compile()
                   //dateRanges.FirstOrDefault(r => orderDetail.CreatedDate > r)
               }
            // FuncToExpression<OrderDetail>(r => orderDetail.CreatedDate > r.CreatedDate)
            //Expression.Lambda<Func<bool>>(Expression.Call(Func<bool> { r => orderDetail.CreatedDate > r }.Method))
            //(dateRanges.)
            ).AsQueryable();


            switch (request.Period)
            {
                case ReportPeriod.Week:
                    group = whereOD.GroupBy(orderDetail => new SalesReportGroupingDTO
                    {
                        Year = orderDetail.CreatedDate.Year,
                        PeriodNum = SqlFunctions.DatePart("week", orderDetail.CreatedDate).Value
                    }
                    );
                    break;
                case ReportPeriod.Month:
                    group = whereOD.GroupBy(orderDetail =>
                        new SalesReportGroupingDTO
                        {
                            Year = orderDetail.CreatedDate.Year,
                            PeriodNum = SqlFunctions.DatePart("month", orderDetail.CreatedDate).Value
                        }
                    );
                    break;
                case ReportPeriod.Year:
                    group = whereOD.GroupBy(orderDetail => new SalesReportGroupingDTO
                    {
                        Year = orderDetail.CreatedDate.Year,
                        PeriodNum = SqlFunctions.DatePart("year", orderDetail.CreatedDate).Value
                    }
                    );
                    break;
                default:
                    group = whereOD.GroupBy(orderDetail => new SalesReportGroupingDTO
                    {
                        Year = orderDetail.CreatedDate.Year,
                        PeriodNum = SqlFunctions.DatePart("week", orderDetail.CreatedDate).Value
                    }
                    );
                    break;
            }

            return group;
        }


        private IQueryable<IGrouping<SalesReportGroupingDTO, OrderDetail>> GetOrderDetailsForRequest(SalesReportRequestDTO request, ReportingDbContext context,List<int?> ordrIds)
        {
            var orderDeliveries = context.OrderDeliveries.Include("Order").Where(t => ordrIds.Any(c => c == t.Order.CityId)).AsQueryable();

            var dateRanges = BuildDataRangeForPeriods(request.NumberOfPeriods, request.Period, TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"));

            var statsStartDate = dateRanges.Last();


            if (request.AgencyIDs != null && request.AgencyIDs.Count > 0)
            {
                orderDeliveries = orderDeliveries.Where(BuildOrExpression<OrderDelivery, int>(p => p.AgentAdmin.AgenID, request.AgencyIDs.AsEnumerable()));
            }



            var whereOD = orderDeliveries
                .Where(od => od.CreatedDate >= statsStartDate)
                .Where(od => (od.StatusId == 4 || od.StatusId == 5 || od.StatusId == 6)) // && request.AgencyIDs.Contains(od.AgentAdmin.AgenID)
                                                                                         //.OrderByDescending(od => od.DeliveryDate)
                .SelectMany(od =>
                    od.Order.OrderDetails.AsQueryable().Where(BuildOrExpression<OrderDetail, int>(p => p.ProdID, request.ProductIds.AsEnumerable())) //det => request.ProductId == det.ProdID)
                );



            var group = whereOD.
                AsEnumerable()
                .GroupBy(orderDetail =>
               new SalesReportGroupingDTO
               {
                   Year = orderDetail.CreatedDate.Year,
                   //StartDate = SqlFunctions.DateAdd("week", SqlFunctions.DatePart("week", orderDetail.CreatedDate).Value, ((DateTime?) new DateTime(orderDetail.CreatedDate.Year, 1, 1)) ?? DateTime.MinValue) //dateRanges.Select(x => dateRanges.Select(o => o.))
                   PeriodNum = SqlFunctions.DatePart("week", orderDetail.CreatedDate).Value,
                   //    PeriodNum = //BuildGreaterThanExpression<OrderDetail, DateTime>(p => p.CreatedDate, dateRanges.AsEnumerable()).Compile()
                   //dateRanges.FirstOrDefault(r => orderDetail.CreatedDate > r)
               }
            // FuncToExpression<OrderDetail>(r => orderDetail.CreatedDate > r.CreatedDate)
            //Expression.Lambda<Func<bool>>(Expression.Call(Func<bool> { r => orderDetail.CreatedDate > r }.Method))
            //(dateRanges.)
            ).AsQueryable();


            switch (request.Period)
            {
                case ReportPeriod.Week:
                    group = whereOD.GroupBy(orderDetail => new SalesReportGroupingDTO
                    {
                        Year = orderDetail.CreatedDate.Year,
                        PeriodNum = SqlFunctions.DatePart("week", orderDetail.CreatedDate).Value
                    }
                    );
                    break;
                case ReportPeriod.Month:
                    group = whereOD.GroupBy(orderDetail =>
                        new SalesReportGroupingDTO
                        {
                            Year = orderDetail.CreatedDate.Year,
                            PeriodNum = SqlFunctions.DatePart("month", orderDetail.CreatedDate).Value
                        }
                    );
                    break;
                case ReportPeriod.Year:
                    group = whereOD.GroupBy(orderDetail => new SalesReportGroupingDTO
                    {
                        Year = orderDetail.CreatedDate.Year,
                        PeriodNum = SqlFunctions.DatePart("year", orderDetail.CreatedDate).Value
                    }
                    );
                    break;
                default:
                    group = whereOD.GroupBy(orderDetail => new SalesReportGroupingDTO
                    {
                        Year = orderDetail.CreatedDate.Year,
                        PeriodNum = SqlFunctions.DatePart("week", orderDetail.CreatedDate).Value
                    }
                    );
                    break;
            }

            return group;
        }


        public static Expression<Func<TElement, bool>> BuildOrExpression<TElement, TValue>(
        Expression<Func<TElement, TValue>> valueSelector,
        IEnumerable<TValue> values
        )
        {
            if (null == valueSelector)
                throw new ArgumentNullException("valueSelector");

            if (null == values)
                throw new ArgumentNullException("values");

            ParameterExpression p = valueSelector.Parameters.Single();

            if (!values.Any())
                return e => false;

            var equals = values.Select(value =>
                (Expression) Expression.Equal(
                     valueSelector.Body,
                     Expression.Constant(
                         value,
                         typeof(TValue)
                     )
                )
            );
            var body = equals.Aggregate<Expression>(
                     (accumulate, equal) => Expression.Or(accumulate, equal)
             );

            return Expression.Lambda<Func<TElement, bool>>(body, p);
        }

        public static Expression<Func<TElement, bool>> BuildGreaterThanExpression<TElement, TValue>(
        Expression<Func<TElement, TValue>> valueSelector,
        IEnumerable<TValue> values
        )
        {
            if (null == valueSelector)
                throw new ArgumentNullException("valueSelector");

            if (null == values)
                throw new ArgumentNullException("values");

            ParameterExpression p = valueSelector.Parameters.Single();

            if (!values.Any())
                return e => false;

            var equals = values.Select(value =>
                (Expression) Expression.LessThanOrEqual(
                     valueSelector.Body,
                     Expression.Constant(
                         value,
                         typeof(TValue)
                     )
                )
            );


            var body = equals.Aggregate<Expression>(
                     (accumulate, equal) => Expression.Or(accumulate, equal)
             );

            return Expression.Lambda<Func<TElement, bool>>(body, p);
        }

        //private static Expression<Func<TElement, bool>> FuncToExpression<TElement, TValue>(Func<TElement, TValue> f)
        //{


        //    return x => f(x);
        //}

        private DateTime GetStartOfPeriod(int numberInPeriod, int year, ReportPeriod period)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            Calendar cal = dfi.Calendar;
            var resp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            switch (period)
            {
                case ReportPeriod.Week:
                    resp = cal.AddWeeks(new DateTime(year, 1, 1), numberInPeriod-1);//.AddDays(1);  //FirstDateOfWeek(year, numberInPeriod, CultureInfo.CurrentCulture);
                    break;
                case ReportPeriod.Month:
                    resp = new DateTime(year, numberInPeriod, 1);
                    break;
                case ReportPeriod.Year:
                    resp = new DateTime(year, 1, 1);
                    break;
                default:
                    break;
            }


            return resp;
        }



        //Utils
        public DateTime GetStartOfPeriod(DateTime date, ReportPeriod Period)
        {
            switch (Period)
            {
                case ReportPeriod.Week:
                    return StartOfWeek(date, CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek);
                case ReportPeriod.Month:
                    return StartOfMonth(date);
                case ReportPeriod.Year:
                    return StartOfYear(date);
                default:
                    return StartOfWeek(date, CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek);
            }
        }

        public DateTime StartOfYear(DateTime dt)
        {
            return new DateTime(dt.Year, 1, 1);
        }

        public DateTime StartOfMonth(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, 1);
        }

        public DateTime StartOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }

        //public static int GetIso8601WeekOfYear(DateTime time)
        //{
        //    DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
        //    if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
        //    {
        //        time = time.AddDays(3);
        //    }

        //    return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
        //}

        public static DateTime FirstDateOfWeek(int year, int weekOfYear, System.Globalization.CultureInfo ci)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = (int) ci.DateTimeFormat.FirstDayOfWeek - (int) jan1.DayOfWeek;
            DateTime firstWeekDay = jan1.AddDays(daysOffset);
            int firstWeek = ci.Calendar.GetWeekOfYear(jan1, ci.DateTimeFormat.CalendarWeekRule, ci.DateTimeFormat.FirstDayOfWeek);
            if ((firstWeek <= 1 || firstWeek >= 52) && daysOffset >= -3)
            {
                weekOfYear -= 1;
            }
            return firstWeekDay.AddDays(weekOfYear * 7);
        }

        //static DateTime FirstDateOfWeek(int year, int weekNum, CalendarWeekRule rule)
        //{
        //    //Debug.Assert(weekNum >= 1);

        //    DateTime jan1 = new DateTime(year, 1, 1);

        //    int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;
        //    DateTime firstMonday = jan1.AddDays(daysOffset);
        //    //Debug.Assert(firstMonday.DayOfWeek == DayOfWeek.Monday);

        //    var cal = CultureInfo.CurrentCulture.Calendar;
        //    int firstWeek = cal.GetWeekOfYear(firstMonday, rule, DayOfWeek.Monday);

        //    if (firstWeek <= 1)
        //    {
        //        weekNum -= 1;
        //    }

        //    DateTime result = firstMonday.AddDays(weekNum * 7);

        //    return result;
        //}

    }

}
