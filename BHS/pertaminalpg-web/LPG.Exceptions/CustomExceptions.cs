﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBG.Shared.Exceptions
{
    /// <summary>
    /// Class to represent a data not found exception
    /// </summary>
    public partial class DataNotFoundException : BaseException
    {
        public object[] Parameters { get; set; }

        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public DataNotFoundException() : base("Cannot find data") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public DataNotFoundException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public DataNotFoundException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public DataNotFoundException(string message, Exception actualException) : base(message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        /// <param name="actualParameters">Parameters used in method where the exception occured</param>
        public DataNotFoundException(string message, Exception actualException, params object[] actualParameters) : base(message, actualException) 
        {
            this.Parameters = actualParameters;
        }
    }

    /// <summary>
    /// Class to represent invalid argument exceptions
    /// </summary>
    public partial class InvalidArgumentException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public InvalidArgumentException() : base("Invalid arguments specified") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public InvalidArgumentException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public InvalidArgumentException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public InvalidArgumentException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent duplicate data exceptions
    /// </summary>
    public partial class DuplicateDataException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public DuplicateDataException() : base("Invalid arguments specified") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public DuplicateDataException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public DuplicateDataException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public DuplicateDataException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent data transaction exceptions
    /// </summary>
    public partial class DataTransactionException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public DataTransactionException() : base("Error while setting data transaction") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public DataTransactionException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public DataTransactionException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public DataTransactionException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent data access exceptions
    /// </summary>
    public partial class DataAccessException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public DataAccessException() : base("Error while accessing data") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public DataAccessException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public DataAccessException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public DataAccessException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent data mapping exceptions
    /// </summary>
    public partial class DataMappingException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public DataMappingException() : base("Error while accessing data") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public DataMappingException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public DataMappingException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public DataMappingException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent document generation exceptions
    /// </summary>
    public partial class DocumentGenerationException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public DocumentGenerationException() : base("Error while accessing data") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public DocumentGenerationException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public DocumentGenerationException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public DocumentGenerationException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent configuration read exceptions
    /// </summary>
    public partial class ConfigReadException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public ConfigReadException() : base("Error while accessing data") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public ConfigReadException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public ConfigReadException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public ConfigReadException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent process exceptions
    /// </summary>
    public partial class ProcessException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public ProcessException() : base("Error while accessing data") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public ProcessException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public ProcessException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public ProcessException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent unknown exceptions
    /// </summary>
    public partial class UnknownException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public UnknownException() : base("Unknown error during the process") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public UnknownException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public UnknownException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public UnknownException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent type mismatch exceptions
    /// </summary>
    public partial class TypeMismatchException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public TypeMismatchException() : base("Error while accessing data") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public TypeMismatchException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public TypeMismatchException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public TypeMismatchException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent API request exceptions
    /// </summary>
    public partial class APIRequestException : BaseException
    {
        private const string _defaultMessage = "Error while requesting API service";

        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public APIRequestException() : base(_defaultMessage) { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public APIRequestException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public APIRequestException(Exception actualException) : base(_defaultMessage, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public APIRequestException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent API header exceptions
    /// </summary>
    public partial class APIHeaderException : BaseException
    {
        private const string _defaultMessage = "Error while adding header to API request";

        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public APIHeaderException() : base(_defaultMessage) { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public APIHeaderException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public APIHeaderException(Exception actualException) : base(_defaultMessage, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public APIHeaderException(string message, Exception actualException) : base(message, actualException) { }
    }

    /// <summary>
    /// Class to represent API response exceptions
    /// </summary>
    public partial class APIResponseException : BaseException
    {
        private const string _defaultMessage = "Error while reading API response";

        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public APIResponseException() : base(_defaultMessage) { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public APIResponseException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public APIResponseException(Exception actualException) : base(_defaultMessage, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public APIResponseException(string message, Exception actualException) : base(message, actualException) { }
    }


    /// <summary>
    /// Class to represent internal server exceptions
    /// </summary>
    public partial class InternalServerException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public InternalServerException() : base("Internal server error during the process") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public InternalServerException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public InternalServerException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public InternalServerException(string message, Exception actualException) : base(message, actualException) { }
    }


    /// <summary>
    /// Class to represent configuration read exceptions
    /// </summary>
    public partial class EmailException : BaseException
    {
        /// <summary>
        /// Initializes the exception with default message
        /// </summary>
        public EmailException() : base("Error while sending email") { }

        /// <summary>
        /// Initializes the exception with given message
        /// </summary>
        /// <param name="message">Exception message</param>
        public EmailException(string message) : base(message) { }

        /// <summary>
        /// Initializes the exception with the given inner exception
        /// </summary>
        /// <param name="actualException">Actual Exception</param>
        public EmailException(Exception actualException) : base(actualException.Message, actualException) { }

        /// <summary>
        /// Initializes the exception with given message and given inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="actualException">Actual Exception</param>
        public EmailException(string message, Exception actualException) : base(message, actualException) { }
    }

}
