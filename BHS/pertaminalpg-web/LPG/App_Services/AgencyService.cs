﻿using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http.Results;

namespace LPG.App_Services
{
    public class AgencyService : ServiceBase, IAgencyService
    {
        public AgencyService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<CompanyModel> ListComapany()
        {
            var comapanyList = new List<CompanyModel>();
            //using (var context = new lpgdevEntities())
            //{
            var data = from company in context.Agencies
                       join r in context.MRegions
                       on company.RegionId equals r.RegionID
                       where company.StatusId == true
                       orderby company.AgenID descending, company.UpdatedDate descending, company.CreatedDate descending


                       select new CompanyModel
                       {
                           CompanyId = company.AgenID,
                           Soldtono = company.SoldToNumber,
                           CompanyName = company.AgencyName,
                           Region = r.RegionCode

                       };
            comapanyList = data.ToList();
            //}
            return comapanyList;
        }

        public CompanyModel getCompanyDetails(int agencyId)
        {
            //using (var context = new lpgdevEntities())
            //{
            var rtns = (from agencies in context.Agencies
                        where agencies.AgenID == agencyId
                        select new CompanyModel()
                        {
                            CompanyId = agencies.AgenID,
                            Soldtono = agencies.SoldToNumber,
                            CompanyName = agencies.AgencyName,
                            RegionId = agencies.RegionId


                        }).FirstOrDefault();

            return rtns;
            //}
        }

        public CompanyModel saveCompanyDetails(CompanyModel entity, int loggedBy)
        {

            try
            {

                //using (var context = new lpgdevEntities())
                //{

                var Data = context.Agencies.Where(r => r.AgenID == entity.CompanyId).FirstOrDefault();
                if (Data != null)
                {

                    Data.StatusId = true;
                    Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    Data.SoldToNumber = entity.Soldtono;
                    //Data.Region = entity.Region;
                    Data.AgencyName = entity.CompanyName;
                    Data.RegionId = entity.RegionId;
                    Data.UpdatedBy = base.UserId.ToShort();
                    Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggedBy);
                }
                else
                {
                    Agency objLocal = new Agency();

                    int SortOrder = 1;
                    int count = (from row in context.Agencies select row).Count();




                    objLocal.AgenID = entity.CompanyId;
                    objLocal.StatusId = true;
                    objLocal.SoldToNumber = entity.Soldtono;
                    objLocal.RegionId = entity.RegionId;
                    objLocal.AgencyName = entity.CompanyName;
                    objLocal.CreatedBy = base.UserId.ToShort();
                    objLocal.UpdatedBy = base.UserId.ToShort();
                    objLocal.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    objLocal.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    context.Agencies.Add(objLocal);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggedBy);
                }
                //}
                return entity;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public bool deleteAgency(int Id, int loggedBy)
        {
            try
            {
                //using (var context = new lpgdevEntities())
                //{
                var Data = context.Agencies.Where(r => r.AgenID == Id).FirstOrDefault();
                if (Data != null)
                {
                    Data.StatusId = false;
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", loggedBy);
                    return true;
                }
                return false;
                //}
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// ADD : Activity Log table
        /// </summary>
        /// <param name="action">Add/Update/Delete</param> 
        /// <param name="action_item">Page Name</param>
        /// <param name="loggedBy">Loggedin UserID</param>
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel
            {
                //ActivityService _activityService = new ActivityService();

                Activity = action,
                ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"),
                ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay,
                CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"),
                UserType = loggedBy,
                Item = "Agen Perusahaan"
            };
            _activityService.AddActivityLog(actModel, "Agen Perusahaan");
        }

        public List<CompanyModel> ListComapanyGetbyRegion(int regionId)
        {
            var comapanyList = new List<CompanyModel>();
            //using (var context = new lpgdevEntities())
            //{
            if (regionId == 0)
            {
                var data = from company in context.Agencies
                           where company.StatusId == true

                           select new CompanyModel
                           {
                               CompanyId = company.AgenID,
                               CompanyName = company.AgencyName

                           };
                comapanyList = data.ToList();
            }
            else
            {
                var data = from company in context.Agencies
                           where company.StatusId == true
                           && company.RegionId == regionId

                           select new CompanyModel
                           {
                               CompanyId = company.AgenID,
                               CompanyName = company.AgencyName

                           };
                comapanyList = data.ToList();
            }

            //}
            return comapanyList;
        }
    }
}
