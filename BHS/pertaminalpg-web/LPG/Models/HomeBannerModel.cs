﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LPG.Models
{
    public class HomeBannerModel
    {
        public int HomeBannerId { get; set; }
        [Required(ErrorMessage = "Banner Image required")]
        public string BannerImage { get; set; }
        [Required(ErrorMessage = "Banner Caption required")]
        public string Caption { get; set; }

        //public int CategoryId { get; set; }
              

        public bool IsActive { get; set; }

        public DateTime LastModifiedDate { get; set; }

        public List<HomeBannerModel> HomeBannerList { get; set; }

        public string ErrorMessage { get; set; }

        public string UpdatedUser { get; set; }

        public bool IsHomeBannerExists { get; set; }

        [Required(ErrorMessage = "Banner Category required")]
        public virtual int CategoryId
        {
            get
            {
                return (int)this.bannerCateg;
            }
            set
            {
                bannerCateg = (BannerCategory)value;
            }
        }

        [EnumDataType(typeof(BannerCategory))]
        public BannerCategory bannerCateg { get; set; }

        public enum BannerCategory
        {

            [Display(Name = "Cover Banner")]
            CoverBanner = 1,
            [Display(Name = "Footer Banner")]
            FooterBanner = 2,
            [Display(Name = "Promo Banner")]
            PromoBanner = 3

            //CoverBanner =1,
            //FooterBanner=2,
            //PromoBanner=3
        }
    }
    
}