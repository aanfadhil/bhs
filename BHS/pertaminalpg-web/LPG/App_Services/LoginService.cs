﻿using LPG.App_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LPG.Models;
using RestSharp;
using Newtonsoft.Json;
using System.Configuration;
using LPG.ActiveDirectory;
using System.Text;
using System.Security.Cryptography;
using Nito.AsyncEx.Synchronous;
using Microsoft.ApplicationInsights;
using System.Data.Entity.Validation;

namespace LPG.App_Services
{
    public class LoginService : AbstractAppService, ILoginService
    {


        string endPoint = Properties.Settings.Default.EndPoint;

        private ISuperAdminService _service;

        public LoginService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry, ISuperAdminService service) : base(context, activityService, _telemetry)
        {
            _service = service;
        }

        enum ADRoles
        {
            HQSuperUser,    // HQ.SuperUser
            PCCPersonnel,   // PCC.Personnel
            SuperAdmin,     // SuperAdmin
            SuperUser       // SuperUser
        }



        public AdminSessionEntity adminLogin(String email, String Password)
        {
            try
            {
                string filepath = ConfigurationManager.AppSettings["DefaultPath_Superadmin"].ToString();
                //using (var context = new lpgdevEntities())
                //{
                string encrptPassword = TokenGenerator.GetHashedPassword(Password, 49);

                if (context.SuperAdmins.Any(u => u.Email == email && u.Password == encrptPassword))
                {
                    var Items = (from users in context.SuperAdmins
                                 join role in context.MSuperUserTypes
                                 on users.UserTypeID equals role.UserTypeID
                                 where users.Email == email && users.Password == encrptPassword && users.StatusID
                                 select new AdminSessionEntity
                                 {
                                     UserId = users.SAdminID,
                                     EmailId = users.Email,
                                     UserType = users.UserTypeID,
                                     accessToken = users.AccToken,
                                     FullName = users.FullName,
                                     UserRole = role.TypeName,
                                     imgProfile = (users.ProfileImage != "") ? (filepath + users.ProfileImage) : "",

                                 });
                    return Items.Distinct().FirstOrDefault();
                }
                else
                    return null;

                //}
            }
            catch (Exception ex)
            {
                telemetry.TrackException(ex);
                throw ex;
            }
        }

        //Test Users
        //a.Trainee01(password: 123@ptm) à HQ.SuperUser
        //b.Trainee02 (password: 123@ptm) à PCC.Personnel
        //c.Trainee03 (password: 123@ptm) à SuperAdmin
        //d.Trainee04(password: 123@ptm) à SuperUser
        public AdminSessionEntity AdminLoginActiveDirectoryAsync(String username, String password)
        {
            try
            {
                var result = AuthenticateUserInADAsync(username, password);

                //Check if user exists in the database by email
                //If exists = just use that user for login
                //If user doesn't exist - create it and log in

                AdminSessionEntity resp = new AdminSessionEntity();

                if (result != null && result.UserPropertiesList != null)
                {

                    string filepath = ConfigurationManager.AppSettings["DefaultPath_Superadmin"].ToString();
                    var adRole = result.UserPropertiesList.mRoleList.First();

                    

                    short roleId = 0;

                    if (adRole != null)
                    {
                        switch (adRole)
                        {
                            case "HQ.SuperUser":
                                roleId = 4;
                                break;
 
                            case "PCC.Personnel":
                                roleId = 3;
                                break;

                            case "SuperAdmin":
                                roleId = 2;
                                break;

                            case "SuperUser":
                                roleId = 1;
                                break;

                            default:
                                //Role is not supported - returning null - cannot log in
                                return null;

                        }
                    }
                    else
                    {
                        //User has no roles defined in ActiveDirectory - cannot log in
                        return null;
                    }


                    var wilayahAuthParam = result.UserPropertiesList.mAuthObjectValueList.Where(t => t.mAuthObjectName.Trim() == "WILAYAH").FirstOrDefault();
                    var phoneAuthParam = result.UserPropertiesList.mAuthObjectValueList.Where(t => t.mAuthObjectName.Trim().ToLower() == "phone").FirstOrDefault();
                    List<int> cityIDs = null;

                    if(wilayahAuthParam != null)
                    {
                        cityIDs = wilayahAuthParam.Value1.Split(',').Select(t => Convert.ToInt32(t)).ToList();
                    }

                    string phone = null;
                    if(phoneAuthParam != null)
                    {
                        phone = phoneAuthParam.Value1;
                    }

                    //using (var context = new lpgdevEntities())
                    //{

                    //TODO: Check Roles

                    //User exists 
                    if (context.SuperAdmins.Any(u => u.Email == result.UserPropertiesList.mEmail))
                    {

                        var Items = (from users in context.SuperAdmins
                                     join role in context.MSuperUserTypes
                                     on users.UserTypeID equals role.UserTypeID
                                     where users.Email == result.UserPropertiesList.mEmail && users.StatusID
                                     select new AdminSessionEntity
                                     {
                                         UserId = users.SAdminID,
                                         EmailId = users.Email,
                                         UserType = users.UserTypeID,
                                         accessToken = users.AccToken,
                                         FullName = users.FullName,
                                         UserRole = role.TypeName,
                                         imgProfile = (users.ProfileImage != "") ? (users.ProfileImage) : "",
                                         CityId = users.CityId != null ? users.CityId.Value : 0
                                     });

                        var user = Items.Distinct().FirstOrDefault();

                        if(user != null && cityIDs != null)
                        {
                            _service.UpdateSuperAdminCities(user.UserId, cityIDs, 1);
                        }
                        
                        return user;
                    }
                    else
                    { // Create new user if the role is correct 

                        SuperAdminModel model = new SuperAdminModel();
                        //SuperAdminService _service = new SuperAdminService();

                        model.email = result.UserPropertiesList.mEmail;
                        model.userTypeId = roleId;
                        model.full_name = result.UserPropertiesList.mFullName;
                        
                        if (result.UserPropertiesList.mTelephone != null)
                        {
                            model.mobile_number = result.UserPropertiesList.mTelephone;
                        }

                        if (phone != null)
                        {
                            model.mobile_number = phone;
                        }


                        model.statusId = true;
                        model.password = TokenGenerator.GetHashedPassword(password);

                        try
                        {
                            model = _service.SaveSuperAdminUser(model, 1);

                            if (model != null)
                            {
                                model.password = TokenGenerator.GetHashedPassword(password);
                                context.SaveChanges();
                            }
                            else
                            {
                                //User wasn't saved for some reason
                                return null;
                            }
                        }
                        catch (DbEntityValidationException e)
                        {

                            Console.Out.WriteLine(e.EntityValidationErrors.ToString());

                            telemetry.TrackException(e);
                            return null;
                        }
                        catch (Exception e)
                        {

                            telemetry.TrackException(e);
                            return null;
                        }

                        var Items = (from users in context.SuperAdmins
                                     join role in context.MSuperUserTypes
                                     on users.UserTypeID equals role.UserTypeID
                                     where users.Email == result.UserPropertiesList.mEmail && users.StatusID
                                     select new AdminSessionEntity
                                     {
                                         UserId = users.SAdminID,
                                         EmailId = users.Email,
                                         UserType = users.UserTypeID,
                                         accessToken = users.AccToken,
                                         FullName = users.FullName,
                                         UserRole = role.TypeName,
                                         imgProfile = (users.ProfileImage != "") ? (filepath + users.ProfileImage) : "",

                                     });

                        var user = Items.Distinct().FirstOrDefault();

                        if (user != null && cityIDs != null)
                        {
                            _service.UpdateSuperAdminCities(user.UserId, cityIDs, 1);
                        }

                        return user;
                    }

                    //}

                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                telemetry.TrackException(e);
                return null;
            }
        }

        private ResponseUsersData AuthenticateUserInADAsync(String login, String password)
        {

            AuthenticateUserSoapClient client = new AuthenticateUserSoapClient("AuthenticateUserSoap");//ConfigurationManager.AppSettings["PertaminaADEndPoint"].ToString());

            //var user = context.SuperAdmins.Where(x => x.MobileNum == login);
            //if(user.FirstOrDefault() != null)
            //{
            //    if (user.FirstOrDefault().UserTypeID == 1)
            //        login = user.FirstOrDefault().FullName;
            //}
          
            var companyCode = "1010";
            var applicationName = "BrightHomeService";
            var domainName = "PERTAMINA";
            var todayInIndonesia = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"), "SE Asia Standard Time").ToString("yyyy-MM-dd"); //today.ToString("yyyy-MM-dd", )
            var saltkey = "Y6qfKZ4anOKi6Cwn5Eb0mA==";

            var key = Sha256(login + saltkey);

            //var request = new AuthenticateUserADRequest()
            //{
            //    Body = new AuthenticateUserADRequestBody() {
            //        CompanyCode = companyCode,
            //        DomainName = domainName,
            //        ApplicationName = applicationName,
            //        UserName = login,
            //        Password = password,
            //        key = key
            //    }
            //};

            //LogHelper.WriteLog("Key: " + key);

            //ResponseUsersData fakeResponse = new ResponseUsersData()
            //{
            //    ResponseMessage = new ResponseMessageClass()
            //    {
            //        Message = "Got a user"
            //    },
            //    UserPropertiesList = new GeneralUserProfile()
            //    {
            //        mUserName = login,
            //        mProfileExist = true,
            //        mCompanyCode = "1010",
            //        mEmail = login + "@gmail.com",
            //        //mRoleList = new ArrayOfString() { "HQ.SuperUser" },
            //        //mRoleGroupList = new ArrayOfString() { "HQ.SuperUser" },
            //        //mRoleGroupNameList = new ArrayOfString() { "HQ.SuperUser" },
            //        mUserGroupList = new ArrayOfString() { "HQ.SuperUser" },
            //        mFullName = "Full name",
            //        mTelephone = "+62337722889",
            //        mUserId = "123",
            //        mTitle = "CTO"
            //    }
            //};

            return client.AuthenticateUserAD(companyCode, domainName, applicationName, login, password, key);
            //return fakeResponse;
        }

        private static String Sha256Hash(String value)
        {

            StringBuilder Sb = new StringBuilder();

            using (SHA256 hash = SHA256.Create())
            {

                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));

            }

            return Sb.ToString();
        }

        static string Sha256(string randomString)
        {
            SHA256Managed crypt = new SHA256Managed();
            StringBuilder hash = new StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString), 0, Encoding.UTF8.GetByteCount(randomString));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

        public string ForgotPassword(string EmailId, string new_password, string confirm_password)
        {
            string result = "";
            try
            {
                //using (var context = new lpgdevEntities())
                //{
                var Data = context.SuperAdmins.Where(r => r.Email == EmailId && r.StatusID).FirstOrDefault();
                if (Data != null)
                {
                    //if (new_password.Trim() != confirm_password.Trim())
                    //{
                    //    result = MessageSource.GetMessage("reset.mismatch");
                    //}
                    //else
                    //{
                    string password = SuperAdminService.GenerateResetPassword();

                    if (!string.IsNullOrEmpty(password))
                    {
                        Data.Password = TokenGenerator.GetHashedPassword(password, 49);
                    }
                    context.SaveChanges();
                    EmailServices.SendMail(Data.Email, Constants.EmailSubjectForgotPwd, Constants.EmailBody[0] + Data.FullName + Constants.EmailBody[1] + Data.Email + Constants.EmailBody[2] + password + Constants.EmailBody[3]);
                    result = MessageSource.GetMessage("reset.password");
                    //}
                }
                else
                {
                    result = MessageSource.GetMessage("reset.invalid");
                }
                //}
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
        public void ChangePassword(int user_id, string new_password, string email)
        {
            try
            {
                //using (var context = new lpgdevEntities())
                //{
                var Data = context.SuperAdmins.Where(r => r.SAdminID == user_id).FirstOrDefault();
                if (Data != null)
                {
                    Data.Password = TokenGenerator.GetHashedPassword(new_password, 49);
                    context.SaveChanges();
                }
                //}
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
