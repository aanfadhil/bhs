﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPG.App_Classes
{
    public class CommenExecutionStatus
    {       

        public string CommenResultMessage { get; set; }

        public bool isSessionAlive { get; set; }

        public bool ExecuteStatus { get; set; }

        public bool IsPasswordCorrect { get; set; }
    }
}
