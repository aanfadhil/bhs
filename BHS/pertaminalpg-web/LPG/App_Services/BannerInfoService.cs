﻿using LPG.App_Classes;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using Microsoft.ApplicationInsights;

namespace LPG.App_Services
{

    public class BannerInfoService : ServiceBase, IBannerInfoService
    {
        public BannerInfoService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<BannerInfoModel> ListBannerInfo()
        {
            var bannerInfoList = new List<BannerInfoModel>();
            string filepath = ConfigurationManager.AppSettings["DefaultPath_banners"].ToString();
            using (var context = new lpgdevEntities())
            {
                var data = from bannerInfo in context.PromoInfoes
                           join user in context.SuperAdmins
                           on bannerInfo.UpdatedBy equals user.SAdminID
                           orderby bannerInfo.InfoID descending, bannerInfo.CreatedDate descending
                           select new BannerInfoModel
                           {
                               BannerInfoId = bannerInfo.InfoID,
                               Caption = bannerInfo.Caption,
                               Position = bannerInfo.Position,
                               BannerImage = (bannerInfo.InfoImage != "") ? (filepath + bannerInfo.InfoImage) : "",
                               LastModifiedDate = bannerInfo.UpdatedDate,
                               Status = bannerInfo.StatusID,
                               UpdatedUser = user.FullName,
                               SyaratKetentuan = bannerInfo.SyaratKetentuan


                           };
                bannerInfoList = data.ToList();
            }

            return bannerInfoList;
        }


        //public List<BannerInfoModel> getActiveBanners()
        //{
        //    var bannerInfoList = new List<BannerInfoModel>();
        //    using (var context = new lpgdevEntities())
        //    {
        //        var data = from bannerInfo in context.PromoInfoes
        //                   where bannerInfo.InfoID == bannerInfo.InfoID

        //                   select new BannerInfoModel
        //                   {
        //                       BannerInfoId = bannerInfo.InfoID,
        //                       Caption = bannerInfo.Caption,
        //                       Position = bannerInfo.Position,
        //                       BannerImage = bannerInfo.InfoImage


        //                   };
        //        if (data.Count() > 0)
        //            bannerInfoList = data.ToList();
        //    }

        //    return bannerInfoList;
        //}


        /// <summary>
        /// To Check Available Positions
        /// </summary>
        /// <param name="InfoID"></param>
        /// <returns></returns>
        public List<SelectListItem> GetAvailablePositions(int? InfoID)
        {
            var PositionList = new List<SelectListItem>();
            using (var context = new lpgdevEntities())
            {
                if (InfoID == null)
                {
                    var data = from bannerInfo in context.PromoInfoes
                               where bannerInfo.InfoID == bannerInfo.InfoID

                               select new SelectListItem
                               {

                                   Value = bannerInfo.Position.ToString(),
                                   Text = bannerInfo.Position.ToString(),

                               };
                    if (data.Count() > 0)
                        PositionList = data.ToList();
                }
                else
                {
                    var data = from bannerInfo in context.PromoInfoes
                               where bannerInfo.InfoID != InfoID

                               select new SelectListItem
                               {

                                   Value = bannerInfo.Position.ToString(),
                                   Text = bannerInfo.Position.ToString(),

                               };
                    if (data.Count() > 0)
                        PositionList = data.ToList();
                }


            }

            return PositionList;
        }



        public BannerInfoModel saveBannerDetails(BannerInfoModel entity, string Cmd, int loggedBy)
        {

            try
            {


                using (var context = new lpgdevEntities())
                {
                    short Status = 0;
                    if (Cmd == "btnSaveAndPublish")
                    {
                        Status = 1;
                    }
                    else if (Cmd == "btnSaveUnpublish")
                    {
                        Status = 0;
                    }

                    var Data = context.PromoInfoes.Where(r => r.InfoID == entity.BannerInfoId).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.InfoID = entity.BannerInfoId;
                        Data.Caption = entity.Caption;
                        Data.InfoImage = (entity.BannerImage != "") ? Path.GetFileName(entity.BannerImage) : "";
                        Data.Position = entity.Position;
                        Data.StatusID = Status;
                        Data.SyaratKetentuan = entity.SyaratKetentuan;
                        Data.CreatedBy = base.UserId.ToShort();
                        Data.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        Data.UpdatedBy = base.UserId.ToShort();
                        Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                        context.SaveChanges();
                        if (entity.PengaturanPromo != null)
                        {
                            var promo = context.PromoVouchers.FirstOrDefault(t => t.PromoID == entity.PengaturanPromo.PromoID);
                            if (promo != null)
                            {
                                var infoes = promo.PromoInfoes;
                                foreach (var item in infoes)
                                {
                                    promo.PromoInfoes.Remove(item);
                                }

                                promo.PromoInfoes.Add(Data);

                            }
                        }

                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggedBy);
                    }
                    else
                    {
                        PromoInfo objLocal = new PromoInfo();
                        objLocal.InfoID = entity.BannerInfoId;
                        objLocal.Caption = entity.Caption;
                        objLocal.InfoImage = (entity.BannerImage != "") ? Path.GetFileName(entity.BannerImage) : "";
                        objLocal.Position = entity.Position;
                        objLocal.StatusID = Status;
                        objLocal.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        objLocal.SyaratKetentuan = entity.SyaratKetentuan;
                        objLocal.CreatedBy = base.UserId.ToShort();
                        objLocal.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        objLocal.UpdatedBy = base.UserId.ToShort();
                        context.PromoInfoes.Add(objLocal);
                        context.SaveChanges();
                        if (entity.PengaturanPromo != null)
                        {
                            var promo = context.PromoVouchers.FirstOrDefault(t => t.PromoID == entity.PengaturanPromo.PromoID);
                            if (promo != null)
                            {
                                var infoes = promo.PromoInfoes;
                                foreach (var item in infoes)
                                {
                                    promo.PromoInfoes.Remove(item);
                                }

                                promo.PromoInfoes.Add(objLocal);

                            }
                        }


                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggedBy);
                    }

                    

                }
                return entity;
            }
            catch (Exception EX)
            {
                string msg = EX.Message.ToString();
                throw;
            }

        }

        public BannerInfoModel getBannerInfoDetails(int bannerInfoId)//, LanguageEnum langEnum)
        {
            string filepath = ConfigurationManager.AppSettings["DefaultPath_banners"].ToString();
            using (var context = new lpgdevEntities())
            {
                var rtns = (from bannerInfo in context.PromoInfoes
                            where bannerInfo.InfoID == bannerInfoId
                            select new BannerInfoModel()
                            {
                                BannerInfoId = bannerInfo.InfoID,
                                Caption = bannerInfo.Caption,
                                BannerImage = (bannerInfo.InfoImage != "") ? (filepath + bannerInfo.InfoImage) : "",
                                Position = bannerInfo.Position,
                                SyaratKetentuan = bannerInfo.SyaratKetentuan,
                                PengaturanPromo = bannerInfo.PromoVoucher != null ? new PengaturanPromoModel()
                                {
                                    PromoID = bannerInfo.PromoVoucher.PromoID,
                                    Voucher = bannerInfo.PromoVoucher.Voucher,
                                    Potongan = bannerInfo.PromoVoucher.Potongan,
                                    IsActive = bannerInfo.PromoVoucher.IsActive,
                                    //LastActivity = t.UpdatedBy??t.CreatedBy + " at " + (t.UpdatedDate??t.CreatedDate).Value.ToString("DD MMM YYYY HH:mm"),
                                    Quota = bannerInfo.PromoVoucher.Quota.Value,
                                    QuotaUsed = bannerInfo.PromoVoucher.QuotaUsed,
                                    UserQuota = bannerInfo.PromoVoucher.UserQuota,
                                    Title = bannerInfo.PromoVoucher.Title
                                } : null

                            }).FirstOrDefault();
                

                return rtns;
            }

        }

        public bool deleteBannerInfo(int id, int loggedBy)
        {
            bool status = false;
            using (var context = new lpgdevEntities())
            {
                var data = context.PromoInfoes.Where(r => r.InfoID == id).FirstOrDefault();
                if (data != null)
                {
                    context.PromoInfoes.Remove(data);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", loggedBy);
                    status = true;
                }
            }
            return status;
        }
        /// <summary>
        /// ADD : Activity Log table
        /// </summary>
        /// <param name="action">Add/Update/Delete</param> 
        /// <param name="action_item">Page Name</param>
        /// <param name="loggedBy">Loggedin UserID</param>
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = "Info Promo";
            _activityService.AddActivityLog(actModel, "Info Promo");
        }

    }
}
