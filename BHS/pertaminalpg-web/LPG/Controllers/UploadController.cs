﻿using LPG.App_Services;
using LPG.Models;
using Microsoft.ApplicationInsights;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace LPG.Controllers
{

    public class UploadController : ApiController, IUploadController
    {

        private ISuperAdminService SuperAdminService;
        private IAgentBossService AgentBossService;
        private IAgentAdminService AgentAdminService;
        private IDriverService DriverService;
        private ICustomerService CustomerService;
        private TelemetryClient telemetryClient = new TelemetryClient();

        public UploadController(ISuperAdminService _SuperAdminService,
            IAgentBossService _AgentBossService,
            IAgentAdminService _AgentAdminService,
            IDriverService _DriverService,
            ICustomerService _CustomerService) : base()
        {
            SuperAdminService = _SuperAdminService;
            AgentBossService = _AgentBossService;
            AgentAdminService = _AgentAdminService;
            DriverService = _DriverService;
            CustomerService = _CustomerService;
            telemetryClient = new TelemetryClient();
        }

        public UploadController() : base()
        {
            telemetryClient = new TelemetryClient();
        }


        protected CloudBlobContainer GetCloudBlobContainer()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                    CloudConfigurationManager.GetSetting("pertaminalpg_AzureStorageConnectionString"));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("lpg-cms-static-assets");
            container.CreateIfNotExists();
            return container;
        }

        protected string UploadImgToAzure(string localPathToFile, string filename)
        {

            var container = GetCloudBlobContainer();
            var blobRef = container.GetBlockBlobReference(filename);
            using (var fileStream = System.IO.File.OpenRead(localPathToFile))
            {
                blobRef.UploadFromStream(fileStream);
            }
            return "https://pertaminalpg.blob.core.windows.net/lpg-cms-static-assets/" + filename;
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> PostProfileImage()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string _imgname = "";
            try
            {

                var httpRequest = this.Request; //System.Web.HttpContext.Current.Request;
                //if (!httpRequest.Content.IsMimeMultipartContent())
                //{
                //    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                //}

                var localFilePath = "~/extfiles/profile/";


                string root = System.Web.HttpContext.Current.Server.MapPath(localFilePath);
                Directory.CreateDirectory(root);
                var provider = new MultipartFormDataStreamProvider(root);


                await Request.Content.ReadAsMultipartAsync(provider);

                var userId = provider.FormData.Get("userId").ToInt();
                var userType = provider.FormData.Get("userType").ToInt();

                foreach (var file in provider.FileData)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = file;//httpRequest.Files[file];
                    var filename = file.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                    //var fileObj = File.ReadAllBytes(file.LocalFileName);

                    if (postedFile != null) // && postedFile.Headers.ContentLength > 0
                    {

                        int MaxContentLength = 1024 * 1024 * 50; //Size = 5 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png" };

                        //var ext = filename.Substring(filename.LastIndexOf('.'));
                        var extension = System.IO.Path.GetExtension(filename).ToLower();



                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        //else if (postedFile.Headers.ContentLength > MaxContentLength)
                        //{

                        //    var message = string.Format("Please Upload a file upto 1 mb.");

                        //    dict.Add("error", message);
                        //    return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        //}
                        else
                        {
                            string path = HttpContext.Current.Server.MapPath("~/extfiles/profile/");
                            Directory.CreateDirectory(path);
                            //int userId = 0;
                            //if (httpRequest.Form["userId"] != null)
                            //    userId = httpRequest.Form["userId"].ToInt();

                            //int userType = 0;
                            //if (httpRequest.Form["userType"] != null)
                            //    userType = httpRequest.Form["userType"].ToInt();

                            if (userId > 0 && userType > 0)
                            {
                                string apiProfileUpdate = "";
                                bool userExist = false;
                                switch ((UserType) userType)
                                {
                                    case UserType.SuperUser:
                                        apiProfileUpdate = Constants.UpdateProfilePhoto_SuperUser;
                                        path = HttpContext.Current.Server.MapPath("~/extfiles/profile/superuser/");
                                        //Directory.CreateDirectory(path);
                                        userExist = SuperAdminService.IsExists(userId);
                                        break;
                                    case UserType.AgentBoss:
                                        apiProfileUpdate = Constants.UpdateProfilePhoto_AgentBoss;
                                        path = HttpContext.Current.Server.MapPath("~/extfiles/profile/agentboss/");
                                        //Directory.CreateDirectory(path);
                                        userExist = AgentBossService.IsExists(userId);
                                        break;
                                    case UserType.AgentAdmin:
                                        apiProfileUpdate = Constants.UpdateProfilePhoto_AgentAdmin;
                                        path = HttpContext.Current.Server.MapPath("~/extfiles/profile/agentadmin/");
                                        //Directory.CreateDirectory(path);
                                        userExist = AgentAdminService.IsExists(userId);
                                        break;
                                    case UserType.Driver:
                                        apiProfileUpdate = Constants.UpdateProfilePhoto_Driver;
                                        path = HttpContext.Current.Server.MapPath("~/extfiles/profile/driver/");
                                        //Directory.CreateDirectory(path);
                                        userExist = DriverService.IsExists(userId);
                                        break;
                                    case UserType.Consumer:
                                        apiProfileUpdate = Constants.UpdateProfilePhoto_Customer;
                                        path = HttpContext.Current.Server.MapPath("~/extfiles/profile/customer/");
                                        //Directory.CreateDirectory(path);
                                        userExist = CustomerService.IsExists(userId);
                                        break;
                                }

                                if (!userExist)
                                {

                                    dict.Add("error", "Invalid User");
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                                }

                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                }
                                _imgname = string.Format("ProfileImg_{0}{1}", Guid.NewGuid().ToString(), extension);
                                var _comPath = string.Format("{0}{1}", path, _imgname);

                                //postedFile.SaveAs(_comPath);

                                var uploadedImg = UploadImgToAzure(postedFile.LocalFileName, _imgname);

                                if (!string.IsNullOrEmpty(apiProfileUpdate))
                                {
                                    var request = new ProfilePhotoChangeRequest()
                                    {
                                        user_id = userId,
                                        profile_image = uploadedImg
                                    };

                                    SaveProfileImage(apiProfileUpdate, request);
                                }
                            }
                            else
                            {

                                dict.Add("error", "userId or userType cannot be empty");
                                return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                            }
                        }
                    }
                    
                    return Request.CreateErrorResponse(HttpStatusCode.OK, "SUCCESS");
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                telemetryClient.TrackException(ex);
                LogHelper.PublishException(ex);
                ExceptionHelper.Log(ex);
                var res = string.Format("ERROR");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
        }

        [NonAction]
        private void SaveProfileImage(string resource, ProfilePhotoChangeRequest model)
        {
            try
            {
                string endPoint = Properties.Settings.Default.EndPoint;
                RestClient client = new RestClient(endPoint);
                var request = new RestRequest(resource, Method.POST);
                request.AddJsonBody(model);
                client.Execute(request);
            }
            catch (Exception ex)
            {

                telemetryClient.TrackException(ex);
            }
        }
    }
}
