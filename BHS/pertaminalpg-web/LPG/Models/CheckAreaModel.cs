﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class CheckAreaModel
    {
        public int? addr_id { get; set; }
        public int? consumer_id { get; set; }
        public string consumer_name { get; set; }
        public string mobile_number { get; set; }
        public int? consumer_addr_id { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string address { get; set; }
        public string postal_code { get; set; }
        public bool? is_default { get; set; }
        public string region { get; set; }
        public int? city_id { get; set; }
        public string more_info { get; set; }
    }

    public class CheckAreaFilter
    {
        public int? consumer_id { get; set; }
        public int region_id { get; set; }
        public bool is_diluar_jangkauan { get; set; }
    }

    public class CheckAreaViewModel
    {
        public List<RegionModel> Regions { get; set; }
        public RegionModel DefaultRegion { get; set; }
    }

}