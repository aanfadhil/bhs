﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.City
{
    class CityDto
    {
        public int city_id { get; set; }
        public string city_name { get; set; }
        public string city_code { get; set; }
    }
}
