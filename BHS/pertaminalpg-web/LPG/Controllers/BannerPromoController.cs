﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.IO;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class BannerPromoController : AbstractController<IHomeBannerServices>
    {

        SessionManagement sessionManagement;
        //HomeBannerServices serviceObject = new HomeBannerServices();

        string endPoint = Properties.Settings.Default.EndPoint;

        public BannerPromoController(IHomeBannerServices _serviceObject) : base(_serviceObject)
        {
        }

        // GET: BannerPromo
        public ActionResult AddBanner(int? id)
        {
            try
            {
                HomeBannerModel model = new HomeBannerModel();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {


                    var convid = id ?? 0;
                    if (convid > 0)
                    {

                        var vm = serviceObject.getHomeBannerDetails(convid);
                        if (vm != null)
                        {
                            return View(vm);
                        }
                        else
                        {
                            return View(model);
                        }
                    }
                    else
                    {
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { area = "Admin" });
                }

            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }
        }

        public ActionResult ListBanner()
        {
            var vm = new HomeBannerModel();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.HomeBannerList = serviceObject.ListHomeBanner();
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult AddBanner(HomeBannerModel vm, string Cmd)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }

            try
            {

                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    serviceObject.saveBannerDetails(vm, Cmd, adminSessionInfo.UserId);

                    SetFlash(FlashMessageType.Success, "Input banner promo berhasil");

                    return RedirectToAction("ListBanner", "BannerPromo");
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error");
            }
        }

        public ActionResult deletebannerPromo(int? id)
        {

            if (id != null & id > 0)
            {

                try
                {
                    sessionManagement = new SessionManagement();
                    AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                    ViewBag.Role = adminSessionInfo.UserRole;
                    if (adminSessionInfo != null)
                    {
                        serviceObject.deleteHomBanner(id.ToInt(), adminSessionInfo.UserId);

                    }

                }
                catch
                {

                }
            }
            return RedirectToAction("ListBanner", "BannerPromo");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            string _imgname = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {

                var bannerType = System.Web.HttpContext.Current.Request.Form["BannerType"];

                if (bannerType == null)
                {

                    bannerType = "1"; // Cover
                }


                var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    _imgname = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/extfiles/promotion/Promobanner_") + _imgname + _ext;
                    _imgname = "Promobanner_" + _imgname + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    if (!Directory.Exists(Path.GetDirectoryName(path)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(path));
                    }

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width > 200)
                    {
                        int width = 1000;
                        int height = 1000;

                        switch (bannerType)
                        {
                            case "1": //Cover 1000x1000

                                break;
                            case "2": // Footer - 1500x600
                                width = 1500;
                                height = 600;
                                break;

                            case "3": // Promo banner - 1000x1000
                                break;

                            default:
                                break;
                        }

                        img.Resize(width, height, true, true);
                    }
                    img.Save(_comPath);
                    // end resize
                }
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }

    }
}
