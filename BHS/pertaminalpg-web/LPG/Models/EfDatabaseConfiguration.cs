﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;

namespace Pertamina.LPG.API.Config
{
    public class EfDatabaseConfiguration : DbConfiguration
    {

        public EfDatabaseConfiguration()
        {
            SetExecutionStrategy(
            "System.Data.SqlClient",
            () => new SqlAzureExecutionStrategy(5, TimeSpan.FromSeconds(30)));
        }

    }
}
