﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IUserService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        UserRegistrtaionModel getAgencyDetails(int distributionId);
        bool IsExists(int id);
        bool IsExistsEmail(string email, int userId);
        bool IsExistsMobileNo(string mobileNo, int userId);
        List<RegionModel> ListRegion();
        UserRegistrtaionModel saveAgency(UserRegistrtaionModel entity);
        UserRegistrtaionModel saveAgentAdmin(UserRegistrtaionModel entity);
        UserRegistrtaionModel saveAgentBoss(UserRegistrtaionModel entity);
        UserRegistrtaionModel saveDistribution(UserRegistrtaionModel entity);
        UserRegistrtaionModel saveDriver(UserRegistrtaionModel entity);
    }
}