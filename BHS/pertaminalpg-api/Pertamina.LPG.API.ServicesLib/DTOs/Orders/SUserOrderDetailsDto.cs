﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Orders
{
    public class SUserOrderDetailsDto : OrderDto
    {         
        public string order_type { get; set; }        
        public string consumer_name { get; set; }
        public string consumer_mobile { get; set; }
        public string consumer_address { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
        public string more_info { get; set; }
        public string voucher { get; set; }
        public decimal? potongan_voucher { get; set; }
    }
}

