﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Product
{
    public class DetailPriceDto
    {
        public int product_id { get; set; }
        public string product_title { get; set; }
        public string product_image { get; set; }
        public int Price_refil { get; set; }
        public int price_of_the_tube { get; set; }
    }
}
