﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.Orders
{
    public class OrderDeliveryDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int DelvID { get; set; }
        public int OrdrID { get; set; }
        public int DrvrID { get; set; }
        public int AgadmID { get; set; }
        public DateTime AcceptedDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public short StatusId { get; set; }
        public int deviation { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class OrderDeliveryMapper : MapperBase<OrderDelivery, OrderDeliveryDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<OrderDelivery, OrderDeliveryDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<OrderDelivery, OrderDeliveryDTO>>) (p => new OrderDeliveryDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    DelvID = p.DelvID,
                    OrdrID = p.OrdrID,
                    DrvrID = p.DrvrID,
                    AgadmID = p.AgadmID,
                    AcceptedDate = p.AcceptedDate,
                    StartDate = p.StartDate,
                    DeliveryDate = p.DeliveryDate,
                    StatusId = p.StatusId,
                    deviation = p.deviation,
                    CreatedDate = p.CreatedDate
                }));
            }
        }

        public override void MapToModel(OrderDeliveryDTO dto, OrderDelivery model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.DelvID = dto.DelvID;
            model.OrdrID = dto.OrdrID;
            model.DrvrID = dto.DrvrID;
            model.AgadmID = dto.AgadmID;
            model.AcceptedDate = dto.AcceptedDate;
            model.StartDate = dto.StartDate;
            model.DeliveryDate = dto.DeliveryDate;
            model.StatusId = dto.StatusId;
            model.deviation = dto.deviation;
            model.CreatedDate = dto.CreatedDate;

        }
    }
}
