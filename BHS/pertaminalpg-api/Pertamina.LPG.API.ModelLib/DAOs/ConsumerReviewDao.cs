﻿using Pertamina.LPG.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pertamina.LPG.API.DAOs
{
    public class ConsumerReviewDao : DAO
    {
        public ConsumerReview Insert(ConsumerReview review)
        {
            var reviews = _context.ConsumerReviews.Where(t => t.DrvrID == review.DrvrID && t.ConsID == review.ConsID && t.OrdrID == review.OrdrID);

            foreach (var item in reviews)
            {
                _context.ConsumerReviews.Remove(item);
            }
            
            _context.ConsumerReviews.Add(review);
            _context.SaveChanges();
            return review;
        }

        public List<ConsumerReview> GetReviewByDriver(int driverId)
        {
            return _context.ConsumerReviews.Where(x => x.DrvrID == driverId).ToList();            
        }


        public List<ConsumerReviewReason> GetReviewReason()
        {
            return _context.ConsumerReviewReasons.ToList();
        }
    }
}