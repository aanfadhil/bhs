﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.Orders
{
    public class OrderAllocationLogDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long AlogID { get; set; }
        public int OrdrID { get; set; }
        public int AgadmID { get; set; }
        public int DbptID { get; set; }
        public decimal? Distance { get; set; }
        public bool TimeSlotAvailable { get; set; }
        public short AllocationStatus { get; set; }
        public short AssignmentType { get; set; }
        public short CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class OrderAllocationLogMapper : MapperBase<OrderAllocationLog, OrderAllocationLogDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<OrderAllocationLog, OrderAllocationLogDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<OrderAllocationLog, OrderAllocationLogDTO>>) (p => new OrderAllocationLogDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    AlogID = p.AlogID,
                    OrdrID = p.OrdrID,
                    AgadmID = p.AgadmID,
                    DbptID = p.DbptID,
                    Distance = p.Distance,
                    TimeSlotAvailable = p.TimeSlotAvailable,
                    AllocationStatus = p.AllocationStatus,
                    AssignmentType = p.AssignmentType,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate
                }));
            }
        }

        public override void MapToModel(OrderAllocationLogDTO dto, OrderAllocationLog model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.AlogID = dto.AlogID;
            model.OrdrID = dto.OrdrID;
            model.AgadmID = dto.AgadmID;
            model.DbptID = dto.DbptID;
            model.Distance = dto.Distance;
            model.TimeSlotAvailable = dto.TimeSlotAvailable;
            model.AllocationStatus = dto.AllocationStatus;
            model.AssignmentType = dto.AssignmentType;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;

        }
    }
}
