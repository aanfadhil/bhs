﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.DistributionPoints
{
    public class DistributionPointDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int DbptID { get; set; }
        public int AgenID { get; set; }
        public string DistributionPointName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Address { get; set; }
        public bool StatusId { get; set; }
        public short CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public short UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class DistributionPointMapper : MapperBase<DistributionPoint, DistributionPointDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<DistributionPoint, DistributionPointDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<DistributionPoint, DistributionPointDTO>>) (p => new DistributionPointDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    DbptID = p.DbptID,
                    AgenID = p.AgenID,
                    DistributionPointName = p.DistributionPointName,
                    Latitude = p.Latitude,
                    Longitude = p.Longitude,
                    Address = p.Address,
                    StatusId = p.StatusId,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate
                }));
            }
        }

        public override void MapToModel(DistributionPointDTO dto, DistributionPoint model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.DbptID = dto.DbptID;
            model.AgenID = dto.AgenID;
            model.DistributionPointName = dto.DistributionPointName;
            model.Latitude = dto.Latitude;
            model.Longitude = dto.Longitude;
            model.Address = dto.Address;
            model.StatusId = dto.StatusId;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;

        }
    }
}
