ALTER TABLE [dbo].[Order] ADD [Voucher] [varchar](100) NULL
ALTER TABLE [dbo].[Order] ADD [PotonganVoucher] [decimal](18, 3) NULL
ALTER TABLE [dbo].[Order] ADD [PromoID] [int] NULL

ALTER TABLE [dbo].[Order] ADD  CONSTRAINT [DF_Order_PotonganVoucher]  DEFAULT ((0)) FOR [PotonganVoucher]
GO

	ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_PromoVoucher] FOREIGN KEY([PromoID])
REFERENCES [dbo].[PromoVoucher] ([PromoID])
GO

ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_PromoVoucher]
GO


USE [LPG]
GO
/****** Object:  StoredProcedure [dbo].[GetOrderList]    Script Date: 8/10/2018 8:46:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetOrderList]

as
Begin

declare @OrderList table
(
[NumberOfProducts] int,
[OrdrID] int,
[CityID] int,
[CityName] varchar(255),
[InvoiceNumber] varchar(100),
[BuyingStatus] int,
[StatusID] int,
[GrandTotal] numeric(18,3),
[DeliveryDate] date,
[Name] varchar(50),
[Address] varchar(150),
[AgentAdminName] varchar(500),
[MobileNumber] varchar(50),
[SlotName] varchar(500),
[OrderDate] date,
[RegionCode] varchar(50),
[RegionName] varchar(255),
[ProductList] varchar(max),
Quantity varchar(max),
Voucher varchar(100),
PromoID int
);

insert into @OrderList
SELECT count(1) as [NumberOfProducts],
		[Extent1].[OrdrID] AS [OrdrID], 
		[Extent10].ID AS [CityId], 
		[Extent10].CityName AS [CityName],
		[Extent1].[InvoiceNumber] AS [InvoiceNumber], 
		1 AS [BuyingStatus], 
		 CAST( [Extent1].[StatusID] AS int) AS [StatusID], 
		[Extent1].[GrandTotal] AS [GrandTotal], 
		 CAST( [Extent1].[DeliveryDate] AS datetime2) AS [DeliveryDate],
		-- [Extent1].[NumberOfProducts] AS [NumberOfProducts], 
		[Extent4].[Name] AS [Name], 
		[Extent5].[Address] AS [Address], 
		--CASE WHEN ([Extent5].[RegionName] IS NULL) THEN N'tidak ada data' ELSE [Extent5].[RegionName] END AS [RegionName], 
		CASE WHEN ([Extent6].[AgentAdminName] IS NULL) THEN N'tidak ada data' ELSE [Extent6].[AgentAdminName] END AS [AgentAdminName], 
		CASE WHEN ([Extent6].[MobileNumber] IS NULL) THEN N'tidak ada data' ELSE [Extent6].[MobileNumber] END AS [MobileNumber], 
		--[Extent3].[ProductName] AS [ProductName], 
		 
		--CASE WHEN (0 <> [Extent2].[RefillQuantity]) THEN 2 WHEN (0 <> [Extent2].[Quantity]) THEN 1 ELSE 3 END AS [C7], 
		[Extent9].[SlotName] AS [SlotName], 
		[Extent1].[OrderDate] AS [OrderDate], 
		[Extent8].[RegionCode] AS [RegionCode], 
		[Extent8].[RegionName] AS [RegionName],
	 STUFF(
         (
		 SELECT DISTINCT ',' + convert (varchar,prod.productname)
		 -- + CASE WHEN ( orddet.[RefillQuantity] > 0 and orddet.[Quantity] > 0) THEN '[Refill]+[Tabung + Isi]' 
			--WHEN (orddet.[Quantity] > 0 and not exists (Select 1 from [dbo].[OrderPrdocuctExchange] ex Where ex.ordrid = ord.ordrid)) THEN '[Tabung + Isi]'
			--WHEN ( orddet.[RefillQuantity] > 0) THEN '[Refill]'
			--WHEN (orddet.[Quantity] > 0 and exists (Select 1
   --                from [dbo].[OrderPrdocuctExchange] ex
   --                Where ex.ordrid = ord.ordrid
   --               ))
		 --THEN '[Tukar Tambah]' 
		 --ELSE ''
		 --END
		 + CASE WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '[Tabung + Isi  + Refill +  Tukar Tambah]'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Refill]'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Tukar Tambah]' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '[Refill + Tukar Tambah]'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN '[Tukar Tambah]'
		 WHEN (orddet.[RefillQuantity] > 0) THEN '[Refill]'		 
		 WHEN (orddet.[Quantity] > 0) THEN '[Tabung + Isi]' ELSE '' END
          from [Order] ord
	 INNER JOIN orderdetails orddet
	 on ord.ordrid = orddet.ordrid
	 INNER JOIN [dbo].[Product] AS prod ON orddet.prodid = prod.prodid
	 LEFT OUTER JOIN [dbo].[OrderPrdocuctExchange] ordExch on ordExch.ordrid = ord.ordrid and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.ordrid = [Extent1].ordrid 
          FOR XML PATH (''))
          , 1, 1, '')  AS ProductList,
	  STUFF(
         (
		 SELECT DISTINCT ',' + 
		 CASE 
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+')'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '('+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN convert(varchar,ordExch.[ExchangeQuantity])
		 WHEN (orddet.[RefillQuantity] > 0) THEN convert(varchar,orddet.[RefillQuantity])		 
		 WHEN (orddet.[Quantity] > 0) THEN convert(varchar,orddet.[Quantity]) ELSE '0' END
          from [Order] ord
	 INNER JOIN orderdetails orddet
	 on ord.ordrid = orddet.ordrid
	 LEFT OUTER JOIN [dbo].[OrderPrdocuctExchange] ordExch on ordExch.ordrid = ord.ordrid and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.ordrid = [Extent1].ordrid 
          FOR XML PATH (''))
          , 1, 1, '')  AS Quantity,
		  [Extent1].Voucher as [Voucher],
		  [Extent1].PromoID as [PromoID]
FROM [Order] as [Extent1]
    INNER JOIN [dbo].[OrderDetails] AS [Extent2] ON [Extent1].[OrdrID] = [Extent2].[OrdrID]
    INNER JOIN [dbo].[Product] AS [Extent3] ON [Extent2].[ProdID] = [Extent3].[ProdID]
    INNER JOIN [dbo].[Consumer] AS [Extent4] ON [Extent1].[ConsID] = [Extent4].[ConsID]
    INNER JOIN [dbo].[ConsumerAddress] AS [Extent5] ON [Extent1].[AddrID] = [Extent5].[AddrID]
    LEFT OUTER JOIN [dbo].[AgentAdmin] AS [Extent6] ON [Extent1].[AgadmID] = [Extent6].[AgadmID]
    LEFT OUTER JOIN [dbo].[Agency] AS [Extent7] ON [Extent6].[AgenID] = [Extent7].[AgenID]
    LEFT OUTER JOIN [dbo].[MRegion] AS [Extent8] ON [Extent7].[RegionId] = [Extent8].[RegionID]
	LEFT OUTER JOIN [dbo].[MCity] AS [Extent10] ON [Extent10].[ID] = [Extent1].[CityId]
    INNER JOIN [dbo].[MDeliverySlot] AS [Extent9] ON [Extent1].[DeliverySlotID] = [Extent9].[SlotID]

group by
[Extent1].[OrdrID],
[Extent1].[InvoiceNumber],
[Extent1].[StatusID],
[Extent1].[GrandTotal],
[Extent1].[DeliveryDate],
[Extent1].[Voucher],
[Extent1].[PromoID],
[Extent4].[Name],
[Extent5].[Address],
[Extent5].[RegionName],
[Extent6].[AgentAdminName],
[Extent6].[MobileNumber],
[Extent10].ID,
[Extent10].CityName,
--[Extent2].[RefillQuantity],
--[Extent2].[Quantity],
[Extent9].[SlotName],
[Extent1].[OrderDate],
[Extent8].[RegionCode],
[Extent8].[RegionName]

insert into @OrderList	
SELECT count(1) as [NumberOfProducts],
		[Extent1].[TeleOrdID] AS [OrdrID],
		[Extent1].[CityId] AS [CityId], 
		[Extent9].[CityName] AS [CityName],
		[Extent1].[InvoiceNumber] AS [InvoiceNumber], 
		CASE WHEN ([Extent1].[DeliveryType] = 1) THEN 2 ELSE 3 END AS [BuyingStatus], 
		 CAST( [Extent1].[StatusID] AS int) AS [StatusID], 
		[Extent1].[GrantTotal] AS [GrandTotal], 
		 CAST( [Extent1].[DeliveryDate] AS datetime2) AS [DeliveryDate],
		-- [Extent1].[NumberOfProducts] AS [NumberOfProducts], 
		[Extent3].[CustomerName] AS [Name], 
		[Extent3].[Address] AS [Address], 
		--CASE WHEN ([Extent3].[Address] IS NULL) THEN N'tidak ada data' ELSE [Extent3].[Address] END AS [RegionName], 
		CASE WHEN ([Extent4].[AgentAdminName] IS NULL) THEN N'tidak ada data' ELSE [Extent4].[AgentAdminName] END AS [AgentAdminName], 
		CASE WHEN ([Extent4].[MobileNumber] IS NULL) THEN N'tidak ada data' ELSE [Extent4].[MobileNumber] END AS [MobileNumber], 
		--[Extent3].[ProductName] AS [ProductName], 
		 
		--CASE WHEN (0 <> [Extent2].[RefillQuantity]) THEN 2 WHEN (0 <> [Extent2].[Quantity]) THEN 1 ELSE 3 END AS [C7], 
		 [Extent8].[SlotName] AS [SlotName], 
		[Extent1].[OrderDate] AS [OrderDate], 
		[Extent6].[RegionCode] AS [RegionCode], 
		[Extent6].[RegionName] AS [RegionName],
		

   STUFF(
         (
		 SELECT DISTINCT ',' + convert (varchar,prod.productname)
		 -- + CASE WHEN ( orddet.[RefillQuantity] > 0 and orddet.[Quantity] > 0) THEN '[Refill]+[Tabung + Isi]' 
		 --WHEN (orddet.[Quantity] > 0 and not exists (Select 1
   --                from [dbo].[TeleOrderPrdocuctExchange] ex
   --                Where ex.[TeleOrdID] = ord.[TeleOrdID]
   --               )) THEN '[Tabung + Isi]'
			--WHEN ( orddet.[RefillQuantity] > 0) THEN '[Refill]'
			--WHEN (orddet.[Quantity] > 0 and exists (Select 1
   --                from [dbo].[TeleOrderPrdocuctExchange] ex
   --                Where ex.[TeleOrdID] = ord.[TeleOrdID]
   --               ))
		 --THEN '[Tukar Tambah]' 
		 --ELSE ''
		 --END
		 + CASE WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '[Tabung + Isi  + Refill +  Tukar Tambah]'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Refill]'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Tukar Tambah]' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '[Refill + Tukar Tambah]'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN '[Tukar Tambah]'
		 WHEN (orddet.[RefillQuantity] > 0) THEN '[Refill]'		 
		 WHEN (orddet.[Quantity] > 0) THEN '[Tabung + Isi]' ELSE '' END
          from [TeleOrder] ord
	 INNER JOIN [TeleOrderDetails] orddet
	 on ord.[TeleOrdID] = orddet.[TeleOrdID]
	 INNER JOIN [dbo].[Product] AS prod ON orddet.prodid = prod.prodid
	 LEFT OUTER JOIN [dbo].[TeleOrderPrdocuctExchange] ordExch on ordExch.[TeleOrdID] = ord.[TeleOrdID] and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.[TeleOrdID] = [Extent1].[TeleOrdID] 
          FOR XML PATH (''))
          , 1, 1, '')  AS ProductList,

	  STUFF(
         (
		 SELECT DISTINCT ',' + 
		 CASE 
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+')'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '('+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN convert(varchar,ordExch.[ExchangeQuantity])
		 WHEN (orddet.[RefillQuantity] > 0) THEN convert(varchar,orddet.[RefillQuantity])		 
		 WHEN (orddet.[Quantity] > 0) THEN convert(varchar,orddet.[Quantity]) ELSE '0' END
          from [TeleOrder] ord
	 INNER JOIN [TeleOrderDetails] orddet
	 on ord.[TeleOrdID] = orddet.[TeleOrdID]
	 LEFT OUTER JOIN [dbo].[TeleOrderPrdocuctExchange] ordExch on ordExch.[TeleOrdID] = ord.[TeleOrdID] and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.[TeleOrdID] = [Extent1].[TeleOrdID] 
          FOR XML PATH (''))
          , 1, 1, '')  AS Quantity,
		  null as Voucher,
		  null as PromoID
FROM [dbo].[TeleOrder] AS [Extent1]
    INNER JOIN [dbo].[TeleOrderDetails] AS [Extent2] ON [Extent1].[TeleOrdID] = [Extent2].[TeleOrdID]
	LEFT OUTER JOIN [dbo].[TeleCustomer] AS [Extent3] ON [Extent1].[TeleOrdID] = [Extent3].[TeleOrdID]
    INNER JOIN [dbo].[AgentAdmin] AS [Extent4] ON [Extent1].[AgadmID] = [Extent4].[AgadmID]
    INNER JOIN [dbo].[Agency] AS [Extent5] ON [Extent4].[AgenID] = [Extent5].[AgenID]
    LEFT OUTER JOIN [dbo].[MRegion] AS [Extent6] ON [Extent5].[RegionId] = [Extent6].[RegionID]
    INNER JOIN [dbo].[Product] AS [Extent7] ON [Extent2].[ProdID] = [Extent7].[ProdID]
    LEFT OUTER JOIN [dbo].[MDeliverySlot] AS [Extent8] ON [Extent1].[DeliverySlotID] = [Extent8].[SlotID]
	LEFT OUTER JOIN [dbo].[MCity] AS [Extent9] ON [Extent1].[CityId] = [Extent9].[ID]

group by
[Extent1].[TeleOrdID],
[Extent1].[InvoiceNumber],
[Extent1].[DeliveryType],
[Extent1].[StatusID],
[Extent1].[GrantTotal],
[Extent1].[DeliveryDate],
[Extent3].[CustomerName],
[Extent3].[Address],
[Extent4].[AgentAdminName],
[Extent4].[MobileNumber],
--[Extent2].[RefillQuantity],
--[Extent2].[Quantity],
[Extent8].[SlotName],
[Extent1].[OrderDate],
[Extent6].[RegionCode],
[Extent6].[RegionName],
[Extent1].CityId,
[Extent9].CityName


End


/****** Object:  Table [dbo].[SACity]    Script Date: 8/11/2018 12:48:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SACity](
	[SECityId] [int] IDENTITY(1,1) NOT NULL,
	[SAdminID] [smallint] NULL,
	[CityID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_SACity] PRIMARY KEY CLUSTERED 
(
	[SECityId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SACity] ADD  CONSTRAINT [DF_SACity_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[SACity]  WITH CHECK ADD  CONSTRAINT [FK_SACity_MCity] FOREIGN KEY([SECityId])
REFERENCES [dbo].[MCity] ([ID])
GO

ALTER TABLE [dbo].[SACity] CHECK CONSTRAINT [FK_SACity_MCity]
GO

ALTER TABLE [dbo].[SACity]  WITH CHECK ADD  CONSTRAINT [FK_SACity_SuperAdmin] FOREIGN KEY([SAdminID])
REFERENCES [dbo].[SuperAdmin] ([SAdminID])
GO

ALTER TABLE [dbo].[SACity] CHECK CONSTRAINT [FK_SACity_SuperAdmin]
GO

/****** Object:  StoredProcedure [dbo].[GetNotificationHistoryCount]    Script Date: 8/24/2018 8:14:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetNotificationHistoryCount] --[GetNotificationHistoryCount] 10,1,''
@PageSize int = null,
@PageNumber int,
@Search varchar(max) = null
as
Begin

	if @PageSize = null 
	begin
		set @PageSize =  10000
		--(SELECT top 3000 count(*)
  --    FROM NotificationLog ph
	 -- where Failure <= 0)

	  set @PageNumber = 1

	end

	  select 
	 -- (SELECT top 3000 Count(ph) 
  --    FROM NotificationLog ph
	 -- where Failure <= 0
	 -- and
	 -- (
		--ph.UserName like '%'+@Search+'%'
		--or
		--ph.MobileNumber like '%'+@Search+'%'
		--or
		--ph.Message like '%' + @Search + '%'
		--or
		--ph.Title like '%' + @Search + '%'
	 -- )) 
	 10000
	  as Filtered
	  ,
	  --(SELECT top 3000 Count(*) 
   --   FROM NotificationLog ph
	  --where Failure <= 0
	  --)
	  10000 
	  as UnFiltered

End

GO
/****** Object:  StoredProcedure [dbo].[GetNotificationHistory]    Script Date: 8/24/2018 8:13:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[GetNotificationHistory]    Script Date: 8/14/2018 8:40:43 AM ******/
ALTER PROCEDURE [dbo].[GetNotificationHistory] --[GetNotificationHistory] 10,1,''
@PageSize int = null,
@PageNumber int,
@Search varchar(max) = null
as
Begin

	--select count(*) from NotificationLog
	--select count(*) from NotificationLog where FCMResponse not like ('%"success":0,%')

	if @PageSize is null 
	begin
		set @PageSize =  (SELECT count(*)
      FROM NotificationLog ph
	  where Failure <= 0)

	  set @PageNumber = 1

	end


  ;WITH pf
  as(
   SELECT top 3000 ph.LogId
      FROM NotificationLog ph
	  where isnull(ph.Failure,0) <= 0
  ORDER BY (
		ph.LogId
	  ) desc
	  , ph.CreatedDate desc
	  ),
  pg AS
  (
    SELECT ph.LogId
      FROM pf inner join NotificationLog ph on ph.LogId = pf.LogId
	  where isnull(ph.Failure,0) <= 0
	  and
	  (
		ph.UserName like '%'+@Search+'%'
		or
		ph.MobileNumber like '%'+@Search+'%'
		or
		ph.Title like '%' + @Search + '%'
		or
		ph.Message like '%' + @Search + '%'
	  )
      ORDER BY (
		ph.LogId
	  ) desc
	  , ph.CreatedDate desc
      OFFSET @PageSize * (@PageNumber - 1) ROWS
      FETCH NEXT @PageSize ROWS ONLY
  )
  SELECT 
	pg.LogId as LogId,
	ph.Title as Title,
	ph.Message as Message,
	ph.MobileNumber as MobileNumber,
	ph.UserID as UserID,
	ph.UserName as UserName,
	ph.UserRole as UserRole,
	ph.CreatedDate as CreatedDate
  FROM pg 
  inner join NotificationLog ph on pg.LogId = ph.LogId
	 ORDER BY (
		ph.LogId
	  ) desc
	  , ph.CreatedDate desc

	  --SELECT count(*)
   --   FROM TblT_MaterialPlanningHeader ph
	  ----inner join TblM_ProjectTeamMember ptm on ph.WBS_Lv1 = ptm.ProjectCodeLv1 and convert(nvarchar(100),ptm.Nopek) = @Nopek
	  --inner join TblM_Rig ptm on ph.RigId = ptm.RigId and ptm.AdminProject = @username
	  --where ReqStatus in (7,14)
	
	


End

GO

/****** Object:  StoredProcedure [dbo].[GetNotificationHistoryPaging]    Script Date: 8/24/2018 8:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[GetNotificationHistory]    Script Date: 8/14/2018 8:40:43 AM ******/
ALTER PROCEDURE [dbo].[GetNotificationHistoryPaging] --[GetNotificationHistoryPaging] 10,1,''
@PageSize int = null,
@PageNumber int,
@Search varchar(max) = null
as
Begin

	--select count(*) from NotificationLog
	--select count(*) from NotificationLog where FCMResponse not like ('%"success":0,%')

	if @PageSize is null 
	begin
		set @PageSize =  10000
		--(SELECT count(*)
  --    FROM NotificationLog ph
	 -- where Failure <= 0)

	  set @PageNumber = 1

	end


  ;WITH pf
  as(
   SELECT top 10000 ph.LogId
      FROM NotificationLog ph
	  --where isnull(ph.Failure,0) <= 0
  ORDER BY (
		ph.LogId
	  ) desc
	  , ph.CreatedDate desc
	  ),
  pg AS
  (
    SELECT ph.LogId
      FROM NotificationLog ph
	  where --isnull(ph.Failure,0) <= 0
	  --and
	  (
		ph.UserName like '%'+@Search+'%'
		or
		ph.MobileNumber like '%'+@Search+'%'
		or
		ph.Title like '%' + @Search + '%'
		or
		ph.Message like '%' + @Search + '%'
	  )
      ORDER BY (
		ph.LogId
	  ) desc
	  , ph.CreatedDate desc
      OFFSET @PageSize * (@PageNumber - 1) ROWS
      FETCH NEXT @PageSize ROWS ONLY
  )
  SELECT 
	pg.LogId as LogId,
	ph.Title as Title,
	ph.Message as Message,
	ph.MobileNumber as MobileNumber,
	ph.UserID as UserID,
	ph.UserName as UserName,
	ph.UserRole as UserRole,
	ph.CreatedDate as CreatedDate,
	ph.IsRead as IsRead,
	ph.ReadAt as ReadAt,
	ph.Success as Success
  FROM pg 
  inner join NotificationLog ph on pg.LogId = ph.LogId
	 ORDER BY (
		ph.LogId
	  ) desc
	  , ph.CreatedDate desc

	  --SELECT count(*)
   --   FROM TblT_MaterialPlanningHeader ph
	  ----inner join TblM_ProjectTeamMember ptm on ph.WBS_Lv1 = ptm.ProjectCodeLv1 and convert(nvarchar(100),ptm.Nopek) = @Nopek
	  --inner join TblM_Rig ptm on ph.RigId = ptm.RigId and ptm.AdminProject = @username
	  --where ReqStatus in (7,14)
	
	


End
GO

/****** Object:  StoredProcedure [dbo].[GetOrderListPaging]    Script Date: 8/24/2018 8:23:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO



ALTER PROCEDURE [dbo].[GetOrderListPaging] --[GetOrderListPaging] 10,1,'boy pan','nama','88,86,87'
@PageSize int,
@PageNumber int,
@Search varchar(500),
@SearchType varchar(300) = '',
@Cities varchar(500)
as
Begin

--search type
--nama
--invoice
--wilayah
--kota
--voucher

declare @OrderList table
(
[NumberOfProducts] int,
[OrdrID] int,
[CityID] int,
[CityName] varchar(255),
[InvoiceNumber] varchar(100),
[BuyingStatus] int,
[StatusID] int,
[GrandTotal] numeric(18,3),
[DeliveryDate] date,
[Name] varchar(50),
[Address] varchar(150),
[AgentAdminName] varchar(500),
[MobileNumber] varchar(50),
[SlotName] varchar(500),
[OrderDate] date,
[RegionCode] varchar(50),
[RegionName] varchar(255),
[ProductList] varchar(max),
Quantity varchar(max),
Voucher varchar(100),
PromoID int,
Agen varchar(max),
AgenID int,
Driver varchar(max),
DriverID int,
Rating int,
[sort] int
);

declare @OrderPage table
(
	[ID] int,
	[BuyingStatus] int,
	[CreatedDate] datetime
)



insert into @OrderPage
select Extent1.OrdrID, 1 , Extent1.CreatedDate
from [Order] as [Extent1]
INNER JOIN [dbo].[Consumer] AS [Extent4] ON [Extent1].[ConsID] = [Extent4].[ConsID]
LEFT OUTER JOIN [dbo].[AgentAdmin] AS [Extent6] ON [Extent1].[AgadmID] = [Extent6].[AgadmID]
LEFT OUTER JOIN [dbo].[Agency] AS [Extent7] ON [Extent6].[AgenID] = [Extent7].[AgenID]
LEFT OUTER JOIN [dbo].[MRegion] AS [Extent8] ON [Extent7].[RegionId] = [Extent8].[RegionID]
LEFT OUTER JOIN [dbo].[MCity] AS [Extent10] ON [Extent10].[ID] = [Extent1].[CityId]
where 1=1
and
(
	(@Cities is not null) and exists(select * from dbo.SplitString(@Cities,',') where ltrim(rtrim(Item)) = CONVERT(varchar(20),Extent1.CityId))
	or
	(@Cities is null)
)
and
(
	((@SearchType = 'nama') and (Extent4.Name like '%'+@Search+'%'))
	or
	((@SearchType = 'invoice') and (Extent1.InvoiceNumber like '%'+@Search+'%'))
	or
	((@SearchType = 'wilayah') and (Extent8.RegionName like '%'+@Search+'%'))
	or
	((@SearchType = 'kota') and (Extent10.CityName like '%'+@Search+'%'))
	or
	((@SearchType = 'voucher') and (Extent1.Voucher like '%'+@Search+'%'))
	or
	(@SearchType = '')
);

insert into @OrderPage
select Extent1.TeleOrdID, 
CASE WHEN ([Extent1].[DeliveryType] = 1) THEN 2 ELSE 3 END, 
Extent1.CreatedDate
from [dbo].[TeleOrder] AS [Extent1]
LEFT OUTER JOIN [dbo].[TeleCustomer] AS [Extent3] ON [Extent1].[TeleOrdID] = [Extent3].[TeleOrdID]
INNER JOIN [dbo].[AgentAdmin] AS [Extent4] ON [Extent1].[AgadmID] = [Extent4].[AgadmID]
INNER JOIN [dbo].[Agency] AS [Extent5] ON [Extent4].[AgenID] = [Extent5].[AgenID]
LEFT OUTER JOIN [dbo].[MRegion] AS [Extent6] ON [Extent5].[RegionId] = [Extent6].[RegionID]
LEFT OUTER JOIN [dbo].[MCity] AS [Extent9] ON [Extent1].[CityId] = [Extent9].[ID]
where 1=1
and
(
	(@Cities is not null) and exists(select * from dbo.SplitString(@Cities,',') where ltrim(rtrim(Item)) = CONVERT(varchar(20),Extent1.CityId))
	or
	(@Cities is null)
)
and
(
	((@SearchType = 'nama') and ([Extent3].CustomerName like '%'+@Search+'%'))
	or
	((@SearchType = 'invoice') and (Extent1.InvoiceNumber like '%'+@Search+'%'))
	or
	((@SearchType = 'wilayah') and ([Extent6].RegionName like '%'+@Search+'%'))
	or
	((@SearchType = 'kota') and ([Extent9].CityName like '%'+@Search+'%'))
	--or
	--((@SearchType = 'voucher') and (Extent1.Voucher like '%'+@Search+'%'))
	or
	(@SearchType = '')
)

if @PageSize is null 
begin
	set @PageSize =  (select count(*) from @OrderPage)

	set @PageNumber = 1

end

;with pg as
(
	select 
	ID,BuyingStatus, ROW_NUMBER() OVER (order by CreatedDate desc, ID desc) AS RowNum
	from @OrderPage 
	order by CreatedDate desc, ID desc
	OFFSET @PageSize * (@PageNumber - 1) ROWS
    FETCH NEXT @PageSize ROWS ONLY
)
insert into @OrderList
SELECT count(1) as [NumberOfProducts],
		[Extent1].[OrdrID] AS [OrdrID], 
		[Extent10].ID AS [CityId], 
		[Extent10].CityName AS [CityName],
		[Extent1].[InvoiceNumber] AS [InvoiceNumber], 
		1 AS [BuyingStatus], 
		 CAST( [Extent1].[StatusID] AS int) AS [StatusID], 
		[Extent1].[GrandTotal] AS [GrandTotal], 
		 CAST( [Extent1].[DeliveryDate] AS datetime2) AS [DeliveryDate],
		-- [Extent1].[NumberOfProducts] AS [NumberOfProducts], 
		[Extent4].[Name] AS [Name], 
		[Extent5].[Address] AS [Address], 
		--CASE WHEN ([Extent5].[RegionName] IS NULL) THEN N'tidak ada data' ELSE [Extent5].[RegionName] END AS [RegionName], 
		CASE WHEN ([Extent6].[AgentAdminName] IS NULL) THEN N'tidak ada data' ELSE [Extent6].[AgentAdminName] END AS [AgentAdminName], 
		CASE WHEN ([Extent6].[MobileNumber] IS NULL) THEN N'tidak ada data' ELSE [Extent6].[MobileNumber] END AS [MobileNumber], 
		--[Extent3].[ProductName] AS [ProductName], 
		 
		--CASE WHEN (0 <> [Extent2].[RefillQuantity]) THEN 2 WHEN (0 <> [Extent2].[Quantity]) THEN 1 ELSE 3 END AS [C7], 
		[Extent9].[SlotName] AS [SlotName], 
		[Extent1].[OrderDate] AS [OrderDate], 
		[Extent8].[RegionCode] AS [RegionCode], 
		[Extent8].[RegionName] AS [RegionName],
	 STUFF(
         (
		 SELECT DISTINCT ',' + convert (varchar,prod.productname)
		 -- + CASE WHEN ( orddet.[RefillQuantity] > 0 and orddet.[Quantity] > 0) THEN '[Refill]+[Tabung + Isi]' 
			--WHEN (orddet.[Quantity] > 0 and not exists (Select 1 from [dbo].[OrderPrdocuctExchange] ex Where ex.ordrid = ord.ordrid)) THEN '[Tabung + Isi]'
			--WHEN ( orddet.[RefillQuantity] > 0) THEN '[Refill]'
			--WHEN (orddet.[Quantity] > 0 and exists (Select 1
   --                from [dbo].[OrderPrdocuctExchange] ex
   --                Where ex.ordrid = ord.ordrid
   --               ))
		 --THEN '[Tukar Tambah]' 
		 --ELSE ''
		 --END
		 + CASE WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '[Tabung + Isi  + Refill +  Tukar Tambah]'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Refill]'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Tukar Tambah]' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '[Refill + Tukar Tambah]'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN '[Tukar Tambah]'
		 WHEN (orddet.[RefillQuantity] > 0) THEN '[Refill]'		 
		 WHEN (orddet.[Quantity] > 0) THEN '[Tabung + Isi]' ELSE '' END
          from [Order] ord
	 INNER JOIN orderdetails orddet
	 on ord.ordrid = orddet.ordrid
	 INNER JOIN [dbo].[Product] AS prod ON orddet.prodid = prod.prodid
	 LEFT OUTER JOIN [dbo].[OrderPrdocuctExchange] ordExch on ordExch.ordrid = ord.ordrid and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.ordrid = [Extent1].ordrid 
          FOR XML PATH (''))
          , 1, 1, '')  AS ProductList,
	  STUFF(
         (
		 SELECT DISTINCT ',' + 
		 CASE 
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+')'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '('+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN convert(varchar,ordExch.[ExchangeQuantity])
		 WHEN (orddet.[RefillQuantity] > 0) THEN convert(varchar,orddet.[RefillQuantity])		 
		 WHEN (orddet.[Quantity] > 0) THEN convert(varchar,orddet.[Quantity]) ELSE '0' END
          from [Order] ord
	 INNER JOIN orderdetails orddet
	 on ord.ordrid = orddet.ordrid
	 LEFT OUTER JOIN [dbo].[OrderPrdocuctExchange] ordExch on ordExch.ordrid = ord.ordrid and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.ordrid = [Extent1].ordrid 
          FOR XML PATH (''))
          , 1, 1, '')  AS Quantity,
		  [Extent1].Voucher as [Voucher],
		  [Extent1].PromoID as [PromoID],
		  [Extent7].AgencyName as [Agen],
		  [Extent7].AgenID as [AgenID],
		  NULL AS Driver,
		  NULL as DriverID,
		  NULL as Rating,
		  pg.RowNum
FROM 
	pg
	INNER JOIN [Order] as [Extent1] on Extent1.OrdrID = pg.ID and pg.BuyingStatus = 1
    INNER JOIN [dbo].[OrderDetails] AS [Extent2] ON [Extent1].[OrdrID] = [Extent2].[OrdrID]
    INNER JOIN [dbo].[Product] AS [Extent3] ON [Extent2].[ProdID] = [Extent3].[ProdID]
    INNER JOIN [dbo].[Consumer] AS [Extent4] ON [Extent1].[ConsID] = [Extent4].[ConsID]
    INNER JOIN [dbo].[ConsumerAddress] AS [Extent5] ON [Extent1].[AddrID] = [Extent5].[AddrID]
    LEFT OUTER JOIN [dbo].[AgentAdmin] AS [Extent6] ON [Extent1].[AgadmID] = [Extent6].[AgadmID]
    LEFT OUTER JOIN [dbo].[Agency] AS [Extent7] ON [Extent6].[AgenID] = [Extent7].[AgenID]
    LEFT OUTER JOIN [dbo].[MRegion] AS [Extent8] ON [Extent7].[RegionId] = [Extent8].[RegionID]
	LEFT OUTER JOIN [dbo].[MCity] AS [Extent10] ON [Extent10].[ID] = [Extent1].[CityId]
    INNER JOIN [dbo].[MDeliverySlot] AS [Extent9] ON [Extent1].[DeliverySlotID] = [Extent9].[SlotID]
	
group by
[Extent1].[OrdrID],
[Extent1].[InvoiceNumber],
[Extent1].[StatusID],
[Extent1].[GrandTotal],
[Extent1].[DeliveryDate],
[Extent1].[Voucher],
[Extent1].[PromoID],
[Extent4].[Name],
[Extent5].[Address],
[Extent5].[RegionName],
[Extent6].[AgentAdminName],
[Extent6].[MobileNumber],
[Extent10].ID,
[Extent10].CityName,
--[Extent2].[RefillQuantity],
--[Extent2].[Quantity],
[Extent9].[SlotName],
[Extent1].[OrderDate],
[Extent8].[RegionCode],
[Extent8].[RegionName],
[Extent7].AgencyName,
[Extent7].AgenID,
pg.RowNum

;with pg as
(
	select 
	ID,BuyingStatus,ROW_NUMBER() OVER (order by CreatedDate desc, ID desc) AS RowNum
	from @OrderPage 
	order by CreatedDate desc, ID desc
	OFFSET @PageSize * (@PageNumber - 1) ROWS
    FETCH NEXT @PageSize ROWS ONLY
)
insert into @OrderList	
SELECT count(1) as [NumberOfProducts],
		[Extent1].[TeleOrdID] AS [OrdrID],
		[Extent1].[CityId] AS [CityId], 
		[Extent9].[CityName] AS [CityName],
		[Extent1].[InvoiceNumber] AS [InvoiceNumber], 
		CASE WHEN ([Extent1].[DeliveryType] = 1) THEN 2 ELSE 3 END AS [BuyingStatus], 
		 CAST( [Extent1].[StatusID] AS int) AS [StatusID], 
		[Extent1].[GrantTotal] AS [GrandTotal], 
		 CAST( [Extent1].[DeliveryDate] AS datetime2) AS [DeliveryDate],
		-- [Extent1].[NumberOfProducts] AS [NumberOfProducts], 
		[Extent3].[CustomerName] AS [Name], 
		[Extent3].[Address] AS [Address], 
		--CASE WHEN ([Extent3].[Address] IS NULL) THEN N'tidak ada data' ELSE [Extent3].[Address] END AS [RegionName], 
		CASE WHEN ([Extent4].[AgentAdminName] IS NULL) THEN N'tidak ada data' ELSE [Extent4].[AgentAdminName] END AS [AgentAdminName], 
		CASE WHEN ([Extent4].[MobileNumber] IS NULL) THEN N'tidak ada data' ELSE [Extent4].[MobileNumber] END AS [MobileNumber], 
		--[Extent3].[ProductName] AS [ProductName], 
		 
		--CASE WHEN (0 <> [Extent2].[RefillQuantity]) THEN 2 WHEN (0 <> [Extent2].[Quantity]) THEN 1 ELSE 3 END AS [C7], 
		 [Extent8].[SlotName] AS [SlotName], 
		[Extent1].[OrderDate] AS [OrderDate], 
		[Extent6].[RegionCode] AS [RegionCode], 
		[Extent6].[RegionName] AS [RegionName],
		

   STUFF(
         (
		 SELECT DISTINCT ',' + convert (varchar,prod.productname)
		 -- + CASE WHEN ( orddet.[RefillQuantity] > 0 and orddet.[Quantity] > 0) THEN '[Refill]+[Tabung + Isi]' 
		 --WHEN (orddet.[Quantity] > 0 and not exists (Select 1
   --                from [dbo].[TeleOrderPrdocuctExchange] ex
   --                Where ex.[TeleOrdID] = ord.[TeleOrdID]
   --               )) THEN '[Tabung + Isi]'
			--WHEN ( orddet.[RefillQuantity] > 0) THEN '[Refill]'
			--WHEN (orddet.[Quantity] > 0 and exists (Select 1
   --                from [dbo].[TeleOrderPrdocuctExchange] ex
   --                Where ex.[TeleOrdID] = ord.[TeleOrdID]
   --               ))
		 --THEN '[Tukar Tambah]' 
		 --ELSE ''
		 --END
		 + CASE WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '[Tabung + Isi  + Refill +  Tukar Tambah]'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Refill]'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Tukar Tambah]' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '[Refill + Tukar Tambah]'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN '[Tukar Tambah]'
		 WHEN (orddet.[RefillQuantity] > 0) THEN '[Refill]'		 
		 WHEN (orddet.[Quantity] > 0) THEN '[Tabung + Isi]' ELSE '' END
          from [TeleOrder] ord
	 INNER JOIN [TeleOrderDetails] orddet
	 on ord.[TeleOrdID] = orddet.[TeleOrdID]
	 INNER JOIN [dbo].[Product] AS prod ON orddet.prodid = prod.prodid
	 LEFT OUTER JOIN [dbo].[TeleOrderPrdocuctExchange] ordExch on ordExch.[TeleOrdID] = ord.[TeleOrdID] and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.[TeleOrdID] = [Extent1].[TeleOrdID] 
          FOR XML PATH (''))
          , 1, 1, '')  AS ProductList,

	  STUFF(
         (
		 SELECT DISTINCT ',' + 
		 CASE 
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+')'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '('+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN convert(varchar,ordExch.[ExchangeQuantity])
		 WHEN (orddet.[RefillQuantity] > 0) THEN convert(varchar,orddet.[RefillQuantity])		 
		 WHEN (orddet.[Quantity] > 0) THEN convert(varchar,orddet.[Quantity]) ELSE '0' END
          from [TeleOrder] ord
	 INNER JOIN [TeleOrderDetails] orddet
	 on ord.[TeleOrdID] = orddet.[TeleOrdID]
	 LEFT OUTER JOIN [dbo].[TeleOrderPrdocuctExchange] ordExch on ordExch.[TeleOrdID] = ord.[TeleOrdID] and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.[TeleOrdID] = [Extent1].[TeleOrdID] 
          FOR XML PATH (''))
          , 1, 1, '')  AS Quantity,
		  null as Voucher,
		  null as PromoID,
		  [Extent5].AgencyName as [Agen],
		  [Extent5].AgenID as [AgenID],
		  NULL AS Driver,
		  NULL as DriverID,
		  NULL as Rating,
		  pg.RowNum
FROM 
	pg
	INNER JOIN [dbo].[TeleOrder] AS [Extent1] on Extent1.TeleOrdID = pg.ID and pg.BuyingStatus <> 1
    INNER JOIN [dbo].[TeleOrderDetails] AS [Extent2] ON [Extent1].[TeleOrdID] = [Extent2].[TeleOrdID]
	LEFT OUTER JOIN [dbo].[TeleCustomer] AS [Extent3] ON [Extent1].[TeleOrdID] = [Extent3].[TeleOrdID]
    INNER JOIN [dbo].[AgentAdmin] AS [Extent4] ON [Extent1].[AgadmID] = [Extent4].[AgadmID]
    INNER JOIN [dbo].[Agency] AS [Extent5] ON [Extent4].[AgenID] = [Extent5].[AgenID]
    LEFT OUTER JOIN [dbo].[MRegion] AS [Extent6] ON [Extent5].[RegionId] = [Extent6].[RegionID]
    INNER JOIN [dbo].[Product] AS [Extent7] ON [Extent2].[ProdID] = [Extent7].[ProdID]
    LEFT OUTER JOIN [dbo].[MDeliverySlot] AS [Extent8] ON [Extent1].[DeliverySlotID] = [Extent8].[SlotID]
	LEFT OUTER JOIN [dbo].[MCity] AS [Extent9] ON [Extent1].[CityId] = [Extent9].[ID]

group by
[Extent1].[TeleOrdID],
[Extent1].[InvoiceNumber],
[Extent1].[DeliveryType],
[Extent1].[StatusID],
[Extent1].[GrantTotal],
[Extent1].[DeliveryDate],
[Extent3].[CustomerName],
[Extent3].[Address],
[Extent4].[AgentAdminName],
[Extent4].[MobileNumber],
--[Extent2].[RefillQuantity],
--[Extent2].[Quantity],
[Extent8].[SlotName],
[Extent1].[OrderDate],
[Extent6].[RegionCode],
[Extent6].[RegionName],
[Extent1].CityId,
[Extent9].CityName,
[Extent5].AgencyName,
[Extent5].AgenID,
pg.RowNum

declare @count int = (select count(*) from @OrderPage)


select *,@count as Total from @OrderList order by sort


End
GO

/****** Object:  StoredProcedure [dbo].[GetCustomerPaging]    Script Date: 8/24/2018 8:23:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetCustomerPaging] --[GetCustomerPaging] 10,503,''
@PageSize int = null,
@PageNumber int,
@Search varchar(max) = null
as
Begin

	--select count(*) from NotificationLog
	--select count(*) from NotificationLog where FCMResponse not like ('%"success":0,%')

	if @PageSize is null 
	begin
		set @PageSize =  (SELECT count(*)
      FROM Consumer ph
	  where StatusID = 1)

	  set @PageNumber = 1

	end

	declare @count int = (select count(*) from Consumer where StatusID = 1)

	set @Search = ltrim(rtrim(@Search))

   SELECT ph.ConsID,
   ph.Name,
   ca.Address,
   ca.RegionName,
   ca.PostalCode,
   ph.PhoneNumber,
   @count as Total
      FROM Consumer ph
	  left join ConsumerAddress ca on ph.ConsID = ca.ConsID and ca.IsDefault = 1
	  where 
	  ph.StatusID = 1
	  and
		(
			ph.Name like '%'+@Search+'%' 
			or ca.RegionName like '%'+@Search+'%' 
			or ca.Address like '%'+@Search+'%' 
			or ca.PostalCode like '%'+@Search+'%' 
			or ph.PhoneNumber like '%'+@Search+'%'
		)
  ORDER BY (
		ph.ConsID
	  ) desc
	  OFFSET @PageSize * (@PageNumber - 1) ROWS
      FETCH NEXT @PageSize ROWS ONLY
	  


End
GO

/****** Object:  StoredProcedure [dbo].[GetIssuesListForSUser]    Script Date: 8/24/2018 8:23:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[GetIssuesListForSUser]
AS
BEGIN 
select ord.[OrdrID],ord.[InvoiceNumber],ord.[OrderDate],ord.[OrderTime],cons.[Name],cons.[PhoneNumber],ord.[GrandTotal],addr.[Address]
from [dbo].[Order] as ord
INNER JOIN [OrderAllocationLog] OAL ON ord.OrdrID = OAL.OrdrID
left outer join [dbo].[Consumer] as cons on cons.[ConsID]=ord.[ConsID]
left outer join [dbo].[ConsumerAddress] as Addr on addr.[AddrID]=ord.[AddrID]
where ord.[StatusID]=1 AND OAL.AssignmentType = 2 AND OAL.CreatedDate < DATEADD (mi , (-5) , getdate())
END
GO

/****** Object:  StoredProcedure [dbo].[GetIssuesListBySUser]    Script Date: 8/24/2018 8:24:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[GetIssuesListBySUser] --[GetIssuesListBySUser] 157
@seId int
AS
BEGIN 

declare @cities table (
	cityId int
)

insert into @cities
select CityID from SACity where SAdminID = @seId

select ord.[OrdrID],ord.[InvoiceNumber],ord.[OrderDate],ord.[OrderTime],cons.[Name],cons.[PhoneNumber],ord.[GrandTotal],addr.[Address]
from [dbo].[Order] as ord
--INNER JOIN [OrderAllocationLog] OAL ON ord.OrdrID = OAL.OrdrID
left outer join [dbo].[Consumer] as cons on cons.[ConsID]=ord.[ConsID]
left outer join [dbo].[ConsumerAddress] as Addr on addr.[AddrID]=ord.[AddrID]
where ord.[StatusID]=1 
and 
(
	--((ord.CityId is null) 
	--or
	not exists(select cityId from @cities)
	or
	((ord.CityId is not null) and (ord.CityId in (select cityId from @cities)))
)
order by ord.OrdrID desc

END
GO

/****** Object:  StoredProcedure [dbo].[GetIssuesCountBySUser]    Script Date: 8/24/2018 8:25:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[GetIssuesCountBySUser] --[GetIssuesListBySUser] 157
@seId int
AS
BEGIN 

declare @cities table (
	cityId int
)

insert into @cities
select CityID from SACity where SAdminID = @seId

select count(*) as Total
from [dbo].[Order] as ord
--INNER JOIN [OrderAllocationLog] OAL ON ord.OrdrID = OAL.OrdrID
left outer join [dbo].[Consumer] as cons on cons.[ConsID]=ord.[ConsID]
left outer join [dbo].[ConsumerAddress] as Addr on addr.[AddrID]=ord.[AddrID]
where ord.[StatusID]=1 
and 
(
	--((ord.CityId is null) 
	--or
	not exists(select cityId from @cities)
	or
	((ord.CityId is not null) and (ord.CityId in (select cityId from @cities)))
)
END
GO

/****** Object:  StoredProcedure [dbo].[GetUserNewNotificationCount]    Script Date: 8/24/2018 8:25:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetUserNewNotificationCount] --[GetUserNewNotificationCount] 39,'admin agen'
@UserId int,
@Role varchar(100)
as
Begin

	declare @read smallint = 0

  ;WITH pg
  as(
   SELECT top 1000 ph.*
      FROM NotificationLog ph
	  where isnull(ph.Failure,0) <= 0
	  --and isnull(ph.isRead,1) = 0
	  order by LogId desc
  )
	select 
	count(n.LogId) as Total
	from pg as n
	where 
	n.UserId = @UserId
	and n.UserRole = @Role
	and n.isRead = @read

End
GO

/****** Object:  StoredProcedure [dbo].[GetUserNotificationHistory]    Script Date: 8/24/2018 8:27:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[GetNotificationHistory]    Script Date: 8/14/2018 8:40:43 AM ******/
ALTER PROCEDURE [dbo].[GetUserNotificationHistory] --[GetUserNotificationHistory] 10,1,'',39,'admin agen'
@PageSize int = null,
@PageNumber int,
@Search varchar(max) = null,
@UserId int,
@Role varchar(100)
as
Begin

	if @PageSize is null 
	begin
		set @PageSize =  10000
		--(SELECT count(*)
  --    FROM NotificationLog ph
	 -- where Failure <= 0)

	  set @PageNumber = 1

	end

	if @Search is null
	begin
		set @Search = ''
	end

	declare @logid table (
		logid int
	)

  ;WITH pg
  as(
   SELECT top 5000 ph.*
      FROM NotificationLog ph
	  --where isnull(ph.Failure,0) <= 0
	  order by LogId desc
  ),
  pages as(
	select 
	n.LogId
	from pg as n
	where 
	n.UserId = @UserId
	  and n.UserRole = @Role
	  and
	  (
		n.Title like '%' + @Search + '%'
		or
		n.Message like '%' + @Search + '%'
	  )
	ORDER BY 
		n.LogId desc
      OFFSET @PageSize * (@PageNumber - 1) ROWS
      FETCH NEXT @PageSize ROWS ONLY
  )
  insert into @logid
  select LogId from pages

  

  SELECT 
	pages.LogId as LogId,
	ph.Title as Title,
	ph.Message as Message,
	ph.MobileNumber as MobileNumber,
	ph.UserID as UserID,
	ph.UserName as UserName,
	ph.UserRole as UserRole,
	ph.CreatedDate as CreatedDate,
	ph.isRead as IsRead
  FROM @logid as pages
  inner join NotificationLog ph on pages.LogId = ph.LogId
	 ORDER BY (
		ph.LogId
	  ) desc
	  , ph.CreatedDate desc

	  --SELECT count(*)
   --   FROM TblT_MaterialPlanningHeader ph
	  ----inner join TblM_ProjectTeamMember ptm on ph.WBS_Lv1 = ptm.ProjectCodeLv1 and convert(nvarchar(100),ptm.Nopek) = @Nopek
	  --inner join TblM_Rig ptm on ph.RigId = ptm.RigId and ptm.AdminProject = @username
	  --where ReqStatus in (7,14)
	
	
	update nl
  set nl.isRead = 1, nl.ReadAt = getutcdate()
  from @logid p
  inner join notificationlog nl  on nl.LogId = p.logid
  where nl.IsRead is null or nl.IsRead = 0
	

End
GO

/****** Object:  StoredProcedure [dbo].[GetUserNewNotificationCount]    Script Date: 8/24/2018 8:28:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetUserNewNotificationCount] --[GetUserNewNotificationCount] 39,'admin agen'
@UserId int,
@Role varchar(100)
as
Begin

	declare @read smallint = 0

  ;WITH pg
  as(
   SELECT top 1000 ph.*
      FROM NotificationLog ph
	  where isnull(ph.Failure,0) <= 0
	  --and isnull(ph.isRead,1) = 0
	  order by LogId desc
  )
	select 
	count(n.LogId) as Total
	from pg as n
	where 
	n.UserId = @UserId
	and n.UserRole = @Role
	and n.isRead = @read

End
GO



alter table notificationlog
add IsRead smallint 

ALTER TABLE notificationlog add CONSTRAINT notificationlog_isread DEFAULT 0 FOR IsRead;
GO

IF EXISTS (SELECT name FROM sys.indexes  
            WHERE name = N'IX_NotifLog_IsRead')   
    DROP INDEX IX_NotifLog_IsRead ON dbo.NotificationLog
GO  
-- Create a nonclustered index called IX_ProductVendor_VendorID   
-- on the Purchasing.ProductVendor table using the BusinessEntityID column.   
CREATE NONCLUSTERED INDEX IX_NotifLog_IsRead   
    ON dbo.NotificationLog    (IsRead);   
GO  

alter table notificationlog
add ReadAt datetime

alter table Driver
add Latitude nvarchar(100)

alter table Driver
add Longitude nvarchar(100)

alter table [order]
add SummaryItem nvarchar(max) null

alter table [order]
add CountItem nvarchar(500) null


alter table TeleOrder
add SummaryItem nvarchar(max) null

alter table TeleOrder
add CountItem nvarchar(500) null

begin tran ss

update [order]
set SummaryItem = STUFF(
         (
		 SELECT DISTINCT ',' + convert (varchar,prod.productname)
		 -- + CASE WHEN ( orddet.[RefillQuantity] > 0 and orddet.[Quantity] > 0) THEN '[Refill]+[Tabung + Isi]' 
			--WHEN (orddet.[Quantity] > 0 and not exists (Select 1 from [dbo].[OrderPrdocuctExchange] ex Where ex.ordrid = ord.ordrid)) THEN '[Tabung + Isi]'
			--WHEN ( orddet.[RefillQuantity] > 0) THEN '[Refill]'
			--WHEN (orddet.[Quantity] > 0 and exists (Select 1
   --                from [dbo].[OrderPrdocuctExchange] ex
   --                Where ex.ordrid = ord.ordrid
   --               ))
		 --THEN '[Tukar Tambah]' 
		 --ELSE ''
		 --END
		 + CASE WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '[Tabung + Isi  + Refill +  Tukar Tambah]'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Refill]'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Tukar Tambah]' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '[Refill + Tukar Tambah]'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN '[Tukar Tambah]'
		 WHEN (orddet.[RefillQuantity] > 0) THEN '[Refill]'		 
		 WHEN (orddet.[Quantity] > 0) THEN '[Tabung + Isi]' ELSE '' END
          from [Order] ord
	 INNER JOIN orderdetails orddet
	 on ord.ordrid = orddet.ordrid
	 INNER JOIN [dbo].[Product] AS prod ON orddet.prodid = prod.prodid
	 LEFT OUTER JOIN [dbo].[OrderPrdocuctExchange] ordExch on ordExch.ordrid = ord.ordrid and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.ordrid = o.OrdrID
          FOR XML PATH (''))
          , 1, 1, ''),
		  	  CountItem = STUFF(
         (
		 SELECT DISTINCT ',' + 
		 CASE 
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+')'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '('+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN convert(varchar,ordExch.[ExchangeQuantity])
		 WHEN (orddet.[RefillQuantity] > 0) THEN convert(varchar,orddet.[RefillQuantity])		 
		 WHEN (orddet.[Quantity] > 0) THEN convert(varchar,orddet.[Quantity]) ELSE '0' END
          from [Order] ord
	 INNER JOIN orderdetails orddet
	 on ord.ordrid = orddet.ordrid
	 LEFT OUTER JOIN [dbo].[OrderPrdocuctExchange] ordExch on ordExch.ordrid = ord.ordrid and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.ordrid = o.OrdrID
          FOR XML PATH (''))
          , 1, 1, '') 
from [Order] o
where OrdrID in (9529,9510)

select * from [Order] where OrdrID in (9529,9510)


update TeleOrder
set SummaryItem = STUFF(
         (
		 SELECT DISTINCT ',' + convert (varchar,prod.productname)
		 -- + CASE WHEN ( orddet.[RefillQuantity] > 0 and orddet.[Quantity] > 0) THEN '[Refill]+[Tabung + Isi]' 
		 --WHEN (orddet.[Quantity] > 0 and not exists (Select 1
   --                from [dbo].[TeleOrderPrdocuctExchange] ex
   --                Where ex.[TeleOrdID] = ord.[TeleOrdID]
   --               )) THEN '[Tabung + Isi]'
			--WHEN ( orddet.[RefillQuantity] > 0) THEN '[Refill]'
			--WHEN (orddet.[Quantity] > 0 and exists (Select 1
   --                from [dbo].[TeleOrderPrdocuctExchange] ex
   --                Where ex.[TeleOrdID] = ord.[TeleOrdID]
   --               ))
		 --THEN '[Tukar Tambah]' 
		 --ELSE ''
		 --END
		 + CASE WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '[Tabung + Isi  + Refill +  Tukar Tambah]'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Refill]'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '[Tabung + Isi  + Tukar Tambah]' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '[Refill + Tukar Tambah]'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN '[Tukar Tambah]'
		 WHEN (orddet.[RefillQuantity] > 0) THEN '[Refill]'		 
		 WHEN (orddet.[Quantity] > 0) THEN '[Tabung + Isi]' ELSE '' END
          from [TeleOrder] ord
	 INNER JOIN [TeleOrderDetails] orddet
	 on ord.[TeleOrdID] = orddet.[TeleOrdID]
	 INNER JOIN [dbo].[Product] AS prod ON orddet.prodid = prod.prodid
	 LEFT OUTER JOIN [dbo].[TeleOrderPrdocuctExchange] ordExch on ordExch.[TeleOrdID] = ord.[TeleOrdID] and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.[TeleOrdID] = t.[TeleOrdID] 
          FOR XML PATH (''))
          , 1, 1, '')  ,
		  
	  CountItem = STUFF(
         (
		 SELECT DISTINCT ',' + 
		 CASE 
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0 AND ordExch.[ExchangeQuantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'
		 WHEN ( orddet.[RefillQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,orddet.[RefillQuantity])+')'
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[Quantity] > 0) THEN '('+convert(varchar,orddet.[Quantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')' 
		 WHEN ( ordExch.[ExchangeQuantity] > 0 AND orddet.[RefillQuantity] > 0) THEN '('+convert(varchar,orddet.[RefillQuantity])+' + '+convert(varchar,ordExch.[ExchangeQuantity])+')'		
		 WHEN (ordExch.[ExchangeQuantity] > 0) THEN convert(varchar,ordExch.[ExchangeQuantity])
		 WHEN (orddet.[RefillQuantity] > 0) THEN convert(varchar,orddet.[RefillQuantity])		 
		 WHEN (orddet.[Quantity] > 0) THEN convert(varchar,orddet.[Quantity]) ELSE '0' END
          from [TeleOrder] ord
	 INNER JOIN [TeleOrderDetails] orddet
	 on ord.[TeleOrdID] = orddet.[TeleOrdID]
	 LEFT OUTER JOIN [dbo].[TeleOrderPrdocuctExchange] ordExch on ordExch.[TeleOrdID] = ord.[TeleOrdID] and ordExch.[ProdID] = orddet.[ProdID]
          WHERE ord.[TeleOrdID] = t.[TeleOrdID] 
          FOR XML PATH (''))
          , 1, 1, '')
from TeleOrder t


select * from TeleOrder


rollback tran ss