﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IActivityService
    {
        ActivityModel AddActivityLog(ActivityModel model, string pageName);
        List<ActivityModel> ListActivity(int loggedBy);
    }
}