﻿using Pertamina.LPG.API.DTOs;
using Pertamina.LPG.API.DTOs.SuperUser;
using Pertamina.LPG.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Pertamina.LPG.API.DTOs.Gen;

namespace Pertamina.LPG.API.Controllers
{
    public class SuperUserController : ApiController
    {
        [HttpPost]
        [ActionName("login")]
        public NegotiatedContentResult<LoginResponse> PostLogin([FromBody]LoginRequest request)
        {
            LoginResponse resp = SuperUserServices.Login(request);
            return Content(HttpStatusCode.OK, resp);
        }
        [HttpPost]
        [ActionName("forgot_password")]
        public NegotiatedContentResult<ForgotPasswordResponse> PostForgotPassword([FromBody]ForgotPasswordRequest request)
        {
            ForgotPasswordResponse resp = SuperUserServices.ForgotPassword(request);
            return Content(HttpStatusCode.OK, resp);
        }
        [HttpPost]
        [ActionName("change_password_super_user")]
        public NegotiatedContentResult<ResponseDto> PostChangePassword([FromBody]ChangePasswordSuperUserRequest request)
        {
            ResponseDto resp = SuperUserServices.ChangePassword(request);
            return Content(HttpStatusCode.OK, resp);
        }
        [HttpPost]
        [ActionName("change_city_super_user")]
        public NegotiatedContentResult<ResponseDto> PostChangeCity([FromBody]ChangeCitySuperUserRequest request)
        {
            ResponseDto resp = SuperUserServices.ChangeCity(request);
            return Content(HttpStatusCode.OK, resp);
        }
        [HttpPost]
        [ActionName("change_profile_super_user")]
        public NegotiatedContentResult<ResponseDto> PostChangeProfile([FromBody]ChangeProfileSuperUserRequest request)
        {
            ResponseDto resp = SuperUserServices.ChangeProfile(request);
            return Content(HttpStatusCode.OK, resp);
        }
        [HttpPost]
        [ActionName("get_super_user_details")]
        public NegotiatedContentResult<ResponseDto> PostGetDetails([FromBody]GetSuperUserDetailsRequest request)
        {
            ResponseDto resp = SuperUserServices.GetDetails(request);
            return Content(HttpStatusCode.OK, resp);
        }
        [HttpPost]
        [ActionName("check_otp")]
        public NegotiatedContentResult<ResponseDto> PostCheckOtp([FromBody]CheckOtpRequest request)
        {
            ResponseDto resp = SuperUserServices.CheckOtp(request);
            return Content(HttpStatusCode.OK, resp);
        }
        [HttpPost]
        [ActionName("resend_otp")]
        public NegotiatedContentResult<ResendOtpResponse> PostResendOtp([FromBody]ResendOtpRequest request)
        {
            ResendOtpResponse resp = SuperUserServices.ResendOtp(request);
            return Content(HttpStatusCode.OK, resp);
        }
        [HttpPost]
        [ActionName("reset_password")]
        public NegotiatedContentResult<ResponseDto> PostResetPassword([FromBody]ResetPasswordRequest request)
        {
            ResponseDto resp = SuperUserServices.ResetPassword(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("change_profilephoto_super_user")]
        public NegotiatedContentResult<ResponseDto> PostChangeProfilePhoto([FromBody]ChangeProfilePhotoRequest request)
        {
            ResponseDto resp = SuperUserServices.ChangeProfilePhoto(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("get_notification_history")]
        public NegotiatedContentResult<GetNotificationHistoryResponse> PostGetNotificationHistory([FromBody]GetNotificationHistoryRequest request)
        {
            SuperUserServices suServices = new SuperUserServices();

            GetNotificationHistoryResponse resp = suServices.GetNotificationHistory(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("get_notification_count")]
        public NegotiatedContentResult<GetNotificationCountResponse> PostGetNotificationCount([FromBody]GetNotificationHistoryRequest request)
        {
            SuperUserServices suServices = new SuperUserServices();
            GetNotificationCountResponse resp = suServices.GetNewNotificationCount(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("send_post_notification")]
        public NegotiatedContentResult<SendPostNotificationResponse> SendPushNotification([FromBody]SendPostNotificationRequest request)
        {
            SuperUserServices suServices = new SuperUserServices();
            SendPostNotificationResponse resp = suServices.SendPostNotification(request);
            return Content(HttpStatusCode.OK, resp);
        }

        [HttpPost]
        [ActionName("get_latest_version")]
        public NegotiatedContentResult<GetLatestVersionResponse> PostGetLatestApkVersion([FromBody]GetLatestVersionRequest request)
        {
            AgentAdminServices _adminServices = new AgentAdminServices();

            GetLatestVersionResponse resp = new GetLatestVersionResponse();
            resp.has_resource = 1;
            switch ((request.app_os ?? "").ToUpper())
            {
                case "ANDROID":
                    var version = (new UserServices()).GetAppVersion(VersionAppType.Android, VersionUserType.SE).Split(';');

                    resp.version = version[0];
                    resp.version_code = version[1];
                    resp.app_id = version[2];
                    break;
                case "IOS":
                    var iosversion = (new UserServices()).GetAppVersion(VersionAppType.IOS, VersionUserType.SE).Split(';');


                    resp.version = iosversion[0];
                    resp.version_code = iosversion[1];
                    resp.app_id = iosversion[2];
                    break;

                default:
                    resp.code = 1;
                    resp.message = "jenis app tidak ditemukan";
                    resp.has_resource = 0;
                    break;
            }

            return Content(HttpStatusCode.OK, resp);
        }

    }
}
