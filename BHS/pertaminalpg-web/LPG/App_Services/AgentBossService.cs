﻿using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace LPG.App_Services
{
    public class AgentBossService : AbstractAppService, IAgentBossService
    {
        public AgentBossService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<AgentBossModel> ListAgentBoss()
        {
            var agentList = new List<AgentBossModel>();
            //using (var context = new lpgdevEntities())
            //{
            var data = from agent in context.AgentBosses
                       join company in context.Agencies
                       on agent.AgenID equals company.AgenID
                       where agent.StatusId == true
                       orderby agent.AbosID descending, agent.CreatedDate descending
                       select new AgentBossModel
                       {
                           AgentBossId = agent.AbosID,
                           OwnerName = agent.OwnerName,
                           CompanyName = company.AgencyName,
                           MobileNo = agent.MobileNumber,
                           AgentBossEmail = agent.Email

                       };
            agentList = data.ToList();
            //}

            return agentList;
        }

        public AgentBossModel getActiveAgentBoss(int agentBossId)
        {
            AgentBossModel rtns = new AgentBossModel();
            //using (var context = new lpgdevEntities())
            //{
            rtns = (from agent in context.AgentBosses
                    join company in context.Agencies
                    on agent.AgenID equals company.AgenID
                    where agent.AbosID == agentBossId

                    select new AgentBossModel()
                    {
                        AgentBossId = agent.AbosID,
                        OwnerName = agent.OwnerName,
                        AgenDriverAgenId = company.AgenID,
                        MobileNo = agent.MobileNumber,
                        AgentBossEmail = agent.Email

                    }).FirstOrDefault();
            rtns.MobileNo = rtns.MobileNo?.ToTrimCode();
            return rtns;
            //}
        }

        public AgentBossModel saveAgentBossDetails(AgentBossModel entity, int loggedBy)
        {

            try
            {

                //using (var context = new lpgdevEntities())
                //{

                var Data = context.AgentBosses.Where(r => r.AbosID == entity.AgentBossId).FirstOrDefault();
                if (Data != null)
                {

                    Data.StatusId = true;
                    Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    Data.MobileNumber = entity.CountryCode.ToDefaultString() + entity.MobileNo;
                    Data.OwnerName = entity.OwnerName;
                    Data.AgenID = entity.AgenDriverAgenId;
                    Data.Email = entity.AgentBossEmail;
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggedBy);
                }
                else
                {
                    AgentBoss objLocal = new AgentBoss();

                    objLocal.AbosID = entity.AgentBossId;
                    objLocal.OwnerName = entity.OwnerName;
                    objLocal.StatusId = true;
                    objLocal.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                    objLocal.MobileNumber = entity.CountryCode.ToDefaultString() + entity.MobileNo;
                    objLocal.Email = entity.AgentBossEmail;

                    context.AgentBosses.Add(objLocal);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggedBy);
                }
                //}
                return entity;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool DeleteAgentBoss(int Id, int loggedBy)
        {
            try
            {
                //using (var context = new lpgdevEntities())
                //{
                var Data = context.AgentBosses.Where(r => r.AbosID == Id).FirstOrDefault();
                if (Data != null)
                {
                    Data.StatusId = false;
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", loggedBy);
                    return true;
                }
                return false;
                //}
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool IsExists(int id)
        {
            //using (var context = new lpgdevEntities())
            //{
            return context.AgentBosses.Where(c => c.AbosID == id).Any();
            //}

        }

        public bool IsExistsMobileNo(string mobileNo, int userId)
        {

            //using (var context = new lpgdevEntities())
            //{
            if (userId > 0)
                return context.AgentBosses.Where(c => c.MobileNumber == mobileNo && c.StatusId == true && c.AbosID != userId).Any();
            else
                return context.AgentBosses.Where(c => c.MobileNumber == mobileNo && c.StatusId == true).Any();

            //}

        }
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = "Agen Bos";
            _activityService.AddActivityLog(actModel, "Agen Bos");
        }

    }
}
