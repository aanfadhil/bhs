﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.Consumers
{
    public class ConsumerReviewDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int ReviewID { get; set; }
        public int ConsID { get; set; }
        public int OrdrID { get; set; }
        public int DrvrID { get; set; }
        public int Rating { get; set; }
        public int? ReasonID { get; set; }
        public string Comments { get; set; }
    }

    public class ConsumerReviewMapper : MapperBase<ConsumerReview, ConsumerReviewDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<ConsumerReview, ConsumerReviewDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<ConsumerReview, ConsumerReviewDTO>>) (p => new ConsumerReviewDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    ReviewID = p.ReviewID,
                    ConsID = p.ConsID,
                    OrdrID = p.OrdrID,
                    DrvrID = p.DrvrID,
                    Rating = p.Rating,
                    ReasonID = p.ReasonID,
                    Comments = p.Comments
                }));
            }
        }

        public override void MapToModel(ConsumerReviewDTO dto, ConsumerReview model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.ReviewID = dto.ReviewID;
            model.ConsID = dto.ConsID;
            model.OrdrID = dto.OrdrID;
            model.DrvrID = dto.DrvrID;
            model.Rating = dto.Rating;
            model.ReasonID = dto.ReasonID;
            model.Comments = dto.Comments;

        }
    }
}
