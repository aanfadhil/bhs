﻿using System;
using System.Linq;
using Pertamina.LPG.API.DAOs;
using Pertamina.LPG.API.Models;
using System.Collections.Generic;

namespace Pertamina.LPG.API.DAOs
{
    public class SuperUserDao : DAO
    {
        public SuperAdmin FindByMobileNumber(string mobileNumber)
        {
            var boss = _context.SuperAdmins.Where(a => a.StatusID && a.MobileNum.Replace("+62", "") == mobileNumber.Replace("+62", ""));
            if (boss.Count() > 0)
            {
                return boss.Single();
            }
            return null;
        }

        public void Update(SuperAdmin boss)
        {
            _context.Entry(boss).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }

        public SuperAdmin FindById(int userId)
        {
            return _context.SuperAdmins.Find(userId);
        }

        public SuperAdmin FindByIdWithDetails(int userId)
        {
            return _context.SuperAdmins.Include("SACities").Where(t => t.SAdminID == userId).FirstOrDefault();
        }

        public SuperAdmin FindActiveById(int userId)
        {
            return _context.SuperAdmins.Where(t=> t.SAdminID == userId && t.StatusID).FirstOrDefault();
        }

        public List<SuperAdmin> FindAdminByCity(int cityId)
        {
            return _context.SuperAdmins.Where(admin => admin.CityId == cityId).ToList();
        }

        public List<SuperAdmin> FindAdminBySACity(int cityId)
        {
            return _context.SuperAdmins.Include("SACities").Where(admin => admin.SACities.Any(sac => sac.CityID == cityId)).ToList();
        }

        public IEnumerable<MasterDataExtraction_Result> ExportMasterData()
        {
            var customers = from customer in _context.MasterDataExtraction()
                            select customer;

            return customers;
        }

        public List<GetUserNotificationHistory_Result> GetNotificationList(int userId, string search, int pagelength, int pagenumber)
        {
            return _context.GetUserNotificationHistory(pagelength, pagenumber, search, userId, "SE").ToList();
        }

        public int GetNotificationCount(int userId, string search, int pagelength, int pagenumber)
        {
            var count = _context.GetUserNotificationHistoryCount(pagelength, pagenumber, search, userId, "SE").FirstOrDefault();
            if (count == null)
            {
                return 0;
            }
            return count.Total.Value;
        }

        public int GetNewNotificationCount(int userId)
        {
            var count = _context.GetUserNewNotificationCount(userId, "SE").FirstOrDefault();
            if (count == null)
            {
                if (count.Total == null)
                    return 0;
            }
            return count.Total.Value;
        }
    }
}
