﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LPG.Models
{
    public class ProductModel
    {
        public int ProdID { get; set; }
        [Required(ErrorMessage = "Product Name required")]
        public string ProductName { get; set; }
        [Required(ErrorMessage = "Position required")]
        public int Position { get; set; }
        public string ProductImage { get; set; }
        public string ProductImageDetails { get; set; }
        [Required(ErrorMessage = "Tube Price required")]
        public decimal TubePrice { get; set; }
        //[Required(ErrorMessage = "Tube Promo price required")]
        public decimal TubePromoPrice { get; set; }
        [Required(ErrorMessage = "Refill price required")]
        public decimal RefillPrice { get; set; }
        //[Required(ErrorMessage = "Refill promo price required")]
        public decimal RefillPromoPrice { get; set; }
        [Required(ErrorMessage = "Shipping price required")]
        public decimal ShippingPrice { get; set; }
        //[Required(ErrorMessage = "Shipping promo price required")]
        public decimal ShippingPromoPrice { get; set; }
        public bool Published { get; set; }
        public bool StatusId { get; set; }
        public short CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public short UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string PublishBit { get; set; }
        public string UserName { get; set; }
        public decimal TotalAmount { get; set; }
        public int Quantity { get; set; }
        public decimal? PromoPrice { get; set; }
        public decimal? PostalFee { get; set; }
        public decimal? PromoPostage { get; set; }
        public decimal? TotalIAmount { get; set; }
        public decimal? TotalPAmount { get; set; }
        public decimal? TotalNEnds { get; set; }
        public List<OrderModel> orderList { get; set; }
        public List<ProductExchangeList> ExchangeProductLists { get; set; }
        public ProductExchangeModel productExModel { get; set; }
        // public List<ProductExchange> product_exchange { get; set; }
        public List<ProductExchange> exchange { get; set; }
        public string ProductCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ProductRefillName { get; set; }
        public int RefillQuantity { get; set; }
        public decimal? TotalPromoProduct { get; set; }
        public decimal? TotalRefillAmount { get; set; }
        public decimal? TotalPromoRefillAmount { get; set; }
        public decimal? TotalShippingAmount { get; set; }
        public decimal? TotalPromoShippingAmount { get; set; }
        public decimal PromoProduct { get; set; }
        public decimal RefillAmount { get; set; }
        public decimal PromoRefillAmount { get; set; }
        public decimal? ShippingAmount { get; set; }
        public decimal? PromoShippingAmount { get; set; }
        public string ProductFile { get; set; }
    }

    public class ProductDto
    {
        public int product_id { get; set; }
        public int product_position { get; set; }
        public bool product_published { get; set; }
        public string product_published_status { get; set; }
        public DateTime product_createdon { get; set; }
        public string product_title { get; set; }
        public string product_image { get; set; }
        public decimal Price_refil { get; set; }
        public decimal price_of_the_tube { get; set; }

    }

    public class ProductPagenationDetailsDto
    {
        public int total_num_products { get; set; }
        public int row_per_page { get; set; }
        public int page_number { get; set; }
        public int total_pages { get; set; }
    }

    public class GetProductListResponse
    {
        public ProductPagenationDetailsDto product_details { get; set; }
        public ProductDto[] products { get; set; }
    }

    public class ProductDetailsDto
    {
        public int product_id { get; set; }
        public string product_name { get; set; }
        public int position { get; set; }
        public string product_image { get; set; }
        public string product_image_details { get; set; }
        public decimal tube_price { get; set; }
        public decimal tube_promo_price { get; set; }
        public decimal refill_price { get; set; }
        public decimal refill_promo_price { get; set; }
        public decimal shipping_price { get; set; }
        public decimal shipping_promo_price { get; set; }
        public bool has_exchange { get; set; }
        public ProductExchangeList[] exchange { get; set; }

    }

    public class GetProductDetailsResponse
    {
        public ProductDetailsDto product_details { get; set; }
    }

    public class AddProductRequest
    {
        public int suser_id { get; set; }
        public string auth_token { get; set; }
        public string product_name { get; set; }
        public int position { get; set; }
        public string product_image { get; set; }
        public string product_image_details { get; set; }
        public decimal tube_price { get; set; }
        public decimal tube_promo_price { get; set; }
        public decimal refill_price { get; set; }
        public decimal refill_promo_price { get; set; }
        public decimal shipping_price { get; set; }
        public decimal shipping_promo_price { get; set; }
        public bool published { get; set; }
        public List<ProductExchangeList> exchange { get; set; }
    }
}