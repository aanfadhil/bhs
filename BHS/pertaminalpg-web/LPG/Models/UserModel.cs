﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public enum UserType
    {
        SuperUser = 1,
        AgentBoss = 2,
        AgentAdmin = 3,
        Driver = 4,
        Consumer = 5
    }
}