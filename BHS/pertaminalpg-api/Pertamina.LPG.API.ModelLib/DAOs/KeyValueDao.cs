﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pertamina.LPG.API.Models;
using Pertamina.LPG.API.Utils;
//using System.Data.Entity.Core.Objects.SqlClient;
using Pertamina.LPG.API.Util;
using System.Data.Entity.SqlServer;

namespace Pertamina.LPG.API.DAOs
{
    public class KeyValueDao : DAO
    {
        public MKeyValue GetOneByKey(string key)
        {
            return _context.MKeyValues.Where(t => t.KeyName.Trim() == key.Trim()).FirstOrDefault();
        }
    }
}
