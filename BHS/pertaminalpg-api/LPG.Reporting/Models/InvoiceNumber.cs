namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class InvoiceNumber
    {
        [Key]
        public int InvnrID { get; set; }

        [Column("InvoiceNumber")]
        public int InvoiceNumber1 { get; set; }

        public int AgenID { get; set; }

        public virtual Agency Agency { get; set; }
    }
}
