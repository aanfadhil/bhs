namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PromoInfo")]
    public partial class PromoInfo
    {
        [Key]
        public int InfoID { get; set; }

        [Required]
        [StringLength(50)]
        public string Caption { get; set; }

        [Required]
        [StringLength(100)]
        public string InfoImage { get; set; }

        public int Position { get; set; }

        public short StatusID { get; set; }

        public short CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public short UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public virtual SuperAdmin SuperAdmin { get; set; }

        public virtual SuperAdmin SuperAdmin1 { get; set; }
    }
}
