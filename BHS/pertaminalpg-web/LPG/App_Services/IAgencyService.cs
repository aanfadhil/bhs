﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IAgencyService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        bool deleteAgency(int Id, int loggedBy);
        CompanyModel getCompanyDetails(int agencyId);
        List<CompanyModel> ListComapany();
        List<CompanyModel> ListComapanyGetbyRegion(int regionId);
        CompanyModel saveCompanyDetails(CompanyModel entity, int loggedBy);
    }
}