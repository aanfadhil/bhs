﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using LPG.Reporting.DTOs.Consumers;
using LPG.Reporting.DTOs.Orders;

namespace LPG.Reporting.DTOs.SystemActors
{
    [DataContract]
    public class DriverDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        [DataMember]
        public int DrvrID { get; set; }
        [DataMember]
        [Required]
        [StringLength(100)]
        public string DriverName { get; set; }
        [DataMember]
        public int AgenID { get; set; }
        [DataMember]
        public int DbptID { get; set; }
        [DataMember]
        [Required]
        [StringLength(20)]
        public string MobileNumber { get; set; }
        [DataMember]
        public bool StatusId { get; set; }
        [DataMember]
        public short CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public short UpdatedBy { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        [StringLength(64)]
        public string AccToken { get; set; }
        [DataMember]
        [StringLength(255)]
        public string AppID { get; set; }
        [DataMember]
        [StringLength(255)]
        public string AppToken { get; set; }
        [DataMember]
        public DateTime? LastLogin { get; set; }
        [DataMember]
        [StringLength(255)]
        public string ProfileImage { get; set; }
        [DataMember]
        [StringLength(50)]
        public string Password { get; set; }
        [DataMember]
        public IEnumerable<ConsumerReviewDTO> ConsumerReviews { get; set; }
        [DataMember]
        public IEnumerable<OrderDTO> Orders { get; set; }
        [DataMember]
        public IEnumerable<OrderDeliveryDTO> OrderDeliveries { get; set; }
        [DataMember]
        public IEnumerable<TeleOrderDTO> TeleOrders { get; set; }
        [DataMember]
        public IEnumerable<TeleOrderDeliveryDTO> TeleOrderDeliveries { get; set; }
    }

    public class DriverMapper : MapperBase<Driver, DriverDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private ConsumerReviewMapper _consumerReviewMapper = new ConsumerReviewMapper();
        private OrderMapper _orderMapper = new OrderMapper();
        private OrderDeliveryMapper _orderDeliveryMapper = new OrderDeliveryMapper();
        private TeleOrderMapper _teleOrderMapper = new TeleOrderMapper();
        private TeleOrderDeliveryMapper _teleOrderDeliveryMapper = new TeleOrderDeliveryMapper();
        public override Expression<Func<Driver, DriverDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Driver, DriverDTO>>) (p => new DriverDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    DrvrID = p.DrvrID,
                    DriverName = p.DriverName,
                    AgenID = p.AgenID,
                    DbptID = p.DbptID,
                    MobileNumber = p.MobileNumber,
                    StatusId = p.StatusId,
                    CreatedBy = p.CreatedBy,
                    CreatedDate = p.CreatedDate,
                    UpdatedBy = p.UpdatedBy,
                    UpdatedDate = p.UpdatedDate,
                    AccToken = p.AccToken,
                    AppID = p.AppID,
                    AppToken = p.AppToken,
                    LastLogin = p.LastLogin,
                    ProfileImage = p.ProfileImage,
                    Password = p.Password,
                    ConsumerReviews = p.ConsumerReviews.AsQueryable().Select(this._consumerReviewMapper.SelectorExpression),
                    Orders = p.Orders.AsQueryable().Select(this._orderMapper.SelectorExpression),
                    OrderDeliveries = p.OrderDeliveries.AsQueryable().Select(this._orderDeliveryMapper.SelectorExpression),
                    TeleOrders = p.TeleOrders.AsQueryable().Select(this._teleOrderMapper.SelectorExpression),
                    TeleOrderDeliveries = p.TeleOrderDeliveries.AsQueryable().Select(this._teleOrderDeliveryMapper.SelectorExpression)
                }));
            }
        }

        public override void MapToModel(DriverDTO dto, Driver model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.DrvrID = dto.DrvrID;
            model.DriverName = dto.DriverName;
            model.AgenID = dto.AgenID;
            model.DbptID = dto.DbptID;
            model.MobileNumber = dto.MobileNumber;
            model.StatusId = dto.StatusId;
            model.CreatedBy = dto.CreatedBy;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedBy = dto.UpdatedBy;
            model.UpdatedDate = dto.UpdatedDate;
            model.AccToken = dto.AccToken;
            model.AppID = dto.AppID;
            model.AppToken = dto.AppToken;
            model.LastLogin = dto.LastLogin;
            model.ProfileImage = dto.ProfileImage;
            model.Password = dto.Password;

        }
    }
}
