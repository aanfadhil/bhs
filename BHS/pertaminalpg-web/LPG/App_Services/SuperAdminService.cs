﻿using LPG.App_Classes;
using LPG.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Configuration;
using System.IO;
using LPG.Utilities;
using System.Web.Configuration;

using Microsoft.ApplicationInsights;

namespace LPG.App_Services
{
    public class SuperAdminService : AbstractAppService, ISuperAdminService
    {

        private const string ALGORITHM = "HmacSHA256";
        private const string SALT = "yz7LWQtFBXpMj9W8fvUh";
        private const string CHARS = "abcdefghiABCDEFGHIjklmnopqrstuvwxyzJKLMNOPQRSTUVWXYZ";
        private const string NUMS = "1234567890";
        //private const string RESET_PASSWORD_DEFAULT = "1111";
        private const int RESET_PASSWORD_LENGTH = 6;

        public SuperAdminService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        //public SuperAdminService(lpgdevEntities context, TelemetryClient _telemetry) : base(context, _telemetry)
        //{
        //}

        public static string GenerateResetPassword()
        {
            string password = string.Empty;
            //password = RESET_PASSWORD_DEFAULT;
            Random rnd = new Random();
            while (password.Length < RESET_PASSWORD_LENGTH)
            {
                password += CHARS[rnd.Next(CHARS.Length)];
                password += NUMS[rnd.Next(NUMS.Length)];
            }
            return password;
        }
        // List : SuperAdmin list service method
        public List<SuperAdminModel> ListSuperAdminUser()
        {
            var userList = new List<SuperAdminModel>();
            string filepath = ConfigurationManager.AppSettings["DefaultPath_Superadmin"].ToString();
            using (var context = new lpgdevEntities())
            {
                var data = from user in context.SuperAdmins
                           join usertype in context.MSuperUserTypes
                           on user.UserTypeID equals usertype.UserTypeID
                           where user.StatusID == true
                           orderby user.SAdminID
                           select new SuperAdminModel
                           {
                               sAdminId = user.SAdminID,
                               full_name = user.FullName,
                               profile_image = (user.ProfileImage != "") ? (user.ProfileImage) : "",
                               email = user.Email,
                               mobile_number = user.MobileNum,
                               type_name = usertype.TypeName,
                               userTypeId = user.UserTypeID,
                           };

                userList = data.ToList();
            }
            return userList;
        }

        public void UpdateSuperAdminCities(int saID, List<int> cityIds, int loggeduserId)
        {

            var saOldCities = context.SACities.Where(t => t.SAdminID == saID);

            foreach (var item in saOldCities)
            {
                context.SACities.Remove(item);
            }

            context.SaveChanges();

            var sa = context.SuperAdmins.Where(t => t.SAdminID == saID).FirstOrDefault();


            if (sa != null)
            {

                //var cities = context.MCities.Where(t => cityIds.Any(c => c.ToString() == t.CityCode.Trim()));
                var lastSE = context.SACities.OrderByDescending(u => u.SECityId).FirstOrDefault();
                //var lastSEID = 0;

                //if(lastSE != null)
                //{
                //    //lastSEID = lastSE.SECityId;
                //}

                foreach (var item in cityIds)
                {
                    //lastSEID += 1;
                    var saCity = new SACity() { CityID = item, SAdminID = sa.SAdminID };
                    sa.SACities.Add(saCity);

                }

                context.SaveChanges();
            }



        }

        public void UpdateSuperAdminPhone(int saID, string phone)
        {
            var sa = context.SuperAdmins.Where(t => t.SAdminID == saID).FirstOrDefault();

            if (sa != null)
            {
                //var cities = context.MCities.Where(t => cityIds.Any(c => c.ToString() == t.CityCode.Trim()));
                sa.MobileNum = phone;

                context.SaveChanges();
            }

        }


        // Add/Update : SuperAdmin details add/update to DB
        public SuperAdminModel SaveSuperAdminUser(SuperAdminModel model, int loggeduserId)
        {
            try
            {
                string filepath_ProfileImage = model.profile_image;
                string filename_ProfileImage = "";

                if (filepath_ProfileImage != "")
                {
                    filename_ProfileImage = Path.GetFileName(filepath_ProfileImage);
                }

                using (var context = new lpgdevEntities())
                {
                    var Data = context.SuperAdmins.Where(r => r.SAdminID == model.sAdminId).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.ProfileImage = filename_ProfileImage;
                        Data.FullName = model.full_name;
                        Data.Email = model.email;
                        Data.MobileNum = model.CountryCode.ToDefaultString() + model.mobile_number; // add +62 code with mobile number
                        Data.UserTypeID = model.userTypeId;
                        Data.UpdatedBy = loggeduserId;
                        Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        Data.CityId = model.cityId;
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggeduserId);
                    }
                    else
                    {
                        SuperAdmin sAdmin = new SuperAdmin();
                        string newPassword = "";
                        string default_password = "";
                        sAdmin.SAdminID = model.sAdminId;
                        sAdmin.FullName = model.full_name;
                        sAdmin.Email = model.email;
                        //sAdmin.Password = TokenGenerator.GetHashedPassword(ConfigurationManager.AppSettings["DefaultPassword"], 49);
                        sAdmin.StatusID = true;
                        sAdmin.ProfileImage = filename_ProfileImage;
                        sAdmin.MobileNum = model.CountryCode.ToDefaultString() + model.mobile_number;
                        sAdmin.CityId = model.cityId;
                        sAdmin.UserTypeID = model.userTypeId;
                        sAdmin.CreatedBy = loggeduserId;
                        sAdmin.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        if (model.userTypeId.ToString() == ConfigurationManager.AppSettings["HQUserRoleId"])
                        {
                            try
                            {
                                default_password = GenerateResetPassword();
                                if (default_password != "")
                                {
                                    sAdmin.Password = TokenGenerator.GetHashedPassword(default_password);
                                }

                                EmailServices.SendMail(model.email, Constants.EmailSubject[0] + model.full_name + Constants.EmailSubject[1], Constants.EmailBody[0] + model.full_name + Constants.EmailBody[1] + model.email + Constants.EmailBody[2] + default_password + Constants.EmailBody[3]);

                            }
                            catch
                            {

                                throw;
                            }
                        }
                        else
                        {
                            newPassword = SMSService.SendOTP(model.mobile_number);
                            if (newPassword != "")
                            {
                                sAdmin.Password = TokenGenerator.GetHashedPassword(newPassword);
                            }
                            else
                            {
                                default_password = GenerateResetPassword();
                                if (default_password != "")
                                {
                                    sAdmin.Password = TokenGenerator.GetHashedPassword(default_password);
                                }
                            }
                        }

                        context.SuperAdmins.Add(sAdmin);
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggeduserId);

                    }
                }

                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int GetRegisteredUserTotal()
        {
            using (var context = new lpgdevEntities())
            {
                return context.Consumers.Where(c => c.StatusID == 1).Count();
            }
        }

        public HighChartBar<decimal> GetRegisteredUserByMonth(int monthsToShow)
        {
            var resp = new HighChartBar<decimal>();

            using (var context = new lpgdevEntities())
            {
                var report = context.Consumers.
                    OrderByDescending(c => c.CreatedDate).
                    Where(c => c.StatusID == 1).
                    GroupBy(c => new { Year = c.CreatedDate.Year, Month = c.CreatedDate.Month }).
                    Select(c => new { Year = c.Key.Year, Month = c.Key.Month, Count = c.Count() }).
                    AsEnumerable().Select(c => new
                    {
                        x = c.Month + "-" + c.Year,
                        y = c.Count
                    }).OrderBy(c => c.x);


                if (report != null)
                {
                    resp.y = report.Select(c => (decimal)c.y).ToList();
                    resp.x = report.Select(c => c.x).ToList();
                }
            }

            return resp;
        }
        // Add/Update : SuperAdmin password details update to DB

        public SuperAdminModel SaveSuperAdminPassword(SuperAdminModel model)
        {
            try
            {
                using (var context = new lpgdevEntities())
                {
                    var Data = context.SuperAdmins.Where(r => r.SAdminID == model.sAdminId).FirstOrDefault();
                    if (Data != null)
                    {
                        string imageName = model.profile_image.Split('/').Length > 0 ? model.profile_image.Split('/').Last() : string.Empty;
                        if (model.password != model.confirm_password)
                            Data.Password = TokenGenerator.GetHashedPassword(model.confirm_password, 49);
                        Data.ProfileImage = imageName;
                        Data.FullName = model.full_name;
                        Data.CityId = model.cityId;
                        context.SaveChanges();
                    }
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }

        }

        // Delete : SuperAdmin details stausId = false
        public bool DeleteSuperAdminUser(int Id, int loggeduserId)
        {
            try
            {
                using (var context = new lpgdevEntities())
                {
                    var Data = context.SuperAdmins.Where(r => r.SAdminID == Id).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.StatusID = false;
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", loggeduserId);
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        // List : UserType list - Getting from db to show dropdown box
        public List<MSuperUserTypeModel> ListSuperUserType()
        {
            var usertypeList = new List<MSuperUserTypeModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from usertype in context.MSuperUserTypes
                           where usertype.StatusId == true
                           orderby usertype.TypeName
                           select new MSuperUserTypeModel
                           {
                               userTypeId = usertype.UserTypeID,
                               type_name = usertype.TypeName
                           };

                usertypeList = data.ToList();
            }
            return usertypeList;
        }

        public List<ProvinceModel> ListProvince()
        {
            var provinceList = new List<ProvinceModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from province in context.MProvinces
                           select new ProvinceModel
                           {
                               ID = province.ID,
                               ProvName = province.ProvName
                           };

                provinceList = data.ToList();
            }
            return provinceList;
        }

        // Get : SuperAdmin get a single user details
        public SuperAdminModel GetSuperAdminUser(int Id)
        {
            SuperAdminModel data = new SuperAdminModel();
            string filepath = ConfigurationManager.AppSettings["DefaultPath_Superadmin"].ToString();
            using (var context = new lpgdevEntities())
            {
                data = (from user in context.SuperAdmins
                        join usertype in context.MSuperUserTypes
                        on user.UserTypeID equals usertype.UserTypeID
                        where user.SAdminID == Id
                        select new SuperAdminModel
                        {
                            sAdminId = user.SAdminID,
                            full_name = user.FullName,
                            profile_image = (user.ProfileImage != "") ? (filepath + user.ProfileImage) : "",
                            mobile_number = user.MobileNum,
                            email = user.Email,
                            userTypeId = user.UserTypeID,
                            type_name = usertype.TypeName,
                            password = user.Password,
                            new_password = user.Password,
                            confirm_password = user.Password,
                            cityId = user.CityId != null ? user.CityId.Value : 0,
                            provinceId = user.MCity != null ? user.MCity.ProvID : 0,
                        }).FirstOrDefault();
                data.mobile_number = data.mobile_number?.ToTrimCode();
                return data;
            }

        }

        public bool IsExists(int id)
        {
            using (var context = new lpgdevEntities())
            {
                return context.SuperAdmins.Where(c => c.SAdminID == id).Any();
            }

        }

        public bool IsExistsEmailID(string email, int userId)
        {
            using (var context = new lpgdevEntities())
            {
                return context.SuperAdmins.Where(c => c.Email == email && c.StatusID == true && c.SAdminID != userId).Any();
            }

        }

        public bool IsExistsMobileNo(string mobileNo, int userId)
        {
            string newMobileNo = MessageSource.ToStandardPhone(mobileNo);
            using (var context = new lpgdevEntities())
            {
                return context.SuperAdmins.Where(c => c.MobileNum == newMobileNo && c.StatusID == true && c.SAdminID != userId).Any();
            }

        }

        public List<SellerReportModel> GetSellerReport(ChartRequestModel model)
        {
            var reportList = new List<SellerReportModel>();
            using (var context = new lpgdevEntities())
            {
                if (model.AgencyId == null)
                {
                    model.AgencyId = new List<int>();
                }
                string agencyIds = string.Join<int>(",", model.AgencyId);
                List<string> weekStartDates = GetStartOfLastXWeeks(6);
                int periodRange = Common.GetAppSetting<int>(Constants.APPSETTING_REPORTPERIOD_RANGE, 6);
                var reports = context.GetSellerReportBySuperAdmin(model.TotalType, model.Period, periodRange, model.ProductId.ToDefaultString(), agencyIds).OrderBy(c => c.Period).ToList();
                if (reports != null && reports.Count() > 0)
                {
                    if (model.Period == 1)
                    {

                        reportList = reports.Select(r => new SellerReportModel
                        {
                            key = DateTime.Parse(r.Period).ToString("MMM yyyy"),
                            value = r.Value.ToDecimal()
                        }).OrderBy(v => DateTime.Parse(v.key)).ToList();
                    }
                    else
                    {
                        if (reports.First().Period.StartsWith("W"))
                        {
                            var index = 5;
                            reportList = reports.Select(r => new SellerReportModel
                            {
                                key = weekStartDates[index--],//r.Period,
                                value = r.Value.ToDecimal()
                            }).OrderBy(v => DateTime.Parse(v.key)).ToList();
                        }
                        else
                        {
                            reportList = reports.Select(r => new SellerReportModel
                            {
                                key = r.Period,
                                value = r.Value.ToDecimal()
                            }).OrderBy(v => v.key).ToList();
                        }
                    }

                }
            }

            return reportList;
        }


        public List<SellerReportModel> GetSellerRatingReport(ChartRequestModel model)
        {
            var reportList = new List<SellerReportModel>();
            using (var context = new lpgdevEntities())
            {
                if (model.AgencyId == null)
                {
                    model.AgencyId = new List<int>();
                }
                string agencyIds = string.Join<int>(",", model.AgencyId);

                List<string> weekStartDates = GetStartOfLastXWeeks(6);

                int periodRange = Common.GetAppSetting<int>(Constants.APPSETTING_REPORTPERIOD_RANGE, 6);

                var reports = context.GetSellerRatingBySuperAdmin(model.Period, periodRange, model.ProductId.ToDefaultString(), agencyIds).ToList();
                if (reports != null && reports.Count() > 0)
                {
                    if (model.Period == 1)
                    {
                        reportList = reports.Select(r => new SellerReportModel
                        {
                            key = DateTime.Parse(r.Period).ToString("MMM yyyy"),
                            value = r.Value.ToDecimal()
                        }).OrderBy(v => DateTime.Parse(v.key)).ToList();
                    }
                    else
                    {
                        if (reports.First().Period.StartsWith("W"))
                        {
                            var index = 5;
                            reportList = reports.Select(r => new SellerReportModel
                            {
                                key = weekStartDates[index--],//r.Period,
                                value = r.Value.ToDecimal()
                            }).OrderBy(v => DateTime.Parse(v.key)).ToList();
                        }
                        else
                        {
                            reportList = reports.Select(r => new SellerReportModel
                            {
                                key = r.Period,
                                value = r.Value.ToDecimal()
                            }).OrderBy(v => v.key).ToList();
                        }
                    }
                }

            }

            return reportList;
        }


        private List<string> GetStartOfLastXWeeks(int weeks)
        {
            List<string> resp = new List<string>();

            var firstWeekStart = GetStartOfTheWeek(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"), DayOfWeek.Monday);

            resp.Add(firstWeekStart.ToString("dd MMM"));

            for (int i = 1; i < weeks; i++)
            {
                resp.Add(firstWeekStart.AddDays(-7 * i).ToString("dd MMM"));
            }

            //resp.Reverse();
            return resp;
        }

        private DateTime GetStartOfTheWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public void CallActivityLog(string action, string action_item, int userId)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = userId;
            actModel.Item = "User Management";
            _activityService.AddActivityLog(actModel, "User Management");
        }



        //Get split of report by their status
        public List<SellerReportModel> GetOrderStatusReport()
        {

            var resp = new List<SellerReportModel>();
            using (var context = new lpgdevEntities())
            {

                var reports = context.Orders
                            .GroupBy(o => o.MOrderStatu)
                            .Select(r => new { key = r.Key.OrderStatus, value = r.Count() });

                if (reports != null)
                {

                    resp = reports.Select(r => new SellerReportModel
                    {
                        key = r.key,
                        value = r.value
                    }).ToList();

                }

            }


            return resp;
        }

        public List<CityModel> ListCities(int provinceId = 0)
        {
            var cityList = new List<CityModel>();

            using (var context = new lpgdevEntities())
            {
                if (provinceId != 0)
                {
                    var data = from city in context.MCities
                               where city.ProvID == provinceId
                               select new CityModel
                               {
                                   CityName = city.CityName,
                                   ID = city.ID,
                                   ProvID = city.ProvID
                               };
                    cityList = data.ToList();
                }
                else
                {
                    var data = from city in context.MCities
                               select new CityModel
                               {
                                   CityName = city.CityName,
                                   ID = city.ID,
                                   ProvID = city.ProvID
                               };
                    cityList = data.ToList();
                }
            }
            return cityList;
        }

        public RegionModel GetSuperAdminDefaultRegion(int saID)
        {
            RegionModel result = null;

            var cities = context.SACities.Include("MCity").Include("MCity.MProvince").Include("MCity.MProvince.MRegion").Where(t => t.SAdminID == saID);

            foreach (var city in cities)
            {
                if (city != null)
                {
                    if (city.MCity != null)
                    {
                        if (city.MCity.MProvince != null)
                        {
                            if (city.MCity.MProvince.MRegion != null)
                            {
                                result = new RegionModel()
                                {
                                    RegionId = city.MCity.MProvince.MRegion.RegionID,
                                    RegionCode = city.MCity.MProvince.MRegion.RegionCode,
                                    RegionName = city.MCity.MProvince.MRegion.RegionName
                                };
                            }
                        }
                    }
                }
            }
            
            return result;
        }
    }
}
