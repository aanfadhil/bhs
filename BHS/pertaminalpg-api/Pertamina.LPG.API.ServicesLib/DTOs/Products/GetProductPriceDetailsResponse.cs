﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Product
{
    public class GetProductPriceDetailsResponse : ResponseDto
    {
        public ProductPriceDetailsDto product_details { get; set; }

    }
}
