﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBG.Shared.Exceptions
{
    /// <summary>
    /// Defines the severity level of an exception
    /// </summary>
    public enum ExceptionSeverity
    {
        Ignore,
        Warning,
        Normal,
        Major = 51,
        Critical,
        Blocker = 101
    }

    public enum ExceptionPublishMode
    {
        File,
        Database,
        Email,
        System
    }
}
