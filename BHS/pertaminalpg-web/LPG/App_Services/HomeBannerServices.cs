﻿using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Configuration;
using Microsoft.ApplicationInsights;

namespace LPG.App_Services
{
    public class HomeBannerServices : ServiceBase, IHomeBannerServices
    {
        public HomeBannerServices(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<HomeBannerModel> ListHomeBanner()
        {
            var homebannerList = new List<HomeBannerModel>();
            string filepath = ConfigurationManager.AppSettings["DefaultPath_promotion"].ToString();
            using (var context = new lpgdevEntities())
            {
                var data = from homebanner in context.PromoBanners
                           join user in context.SuperAdmins
                           on homebanner.UpdatedBy equals user.SAdminID
                           orderby homebanner.BannerID descending, homebanner.CreatedDate descending
                           select new HomeBannerModel
                           {
                               HomeBannerId = homebanner.BannerID,
                               CategoryId = homebanner.Category,
                               Caption = homebanner.Caption,
                               BannerImage = (homebanner.BannerImage != "") ? (filepath + homebanner.BannerImage) : "",
                               LastModifiedDate = homebanner.UpdatedDate,
                               IsActive = homebanner.StatusId,
                               UpdatedUser = user.FullName
                           };
                homebannerList = data.ToList();
            }

            return homebannerList;
        }

        //public List<HomeBannerModel> getActiveBanners()
        //{
        //    var homebannerList = new List<HomeBannerModel>();
        //    using (var context = new lpgdevEntities())
        //    {
        //        var data = from homebanner in context.PromoBanners
        //                   where homebanner.BannerID == homebanner.BannerID
        //                   && homebanner.StatusId==true


        //                   select new HomeBannerModel
        //                   {
        //                       HomeBannerId = homebanner.BannerID,
        //                       Caption = homebanner.Caption,
        //                       CategoryId=homebanner.Category,
        //                       BannerImage=homebanner.BannerImage,
        //                       IsActive = homebanner.StatusId
        //                   };
        //        if (data.Count()>0)
        //        homebannerList = data.ToList();
        //    }

        //    return homebannerList;
        //}

        public HomeBannerModel saveBannerDetails(HomeBannerModel entity, string Cmd, int loggedBy)
        {

            try
            {

                using (var context = new lpgdevEntities())
                {
                    bool Status = false;
                    if (Cmd == "btnSaveAndPublish")
                    {
                        Status = true;
                    }
                    else if (Cmd == "btnSaveUnpublish")
                    {
                        Status = false;
                    }

                    var Data = context.PromoBanners.Where(r => r.BannerID == entity.HomeBannerId).FirstOrDefault();
                    if (Data != null)
                    {

                        Data.StatusId = Status;
                        Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        Data.UpdatedBy = base.UserId.ToShort();
                        Data.Caption = entity.Caption;
                        Data.Category = (int) entity.CategoryId;
                        Data.BannerImage = (entity.BannerImage != "") ? Path.GetFileName(entity.BannerImage) : "";


                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggedBy);
                    }
                    else
                    {
                        PromoBanner objLocal = new PromoBanner();

                        objLocal.BannerID = entity.HomeBannerId;
                        objLocal.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        objLocal.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        objLocal.Caption = entity.Caption;
                        objLocal.Category = entity.CategoryId;
                        objLocal.BannerImage = (entity.BannerImage != "") ? Path.GetFileName(entity.BannerImage) : "";
                        objLocal.CreatedBy = base.UserId.ToShort();
                        objLocal.UpdatedBy = base.UserId.ToShort();
                        objLocal.StatusId = Status;
                        context.PromoBanners.Add(objLocal);
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggedBy);
                    }
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public HomeBannerModel getHomeBannerDetails(int homeBannerId)//, LanguageEnum langEnum)
        {
            string filepath = ConfigurationManager.AppSettings["DefaultPath_promotion"].ToString();
            using (var context = new lpgdevEntities())
            {
                var rtns = (from homebanner in context.PromoBanners
                            where homebanner.BannerID == homeBannerId
                            select new HomeBannerModel()
                            {
                                HomeBannerId = homebanner.BannerID,
                                Caption = homebanner.Caption,
                                BannerImage = (homebanner.BannerImage != "") ? (filepath + homebanner.BannerImage) : "",
                                CategoryId = homebanner.Category,
                                IsActive = homebanner.StatusId

                            }).FirstOrDefault();

                return rtns;
            }
        }

        public bool deleteHomBanner(int id, int loggedBy)
        {
            bool status = false;
            using (var context = new lpgdevEntities())
            {
                var data = context.PromoBanners.Where(r => r.BannerID == id).FirstOrDefault();
                if (data != null)
                {
                    context.PromoBanners.Remove(data);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", loggedBy);
                    status = true;
                }
            }
            return status;
        }

        /// <summary>
        /// ADD : Activity Log table
        /// </summary>
        /// <param name="action">Add/Update/Delete</param> 
        /// <param name="action_item">Page Name</param>
        /// <param name="loggedBy">Loggedin UserID</param>
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = "Banner Adv";
            _activityService.AddActivityLog(actModel, "Banner Adv");
        }
    }
}
