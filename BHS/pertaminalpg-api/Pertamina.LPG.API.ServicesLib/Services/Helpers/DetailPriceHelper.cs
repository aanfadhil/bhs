﻿using Pertamina.LPG.API.DAOs;
using Pertamina.LPG.API.DTOs.Product;
using Pertamina.LPG.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.Services.Helpers
{
    public class DetailPriceHelper
    {
        private const int MAX_LIMIT_TUBE_PROMO = 2000;
        private const int MAX_LIMIT_REFILL_PROMO = 10000;

        public static void CopyFromEntity(DetailPriceDto detailPriceDto, DetailPrice detailPrice)
        {
            detailPriceDto.product_id = detailPrice.ProductID;
            detailPriceDto.product_title = detailPrice.Product.ProductName;
            detailPriceDto.product_image = detailPrice.Product.ProductImage;
            detailPriceDto.price_of_the_tube = detailPrice.TubeNormalPrice;
            detailPriceDto.Price_refil = detailPrice.RefillNormalPrice;
        }

        public static void CopyFromEntity(ProductPriceDetailsDto detailPriceDto, DetailPrice detailPrice)
        {
            detailPriceDto.product_id = detailPrice.Product.ProdID; // product_description
            detailPriceDto.product_name = detailPrice.Product.ProductName;
            detailPriceDto.position = detailPrice.Product.Position;
            detailPriceDto.product_image = detailPrice.Product.ProductImage; //ImagePathService.productImagePath + product.ProductImage;
            detailPriceDto.product_image_details = detailPrice.Product.ProductImageDetails; //ImagePathService.productImagePath + product.ProductImageDetails;
            detailPriceDto.tube_price = detailPrice.TubeNormalPrice;
            detailPriceDto.refill_price = detailPrice.RefillNormalPrice;
            detailPriceDto.shipping_price = detailPrice.Product.ShippingPrice;
            detailPriceDto.shipping_promo_price = detailPrice.Product.ShippingPromoPrice;
            if (detailPrice.Product.ProductExchanges.Count > 0)
            {
                detailPriceDto.has_exchange = true;
                detailPriceDto.exchange = new ExchangeDto[detailPrice.Product.ProductExchanges.Count];
                var prdExchanges = detailPrice.Product.ProductExchanges.ToList();
                for (int i = 0; i < detailPrice.Product.ProductExchanges.Count; i++)
                {
                    ExchangeDto exDto = new ExchangeDto();
                    exDto.exchange_id = prdExchanges[i].PrExID;
                    exDto.exchange_with = prdExchanges[i].ExchangeWith;
                    exDto.exchange_quantity = prdExchanges[i].ExchangeQuantity;
                    exDto.exchange_price = prdExchanges[i].ExchangePrice.Value + detailPrice.RefillNormalPrice;
                    exDto.exchange_promo_price = prdExchanges[i].ExchangePromoPrice + detailPrice.RefillPromoPrice;
                    detailPriceDto.exchange[i] = exDto;
                }
            }


            if (IsOnProductPromo(detailPriceDto.product_id))
            {
                detailPriceDto.tube_promo_price = detailPrice.TubePromoPrice;
            }
            else
            {
                detailPriceDto.tube_promo_price = detailPrice.TubeNormalPrice;
            }

            if (IsOnRefillPromo(detailPriceDto.product_id))
            {
                detailPriceDto.refill_promo_price = detailPrice.RefillPromoPrice;
            }
            else
            {
                detailPriceDto.refill_promo_price = detailPrice.RefillNormalPrice;
            }

        }



        public static void CopyFromEntity(GetProductPriceByCityResponse dto, Reminder reminder)
        {
            dto.reminder_description = reminder == null ? string.Empty : reminder.Description;
            dto.reminder_id = reminder == null ? 0 : reminder.RmdrID;
            dto.reminder_image = reminder == null ? string.Empty : ImagePathService.reminderImagePath + reminder.ReminderImage;
        }

        public static void CopyFromEntity(ProductPriceDetailsDto detailPriceDto, Product products)
        {
            detailPriceDto.product_id = products.ProdID; // product_description
            detailPriceDto.product_name = products.ProductName;
            detailPriceDto.position = products.Position;
            detailPriceDto.product_image = products.ProductImage; //ImagePathService.productImagePath + product.ProductImage;
            detailPriceDto.product_image_details = products.ProductImageDetails; //ImagePathService.productImagePath + product.ProductImageDetails;
            detailPriceDto.tube_price = 0;
            detailPriceDto.tube_promo_price = 0;
            detailPriceDto.refill_price = 0;
            detailPriceDto.refill_promo_price = 0;
            detailPriceDto.shipping_price = products.ShippingPrice;
            detailPriceDto.shipping_promo_price = products.ShippingPromoPrice;
            if (products.ProductExchanges.Count > 0)
            {
                detailPriceDto.has_exchange = true;
                detailPriceDto.exchange = new ExchangeDto[products.ProductExchanges.Count];
                var prdExchanges = products.ProductExchanges.ToList();
                for (int i = 0; i < products.ProductExchanges.Count; i++)
                {
                    ExchangeDto exDto = new ExchangeDto();
                    exDto.exchange_id = prdExchanges[i].PrExID;
                    exDto.exchange_with = prdExchanges[i].ExchangeWith;
                    exDto.exchange_quantity = prdExchanges[i].ExchangeQuantity;
                    exDto.exchange_price = prdExchanges[i].ExchangePrice.Value;
                    exDto.exchange_promo_price = prdExchanges[i].ExchangePromoPrice;
                    detailPriceDto.exchange[i] = exDto;
                }
            }
        }

        private static bool IsOnProductPromo(int productId)
        {
            using (OrderDao dao = new OrderDao())
            {
                List<OrderDetail> orderList = dao.GetOrderListByProduct(productId);

                if (orderList != null)
                {
                    int product_sold = 0;

                    foreach (OrderDetail ord in orderList)
                    {
                        product_sold += ord.Quantity;
                    }

                    if (product_sold < MAX_LIMIT_TUBE_PROMO)
                        return true;
                    else
                        return false;

                }
                return false;
            }
        }


        private static bool IsOnRefillPromo(int productId)
        {
            using (OrderDao dao = new OrderDao())
            {
                List<OrderDetail> orderList = dao.GetOrderListByProduct(productId);

                if (orderList != null)
                {
                    int product_sold = 0;

                    foreach (OrderDetail ord in orderList)
                    {
                        product_sold += ord.RefillQuantity;
                    }

                    if (product_sold < MAX_LIMIT_REFILL_PROMO)
                        return true;
                    else
                        return false;

                }
                return false;
            }
        }
    }
}
