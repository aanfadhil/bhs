﻿using System;
using System.Collections.Generic;

namespace LPG.Models
{
    public class PromoBannerModel
    {
        public int HomeBannerId { get; set; }

        public string BannerName { get; set; }

        public string BannerImage { get; set; }

        public string BannerText { get; set; }

        public int LanguageId { get; set; }

        public int SortOrder { get; set; }

        public bool IsActive { get; set; }

        public DateTime LastModifiedDate { get; set; }
        public List<HomeBannerModel> HomeBannerList { get; set; }

        public string ErrorMessage { get; set; }

        public bool IsHomeBannerExists { get; set; }
    }
}