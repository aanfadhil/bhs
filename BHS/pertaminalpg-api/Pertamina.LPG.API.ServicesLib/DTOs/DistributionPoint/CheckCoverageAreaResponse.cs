﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.DistributionPoint
{
    public class CheckCoverageAreaResponse : ResponseDto
    {
        public bool on_coverage_area { get; set; }

    }
}
