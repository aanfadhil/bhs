﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class FAQController : AbstractController<IFaqService>
    {
        SessionManagement sessionManagement;

        public FAQController(IFaqService _serviceObject) : base(_serviceObject)
        {
        }

        //FaqService serviceObject = new FaqService();
        // GET: FAQ
        public ActionResult AddFaq(int? id)
        {
            try
            {
                FaqModel model = new FaqModel();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {


                    var convid = id ?? 0;
                    if (convid > 0)
                    {

                        var vm = serviceObject.getFaqDetails(convid);
                        if (vm != null)
                        {
                            BindPositions(vm.FaqId);
                            return View(vm);
                        }
                        else
                        {
                            BindPositions(null);
                            return View(model);
                        }
                    }
                    else
                    {
                        BindPositions(null);
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Login");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
        }

        public ActionResult ListFAQ()
        {
            var vm = new FaqModel();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.FaqList = serviceObject.ListFaq().Where(f => f.StatusId).OrderBy(o => o.Posisi).ToList();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult AddFaq(FaqModel vm)
        {
            if (!ModelState.IsValid)
            {
                BindPositions(null);
                return View(vm);
            }

            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.StatusId = true;

                    serviceObject.saveFaqDetails(vm, adminSessionInfo.UserId);
                    //return RedirectToAction("ListFAQ", "FAQ", this.AddToastMessage("Success", "Faq Added Succesfuly", ToastType.Success));
                    return RedirectToAction("ListFAQ", "FAQ");


                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error", this.AddToastMessage("Error", "Error", ToastType.Error));
            }
        }

        public ActionResult DisableFaq(int? id)
        {
            CommenExecutionStatus EntityModel = new CommenExecutionStatus();
            if (id != null & id > 0)
            {

                try
                {
                    sessionManagement = new SessionManagement();
                    AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                    ViewBag.Role = adminSessionInfo.UserRole;
                    if (adminSessionInfo != null)
                    {

                        EntityModel.ExecuteStatus = serviceObject.deleteFaqDetails(id.ToInt(), adminSessionInfo.UserId);
                        EntityModel.isSessionAlive = true;
                    }
                    else
                    {
                        EntityModel.ExecuteStatus = false;
                        EntityModel.isSessionAlive = false;
                    }

                }
                catch
                {
                    EntityModel.ExecuteStatus = false;
                    EntityModel.isSessionAlive = false;
                }
            }

            return RedirectToAction("ListFAQ", "FAQ");
        }

        public void BindPositions(int? FaqId)
        {
            var CurrentList = new List<SelectListItem>();
            var positionlist = new List<SelectListItem>();
            for (var i = 1; i < 100; i++)
                positionlist.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            CurrentList = serviceObject.GetAvailablePositions(FaqId);
            positionlist.RemoveAll(x => CurrentList.Exists(y => y.Value == x.Value));
            ViewBag.vbPositionlist = positionlist;
        }
    }
}
