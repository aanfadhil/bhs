﻿using Microsoft.ApplicationInsights;
using Pertamina.LPG.API.DAOs;
using Pertamina.LPG.API.DTOs;
using Pertamina.LPG.API.DTOs.Orders;
using Pertamina.LPG.API.Models;
using Pertamina.LPG.API.Services.Helpers;
using Pertamina.LPG.API.Utils;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.Hosting;
using Hangfire;

namespace Pertamina.LPG.API.Services
{
    public class OrdersServices
    {
        #region Telemetry
        private TelemetryClient tm = new TelemetryClient();



        #endregion
        #region Const
        //TODO: place correct ids here
        public const string DAY_NAME_SUNDAY = "Sunday";
        public const short ID_ORDER_RECEIVED = 1,
             ID_ORDER_PROCESSED = 2,
             ID_ORDER_OUT_FOR_DELIVERY = 3,
             ID_ORDER_DELIVERED = 4,
             ID_ORDER_CANCELLED_BY_CONSUMER = 5,
             ID_ORDER_CANCELLED_BY_SYSTEM = 6,
             ID_ORDER_CANCELLED_BY_SUSER = 7,
             ID_ORDER_CANCELLED_BY_HQ_SUSER = 8,
             ID_ORDER_PROCESSED_BUT_NOT_DELIVERED = 9
            ;

        private const int MAX_LIMIT_TUBE_PROMO = 2000;
        private const int MAX_LIMIT_REFILL_PROMO = 10000;


        public const short DELIVERY_STATUS_ASSIGNED = 1,
            DELIVERY_STATUS_OUTFORDELIVERY = 2,
            DELIVERY_STATUS_CANCELLED = 3,
            DELIVERY_STATUS_ONTIMEDELIVERY = 4,
            DELIVERY_STATUS_LATEDELIVERY = 5,
            DELIVERY_STATUS_EARLYDELIVERY = 6;
        public const string APPSETTING_MSG_TO_ASSIGNED_DRIVER = "MsgToAssignedDriver";
        public const string APPSETTING_TITLE_FOR_ASSIGNED_DRIVER = "TitleForAssignedDriver";
        public const string APPSETTING_MSG_TO_AGENT_ADMINS_IN_RANGE = "MsgToAgentAdminsInRange";
        public const string APPSETTING_TITLE_FOR_AGENT_ADMINS_IN_RANGE = "TitleForAgentAdminsInRange";
        public const string APPSETTING_MSG_TO_SUPER_ADMIN = "MsgToSuperAdmin";
        public const string APPSETTING_MSG_TO_AUTO_ADMIN = "MsgToAutoAgentAdmin";
        public const string APPSETTING_TITLE_TO_AUTO_ADMIN = "TitleToAutoAgentAdmin";
        public const string APPSETTING_TITLE_FOR_SUPER_ADMIN = "TitleForSuperAdmin";
        public const string APPSETTING_MSG_TO_CONSUMER_FOR_RATING = "MsgToConsumerForRating";
        public const string APPSETTING_TITLE_FOR_CONSUMER_TO_RATE = "TitleForConsumerToRate";
        public const string APPSETTING_MSG_FOR_CONSUMER_AT_SADMIN_CANCEL = "MsgToConsumerAtSAdminCancel";
        public const string APPSETTING_TITLE_FOR_CONSUMER_AT_SADMIN_CANCEL = "TitleForConsumerAtSAdminCancel";
        public const string APPSETTING_TITLE_FOR_SE_AT_EXPIRED = "TitleForSuperAdminExpired";
        public const string APPSETTING_MSG_FOR_SE_AT_EXPIRED = "MsgForSuperAdminExpired";

        public const string APPSETTING_USER_ROLE_AGEN_ADMIN = "admin agen";
        public const string APPSETTING_USER_ROLE_CONSUMER = "pelanggan";
        public const string APPSETTING_USER_ROLE_DRIVER = "driver";
        public const string APPSETTING_USER_ROLE_SA = "SE";
        public const string ORDER_NOTIFICATION_TEMPLATE = "An order has been posted in your teritory; order Id = {0}";
        public const string ORDER_ALLOCATION_TEMPLATE = "Kindly deliver the following order; order Id = {0}";
        public const int FOLLOWUP_DELAY = 5 * 60 * 1000;
        public const int AUTOASSIGN_DELAY = 5 * 60 * 1000;
        public const int SE_DELAY = 10 * 60 * 1000;
        public const int ORDER_EXPIRE_TIME = 20 * 60 * 1000;

        const double DAYS_TO_LIST = 3;
        public const int MAX_DELIVERY_PER_SLOT = 10;
        #endregion

        #region Enum
        public enum OrderType
        {
            OrderApp, OrderTelp
        }
        #endregion

        #region Private Variables
        private UserServices _userServices = new UserServices();
        private ProductServices productServices = new ProductServices();
        //private OrdersServices _ordersservices = new OrdersServices();
        #endregion

        #region Public Methods       

        public GetActiveOrderCountResponse GetActiveOrderCount(GetActiveOrderCountRequest request)
        {

            GetActiveOrderCountResponse response = new GetActiveOrderCountResponse();
            try
            {
                if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    int orderCount = dao.GetOrderCountFor(request.user_id);
                    response.active_orders = new ActiveOrdersDto { user_id = request.user_id, active_order_count = orderCount };
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = orderCount > 0 ?
                        MessagesSource.GetMessage("active.orders") :
                        MessagesSource.GetMessage("no.active.orders");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                tm.TrackException(ex);
            }

            return response;
        }


        public ResponseDto SubmitReview(SubmitReviewRequest request)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                ConsumerReview review = new ConsumerReview();
                review.Comments = request.comments;
                review.Rating = request.rating;
                review.ReasonID = review.Rating <= 3 ? request.reason_id : (int?)null;
                review.ConsID = request.user_id;
                review.DrvrID = request.driver_id;
                review.OrdrID = request.order_id;
                using (ConsumerReviewDao dao = new ConsumerReviewDao())
                {

                    dao.Insert(review);
                }
                response.code = 0;
                response.has_resource = 1;
                response.message = MessagesSource.GetMessage("review.posted");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        public PlaceOrderResponse PlaceOrder(PlaceOrderRequest request)
        {
            PlaceOrderResponse response = new PlaceOrderResponse();
            try
            {
                if (request.exchange == null)
                {
                    request.exchange = (new List<ExchangeDto>()).ToArray();
                }

                if (request.products == null)
                {
                    request.products = (new List<ProductsDto>()).ToArray();
                }

                if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }

                if (request.delivery_date <= DateTime.MinValue)
                {
                    MakeInvalidDeliveryDateFormat(response);
                    return response;
                }

                if (request.products.Length <= 0 && request.exchange.Length <= 0)
                {
                    response.code = 1;
                    response.has_resource = 0;
                    response.httpCode = HttpStatusCode.InternalServerError;
                    response.message = "Anda tidak memilih produk";
                    return response;
                }

                var total = request.products.Sum(t => t.quantity + t.refill_quantity);
                var totalExchange = request.exchange.Sum(t => t.exchange_quantity);


                if (total + totalExchange <= 0)
                {
                    response.code = 1;
                    response.has_resource = 0;
                    response.httpCode = HttpStatusCode.InternalServerError;
                    response.message = "Anda tidak memilih produk";
                    return response;
                }

                if (!string.IsNullOrEmpty(request.voucher))
                {
                    var promoVoucher = CheckVoucher(request);

                    if (!promoVoucher.is_voucher_valid)
                    {
                        response.code = 1;
                        response.has_resource = 0;
                        response.httpCode = HttpStatusCode.InternalServerError;
                        response.message = promoVoucher.keterangan_voucher;
                        return response;
                    }

                    response.keterangan_voucher = promoVoucher.keterangan_voucher;
                    response.is_voucher_valid = promoVoucher.is_voucher_valid;
                }

                using (AgencyDao agentDao = new AgencyDao())
                {
                    using (UserDao usrdao = new UserDao())
                    {
                        var consumer = _userServices.GetAuthUser(usrdao, request.user_id, request.auth_token, true);
                        if (consumer == null)
                        {
                            _userServices.MakeNouserResponse(response);
                            return response;
                        }

                        foreach (var item in consumer.ConsumerAddresses)
                        {
                            if (item.AddrID == request.address_id)
                            {
                                item.IsDefault = true;
                            }
                            else
                                item.IsDefault = false;
                        }
                        usrdao.Update(consumer);
                        ConsumerAddress consAddr = consumer.ConsumerAddresses.Where(a => a.AddrID == request.address_id).First();


                        var dps = agentDao.GetDistributionAgencies(consAddr.Latitude, consAddr.Longitude);

                        if (dps.Count <= 0)
                        {
                            response.code = 1;
                            response.has_resource = 0;
                            response.httpCode = HttpStatusCode.InternalServerError;
                            response.message = MessagesSource.GetMessage("order.outofrange");
                            return response;
                        }
                    }
                }

                using (OrderDao dao = new OrderDao())
                {
                    Order ord = new Order();

                    UpdatePlaceOrderRequest(request); // To update request from mobile, Reason : not passing expected values from mobile

                    PopulateOrder(ord, request.user_id, request.address_id, request.city_name, request.time_slot_id);

                    //Delivery date can be calculated from the time slot. We'll take the upper bound of the slot
                    //var timeSlot = TimeslotService.GetTimeSlotById(request.time_slot_id);
                    //var timeSlotEndDate = request.delivery_date + timeSlot.EndTine;
                    //We set delivery date to be at most the end of the delivery slot

                    ord.DeliveryDate = request.delivery_date;

                    AddProductsToOrder(ord, request.products);

                    if (request.has_exchange)
                    {
                        if (request.exchange != null)
                        {
                            foreach (ExchangeDto exdto in request.exchange)
                            {
                                OrderPrdocuctExchange opex = new OrderPrdocuctExchange();
                                ProductExchange pxg = dao.FindProductExchangeById(exdto.exchange_id);
                                if (pxg == null)
                                {
                                    //MakeInvalidExchangeInputResponse(response);
                                    response.code = 1;
                                    response.has_resource = 0;
                                    response.message = "Cannot find exchange with id " + exdto.exchange_id;

                                    return response;
                                }
                                opex.ProdID = pxg.ProdID;
                                opex.ExchangePrice = exdto.exchange_price;
                                opex.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                                opex.ExchangePromoPrice = exdto.exchange_promo_price * exdto.exchange_quantity;
                                opex.ExchangeQuantity = exdto.exchange_quantity;
                                opex.ExchangeWith = exdto.exchange_with;
                                opex.StatusId = true;//TODO
                                opex.SubTotal = exdto.exchange_price * exdto.exchange_quantity;
                                //ord.GrandTotal -= (opex.ExchangePrice - opex.ExchangePromoPrice) * opex.ExchangeQuantity;                              

                                var product = request.products.Where(p => p.product_id == pxg.ProdID).FirstOrDefault();
                                if (product == null)
                                {
                                    MakeInvalidExchangeInputResponse(response);
                                    response.code = 1;
                                    response.has_resource = 0;
                                    response.message = "Cannot find product with id " + pxg.ProdID + " in the products array";
                                    return response;

                                }
                                opex.TotalAmount = opex.SubTotal + opex.ExchangePromoPrice + (product.shipping_cost * exdto.exchange_quantity) + (product.shipping_promo * exdto.exchange_quantity);
                                ord.OrderPrdocuctExchanges.Add(opex);
                                ord.ShippingCharge += (product.shipping_cost * exdto.exchange_quantity);
                                ord.PromoShipping += (product.shipping_promo * exdto.exchange_quantity);
                                ord.NumberOfProducts += exdto.exchange_quantity;
                                ord.ExchangeSubTotal += opex.SubTotal;
                                ord.PromoExchange += opex.ExchangePromoPrice;
                                ord.GrandTotal += opex.TotalAmount;
                            }
                        }
                        else
                        {
                            MakeInvalidExchangeInputResponse(response);
                            return response;
                        }
                    }
                    ord.StatusID = ID_ORDER_RECEIVED;// 1;
                    //dao.Insert(ord);
                    //AgencyOrderAlertService.NotifyAgencies(ord.OrdrID);
                    bool IsAgencyAssigned = false;
                    bool DoesUserHaveCoordinates = false;

                    List<OrderAllocationLog> ordAllo = new List<OrderAllocationLog>();
                    using (AgencyDao agentDao = new AgencyDao())
                    {
                        //var dps1 = agentDao.GetDistributionPointsBetween(Convert.ToString(lowLat),
                        //     Convert.ToString(upLat),
                        //     Convert.ToString(loLon),
                        //     Convert.ToString(upLon));
                        using (UserDao usrdao = new UserDao())
                        {
                            var consumer = _userServices.GetAuthUser(usrdao, request.user_id, request.auth_token, true);
                            if (consumer == null)
                            {
                                _userServices.MakeNouserResponse(response);
                                return response;
                            }

                            foreach (var item in consumer.ConsumerAddresses)
                            {
                                if (item.AddrID == request.address_id)
                                {
                                    item.IsDefault = true;
                                }
                                else
                                    item.IsDefault = false;
                            }
                            usrdao.Update(consumer);
                            ConsumerAddress consAddr = consumer.ConsumerAddresses.Where(a => a.AddrID == request.address_id).First();
                            //consAddr.IsDefault = true;



                            //if (consAddr.Latitude == null || consAddr.Latitude == "" || consAddr.Longitude == null || consAddr.Longitude == "")
                            //{
                            //    DoesUserHaveCoordinates = false;
                            //}
                            //else
                            //{
                            DoesUserHaveCoordinates = true;


                            var dps = agentDao.GetDistributionAgencies(consAddr.Latitude, consAddr.Longitude);

                            //if (dps != null && dps.Count > 0)
                            //{
                            ordAllo = dps.Select(x => new OrderAllocationLog()
                            {
                                AgadmID = x.AgadmID,
                                DbptID = x.DbptID,
                                Distance = Convert.ToDecimal(x.DPDistance),
                                UpdatedBy = request.user_id,
                                CreatedBy = Convert.ToInt16(request.user_id),
                                TimeSlotAvailable = (TimeslotService.CheckTimeslotFree(ord.DeliveryDate, consAddr.Latitude, consAddr.Longitude, ord.DeliverySlotID, x.AgenID)), //true,
                                AllocationStatus = 0,
                                AssignmentType = 1,
                                CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"),
                                UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"),
                            }).ToList();

                            if (!string.IsNullOrEmpty(request.voucher))
                            {
                                var promoVoucher = CheckVoucher(request);

                                if (promoVoucher.is_voucher_valid)
                                {

                                    var promoVoucherDao = new PromoVoucherDao();

                                    var promoObj = promoVoucherDao.GetByVoucher(request.voucher);

                                    if (ord.GrandTotal + promoObj.Potongan < 0)
                                    {
                                        ord.GrandTotal = 0;
                                    }
                                    else
                                    {
                                        ord.GrandTotal += promoVoucher.potongan;
                                    }

                                    ord.PotonganVoucher = promoVoucher.potongan;
                                    ord.PromoID = promoObj.PromoID;
                                    ord.Voucher = request.voucher;
                                    ord.IssuedToSE = 0;

                                    response.is_voucher_valid = promoVoucher.is_voucher_valid;
                                    response.keterangan_voucher = promoVoucher.keterangan_voucher;
                                }
                            }



                            dao.Insert(ord, ordAllo);

                            ord = dao.FindById(ord.OrdrID, true);

                            ProductDao proDao = new ProductDao();

                            //AgencyOrderAlertService.NotifyAgencies(ord.OrdrID);

                            using (AgentAdminDao agentAdminDao = new AgentAdminDao())
                            {
                                foreach (var item in ordAllo)
                                {
                                    AgentAdmin agentAdmin = agentAdminDao.FindById(item.AgadmID);
                                    //TODO: Check if AgentAdmin exists
                                    int orderCount = dao.GetAgentOrderCount(agentAdmin.AgadmID, 1);
                                    ReadAndSendPushNotification(APPSETTING_MSG_TO_AGENT_ADMINS_IN_RANGE, APPSETTING_TITLE_FOR_AGENT_ADMINS_IN_RANGE,
                                        agentAdmin.AppToken, ord.OrdrID, 0, orderCount, PushMessagingService.APPSETTING_APPLICATION_ID_AADMIN,
                                        PushMessagingService.APPSETTING_SENDER_ID_AADMIN, (int)PushMessagingService.PushType.TypeOne,
                                        agentAdmin.AgadmID, agentAdmin.AgentAdminName, agentAdmin.MobileNumber, APPSETTING_USER_ROLE_AGEN_ADMIN
                                        );

                                    IsAgencyAssigned = true;
                                }
                            }
                            //}
                            //else
                            //{
                            //    //TODO: Notify SuperAdmin SuperUser on unassigned order

                            //}
                            //}
                        }
                    }

                    try
                    {
                        dao.UpdateSummary(ord.OrdrID);
                    }
                    catch
                    {

                    }

                    var kvDao = new KeyValueDao();

                    int LAST_ORDER = kvDao.GetOneByKey("LAST_ORDER").Value.ToInt();
                    int BEGIN_ORDER = kvDao.GetOneByKey("BEGIN_ORDER").Value.ToInt();


                    //commented at 2018-08-19T18:59 , moved to AllocateOrderToPrefferedAgent
                    //HostingEnvironment.QueueBackgroundWorkItem(t => CancelExpiredOrder(ord.OrdrID));

                    double lat = Convert.ToDouble(ord.ConsumerAddress.Latitude, CultureInfo.InvariantCulture), lon = Convert.ToDouble(ord.ConsumerAddress.Longitude, CultureInfo.InvariantCulture);
                    var currentLocalTime = TimeZoneServices.GetLocalDateTime(lat, lon, DateTime.UtcNow);

                    if (currentLocalTime.Hour > LAST_ORDER || currentLocalTime.Hour < BEGIN_ORDER || (currentLocalTime.Hour == LAST_ORDER && currentLocalTime.Minute > 0 && currentLocalTime.Second > 0))
                    {
                        var besokpagi = (currentLocalTime.Hour <= BEGIN_ORDER) ? currentLocalTime : currentLocalTime.AddDays(1);

                        if (besokpagi.DayOfWeek.ToString() == DAY_NAME_SUNDAY)
                        {
                            besokpagi.AddDays(1);
                        }

                        besokpagi = new DateTime(besokpagi.Year, besokpagi.Month, besokpagi.Day, BEGIN_ORDER, 5, 0);

                        var tsp = besokpagi - currentLocalTime;

                        Hangfire.BackgroundJob.Schedule(() => AllocateOrderToPrefferedAgentOffTime(ord.OrdrID), tsp);
                    }
                    else
                    {
                        HostingEnvironment.QueueBackgroundWorkItem(t => AllocateOrderToPrefferedAgent(ord.OrdrID));
                    }




                    response.code = 0;
                    response.has_resource = 1;
                    response.order_details = new PlaceOrderRespOrderDto
                    {
                        invoice_number = ord.InvoiceNumber,
                        order_id = ord.OrdrID,
                        order_status = ord.MOrderStatu.OrderStatus
                    };
                    response.message = MessagesSource.GetMessage("order.placed");
                    //}
                    //else
                    //{

                    //    if (!DoesUserHaveCoordinates)
                    //    {
                    //        response.code = 1;
                    //        response.has_resource = 0;
                    //        response.message = MessagesSource.GetMessage("order.error.no_address");
                    //    }

                    //    if (!IsAgencyAssigned)
                    //    {

                    //        response.code = 1;
                    //        response.has_resource = 0;
                    //        response.message = MessagesSource.GetMessage("order.error.no_timeslot");
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                tm.TrackException(ex);
            }

            return response;
        }


        public void TestSchedule()
        {
            var currentLocalTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

            var later = currentLocalTime.AddMinutes(2);

            var tsp = later - currentLocalTime;

            Hangfire.BackgroundJob.Schedule(() => AllocateOrderToPrefferedAgentOffTime(9445), tsp);
        }


        public GetReasonsResponseDto GetReasons(AuthBase request)
        {
            GetReasonsResponseDto response = new GetReasonsResponseDto();
            response.service_reason_rating = new List<ReasonDto>();
            using (ConsumerReviewDao crdao = new ConsumerReviewDao())
            {
                response.service_reason_rating = crdao.GetReviewReason().Select(t => new ReasonDto()
                {
                    reason = t.ReasonText,
                    reason_id = t.ReasonID
                }).ToList();
            }

            return response;
        }

        public GetTimeSlotResponse GetTimeSlot(GetTimeslotRequest request)
        {
            GetTimeSlotResponse response = new GetTimeSlotResponse();
            response.has_resource = 1;
            response.message = MessagesSource.GetMessage("with.timeslots");
            try
            {
                if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                TimeSpan tTomm = new TimeSpan(1, 0, 0, 0);
                TimeSpan tDat = new TimeSpan(2, 0, 0, 0);
                //ConsumerAddress consumerAddr = _userServices.GetDefaultUserAddress(request.user_id);
                ConsumerAddress consumerAddr = _userServices.GetDefaultUserAddressForUser(request.user_address_id);
                if (consumerAddr == null)
                {
                    _userServices.MakeNoAddressResponse(response);
                    return response;
                }
                //TimeslotDisplayDto[] timeSlots = TimeslotService.GetTimeslotsDisplay(TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time"), consumerAddr.Latitude, consumerAddr.Longitude, -1, response);


                double lat = Convert.ToDouble(consumerAddr.Latitude, CultureInfo.InvariantCulture), lon = Convert.ToDouble(consumerAddr.Longitude, CultureInfo.InvariantCulture);
                TimeslotDisplayDto[] timeSlots = TimeslotService.GetTimeslotsDisplay(TimeZoneServices.GetLocalDateTime(lat, lon, DateTime.UtcNow), consumerAddr.Latitude, consumerAddr.Longitude, -1, response);
                var timeslotList = timeSlots.ToList();
                response.days = timeSlots.ToList();
                response.code = 0;

            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                tm.TrackException(ex);
            }

            return response;
        }

        public GetOrderListResponse GetOrderList(GetOrderListRequest request)
        {
            GetOrderListResponse response = new GetOrderListResponse();
            try
            {
                if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    List<Order> ordList = dao.GetOrderList(request.user_id, request.current_list);
                    if (ordList == null)
                    {
                        MakeNoOrderFoundResponse(response);
                        return response;
                    }
                    if (ordList.Count <= 0)
                    {
                        MakeNoOrderFoundResponse(response);
                        return response;
                    }
                    response.order_details = new OrderDto[ordList.Count];
                    for (int i = 0; i < ordList.Count; i++)
                    {
                        OrderDto odDto = new OrderDto();
                        OrdersHelper.CopyFromEntity(odDto, ordList[i]);
                        response.order_details[i] = odDto;
                    }

                    response.order_details = response.order_details.OrderByDescending(t => t.order_id).ToArray();

                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("ordr.listed");
                }

            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                tm.TrackException(ex);
            }
            return response;
        }

        public GetOrderDetailsResponse GetOrderDetails(GetOrderDetailsRequest request)
        {
            GetOrderDetailsResponse response = new GetOrderDetailsResponse();
            try
            {
                if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    Order ord = dao.FindById(request.order_id, true);
                    OrderDetailsDto oDto = new OrderDetailsDto();
                    string agentAdminMob = ord.AgentAdmin != null ? ord.AgentAdmin.MobileNumber : string.Empty;
                    OrdersHelper.CopyFromEntity(oDto, ord, agentAdminMob, true);
                    using (ConsumerReviewDao conReviewDao = new ConsumerReviewDao())
                    {
                        if (ord.DrvrID.HasValue)
                        {
                            List<ConsumerReview> conReview = new List<ConsumerReview>();
                            conReview = conReviewDao.GetReviewByDriver(ord.DrvrID.Value);
                            oDto.driver_details.driver_rating = conReview.Count > 0 ? Convert.ToDecimal(conReview.Average(x => x.Rating)) : 0;
                        }
                    }
                    response.order_details = oDto;
                    response.code = 0;
                    response.has_resource = 0;
                    response.message = MessagesSource.GetMessage("ordr.details");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                tm.TrackException(ex);
            }

            return response;
        }

        public ResponseDto CancelOrder(CancelOrderRequest request)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {

                    Order ord = dao.FindById(request.order_id, false);

                    if (ord == null)
                    {
                        response.code = 1;
                        response.has_resource = 0;
                        response.message = MessagesSource.GetMessage("ordr.cant.cancel");
                        return response;
                    }

                    if (ord.StatusID >= 3)
                    {
                        response.code = 1;
                        response.has_resource = 0;
                        response.message = MessagesSource.GetMessage("ordr.cant.cancel");
                        return response;
                    }

                    ord.StatusID = ID_ORDER_CANCELLED_BY_CONSUMER;
                    dao.Update(ord);

                    var orderDeliveries = ord.OrderDeliveries.Where(x => x.OrdrID == ord.OrdrID);
                    if (orderDeliveries.Count() > 0)
                    {
                        OrderDelivery orderDelivery = orderDeliveries.FirstOrDefault();
                        orderDelivery.StatusId = DELIVERY_STATUS_CANCELLED;
                        dao.UpdateDelivery(orderDelivery);
                    }

                    response.code = 0;
                    response.has_resource = 0;
                    response.message = MessagesSource.GetMessage("ordr.cancelled");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                tm.TrackException(ex);
            }

            return response;
        }


        ///////Sprint 2


        public GetUnassignedOrdersResponse GetUnassignedOrders(GetUnassignedOrdersRequest request)
        {
            GetUnassignedOrdersResponse response = new GetUnassignedOrdersResponse();
            try
            {
                if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    response.active_orders = new ActiveOrdersDto();
                    response.active_orders.active_order_count = dao.GetAgentOrderCount(request.user_id, 1);
                    response.active_orders.user_id = request.user_id;
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("order.count");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                tm.TrackException(ex);
            }

            return response;
        }

        public GetAllUnassignedOrdersResponse GetAllUnassignedOrders(GetAllUnAssignedOrdersRequest request)
        {
            GetAllUnassignedOrdersResponse response = new GetAllUnassignedOrdersResponse();
            try
            {
                if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    List<Order> ordrs = dao.GetAgentOrders(request.user_id, 1);
                    response.active_orders = new ActiveOrdersDto();
                    response.active_orders.active_order_count = dao.GetAgentOrderCount(request.user_id, 1);
                    // response.active_orders.user_id = request.user_id;
                    List<OrderFullDetailsDto> ordrDtos = new List<OrderFullDetailsDto>();
                    foreach (Order order in ordrs)
                    {
                        OrderFullDetailsDto dto = new OrderFullDetailsDto();
                        OrdersHelper.CopyFromEntity(dto, order);
                        dto.driver = null;
                        ordrDtos.Add(dto);
                    }
                    response.orders = ordrDtos.OrderByDescending(t => t.order_id).ToArray();
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("ordr.listed");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                tm.TrackException(ex);
            }


            return response;
        }

        public GetDriverDetailsResponse GetDriverDetails(GetDriverDetailsRequest request)
        {
            GetDriverDetailsResponse response = new GetDriverDetailsResponse();
            try
            {
                if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response) && !_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    Order ordr = dao.FindById(request.order_id, true);
                    if (ordr != null)
                    {
                        if (ordr.OrderDeliveries.Count > 0)
                        {
                            OrderDelivery odel = ordr.OrderDeliveries.First();
                            if (odel != null)
                            {
                                Driver drv = odel.Driver;
                                DriverDetailsDto dto = new DriverDetailsDto();
                                OrdersHelper.CopyFromEntity(dto, drv);
                                response.driver_details = dto;
                                response.code = 0;
                                response.has_resource = 1;
                                response.message = MessagesSource.GetMessage("driver.details");
                                return response;
                            }
                        }
                    }
                }
                response.code = 1;
                response.has_resource = 0;
                response.message = MessagesSource.GetMessage("no.details");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                tm.TrackException(ex);
            }
            return response;
        }

        public GetDriverDetailsForReviewResponse GetDriverDetailsForReview(GetDriverDetailsRequest request)
        {
            GetDriverDetailsForReviewResponse response = new GetDriverDetailsForReviewResponse();
            try
            {
                if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response) && !_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    Order ordr = dao.FindById(request.order_id, true);
                    if (ordr != null)
                    {
                        if (ordr.OrderDeliveries.Count > 0)
                        {
                            OrderDelivery odel = ordr.OrderDeliveries.First();
                            if (odel != null)
                            {
                                Driver drv = odel.Driver;
                                OrdersHelper.CopyFromEntity(response, drv);
                                response.code = 0;
                                response.has_resource = 1;
                                response.message = MessagesSource.GetMessage("driver.details");
                                return response;
                            }
                        }
                    }
                }
                response.code = 1;
                response.has_resource = 0;
                response.message = MessagesSource.GetMessage("no.details");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
                tm.TrackException(ex);
            }
            return response;
        }


        public ConfirmOrderResponse ConfirmOrder(ConfirmOrderRequest request)
        {
            ConfirmOrderResponse response = new ConfirmOrderResponse();
            try
            {
                AgentAdmin admin = AgentAdminServices.GetAuthAdmin(request.user_id, request.auth_token, response);
                if (admin == null)
                {
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    Order ordr = dao.FindById(request.order_id);
                    if (ordr != null && ordr.StatusID == ID_ORDER_RECEIVED)
                    {
                        ordr.AgadmID = admin.AgadmID;
                        Driver drv = null;
                        using (DriverDao ddao = new DriverDao())
                        {
                            drv = ddao.FindById(request.driver_id);
                            OrderDelivery odel = new OrderDelivery
                            {
                                DrvrID = drv.DrvrID,
                                AgadmID = request.user_id,
                                CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time")
                            };

                            var timeSlot = TimeslotService.GetTimeSlotById(ordr.DeliverySlotID);
                            if (timeSlot != null)
                            {
                                var timeSlotEndDate = ordr.DeliveryDate + timeSlot.EndTine;
                                //We set delivery date to be at most the end of the delivery slot
                                odel.DeliveryDate = timeSlotEndDate;
                            }
                            else
                            {
                                odel.DeliveryDate = ordr.DeliveryDate;
                            }

                            odel.AcceptedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                            odel.StatusId = DELIVERY_STATUS_ASSIGNED;//1;
                            odel.Order = ordr;
                            ordr.OrderDeliveries.Add(odel);
                        }
                        ordr.StatusID = ID_ORDER_PROCESSED;//2;
                        int agId = drv.AgenID;
                        lock (InvoiceService.monitor)
                        {
                            string invNo = InvoiceService.GenerateInvoiceNumber(agId);
                            ordr.InvoiceNumber = invNo;
                            ordr.DrvrID = request.driver_id;
                            dao.Update(ordr);
                        }
                        ordr = dao.FindById(ordr.OrdrID, true);
                        //if (ordr.DeliveryDate.ToShortDateString() == TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").ToShortDateString())
                        //{
                        int orderCount = dao.GetAssignedOrderCount(request.driver_id, OrdersServices.ID_ORDER_PROCESSED);
                        using (TeleOrderDao teledao = new TeleOrderDao())
                        {
                            orderCount += teledao.GetAssignedOrderCount(request.driver_id, OrdersServices.ID_ORDER_PROCESSED);
                        }
                        ReadAndSendPushNotification(APPSETTING_MSG_TO_ASSIGNED_DRIVER, APPSETTING_TITLE_FOR_ASSIGNED_DRIVER, drv.AppToken, 0, 0, orderCount,
                            PushMessagingService.APPSETTING_APPLICATION_ID_DRIVER, PushMessagingService.APPSETTING_SENDER_ID_DRIVER,
                            (int)PushMessagingService.PushType.TypeOne,
                            drv.DrvrID, drv.DriverName, drv.MobileNumber, APPSETTING_USER_ROLE_DRIVER
                            );
                        //}
                        OrderFullDetailsDto dto = new OrderFullDetailsDto();
                        OrdersHelper.CopyFromEntity(dto, ordr, drv);

                        using (ConsumerReviewDao conReviewDao = new ConsumerReviewDao())
                        {
                            List<ConsumerReview> conReview = new List<ConsumerReview>();
                            conReview = conReviewDao.GetReviewByDriver(request.driver_id);
                            dto.driver.driver_rating = conReview.Count > 0 ? Convert.ToDecimal(conReview.Average(x => x.Rating)) : 0;
                        }

                        response.order_details = dto;
                        response.code = 0;
                        response.has_resource = 1;
                        response.message = MessagesSource.GetMessage("ordr.details");
                        return response;
                    }
                    else if (ordr == null)
                    {
                        response.code = 1;
                        response.has_resource = 0;
                        response.message = MessagesSource.GetMessage("no.details");
                    }
                    else if (ordr.StatusID != ID_ORDER_RECEIVED)
                    {
                        response.code = 1;
                        response.has_resource = 0;
                        response.message = "Order sudah diambil";
                    }

                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        public GetOrgOrderListResponse GetOrgOrderList(GetOrgOrderListRequest request)
        {
            GetOrgOrderListResponse response = new GetOrgOrderListResponse();
            try
            {
                if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    var orders = dao.GetAllOrdersByAgentAdmin(request.user_id);
                    if (orders != null && orders.Count > 0)
                    {
                        #region ActiveOrders
                        List<int> activeStatus = new List<int>() { ID_ORDER_PROCESSED, ID_ORDER_OUT_FOR_DELIVERY };

                        var activeOrders = orders.Where(a => activeStatus.Contains(a.StatusID)).Select
                            (
                                a => OrdersHelper.CopyFromEntity(new OrderDetailsBossDto(), a)
                            ).OrderByDescending(t => t.order_id).ToList();

                        #endregion ActiveOrders

                        #region HistoryOrders

                        List<int> historyStatus = new List<int>() { ID_ORDER_DELIVERED, ID_ORDER_PROCESSED_BUT_NOT_DELIVERED };
                        var historyOrders = orders.Where(a => historyStatus.Contains(a.StatusID)).Select
                            (
                                a => OrdersHelper.CopyFromEntity(new OrderDetailsBossDto(), a)
                            ).OrderByDescending(t => t.order_id).ToList();

                        #endregion HistoryOrders

                        response.orders = new OrdersBossDto() { active_order_count = activeOrders.Count, history_order_count = historyOrders.Count };
                        response.active_orders = activeOrders;
                        response.history_orders = historyOrders;
                    }

                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("ordr.listed");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public GetOrgOrderDetailsResponse GetOrgOrderDetails(GetOrgOrderDetailsRequest request)
        {
            GetOrgOrderDetailsResponse response = new GetOrgOrderDetailsResponse();
            try
            {
                if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                OrderType orderType;
                try
                {
                    orderType = request.order_type.ToEnumValue<OrderType>();
                }
                catch
                {
                    MakeInvalidOrderTypeResponse(response);
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    //Order ord = new Order();
                    //TeleOrder teleOrder = new TeleOrder();
                    OrderFullDetailsDto dto = new OrderFullDetailsDto();
                    if (orderType == OrderType.OrderApp)
                    {
                        Order ord = dao.GetAgentAdminOrder(request.user_id, request.order_id);
                        if (ord == null)
                        {
                            MakeInvalidOrderResponse(response);
                            return response;
                        }
                        dto = new OrderFullDetailsDto();
                        OrdersHelper.CopyFromEntity(dto, ord);
                    }
                    else if (orderType == OrderType.OrderTelp)
                    {
                        TeleOrder teleOrder = dao.GetAgentTeleOrder(request.user_id, request.order_id);
                        if (teleOrder == null)
                        {
                            MakeInvalidTeleOrderResponse(response);
                            return response;
                        }
                        dto = new OrderFullDetailsDto();
                        OrdersHelper.CopyFromEntity(dto, teleOrder);
                    }

                    response.order_details = dto;
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("ordr.details");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        public GetInvoiceDetailsResponse GetInvoiceDetails(GetInvoiceDetailsRequest request)
        {
            GetInvoiceDetailsResponse response = new GetInvoiceDetailsResponse();
            try
            {
                int userType = 0;
                if (request.is_agent_admin == 1)
                {
                    userType = (int)UserType.AgentAdmin;
                    if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                    {
                        return response;
                    }
                }
                else if (request.is_agent_admin == 0)
                {
                    userType = (int)UserType.AgentBoss;
                    if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
                    {
                        return response;
                    }
                }

                OrderType orderType;
                try
                {
                    orderType = request.order_type.ToEnumValue<OrderType>();
                }
                catch
                {
                    MakeInvalidOrderTypeResponse(response);
                    return response;
                }
                response.orders = GetOrderInvoiceOrReciept(request.user_id, orderType, request.order_id, userType);
                if (response.orders == null)
                {
                    MakeNoOrderFoundResponse(response);
                    return response;
                }
                response.code = 0;
                response.has_resource = 1;
                response.message = MessagesSource.GetMessage("invoice.details");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        public GetEReceiptResponse GetERecieptDetails(GetEReceiptRequest request)
        {
            GetEReceiptResponse response = new GetEReceiptResponse();
            try
            {
                if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                response.orders = GetOrderInvoiceOrReciept(request.user_id, OrderType.OrderApp, request.order_id, (int)UserType.Consumer);
                if (response.orders == null)
                {
                    MakeNoOrderFoundResponse(response);
                    return response;
                }
                response.code = 0;
                response.has_resource = 1;
                response.message = MessagesSource.GetMessage("ereceipt.details");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public OrderInvoiceDto GetOrderInvoiceOrReciept(int user_id, OrderType order_type, int order_id, int userType)
        {
            OrderInvoiceDto response = new OrderInvoiceDto();
            if (order_type == OrderType.OrderApp)
            {
                using (OrderDao dao = new OrderDao())
                {
                    Order ord = null;
                    switch (userType)
                    {
                        case (int)UserType.AgentBoss:
                            ord = dao.GetAgentBossOrder(user_id, order_id);
                            break;
                        case (int)UserType.AgentAdmin:
                            ord = dao.GetAgentAdminOrder(user_id, order_id);
                            break;
                        case (int)UserType.Driver:
                            ord = dao.GetDriverOrder(user_id, order_id);
                            break;
                        case (int)UserType.Consumer:
                            ord = dao.GetConsumerOrder(user_id, order_id);
                            break;
                        default:
                            ord = null;
                            break;
                    }
                    //Order ord = (isConReceipt ? dao.GetConsumerOrder(user_id, order_id) : dao.GetAgentAdminOrder(user_id, order_id));
                    if (ord == null)
                        return null;
                    OrderInvoiceDto dto = new OrderInvoiceDto();
                    OrdersHelper.CopyFromEntity(dto, ord);
                    response = dto;
                }
            }
            else if (order_type == OrderType.OrderTelp)
            {
                using (TeleOrderDao dao = new TeleOrderDao())
                {
                    TeleOrder ord = null; //dao.GetAgentOrder(user_id, order_id);
                    switch (userType)
                    {
                        case (int)UserType.AgentBoss:
                            ord = dao.GetAgentBossOrder(user_id, order_id);
                            break;
                        case (int)UserType.AgentAdmin:
                            ord = dao.GetAgentAdminOrder(user_id, order_id);
                            break;
                        case (int)UserType.Driver:
                            ord = dao.GetDriverOrder(user_id, order_id);
                            break;
                        default:
                            ord = null;
                            break;
                    }
                    if (ord == null)
                        return null;
                    OrderInvoiceDto dto = new OrderInvoiceDto();
                    TeleOrderHelper.CopyFromEntity(dto, ord);
                    response = dto;
                }
            }
            return response;
        }

        public static void PopulateOrder(Order ord, int consumerId, int addressId, string cityName, short slotId = 1)
        {
            ord.AddrID = addressId;
            ord.StatusID = ID_ORDER_RECEIVED;//1;
            ord.OrderTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            ord.OrderDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").Date;

            ord.InvoiceNumber = "";
            ord.DeliverySlotID = slotId;
            ord.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            ord.CreatedBy = consumerId;
            ord.UpdatedBy = consumerId;
            ord.UpdatedDate = ord.CreatedDate;
            ord.ConsID = consumerId;
            ord.GrandTotal = 0.0M;

            using (CityDao cityDao = new CityDao())
            {
                MCity selectedCity = cityDao.FindCityByName(cityName);
                if (selectedCity != null)
                    ord.CityId = selectedCity.ID;
            }
        }

        public static void UpdatePlaceOrderRequest(PlaceOrderRequest request)
        {
            // To update request from mobile, Reason : not passing expected values from mobile
            foreach (var item in request.products)
            {
                using (ProductDao prodDao = new ProductDao())
                {
                    DetailPrice product = prodDao.FindProductDetailByCityId(item.product_id, 0, "", request.city_name);
                    UpdateProductForReq(item, product, false);
                    if (request.has_exchange)
                    {
                        UpdateProductExchangeForReq(item, product, request.exchange.ToList(), false);
                    }
                }
            }
        }

        public static void UpdateProductForReq(ProductsDto item, DetailPrice product, bool isPickup)
        {
            item.product_name = product.Product.ProductName;
            if (item.quantity > 0)
            {
                item.unit_price = product.TubeNormalPrice;
                if (!IsOnProductPromo(product.ProductID))
                {
                    item.product_promo = 0;
                }
                else
                {
                    item.product_promo = (product.TubePromoPrice - product.TubeNormalPrice) * item.quantity;
                }
            }
            else
            {
                item.unit_price = 0;
                item.product_promo = 0;
            }

            if (item.refill_quantity > 0)
            {
                item.refill_price = product.RefillNormalPrice;

                if (!IsOnRefillPromo(product.ProductID))
                {
                    item.refill_promo = 0;
                }
                else
                {
                    item.refill_promo = (product.RefillPromoPrice - product.RefillNormalPrice) * item.refill_quantity;
                }

            }
            else
            {
                item.refill_price = 0;
                item.refill_promo = 0;
            }


            item.shipping_cost = product.Product.ShippingPrice;
            item.shipping_promo = product.Product.ShippingPromoPrice - product.Product.ShippingPrice;
            item.sub_total = (item.unit_price * item.quantity) + item.product_promo
                            + (item.refill_price * item.refill_quantity) + item.refill_promo;
            if (!isPickup)
            {
                item.sub_total += (item.shipping_cost * item.quantity) + (item.shipping_promo * item.quantity)
                            + (item.shipping_cost * item.refill_quantity) + (item.shipping_promo * item.refill_quantity);
            }
            //return item;
        }

        public static void UpdateProductExchangeForReq(ProductsDto item, DetailPrice product, List<ExchangeDto> exchangeRequest, bool isPickup)
        {
            using (ProductDao prodDao = new ProductDao())
            {
                foreach (var exchangeItem in exchangeRequest)
                {
                    var exchangeProduct = product.Product.ProductExchanges.Where(p => p.PrExID == exchangeItem.exchange_id && p.ProdID == item.product_id).FirstOrDefault();
                    if (exchangeProduct != null)
                    {
                        exchangeItem.exchange_with = exchangeProduct.ExchangeWith;
                        exchangeItem.exchange_price = exchangeProduct.ExchangePrice.HasValue ? exchangeProduct.ExchangePrice.Value + product.RefillNormalPrice : 0;
                        exchangeItem.exchange_promo_price = exchangeProduct.ExchangePrice.HasValue ? ((exchangeProduct.ExchangePromoPrice + product.RefillPromoPrice) - (exchangeProduct.ExchangePrice.Value + product.RefillNormalPrice)) : 0;
                        item.sub_total += (exchangeItem.exchange_price * exchangeItem.exchange_quantity) + (exchangeItem.exchange_promo_price * exchangeItem.exchange_quantity);

                        if (!isPickup)
                        {
                            item.sub_total += (item.shipping_cost * exchangeItem.exchange_quantity) + (item.shipping_promo * exchangeItem.exchange_quantity);
                        }
                    }
                }
            }
            //return exchangeRequest;
        }

        public static void AddProductsToOrder(Order ord, ICollection<ProductsDto> dtos)
        {
            foreach (var prd in dtos)
            {
                OrderDetail od = new OrderDetail();
                od.Order = ord;
                od.ProdID = prd.product_id;
                od.CreatedDate = ord.CreatedDate;

                od.Quantity = prd.quantity;
                od.UnitPrice = prd.unit_price;
                od.SubTotal = prd.unit_price * prd.quantity; //prd.sub_total;
                od.PromoProduct = prd.product_promo;
                od.ShippingCharge = prd.shipping_cost;
                od.PromoShipping = prd.shipping_promo;

                od.RefillQuantity = prd.refill_quantity;
                od.RefillPrice = prd.refill_price;
                od.PromoRefill = prd.refill_promo;
                od.RefillSubTotal = prd.refill_price * prd.refill_quantity;

                if (prd.quantity > 0)
                {
                    ord.ShippingCharge += (prd.shipping_cost * prd.quantity);
                    ord.PromoShipping += (prd.shipping_promo * prd.quantity);
                    od.TotamAmount = od.SubTotal + prd.product_promo + (prd.shipping_promo * prd.quantity) + (prd.shipping_cost * prd.quantity); //prd.sub_total;
                }
                if (prd.refill_quantity > 0)
                {
                    ord.ShippingCharge += (prd.shipping_cost * prd.refill_quantity);
                    ord.PromoShipping += (prd.shipping_promo * prd.refill_quantity);
                    od.RefillTotalAmount = od.RefillSubTotal + prd.refill_promo + (prd.shipping_promo * prd.refill_quantity) + (prd.shipping_cost * prd.refill_quantity); //prd.sub_total;
                }
                ord.SubTotal += od.SubTotal;
                ord.RefillSubTotal += od.RefillSubTotal;
                ord.PromoProduct += od.PromoProduct;
                ord.PromoRefill += od.PromoRefill;
                ord.GrandTotal += (od.TotamAmount + od.RefillTotalAmount); //prd.sub_total;
                ord.NumberOfProducts += prd.quantity + prd.refill_quantity;
                ord.OrderDetails.Add(od);

                //OrderDetail od = new OrderDetail();
                //od.Order = ord;
                //od.ProdID = prd.product_id;
                //od.CreatedDate = ord.CreatedDate;
                //od.PromoProduct = (prd.unit_price - prd.product_promo) * prd.quantity;
                //od.PromoShipping = (prd.shipping_cost - prd.shipping_promo) * prd.quantity;
                //od.ShippingCharge = prd.shipping_cost;
                //od.SubTotal = prd.sub_total;
                //od.Quantity = prd.quantity;
                //od.UnitPrice = prd.unit_price;                
                //ord.SubTotal += prd.sub_total;
                //ord.ShippingCharge += prd.shipping_cost * prd.quantity;
                //ord.PromoProduct += (prd.product_promo - prd.unit_price) * prd.quantity;
                //ord.PromoShipping += (prd.shipping_promo - prd.shipping_cost) * prd.quantity;
                //ord.OrderDetails.Add(od);
                //ord.GrandTotal += (prd.unit_price - prd.product_promo + prd.shipping_cost - prd.shipping_promo) * prd.quantity;                
            }
        }

        //Sprint 3

        public GetAssignedOrderCountResponse GetAssignedOrderCount(GetAssignedOrderCountRequest request)
        {
            GetAssignedOrderCountResponse response = new GetAssignedOrderCountResponse();
            try
            {
                if (!DriverServices.CheckAuthDriver(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                response.order = new AssignedOrderCountDto();
                using (OrderDao dao = new OrderDao())
                {
                    response.order.assigned_order_count = dao.GetAssignedOrderCount(request.user_id, ID_ORDER_PROCESSED);
                }
                using (TeleOrderDao dao = new TeleOrderDao())
                {
                    response.order.assigned_order_count += dao.GetAssignedOrderCount(request.user_id, ID_ORDER_PROCESSED);
                }
                response.code = 0;
                response.has_resource = 1;
                response.message = MessagesSource.GetMessage("assg.order.count");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public GetDriverOrderListResponse GetDriverOrderList(GetDriverOrderListRequest request)
        {
            GetDriverOrderListResponse response = new GetDriverOrderListResponse();
            try
            {
                if (!DriverServices.CheckAuthDriver(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                List<DriverOrderDetailsDto> ordDtoLst = new List<DriverOrderDetailsDto>();
                using (OrderDao dao = new OrderDao())
                {
                    List<Order> ordList = dao.GetDriverOrderList(request.user_id, request.current_list);
                    response.order_count = ordList.Count;
                    for (int i = 0; i < ordList.Count; i++)
                    {
                        DriverOrderDetailsDto odDto = new DriverOrderDetailsDto();
                        OrdersHelper.CopyFromEntity(odDto, ordList[i]);
                        ordDtoLst.Add(odDto);
                    }
                }
                using (TeleOrderDao dao = new TeleOrderDao())
                {
                    List<TeleOrder> ordList = dao.GetDriverOrderList(request.user_id, request.current_list);
                    response.order_count += ordList.Count;
                    for (int i = 0; i < ordList.Count; i++)
                    {
                        DriverOrderDetailsDto odDto = new DriverOrderDetailsDto();
                        OrdersHelper.CopyFromEntity(odDto, ordList[i]);
                        ordDtoLst.Add(odDto);
                    }
                }
                response.code = 0;
                response.order_details = ordDtoLst.ToArray();
                response.has_resource = 1;
                response.message = MessagesSource.GetMessage("drv.ordr.listed");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public GetDriverOrderResponse GetDriverOrder(GetDriverOrderRequest request)
        {
            GetDriverOrderResponse response = new GetDriverOrderResponse();
            try
            {
                if (!DriverServices.CheckAuthDriver(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                OrderType orderType;
                try
                {
                    orderType = request.order_type.ToEnumValue<OrderType>();
                }
                catch
                {
                    MakeInvalidOrderTypeResponse(response);
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    DriverOrderDetailDto dto = new DriverOrderDetailDto();
                    if (orderType == OrderType.OrderApp)
                    {
                        Order order = dao.FindOrderForDriver(request.order_id, request.user_id);
                        if (order == null)
                        {
                            MakeInvalidOrderResponse(response);
                            return response;
                        }
                        OrdersHelper.CopyFromEntity(dto, order);
                        response.order_details = dto;
                        response.products = OrdersHelper.CopyFromEntity(order);
                        response.has_exchange = (order.OrderPrdocuctExchanges.Count > 0 ? 1 : 0);
                        if (response.has_exchange == 1)
                        {
                            if (response.exchange == null)
                                response.exchange = new List<ExchangeDto>();
                            foreach (var item in order.OrderPrdocuctExchanges)
                            {
                                ExchangeDto exDto = new ExchangeDto();
                                OrdersHelper.CopyFromEntity(exDto, item);
                                response.exchange.Add(exDto);
                            }
                        }
                    }
                    else if (orderType == OrderType.OrderTelp)
                    {
                        TeleOrder teleOrder = dao.FindTeleOrderForDriver(request.order_id, request.user_id);
                        if (teleOrder == null)
                        {
                            MakeInvalidTeleOrderResponse(response);
                            return response;
                        }
                        OrdersHelper.CopyFromEntity(dto, teleOrder);
                        response.order_details = dto;
                        response.products = OrdersHelper.CopyFromEntity(teleOrder);
                        response.has_exchange = (teleOrder.TeleOrderPrdocuctExchanges.Count > 0 ? 1 : 0);
                        if (response.has_exchange == 1)
                        {
                            if (response.exchange == null)
                                response.exchange = new List<ExchangeDto>();
                            foreach (var item in teleOrder.TeleOrderPrdocuctExchanges)
                            {
                                ExchangeDto exDto = new ExchangeDto();
                                TeleOrderHelper.CopyFromEntity(exDto, item);
                                response.exchange.Add(exDto);
                            }
                        }
                    }
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("drv.ordr.got");
                }
            }
            catch (Exception ex)///I Started here
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public CloseOrderResponse CloseOrder(CloseOrderRequest request)
        {
            CloseOrderResponse response = new CloseOrderResponse();
            response.is_success = false;
            try
            {
                if (request.is_driver)
                {
                    if (!DriverServices.CheckAuthDriver(request.user_id, request.auth_token))
                    {
                        DriverServices.MakeNoDriverResponse(response);
                        return response;
                    }
                }
                else
                {
                    if (!SuperUserServices.CheckSuperUser(request.user_id, request.auth_token, response))
                    {
                        return response;
                    }
                }
                OrderType orderType;
                try
                {
                    orderType = request.order_type.ToEnumValue<OrdersServices.OrderType>();
                }
                catch
                {
                    MakeInvalidOrderTypeResponse(response);
                    return response;
                }
                if (orderType == OrderType.OrderApp)
                {
                    using (OrderDao dao = new OrderDao())
                    {
                        Order ord = dao.FindById(request.order_id, false);

                        if (!string.IsNullOrEmpty(request.latitude) && !string.IsNullOrEmpty(request.longitude) && request.is_driver)
                        {
                            double lat = 0;
                            double lng = 0;

                            double addrlat = 0;
                            double addrlng = 0;

                            try
                            {
                                lat = double.Parse(request.latitude, System.Globalization.CultureInfo.InvariantCulture);
                                lng = double.Parse(request.longitude, System.Globalization.CultureInfo.InvariantCulture);

                                addrlat = double.Parse(ord.ConsumerAddress.Latitude, System.Globalization.CultureInfo.InvariantCulture);
                                addrlng = double.Parse(ord.ConsumerAddress.Longitude, System.Globalization.CultureInfo.InvariantCulture);
                            }
                            catch
                            {
                                response.code = 1;
                                response.has_resource = 0;
                                response.message = "invalid coordinate";
                                return response;
                            }

                            var sCoord = new GeoCoordinate(lat, lng);
                            var eCoord = new GeoCoordinate(addrlat, addrlng);

                            var distance = Math.Round(GetDistanceBetweenPoints(lat, lng, addrlat, addrlng), 2);//sCoord.GetDistanceTo(eCoord);

                            if (distance > 300)
                            {
                                response.code = 1;
                                response.has_resource = 0;
                                response.message = "Order belum dapat ditutup. anda berada " + distance + " meter dari tujuan pengiriman";
                                return response;
                            }

                        }


                        ord.StatusID = ID_ORDER_DELIVERED;
                        ord.DeliveredAt = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        dao.Update(ord);

                        OrderDelivery orderDelivery = ord.OrderDeliveries.Where(x => x.OrdrID == ord.OrdrID).FirstOrDefault();
                        if (orderDelivery.DeliveryDate.HasValue)
                        {
                            var driverDeliveredOn = orderDelivery.DeliveryDate.Value;
                            var now = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                            var slotStartDate = now.Date + orderDelivery.Order.MDeliverySlot.StartTime;
                            var slotEndDate = now.Date + orderDelivery.Order.MDeliverySlot.EndTine;


                            if (driverDeliveredOn >= slotStartDate && driverDeliveredOn <= slotEndDate) //Delivered On Time - Within TimeSlot
                            {
                                orderDelivery.deviation = 0;
                                orderDelivery.StatusId = DELIVERY_STATUS_ONTIMEDELIVERY;
                            }
                            else if (driverDeliveredOn < slotStartDate) //
                            {
                                orderDelivery.deviation = (driverDeliveredOn - slotStartDate).TotalMinutes.ToInt();
                                orderDelivery.StatusId = DELIVERY_STATUS_EARLYDELIVERY;
                            }
                            else if (driverDeliveredOn > slotEndDate)
                            {
                                orderDelivery.deviation = (driverDeliveredOn - slotEndDate).TotalMinutes.ToInt();
                                orderDelivery.StatusId = DELIVERY_STATUS_LATEDELIVERY;
                            }
                        }

                        dao.UpdateDelivery(orderDelivery);
                        response.order_details = new CloseOrderDto
                        {
                            order_id = ord.OrdrID,
                            order_status_id = ord.StatusID,
                            order_type = request.order_type
                        };
                        response.is_success = true;

                        using (UserDao userDao = new UserDao())
                        {
                            int type = request.is_driver ? (int)PushMessagingService.PushType.TypeOne : (int)PushMessagingService.PushType.TypeTwo;
                            Consumer consumer = userDao.FindById(ord.ConsID);
                            ReadAndSendPushNotification(APPSETTING_MSG_TO_CONSUMER_FOR_RATING, APPSETTING_TITLE_FOR_CONSUMER_TO_RATE, consumer.AppToken,
                                request.order_id, ord.DrvrID.HasValue ? ord.DrvrID.Value : 0, 0, PushMessagingService.APPSETTING_APPLICATION_ID_CONSUMER,
                                PushMessagingService.APPSETTING_SENDER_ID_CONSUMER, type,
                                consumer.ConsID, consumer.Name, consumer.PhoneNumber, APPSETTING_USER_ROLE_CONSUMER
                                );
                        }
                    }
                }
                else if (orderType == OrderType.OrderTelp)
                {
                    using (TeleOrderDao dao = new TeleOrderDao())
                    {
                        TeleOrder teleOrder = dao.FindById(request.order_id, false);
                        teleOrder.StatusId = ID_ORDER_DELIVERED;
                        dao.Update(teleOrder);

                        TeleOrderDelivery teleOrderDelivery = teleOrder.TeleOrderDeliveries.Where(x => x.TeleOrdID == teleOrder.TeleOrdID).FirstOrDefault();
                        if (teleOrderDelivery.DeliveryDate.HasValue)
                        {

                            var driverDeliveredOn = teleOrderDelivery.DeliveryDate.Value;
                            var now = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                            var slotStartDate = now.Date + teleOrderDelivery.TeleOrder.MDeliverySlot.StartTime;
                            var slotEndDate = now.Date + teleOrderDelivery.TeleOrder.MDeliverySlot.EndTine;

                            if (driverDeliveredOn >= slotStartDate && driverDeliveredOn <= slotEndDate)
                            {

                                teleOrderDelivery.deviation = 0;
                                teleOrderDelivery.StatusId = DELIVERY_STATUS_ONTIMEDELIVERY;

                            }
                            else if (driverDeliveredOn < slotStartDate)
                            {

                                teleOrderDelivery.deviation = (driverDeliveredOn - slotStartDate).TotalMinutes.ToInt();
                                teleOrderDelivery.StatusId = DELIVERY_STATUS_EARLYDELIVERY;
                            }
                            else if (driverDeliveredOn > slotEndDate)
                            {

                                teleOrderDelivery.deviation = (driverDeliveredOn - slotEndDate).TotalMinutes.ToInt();
                                teleOrderDelivery.StatusId = DELIVERY_STATUS_LATEDELIVERY;
                            }
                        }

                        dao.UpdateDelivery(teleOrderDelivery);

                        response.order_details = new CloseOrderDto
                        {
                            order_id = teleOrder.TeleOrdID,
                            order_status_id = teleOrder.StatusId,
                            order_type = request.order_type
                        };

                        response.is_success = true;
                    }
                }
                response.code = 0;
                response.has_resource = 1;
                response.message = MessagesSource.GetMessage("ordr.closed");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        private double GetDistanceBetweenPoints(double lat1, double long1, double lat2, double long2)
        {
            double distance = 0;

            double dLat = (lat2 - lat1) / 180 * Math.PI;
            double dLong = (long2 - long1) / 180 * Math.PI;

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2)
                        + Math.Cos(lat2) * Math.Sin(dLong / 2) * Math.Sin(dLong / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            //Calculate radius of earth
            // For this you can assume any of the two points.
            double radiusE = 6378135; // Equatorial radius, in metres
            double radiusP = 6356750; // Polar Radius

            //Numerator part of function
            double nr = Math.Pow(radiusE * radiusP * Math.Cos(lat1 / 180 * Math.PI), 2);
            //Denominator part of the function
            double dr = Math.Pow(radiusE * Math.Cos(lat1 / 180 * Math.PI), 2)
                            + Math.Pow(radiusP * Math.Sin(lat1 / 180 * Math.PI), 2);
            double radius = Math.Sqrt(nr / dr);

            //Calaculate distance in metres.
            distance = radius * c;
            return distance;
        }

        public ResponseDto SendEmail(SendEmailRequest request)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    Order ord = dao.FindById(request.order_id, false);
                    //TODO 
                    // EmailServices.SendMail(...)
                    response.code = 0;
                    response.has_resource = 0;
                    response.message = MessagesSource.GetMessage("email.sent");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public ResponseDto OutForDelivery(OutForDeliveryRequest request)
        {
            OutForDeliveryResponse response = new OutForDeliveryResponse();
            try
            {
                if (!DriverServices.CheckAuthDriver(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                OrderType orderType;
                try
                {
                    orderType = request.order_type.ToEnumValue<OrdersServices.OrderType>();
                }
                catch
                {
                    MakeInvalidOrderTypeResponse(response);
                    return response;
                }
                if (orderType == OrderType.OrderApp)
                {
                    using (OrderDao dao = new OrderDao())
                    {
                        //Update other out for delivery status to accepted, updated by CVN
                        List<Order> OfdOrders = dao.GetOutForDeliveryOrders(request.user_id);
                        foreach (var item in OfdOrders)
                        {
                            item.StatusID = ID_ORDER_PROCESSED;
                            dao.Update(item);
                        }

                        Order ord = dao.FindById(request.order_id, false);
                        ord.StatusID = ID_ORDER_OUT_FOR_DELIVERY;
                        dao.Update(ord);
                        OrderDelivery orderDelivery = ord.OrderDeliveries.Where(x => x.OrdrID == ord.OrdrID).FirstOrDefault();
                        orderDelivery.StatusId = DELIVERY_STATUS_OUTFORDELIVERY;
                        orderDelivery.StartDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        dao.UpdateDelivery(orderDelivery);
                        OutForDeliveryDto ofdel = new OutForDeliveryDto
                        {
                            order_id = ord.OrdrID,
                            order_status_id = ord.StatusID,
                            order_type = request.order_type
                        };
                        response.order_details = ofdel;
                    }
                }
                else if (orderType == OrderType.OrderTelp)
                {
                    using (TeleOrderDao dao = new TeleOrderDao())
                    {
                        //Update other out for delivery status to accepted, updated by CVN
                        List<TeleOrder> OfdOrders = dao.GetOutForDeliveryTeleOrders(request.user_id);
                        foreach (var item in OfdOrders)
                        {
                            item.StatusId = ID_ORDER_PROCESSED;
                            dao.Update(item);
                        }

                        TeleOrder teleOrder = dao.FindById(request.order_id, false);
                        if (teleOrder == null)
                        {
                            MakeNoOrderFoundResponse(response);
                            return response;
                        }
                        teleOrder.StatusId = ID_ORDER_OUT_FOR_DELIVERY;
                        dao.Update(teleOrder);
                        TeleOrderDelivery orderDelivery = teleOrder.TeleOrderDeliveries.Where(x => x.TeleOrdID == teleOrder.TeleOrdID).FirstOrDefault();
                        orderDelivery.StatusId = DELIVERY_STATUS_OUTFORDELIVERY;
                        orderDelivery.StartDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        dao.UpdateDelivery(orderDelivery);
                        OutForDeliveryDto ofdel = new OutForDeliveryDto
                        {
                            order_id = teleOrder.TeleOrdID,
                            order_status_id = teleOrder.StatusId,
                            order_type = request.order_type
                        };
                        response.order_details = ofdel;
                    }
                }
                response.code = 0;
                response.has_resource = 1;
                response.message = MessagesSource.GetMessage("out.for.delivery");
            }

            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public void MakeNoOrderFoundResponse(ResponseDto response)
        {
            response.code = 1;
            response.has_resource = 0;
            response.message = MessagesSource.GetMessage("no.order");
        }

        public void MakeInvalidOrderTypeResponse(ResponseDto response)
        {
            response.code = 1;
            response.has_resource = 0;
            response.message = MessagesSource.GetMessage("invalid.order.type");
        }

        //updated
        public GetDriverListResponse GetDriverList(GetDriverListRequest request)
        {
            GetDriverListResponse response = new GetDriverListResponse();
            try
            {
                if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    var driverdetails = dao.GetDriverFromOrder(request.user_id, request.order_id);
                    if (driverdetails.Count <= 0)
                    {
                        response.code = 0;
                        response.has_resource = 0;
                        response.message = MessagesSource.GetMessage("driver_list_for_order_not_found");
                        return response;
                    }

                    response.driver_details = new List<DriverDetailListDto>();
                    using (DriverDao drvrDao = new DriverDao())
                    {
                        foreach (var item in driverdetails)
                        {
                            DriverDetailListDto drverDetail = new DriverDetailListDto();
                            //Driver drvr = drvrDao.FindById(item.drvr_id);
                            DriverHelper.CopyFromEntity(drverDetail, item);
                            var drvr = drvrDao.FindById(item.drvr_id);
                            drverDetail.driver_profile_image = drvr.ProfileImage != null ? ImagePathService.driverImagePath + drvr.ProfileImage : string.Empty;
                            using (ConsumerReviewDao conReviewDao = new ConsumerReviewDao())
                            {
                                List<ConsumerReview> conReview = new List<ConsumerReview>();
                                conReview = conReviewDao.GetReviewByDriver(item.drvr_id);
                                drverDetail.driver_rating = conReview.Count > 0 ? Convert.ToDecimal(conReview.Average(x => x.Rating)) : 0;
                            }
                            response.driver_details.Add(drverDetail);
                        }
                    }
                }
                response.code = 0;
                response.has_resource = 1;
                response.message = MessagesSource.GetMessage("driver_list_for_order_found");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        /// <summary>
        /// To get active orders count for boss
        /// </summary>
        /// <param name="request"></param>
        /// <returns>GetActiveOrderCountBossResponse</returns>
        public GetActiveOrderCountBossResponse GetActiveOrderCountBoss(GetActiveOrderCountBossRequest request)
        {
            GetActiveOrderCountBossResponse response = new GetActiveOrderCountBossResponse();
            try
            {
                if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    List<int> arrStatus = new List<int>()
                    {
                        ID_ORDER_PROCESSED ,
                        ID_ORDER_OUT_FOR_DELIVERY
                    };

                    int orderCount = dao.GetAgentBossOrdersCount(request.user_id, arrStatus);
                    response.active_orders = new ActiveOrdersBossDto { active_order_count = orderCount };
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = orderCount > 0 ? MessagesSource.GetMessage("active.orders") : MessagesSource.GetMessage("no.active.orders");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        /// <summary>
        ///  To get order lists for boss
        /// </summary>
        /// <param name="request"></param>
        /// <returns>GetOrderListBossResponse</returns>
        public GetOrderListBossResponse GetOrderListBoss(GetOrderListBossRequest request)
        {
            GetOrderListBossResponse response = new GetOrderListBossResponse();
            try
            {
                if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    var orders = dao.GetAgentBossOrders(request.user_id);
                    if (orders != null && orders.Count > 0)
                    {
                        #region ActiveOrders
                        List<int> activeStatus = new List<int>() { ID_ORDER_PROCESSED, ID_ORDER_OUT_FOR_DELIVERY };

                        var activeOrders = orders.Where(a => activeStatus.Contains(a.StatusID)).Select
                            (
                                a => OrdersHelper.CopyFromEntity(new OrderDetailsBossDto(), a)
                            ).ToList();

                        #endregion ActiveOrders

                        #region HistoryOrders

                        List<int> historyStatus = new List<int>() { ID_ORDER_DELIVERED, ID_ORDER_PROCESSED_BUT_NOT_DELIVERED };
                        var historyOrders = orders.Where(a => historyStatus.Contains(a.StatusID)).Select
                            (
                                a => OrdersHelper.CopyFromEntity(new OrderDetailsBossDto(), a)
                            ).ToList();

                        #endregion ActiveOrders

                        response.orders = new OrdersBossDto() { active_order_count = activeOrders.Count, history_order_count = historyOrders.Count };
                        response.active_orders = activeOrders;
                        response.history_orders = historyOrders;
                    }

                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("boss.ordr.listed");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        /// <summary>
        /// To get order details for boss
        /// </summary>
        /// <param name="request"></param>
        /// <returns>GetOrderDetailsBossResponse</returns>
        public GetOrderDetailsBossResponse GetOrderDetailsBoss(GetOrderDetailsBossRequest request)
        {
            GetOrderDetailsBossResponse response = new GetOrderDetailsBossResponse();
            try
            {
                if (!AgentBossServices.CheckAgentBoss(request.user_id, request.auth_token, response))
                {
                    return response;
                }

                OrderFullDetailsBossDto dto = new OrderFullDetailsBossDto();
                OrderType orderType = request.order_type.ToEnumValue<OrderType>();
                if (orderType == OrderType.OrderApp)
                {
                    Order ord = new OrderDao().FindById(request.order_id, true);
                    if (ord != null)
                        OrdersHelper.CopyFromEntity(dto, ord);
                }
                else if (orderType == OrderType.OrderTelp)
                {
                    TeleOrder ord = new TeleOrderDao().FindById(request.order_id, true);
                    if (ord != null)
                        OrdersHelper.CopyFromEntity(dto, ord, ord.Driver);
                }

                response.order_details = dto;
                response.code = 0;
                response.has_resource = 1;
                response.message = MessagesSource.GetMessage("ordr.details");

            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        /// <summary>
        /// Send e-receipt to agentadmin
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ResponseDto SendInvoiceMail(AgentAdminInvoiceMailSendRequest request)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                {
                    return response;
                }

                using (AgentAdminDao adminDao = new AgentAdminDao())
                {
                    OrderType orderType;
                    try
                    {
                        orderType = request.order_type.ToEnumValue<OrderType>();
                    }
                    catch
                    {
                        MakeInvalidOrderTypeResponse(response);
                        return response;
                    }
                    OrderInvoiceDto orderInvoice = GetOrderInvoiceOrReciept(request.user_id, orderType, request.order_id, (int)UserType.AgentAdmin);
                    if (orderInvoice == null)
                    {
                        MakeNoOrderFoundResponse(response);
                        return response;
                    }
                    var admin = adminDao.FindById(request.user_id);
                    if (string.IsNullOrEmpty(admin.email))
                    {
                        response.code = 1;
                        response.has_resource = 0;
                        response.message = MessagesSource.GetMessage("mail.id.not.found");
                    }
                    string subject = "e-Receipt_" + request.order_id.ToString() + "_" + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").Date.ToShortDateString();
                    //string emailBody = GetEmailBody(admin, orderInvoice);
                    string emailAttachment = CreateEmailAttachment(admin, orderInvoice);
                    string emailBody = CreateEmailBody(orderInvoice);
                    //var attachment = PdfServices.GetAttachment(emailBody, orderInvoice.order_id);
                    byte[] fileBytes = PdfServices.HtmlTOPdf(emailAttachment);
                    EmailServices.SendMailWithAttachment(admin.email, subject, emailBody, request.order_id, fileBytes);

                    //EmailServices.SendMail(admin.email, subject, emailBody.ToString());
                }
                response.code = 0;
                response.has_resource = 0;
                response.message = MessagesSource.GetMessage("invoice.mail.sent");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }


        /// <summary>
        /// Get drivers for agent admin
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetDriverListForAgentAdminResponse GetDriversForAgentAdmin(GetDriverListForAgentAdminRequest request)
        {
            GetDriverListForAgentAdminResponse response = new GetDriverListForAgentAdminResponse();
            try
            {
                if (!AgentAdminServices.CheckAdmin(request.user_id, request.auth_token, response))
                {
                    return response;
                }
                using (DriverDao drvrDao = new DriverDao())
                {
                    var drivers = drvrDao.GetAllDriversByAgentId(request.user_id);
                    if (drivers.Count == 0)
                    {
                        response.code = 0;
                        response.has_resource = 0;
                        response.message = MessagesSource.GetMessage("driver_list_for_order_not_found");
                        return response;
                    }

                    using (DeliverySlotDao slotDao = new DeliverySlotDao())
                    {
                        List<MDeliverySlot> timeSlots = null;
                        DateTime startDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        DateTime endDate = startDate.Date.AddDays(DAYS_TO_LIST);

                        response.driver_details = new List<AllDriversForAdmin>();
                        using (ConsumerReviewDao conReviewDao = new ConsumerReviewDao())
                        {
                            foreach (var item in drivers)
                            {
                                List<TimeslotDisplayDto> slotDtos = new List<TimeslotDisplayDto>();
                                Driver dvr = drvrDao.FindById(item.DrvrID);
                                AllDriversForAdmin driverDetail = new AllDriversForAdmin();
                                driverDetail.drivers = new DriverDto();
                                driverDetail.drivers.driver_id = dvr.DrvrID;
                                driverDetail.drivers.driver_name = dvr.DriverName;
                                driverDetail.drivers.driver_image = dvr.ProfileImage != null ? ImagePathService.driverImagePath + dvr.ProfileImage : string.Empty;
                                timeSlots = slotDao.GetAllSlots();
                                int count = 1;
                                DateTime dt = startDate;
                                int addDayCountToday = 0;
                                int addDayCountTommorw = 1;
                                int addDayCountDayAftrTmrw = 2;
                                for (int intCnt = 0; intCnt < DAYS_TO_LIST; intCnt++)
                                {
                                    TimeSpan elapseTime = TimeSpan.FromMinutes(30);
                                    TimeSpan currentTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay.Add(elapseTime);
                                    List<MDeliverySlot> slots = new List<MDeliverySlot>();
                                    //if (dt.Date == TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").Date)
                                    //{
                                    //    slots = timeSlots.Where(x => x.EndTine >= currentTime).ToList();
                                    //    if (slots.Count <= 0)
                                    //    {
                                    //        dt = dt.AddDays(1);
                                    //        slots = timeSlots;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    slots = timeSlots;
                                    //}
                                    slots = timeSlots;
                                    if (dt.Date.DayOfWeek.ToString() == TimeslotService.DAY_NAME_SUNDAY)
                                    {
                                        dt = dt.AddDays(1);
                                        addDayCountToday++;
                                        addDayCountTommorw++;
                                        addDayCountDayAftrTmrw++;
                                    }

                                    TimeslotDisplayDto dto = new TimeslotDisplayDto();
                                    if (dt.Date == TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").Date.AddDays(addDayCountToday))
                                    {
                                        dto.day_name = TimeslotService.DAY_NAME_TODAY;
                                    }
                                    else if (dt.Date == TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").Date.AddDays(addDayCountTommorw))
                                    {
                                        dto.day_name = TimeslotService.DAY_NAME_TOMORROW;
                                    }
                                    else if (dt.Date == TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").Date.AddDays(addDayCountDayAftrTmrw))
                                    {
                                        dto.day_name = TimeslotService.DAY_NAME_DAY_AFTER_TOMORROW;
                                    }

                                    dto.day_date = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dto.time_slot = new List<TimeslotDaysDto>();
                                    foreach (MDeliverySlot slt in slots)
                                    {
                                        TimeslotDaysDto Daydto = new TimeslotDaysDto();
                                        Daydto.time_slot_id = slt.SlotID;//count;
                                        Daydto.time_slot_name = slt.SlotName;
                                        Daydto.availability = ((slt.EndTine < currentTime && dto.day_name == TimeslotService.DAY_NAME_TODAY) ? 0 : slotDao.CheckAvailabilityForDriver(dt, Daydto.time_slot_id, item.DrvrID)).ToString() + "/" + Common.GetAppSetting<string>(DriverHelper.APPSETTING_MAX_DRIVER_ORDER, string.Empty); ;
                                        dto.time_slot.Add(Daydto);
                                        count++;
                                    }
                                    slotDtos.Add(dto);
                                    dt = dt.AddDays(1);
                                }
                                driverDetail.driver_availability = slotDtos;

                                List<ConsumerReview> conReview = new List<ConsumerReview>();
                                conReview = conReviewDao.GetReviewByDriver(item.DrvrID);
                                driverDetail.drivers.driver_rating = conReview.Count > 0 ? Convert.ToDecimal(conReview.Average(x => x.Rating)) : 0;

                                response.driver_details.Add(driverDetail);
                            }
                        }
                    }
                }
                response.code = 0;
                response.has_resource = 1;
                response.message = MessagesSource.GetMessage("driver_list_for_order_found");
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }

        public GetIssuesCountResponse GetIssuesCount(GetIssuesCountRequest request)
        {
            GetIssuesCountResponse response = new GetIssuesCountResponse();
            try
            {
                if (!_userServices.CheckAuthSuperUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    int orderCount = dao.GetIssueCount(request.user_id);
                    response.issues = new IssueCountDto { issue_count = orderCount };
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = orderCount > 0 ?
                        MessagesSource.GetMessage("active.orders") :
                        MessagesSource.GetMessage("no.active.orders");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        public GetIssuesListResponse GetIssuesList(GetIssuesListRequest request)
        {
            GetIssuesListResponse response = new GetIssuesListResponse();
            try
            {
                if (!_userServices.CheckAuthSuperUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {
                    int orderCount = 0;// dao.GetIssueCount(ID_ORDER_RECEIVED);
                    response.orders = new OrderCountDto { order_count = orderCount };

                    //List<GetIssuesListForSUser_Result> IssuesList = dao.GetIssuesListForSUser();

                    List<GetIssuesListBySUser_Result> IssuesList = dao.GetIssuesListBySUser(request.user_id);
                    orderCount = IssuesList.Count;
                    response.order_details = new IssueDetailsDto[IssuesList.Count];
                    for (int i = 0; i < IssuesList.Count; i++)
                    {
                        IssueDetailsDto orddtlsdto = new IssueDetailsDto();
                        OrdersHelper.CopyFromEntity(orddtlsdto, IssuesList[i]);
                        response.order_details[i] = orddtlsdto;
                    }
                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("ordr.listed");
                }

            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }
            return response;
        }
        public ResponseDto ForcedCancelOrderSuser(ForcedCancelOrderSuserRequest request)
        {
            ResponseDto response = new ResponseDto();
            try
            {
                if (!_userServices.CheckAuthSuperUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }
                if (!CheckOrderStatus(request.order_id))
                {
                    MakeInvalidOrderResponse(response);
                    return response;
                }
                using (OrderDao dao = new OrderDao())
                {

                    Order ord = dao.FindById(request.order_id, false);
                    ord.StatusID = ID_ORDER_CANCELLED_BY_SUSER;
                    dao.Update(ord);


                    using (OrderDeliveryDao odDao = new OrderDeliveryDao())
                    {
                        var orderDelivery = ord.OrderDeliveries.FirstOrDefault();

                        if (orderDelivery != null)
                        {
                            orderDelivery.StatusId = 3;//Cancelled
                            odDao.Update(orderDelivery);
                        }

                    }


                    ReadAndSendPushNotification(APPSETTING_MSG_FOR_CONSUMER_AT_SADMIN_CANCEL, APPSETTING_TITLE_FOR_CONSUMER_AT_SADMIN_CANCEL, ord.Consumer.AppToken, request.order_id, 0, 0, PushMessagingService.APPSETTING_APPLICATION_ID_CONSUMER,
                        PushMessagingService.APPSETTING_SENDER_ID_CONSUMER,
                        (int)PushMessagingService.PushType.TypeThree,
                        ord.Consumer.ConsID, ord.Consumer.Name, ord.Consumer.PhoneNumber, APPSETTING_USER_ROLE_CONSUMER
                        );

                    response.code = 0;
                    response.has_resource = 0;
                    response.message = MessagesSource.GetMessage("ordr.cancelled");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        public GetIssueDetailsResponseSUsers GetIssueDetailsResponseSUsers(GetIssueDetailsRequestSUsers request)
        {
            GetIssueDetailsResponseSUsers response = new GetIssueDetailsResponseSUsers();
            try
            {
                if (!_userServices.CheckAuthSuperUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }

                using (OrderDao dao = new OrderDao())
                {

                    GetOrderDeatilsbyOrderIdForSUser_Result orddtls = dao.GetOrderDeatilsbyOrderIdForSUser(request.order_id);
                    SUserOrderDetailsDto oDto = new SUserOrderDetailsDto();
                    OrdersHelper.CopyFromEntity(oDto, orddtls);
                    response.order_details = oDto;

                    List<GetProductDeatilsbyOrderIdForSUser_Result> prdList = dao.GetProductDeatilsbyOrderIdForSUser(request.order_id);
                    response.product_details = new SUserProductDetailsDto[prdList.Count];
                    for (int i = 0; i < prdList.Count; i++)
                    {
                        SUserProductDetailsDto prddtlsdto = new SUserProductDetailsDto();
                        OrdersHelper.CopyFromEntity(prddtlsdto, prdList[i]);
                        response.product_details[i] = prddtlsdto;
                    }


                    GetAgencyDetailsbyOrderIdForSUser_Result agncyorddtls = dao.GetAgencyDetailsbyOrderIdForSUser(request.order_id);
                    SUserAgencyDetailsDto agncyDto = new SUserAgencyDetailsDto();
                    OrdersHelper.CopyFromEntity(agncyDto, agncyorddtls);
                    response.agency_details = agncyDto;


                    List<OrderPrdocuctExchange> ordprdExcng = dao.GetProductExchangeSuser(request.order_id);
                    response.has_exchange = (ordprdExcng.Count > 0 ? 1 : 0);
                    if (ordprdExcng.Count > 0)
                    {
                        response.exchange = new ExchangeDto[ordprdExcng.Count];
                        for (int i = 0; i < ordprdExcng.Count; i++)
                        {
                            ExchangeDto exchangedto = new ExchangeDto();
                            OrdersHelper.CopyFromEntity(exchangedto, ordprdExcng[i]);
                            response.exchange[i] = exchangedto;
                        }
                    }

                    response.code = 0;
                    response.has_resource = 0;
                    response.message = MessagesSource.GetMessage("issue.details");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        public GetAllOrderDetailsResponse GetAllOrderDetails(GetAllOrderDetailsRequest request)
        {
            GetAllOrderDetailsResponse response = new GetAllOrderDetailsResponse();
            try
            {
                if (!_userServices.CheckAuthUser(request.user_id, request.auth_token))
                {
                    _userServices.MakeNouserResponse(response);
                    return response;
                }

                using (OrderDao dao = new OrderDao())
                {
                    var orderDetails = dao.GetAllOrderForConsumer(request.user_id, request.current_list, request.page_number, request.records_per_page);
                    if (orderDetails.Count == 0)
                    {
                        MakeNoOrderFoundResponse(response);
                        return response;
                    }
                    response.order_details = new List<AllOrderDetails>();
                    foreach (var item in orderDetails)
                    {
                        AllOrderDetails ordDetail = new AllOrderDetails();
                        OrdersHelper.CopyFromEntity(ordDetail, item);
                        using (ConsumerReviewDao conReviewDao = new ConsumerReviewDao())
                        {
                            if (item.DrvrID.HasValue)
                            {
                                List<ConsumerReview> conReview = new List<ConsumerReview>();
                                conReview = conReviewDao.GetReviewByDriver(item.DrvrID.Value);
                                ordDetail.driver_details.driver_rating = conReview.Count > 0 ? Convert.ToDecimal(conReview.Average(x => x.Rating)) : 0;
                            }
                        }
                        response.order_details.Add(ordDetail);
                    }

                    response.order_details = response.order_details.OrderByDescending(t => t.order_id).ToList();

                    response.code = 0;
                    response.has_resource = 1;
                    response.message = MessagesSource.GetMessage("ordr.listed");
                }
            }
            catch (Exception ex)
            {
                response.MakeExceptionResponse(ex);
            }

            return response;
        }

        public bool CheckOrderStatus(int order_id)
        {
            Order order = null;
            using (OrderDao dao = new OrderDao())
            {
                order = GetCheckStatus(dao, order_id);
                if (order == null)
                    return false;
            }

            return order != null;
        }

        public Order GetCheckStatus(OrderDao orderDao, int order_id)
        {
            Order order = null;
            order = orderDao.GetCheckStatusById(order_id);

            if (order != null && order.StatusID == ID_ORDER_RECEIVED)
            {
                return order;
            }
            return null;
        }

        public void MakeInvalidOrderResponse(ResponseDto response)
        {
            response.code = 1;
            response.has_resource = 0;
            response.message = MessagesSource.GetMessage("invalid.order");
        }

        public void MakeInvalidTeleOrderResponse(ResponseDto response)
        {
            response.code = 1;
            response.has_resource = 0;
            response.message = MessagesSource.GetMessage("invalid.tele.order");
        }

        public void MakeInvalidExchangeInputResponse(ResponseDto response)
        {
            response.code = 1;
            response.has_resource = 0;
            response.message = MessagesSource.GetMessage("invalid.exchange.input");
        }

        public void MakeInvalidDeliveryDateFormat(ResponseDto response)
        {
            response.code = 1;
            response.has_resource = 0;
            response.message = MessagesSource.GetMessage("invalid.delivery.date.format");
        }

        public void ReadAndSendPushNotification(string messageTypeKey, string msgTitleKey, string appToken, int orderId, int driverId, int orderCount, string applicationId, string senderId, int type, int userId, string userName, string mobile, string role)
        {
            string msgContent = Common.GetAppSetting<string>(messageTypeKey, string.Empty);
            msgContent = msgContent.Replace("{order_id}", orderId.ToString());
            msgContent = msgContent.Replace("{order_count}", orderCount > 0 ? orderCount.ToString() : string.Empty);
            msgContent = msgContent.Replace("{driver_id}", driverId.ToString());
            string msgTitle = Common.GetAppSetting<string>(msgTitleKey, string.Empty);
            msgTitle = msgTitle.Replace("{order_id}", orderId.ToString());
            msgTitle = msgTitle.Replace("{order_count}", orderCount > 0 ? orderCount.ToString() : string.Empty);
            msgTitle = msgTitle.Replace("{driver_id}", driverId.ToString());
            PushMessagingService.SendPushNotification(appToken, msgContent, msgTitle, applicationId, senderId, orderId, driverId, type, userId, userName, mobile, role);
        }

        public void TestPushNotification(string deviceId)
        {
            ReadAndSendPushNotification(APPSETTING_MSG_TO_ASSIGNED_DRIVER, APPSETTING_TITLE_FOR_ASSIGNED_DRIVER, deviceId, 0, 0, 0, PushMessagingService.APPSETTING_APPLICATION_ID_DRIVER, PushMessagingService.APPSETTING_SENDER_ID_DRIVER, (int)PushMessagingService.PushType.TypeOne,
                0, "", "", ""
                );
        }

        public CheckVoucherResponse CheckVoucher(CheckVoucherRequest request)
        {
            CheckVoucherResponse response = new CheckVoucherResponse()
            {
                keterangan_voucher = MessagesSource.GetMessage("voucher.invalid"),
                message = MessagesSource.GetMessage("voucher.invalid"),
                potongan = 0,
                is_voucher_valid = false
            };

            request.products = request.products ?? new ProductsDto[0];
            request.exchange = request.exchange ?? new ExchangeDto[0];

            if (!string.IsNullOrEmpty(request.voucher))
            {
                var promoVoucherDao = new PromoVoucherDao();

                var promo = promoVoucherDao.GetByVoucher(request.voucher);

                if (promo == null)
                {
                    return response;
                }

                var userUsingCount = promoVoucherDao.UserUsingCount(promo.PromoID, request.user_id);

                if (promo.UserQuota.HasValue)
                {
                    if (userUsingCount >= promo.UserQuota.Value) return response;
                }

                if (promo.Quota.HasValue)
                {
                    var usedQuota = promoVoucherDao.GetUsedQuota(promo.PromoID);

                    if (usedQuota >= promo.Quota) return response;
                }

                if (promo.StartDate.HasValue)
                {
                    if (TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") < promo.StartDate.Value) return response;
                }

                if (promo.EndDate.HasValue)
                {
                    if (TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") > promo.EndDate.Value) return response;
                }

                if (promo.PromoProducts.Count > 0)
                {
                    bool productExists = false;


                    var prodexchangeDao = new ProductDao();

                    if (request.exchange == null)
                    {
                        request.exchange = (new List<ExchangeDto>()).ToArray();
                    }

                    if (request.products == null)
                    {
                        request.products = (new List<ProductsDto>()).ToArray();
                    }

                    var exchangeproduct = prodexchangeDao.GetProductOfExchanges(request.exchange.Select(t => t.exchange_id).ToList());

                    promo.PromoProducts.ToList().ForEach(t =>
                    {
                        if (request.products.ToList().Where(req => req.product_id == t.ProdID).ToList().Count > 0 || exchangeproduct.Where(prex => prex.ProdID == t.ProdID).ToList().Count > 0)
                        {
                            productExists = true;
                        }

                    });

                    if (!productExists)
                    {
                        return response;
                    }

                }

                if (promo.PromoRegions.Count > 0)
                {

                    var cityDao = new CityDao();
                    var city = cityDao.FindCityByName(request.city_name);

                    if (city == null)
                    {
                        return response;
                    }

                    if (promo.PromoRegions.Where(t => t.RegionID == city.MProvince.MRegion.RegionID).ToList().Count <= 0)
                    {
                        return response;
                    }
                }


                if (promo.PromoConsumers.Count > 0)
                {
                    if (promo.PromoConsumers.Where(t => t.ConsID == request.user_id).ToList().Count <= 0)
                    {
                        return response;
                    }
                }
                response.is_voucher_valid = true;
                response.potongan = promo.Potongan * -1;
                response.keterangan_voucher = MessagesSource.GetMessage("voucher.valid");
                response.message = MessagesSource.GetMessage("voucher.valid");
            }

            return response;
        }

        #endregion

        #region Private Methods
        private string GetEmailBody(AgentAdmin admin, OrderInvoiceDto orderInvoice)
        {
            string mailBody = "";
            mailBody += "<style>td{border: 1px solid black;} table{border-style: solid;}</style>";
            mailBody += "<div>Hello " + admin.AgentAdminName + ",</div></br></br>";
            mailBody += "<div>e-Reciept for order id " + orderInvoice.order_id + "</div></br>";
            mailBody += "<table><tr><td>Invoice Number</td><td>" + orderInvoice.invoice_number + "</td></tr>";
            mailBody += "<tr><td>Order Date</td><td>" + orderInvoice.order_date + "</td></tr>";
            mailBody += "<tr><td>Order Time</td><td>" + orderInvoice.order_time + "</td></tr>";
            mailBody += "<tr><td>Consumer Name</td><td>" + orderInvoice.consumer_name + "</td></tr>";
            mailBody += "<tr><td>Consumer Mobile</td><td>" + orderInvoice.consumer_mobile + "</td></tr>";
            mailBody += "<tr><td>Consumer Address</td><td>" + orderInvoice.consumer_address + "</td></tr>";
            mailBody += "<tr><td>Consumer Location</td><td>" + orderInvoice.consumer_location + "</td></tr>";
            mailBody += "<tr><td>Postal Code</td><td>" + orderInvoice.postal_code + "</td></tr>";
            mailBody += "<tr><td>Agency Name</td><td>" + orderInvoice.agency_name + "</td></tr>";
            mailBody += "<tr><td>Agency Address</td><td>" + orderInvoice.agency_address + "</td></tr>";
            mailBody += "<tr><td>Agency Location</td><td>" + orderInvoice.agency_location + "</td></tr>";
            foreach (var item in orderInvoice.products)
            {
                mailBody += "<tr><td>Product Name</td><td>" + item.product_name + "</td></tr>";
                mailBody += "<tr><td>Quantity</td><td>" + item.quantity + "</td></tr>";
                mailBody += "<tr><td>Unit Price</td><td>" + item.unit_price + "</td></tr>";
                mailBody += "<tr><td>Sub Total</td><td>" + item.sub_total + "</td></tr>";
                mailBody += "<tr><td>Product Promo</td><td>" + item.product_promo + "</td></tr>";
                mailBody += "<tr><td>Shipping Cost</td><td>" + item.shipping_cost + "</td></tr>";
                mailBody += "<tr><td>Shipping Promo</td><td>" + item.shipping_promo + "</td></tr>";
            }
            mailBody += "<tr><td>Grant Total</td><td>" + orderInvoice.grand_total + "</td></tr>";

            return mailBody;
        }

        private string CreateEmailBody(OrderInvoiceDto orderInvoice)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~\\Templates\\eRecieptToAdminMessage.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{order_id}", orderInvoice.order_id.ToString());
            return body;
        }

        private string CreateEmailAttachment(AgentAdmin admin, OrderInvoiceDto orderInvoice)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~\\Templates\\eRecieptToAdminTemplate.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{AgentAdminName}", admin.AgentAdminName); //replacing the required things  
            body = body.Replace("{order_id}", orderInvoice.order_id.ToString());
            body = body.Replace("{invoice_number}", orderInvoice.invoice_number);
            body = body.Replace("{order_date}", orderInvoice.order_date);
            body = body.Replace("{order_time}", orderInvoice.order_time.Hours.ToString() + ":" + orderInvoice.order_time.Minutes.ToString());
            body = body.Replace("{consumer_name}", orderInvoice.consumer_name);
            body = body.Replace("{consumer_mobile}", orderInvoice.consumer_mobile);
            body = body.Replace("{consumer_address}", orderInvoice.consumer_address);
            body = body.Replace("{consumer_location}", orderInvoice.consumer_location);
            body = body.Replace("{postal_code}", orderInvoice.postal_code);
            body = body.Replace("{agency_name}", orderInvoice.agency_name);
            body = body.Replace("{agency_address}", orderInvoice.agency_address);
            body = body.Replace("{agency_location}", orderInvoice.agency_location);

            var voucher_template = "";

            if (orderInvoice.potongan_voucher != null)
            {
                if (orderInvoice.potongan_voucher.Value != 0)
                {

                    voucher_template += "<tr>";
                    voucher_template += "<td height='24' style='font-size:14px;font-family:arial;color:#777;text-align:right;' colspan='2'>Potongan Voucher {kode_voucher} (Rp)</td>";
                    voucher_template += "<td height='24' style='font-size:14px;font-family:arial;text-align:right;color:#777;' colspan='2'>{potongan_voucher}</td>";
                    voucher_template += "</tr>";

                    voucher_template = voucher_template.Replace("{kode_voucher}", orderInvoice.voucher);
                    voucher_template = voucher_template.Replace("{potongan_voucher}", string.Format("{0:n0}", (orderInvoice.potongan_voucher == null ? 0.000m : orderInvoice.potongan_voucher)));


                }
            }

            body = body.Replace("{voucher}", voucher_template);

            StringBuilder productsList = new StringBuilder();
            int count = 1;
            foreach (var item in orderInvoice.products)
            {
                string products = "";
                if (item.quantity > 0)
                {
                    products += " <tr>";
                    products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'> " + count + ". " + item.product_name + " </td>";
                    products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + item.quantity + " </td>";
                    products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", item.unit_price) + " </td>";
                    products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", item.quantity * item.unit_price) + "</td>";
                    products += "</tr >";
                    products += " <tr>";
                    products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>Promo </td>";
                    products += " <td height='24' style='font-size: 14px;font-family:arial;text-align:center;color: #777;'> " + item.quantity + " </td >";
                    products += "<td height='24' style='font-size: 14px; font-family:arial; text-align:right; color: #777;'> " + string.Format("{0:n0}", item.product_promo / item.quantity) + " </td >";
                    products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", item.product_promo) + " </td >";
                    products += "</tr >";
                    if (item.shipping_cost > 0)
                    {
                        products += " <tr>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>Ongkos Kirim </td >";
                        products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + item.quantity + " </td>";
                        products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", item.shipping_cost) + " </td>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", item.quantity * item.shipping_cost) + " </td>";
                        products += "</tr>";
                        products += " <tr>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>Promo Ongkos Kirim </td >";
                        products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + item.quantity + " </td>";
                        products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", item.shipping_promo) + " </td>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", item.quantity * item.shipping_promo) + " </td >";
                        products += "</tr >";
                    }
                }
                if (item.refill_quantity > 0)
                {
                    products += "<tr ><td colspan='4' height='15' style='border-bottom: solid 1px #ccc;'></td> </tr >";
                    products += " <tr>";
                    products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>" + item.product_name + "- Refill </td >";
                    products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + item.refill_quantity + " </td>";
                    products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", item.refill_price) + " </td>";
                    products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", item.refill_quantity * item.refill_price) + " </td>";
                    products += "</tr>";
                    products += " <tr>";
                    products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>Promo </td >";
                    products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + item.refill_quantity + " </td>";
                    products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", item.refill_promo / item.refill_quantity) + " </td>";
                    products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", item.refill_promo) + " </td >";
                    products += "</tr >";
                    if (item.shipping_cost > 0)
                    {
                        products += " <tr>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>Ongkos Kirim </td >";
                        products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + item.refill_quantity + " </td>";
                        products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", item.shipping_cost) + " </td>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", item.refill_quantity * item.shipping_cost) + " </td>";
                        products += "</tr>";
                        products += " <tr>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>Promo Ongkos Kirim </td >";
                        products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + item.refill_quantity + " </td>";
                        products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", item.shipping_promo) + " </td>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", item.refill_quantity * item.shipping_promo) + " </td >";
                        products += "</tr >";
                    }
                }
                if (orderInvoice.has_exchange == 1)
                {
                    StringBuilder exchangeList = new StringBuilder();
                    foreach (var itemExchange in orderInvoice.exchange)
                    {
                        products += "<tr ><td colspan='4' height='15' style='border-bottom: solid 1px #ccc;'></td> </tr >";
                        products += " <tr>";
                        //products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>" + item.product_name + " dengan " + itemExchange.exchange_quantity + " x " + itemExchange.exchange_with + " </td>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>" + item.product_name + " dengan " + itemExchange.exchange_with + " </td>";
                        products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + itemExchange.exchange_quantity + " </td>";
                        products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", itemExchange.exchange_price) + " </td>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", itemExchange.exchange_quantity * itemExchange.exchange_price) + "</td>";
                        products += "</tr >";
                        products += " <tr>";
                        products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>Promo</td>";
                        products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + itemExchange.exchange_quantity + " </td>";
                        products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", itemExchange.exchange_promo_price) + " </td>"; // exchange_promo_price is updated here to match ereceipt format(not multiplied with quantity) in API doc
                        products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", itemExchange.exchange_quantity * itemExchange.exchange_promo_price) + "</td>"; // exchange_promo_price is updated here to match ereceipt format(not multiplied with quantity) in API doc
                        products += "</tr >";
                        if (item.shipping_cost > 0)
                        {
                            products += " <tr>";
                            products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>Ongkos Kirim </td >";
                            products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + itemExchange.exchange_quantity + " </td>";
                            products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", item.shipping_cost) + " </td>";
                            products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", itemExchange.exchange_quantity * item.shipping_cost) + " </td>";
                            products += "</tr>";
                            products += " <tr>";
                            products += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>Promo Ongkos Kirim </td >";
                            products += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + itemExchange.exchange_quantity + " </td>";
                            products += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + string.Format("{0:n0}", item.shipping_promo) + " </td>";
                            products += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + string.Format("{0:n0}", itemExchange.exchange_quantity * item.shipping_promo) + " </td >";
                            products += "</tr >";
                        }
                        //string exchanges = "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Tukar harga</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.exchange_price + "</td></tr>";
                        //exchanges += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Tukar promo harga</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.exchange_promo_price + "</td></tr>";
                        //exchanges += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Tukar kuantitas</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.exchange_quantity + "</td></tr>";
                        //exchanges += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Tukar dengan</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.exchange_with + "</td></tr>";
                        exchangeList.Append(products);
                    }
                }

                //string products = "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Nama barang</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.product_name + "</td></tr>";
                //products += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Jumlah</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.quantity + "</td></tr>";
                //products += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Harga satuan</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.unit_price + "</td></tr>";
                //products += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Total</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.sub_total + "</td></tr>";
                //products += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Promo</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.product_promo + "</td></tr>";
                //products += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Ongkos kirim</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.shipping_cost + "</td></tr>";
                //products += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Promo ongkos kirim</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.shipping_promo + "</td></tr>";
                productsList.Append(products);
                count++;
            }
            body = body.Replace("{products}", productsList.ToString());
            //TODO
            //body = body.Replace("{has_exchange}", orderInvoice.has_exchange.ToString());
            //if (orderInvoice.has_exchange == 1)
            //{
            //    StringBuilder exchangeList = new StringBuilder();
            //    foreach (var item in orderInvoice.exchange)
            //    {
            //        string exchanges = " <tr>";
            //        exchanges += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>" + item.product_name + " dengan " + item.exchange_quantity + " x " + item.exchange_with + " </td>";
            //        exchanges += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + item.exchange_quantity + " </td>";
            //        exchanges += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + item.exchange_price + " </td>";
            //        exchanges += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + item.exchange_quantity * item.exchange_price + "</td>";
            //        exchanges += "</tr >";
            //        exchanges += " <tr>";
            //        exchanges += "<td height='24' style='font-size: 14px;font-family:arial;color: #777;'>Tukar Promo</td>";
            //        exchanges += " <td height='24' style='font-size: 14px;font-family:arial;text-align: center;color: #777;'>" + item.exchange_quantity + " </td>";
            //        exchanges += "<td height='24' style='font-size: 14px; font-family:arial; text-align: right; color: #777;'>" + item.exchange_promo_price + " </td>";
            //        exchanges += "<td height='24' style='font-size: 14px;font-family:arial;text-align: right;color: #777;'>" + item.exchange_quantity * item.exchange_promo_price + "</td>";
            //        exchanges += "</tr >";

            //        //string exchanges = "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Tukar harga</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.exchange_price + "</td></tr>";
            //        //exchanges += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Tukar promo harga</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.exchange_promo_price + "</td></tr>";
            //        //exchanges += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Tukar kuantitas</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.exchange_quantity + "</td></tr>";
            //        //exchanges += "<tr><td style='padding-left: 15px;border-right: 1px solid black;border-bottom: 1px solid black;'>Tukar dengan</td><td width='' style='padding-left: 15px;border-bottom: 1px solid black;'>" + item.exchange_with + "</td></tr>";
            //        exchangeList.Append(exchanges);
            //    }
            //    body = body.Replace("{exchanges}", exchangeList.ToString());
            //}
            //else
            //    body = body.Replace("{exchanges}", string.Empty);
            body = body.Replace("{grand_total_with_discount}", string.Format("{0:n0}", orderInvoice.grand_total_with_discount));
            body = body.Replace("{grand_discount}", string.Format("{0:n0}", orderInvoice.grand_discount));
            body = body.Replace("{grand_total}", string.Format("{0:n0}", orderInvoice.grand_total));

            return body;
        }

        public void AllocateOrderToPrefferedAgent(int orderId)
        {

            Thread.Sleep(FOLLOWUP_DELAY);

            using (OrderDao dao = new OrderDao())
            {
                Order order = dao.FindByIdWithAllocation(orderId);

                if (order.StatusID == OrdersServices.ID_ORDER_RECEIVED)
                {

                    string latitude = order.ConsumerAddress.Latitude;
                    string longitude = order.ConsumerAddress.Longitude;
                    //Agency preferredAgency = AgencyService.FindPreferredAgency(order, latitude, longitude);

                    List<AgentAdmin> prefferedAdmins = AgencyService.FindPreferredAdmins(order, latitude, longitude);

                    if (prefferedAdmins.Count > 0)
                    {
                        var agentAdmin = prefferedAdmins[0];


                    }

                    foreach (var agentAdmin in prefferedAdmins)
                    {

                        int orderCount = dao.GetAgentOrderCount(agentAdmin.AgadmID, 1);

                        ReadAndSendPushNotification(APPSETTING_MSG_TO_AUTO_ADMIN, APPSETTING_TITLE_TO_AUTO_ADMIN,
                                            agentAdmin.AppToken, order.OrdrID, 0, orderCount, PushMessagingService.APPSETTING_APPLICATION_ID_AADMIN,
                                            PushMessagingService.APPSETTING_SENDER_ID_AADMIN, (int)PushMessagingService.PushType.TypeOne,
                                            agentAdmin.AgadmID, agentAdmin.AgentAdminName, agentAdmin.MobileNumber, APPSETTING_USER_ROLE_AGEN_ADMIN
                                            );

                        dao.UpdateOrderAllocationAssigment(order.OrdrID, agentAdmin.AgadmID, 2);
                    }

                    HostingEnvironment.QueueBackgroundWorkItem(t => NotifySE(order.OrdrID));

                }
                else
                {
                    return;
                }

                //string appId = order.Consumer.AppID, appToken = order.Consumer.AppToken;
                //string message = string.Format(OrdersServices.ORDER_ALLOCATION_TEMPLATE, order.OrdrID);
                //PushMessagingService.PushNotification(appId, appToken, message);

                //TODO Move to Hangfire
                //HostingEnvironment.QueueBackgroundWorkItem(t => PostProcessAgencyNotification(order.OrdrID, false));
            }
        }

        [Queue("holdorder")]
        [AutomaticRetry(Attempts = 0, LogEvents = true, OnAttemptsExceeded = AttemptsExceededAction.Fail)]
        public void AllocateOrderToPrefferedAgentOffTime(int orderId)
        {
            using (OrderDao dao = new OrderDao())
            {
                Order order = dao.FindByIdWithAllocation(orderId);

                if (order.StatusID == OrdersServices.ID_ORDER_RECEIVED)
                {

                    string latitude = order.ConsumerAddress.Latitude;
                    string longitude = order.ConsumerAddress.Longitude;
                    //Agency preferredAgency = AgencyService.FindPreferredAgency(order, latitude, longitude);

                    List<AgentAdmin> prefferedAdmins = AgencyService.FindPreferredAdmins(order, latitude, longitude);

                    if (prefferedAdmins.Count > 0)
                    {
                        var agentAdmin = prefferedAdmins[0];


                    }

                    foreach (var agentAdmin in prefferedAdmins)
                    {

                        int orderCount = dao.GetAgentOrderCount(agentAdmin.AgadmID, 1);

                        ReadAndSendPushNotification(APPSETTING_MSG_TO_AUTO_ADMIN, APPSETTING_TITLE_TO_AUTO_ADMIN,
                                            agentAdmin.AppToken, order.OrdrID, 0, orderCount, PushMessagingService.APPSETTING_APPLICATION_ID_AADMIN,
                                            PushMessagingService.APPSETTING_SENDER_ID_AADMIN, (int)PushMessagingService.PushType.TypeOne,
                                            agentAdmin.AgadmID, agentAdmin.AgentAdminName, agentAdmin.MobileNumber, APPSETTING_USER_ROLE_AGEN_ADMIN
                                            );

                        dao.UpdateOrderAllocationAssigment(order.OrdrID, agentAdmin.AgadmID, 2);
                    }

                    HostingEnvironment.QueueBackgroundWorkItem(t => NotifySE(order.OrdrID));

                }
                else
                {
                    return;
                }

                //string appId = order.Consumer.AppID, appToken = order.Consumer.AppToken;
                //string message = string.Format(OrdersServices.ORDER_ALLOCATION_TEMPLATE, order.OrdrID);
                //PushMessagingService.PushNotification(appId, appToken, message);

                //TODO Move to Hangfire
                //HostingEnvironment.QueueBackgroundWorkItem(t => PostProcessAgencyNotification(order.OrdrID, false));
            }
        }


        private void NotifySE(int ordrID)
        {
            Thread.Sleep(AUTOASSIGN_DELAY);

            using (OrderDao dao = new OrderDao())
            {
                Order order = dao.FindById(ordrID, true);
                if (order != null)
                {
                    if (order.StatusID == ID_ORDER_RECEIVED)
                    {
                        using (SuperUserDao daoAdmin = new SuperUserDao())
                        {
                            if (order.CityId != null)
                            {
                                List<SuperAdmin> admins = daoAdmin.FindAdminBySACity(order.CityId.Value);
                                if (admins != null && admins.Count != 0)
                                {

                                    order.IssuedToSE = 1;

                                    dao.Update(order);

                                    foreach (SuperAdmin admin in admins)
                                    {
                                        ReadAndSendPushNotification(APPSETTING_MSG_TO_SUPER_ADMIN, APPSETTING_TITLE_FOR_SUPER_ADMIN, admin.AppToken, ordrID, 0, 0,
                                            PushMessagingService.APPSETTING_APPLICATION_ID_SUSER, PushMessagingService.APPSETTING_SENDER_ID_SUSER,
                                            (int)PushMessagingService.PushType.TypeThree,
                                            admin.SAdminID,
                                            admin.FullName,
                                            admin.MobileNum,
                                            APPSETTING_USER_ROLE_SA
                                            );
                                    }
                                }
                            }
                        }

                        HostingEnvironment.QueueBackgroundWorkItem(t => CancelExpiredOrderAfterSE(order.OrdrID));

                    }
                }
            }
        }

        private void CancelExpiredOrderAfterSE(int ordrID)
        {
            Thread.Sleep(SE_DELAY);

            using (OrderDao dao = new OrderDao())
            {
                Order order = dao.FindById(ordrID, true);
                if (order != null)
                {
                    if (order.StatusID == ID_ORDER_RECEIVED)
                    {
                        string appId = order.Consumer.AppID, appToken = order.Consumer.AppToken;
                        order.StatusID = ID_ORDER_CANCELLED_BY_SYSTEM;
                        order.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        dao.Update(order);

                        ReadAndSendPushNotification(APPSETTING_MSG_FOR_CONSUMER_AT_SADMIN_CANCEL, APPSETTING_TITLE_FOR_CONSUMER_AT_SADMIN_CANCEL,
                            order.Consumer.AppToken, ordrID, 0, 0,
                            PushMessagingService.APPSETTING_APPLICATION_ID_CONSUMER, PushMessagingService.APPSETTING_SENDER_ID_CONSUMER,
                            (int)PushMessagingService.PushType.TypeThree,
                            order.Consumer.ConsID,
                            order.Consumer.Name,
                            order.Consumer.PhoneNumber,
                            APPSETTING_USER_ROLE_CONSUMER
                            );


                        using (SuperUserDao daoAdmin = new SuperUserDao())
                        {
                            if (order.CityId != null)
                            {
                                List<SuperAdmin> admins = daoAdmin.FindAdminBySACity(order.CityId.Value);
                                if (admins != null && admins.Count != 0)
                                {
                                    foreach (SuperAdmin admin in admins)
                                    {
                                        ReadAndSendPushNotification(APPSETTING_MSG_FOR_SE_AT_EXPIRED, APPSETTING_TITLE_FOR_SE_AT_EXPIRED, admin.AppToken, ordrID, 0, 0,
                                            PushMessagingService.APPSETTING_APPLICATION_ID_SUSER, PushMessagingService.APPSETTING_SENDER_ID_SUSER,
                                            (int)PushMessagingService.PushType.TypeThree,
                                            admin.SAdminID,
                                            admin.FullName,
                                            admin.MobileNum,
                                            APPSETTING_USER_ROLE_SA
                                            );
                                    }
                                }
                            }

                        }

                    }
                }
            }
        }



        private void CancelExpiredOrder(int ordrID)
        {
            Thread.Sleep(ORDER_EXPIRE_TIME);

            using (OrderDao dao = new OrderDao())
            {
                Order order = dao.FindById(ordrID, true);
                if (order != null)
                {
                    if (order.StatusID == ID_ORDER_RECEIVED)
                    {
                        string appId = order.Consumer.AppID, appToken = order.Consumer.AppToken;
                        order.StatusID = ID_ORDER_CANCELLED_BY_SYSTEM;
                        order.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        dao.Update(order);

                        ReadAndSendPushNotification(APPSETTING_MSG_FOR_CONSUMER_AT_SADMIN_CANCEL, APPSETTING_TITLE_FOR_CONSUMER_AT_SADMIN_CANCEL,
                            order.Consumer.AppToken, ordrID, 0, 0,
                            PushMessagingService.APPSETTING_APPLICATION_ID_CONSUMER, PushMessagingService.APPSETTING_SENDER_ID_CONSUMER,
                            (int)PushMessagingService.PushType.TypeThree,
                            order.Consumer.ConsID,
                            order.Consumer.Name,
                            order.Consumer.PhoneNumber,
                            APPSETTING_USER_ROLE_CONSUMER
                            );


                        using (SuperUserDao daoAdmin = new SuperUserDao())
                        {
                            if (order.CityId != null)
                            {
                                List<SuperAdmin> admins = daoAdmin.FindAdminBySACity(order.CityId.Value);
                                if (admins != null && admins.Count != 0)
                                {
                                    foreach (SuperAdmin admin in admins)
                                    {
                                        ReadAndSendPushNotification(APPSETTING_MSG_TO_SUPER_ADMIN, APPSETTING_TITLE_FOR_SUPER_ADMIN, admin.AppToken, ordrID, 0, 0,
                                            PushMessagingService.APPSETTING_APPLICATION_ID_SUSER, PushMessagingService.APPSETTING_SENDER_ID_SUSER,
                                            (int)PushMessagingService.PushType.TypeThree,
                                            admin.SAdminID,
                                            admin.FullName,
                                            admin.MobileNum,
                                            APPSETTING_USER_ROLE_SA
                                            );
                                    }
                                }
                            }

                        }

                    }
                }
            }
        }

        private static bool IsOnProductPromo(int productId)
        {
            using (OrderDao dao = new OrderDao())
            {
                List<OrderDetail> orderList = dao.GetOrderListByProduct(productId);

                if (orderList != null)
                {
                    int product_sold = 0;

                    foreach (OrderDetail ord in orderList)
                    {
                        product_sold += ord.Quantity;
                    }

                    if (product_sold < MAX_LIMIT_TUBE_PROMO)
                        return true;
                    else
                        return false;

                }
                return false;
            }
        }

        private PlaceOrderResponse CheckVoucher(PlaceOrderRequest request)
        {
            PlaceOrderResponse response = new PlaceOrderResponse()
            {
                keterangan_voucher = MessagesSource.GetMessage("voucher.invalid"),
                potongan = 0,
                is_voucher_valid = false
            };

            if (!string.IsNullOrEmpty(request.voucher))
            {
                var promoVoucherDao = new PromoVoucherDao();

                var promo = promoVoucherDao.GetByVoucher(request.voucher);

                if (promo == null)
                {
                    return response;
                }

                var userUsingCount = promoVoucherDao.UserUsingCount(promo.PromoID, request.user_id);

                if (promo.UserQuota.HasValue)
                {
                    if (userUsingCount >= promo.UserQuota.Value) return response;
                }

                if (promo.Quota.HasValue)
                {
                    var usedQuota = promoVoucherDao.GetUsedQuota(promo.PromoID);

                    if (usedQuota >= promo.Quota) return response;
                }

                if (promo.StartDate.HasValue)
                {
                    if (TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") < promo.StartDate.Value) return response;
                }

                if (promo.EndDate.HasValue)
                {
                    if (TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time") > promo.EndDate.Value) return response;
                }

                if (promo.PromoProducts.Count > 0)
                {
                    bool productExists = false;


                    var prodexchangeDao = new ProductDao();

                    if (request.exchange == null)
                    {
                        request.exchange = (new List<ExchangeDto>()).ToArray();
                    }

                    if (request.products == null)
                    {
                        request.products = (new List<ProductsDto>()).ToArray();
                    }

                    var exchangeproduct = prodexchangeDao.GetProductOfExchanges(request.exchange.Select(t => t.exchange_id).ToList());

                    promo.PromoProducts.ToList().ForEach(t =>
                    {
                        if (request.products.ToList().Where(req => req.product_id == t.ProdID).ToList().Count > 0 || exchangeproduct.Where(prex => prex.ProdID == t.ProdID).ToList().Count > 0)
                        {
                            productExists = true;
                        }
                    });

                    if (!productExists)
                    {
                        return response;
                    }

                }

                if (promo.PromoRegions.Count > 0)
                {
                    var cityDao = new CityDao();
                    var city = cityDao.FindCityByName(request.city_name);

                    if (city == null)
                    {
                        return response;
                    }

                    if (promo.PromoRegions.Where(t => t.RegionID == city.MProvince.MRegion.RegionID).ToList().Count <= 0)
                    {
                        return response;
                    }
                }


                if (promo.PromoConsumers.Count > 0)
                {
                    if (promo.PromoConsumers.Where(t => t.ConsID == request.user_id).ToList().Count <= 0)
                    {
                        return response;
                    }
                }

                response.is_voucher_valid = true;
                response.potongan = promo.Potongan * -1;
                response.keterangan_voucher = MessagesSource.GetMessage("voucher.valid");
            }

            return response;
        }

        private static bool IsOnRefillPromo(int productId)
        {
            using (OrderDao dao = new OrderDao())
            {
                List<OrderDetail> orderList = dao.GetOrderListByProduct(productId);

                if (orderList != null)
                {
                    int product_sold = 0;

                    foreach (OrderDetail ord in orderList)
                    {
                        product_sold += ord.RefillQuantity;
                    }

                    if (product_sold < MAX_LIMIT_REFILL_PROMO)
                        return true;
                    else
                        return false;

                }
                return false;
            }
        }
        #endregion
    }

}
