namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MFunctionality
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MFunctionality()
        {
            AccessControls = new HashSet<AccessControl>();
        }

        [Key]
        public int FuncID { get; set; }

        public short ModID { get; set; }

        [Required]
        [StringLength(100)]
        public string FunctionalityName { get; set; }

        public bool StatusId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccessControl> AccessControls { get; set; }

        public virtual MModule MModule { get; set; }
    }
}
