﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LPG
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Enable CORS support
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));


            config.Routes.MapHttpRoute(
            name: "DefaultApi",
            routeTemplate: "api/{controller}/{action}/{id}",
            defaults: new { id = RouteParameter.Optional }

        );

            config.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(UnityConfig.Container);


        }
    }
}
