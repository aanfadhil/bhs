﻿using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace LPG.App_Services
{
    public class AgentAdminService : ServiceBase, IAgentAdminService
    {
        public AgentAdminService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<AgentAdminModel> ListAgentAdmin()
        {
            var agentList = new List<AgentAdminModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from agent in context.AgentAdmins
                           join company in context.Agencies
                           on agent.AgenID equals company.AgenID
                           join distributionpoin in context.DistributionPoints
                           on agent.DbptID equals distributionpoin.DbptID
                           where agent.StatusId == true
                           orderby agent.AgadmID descending
                           select new AgentAdminModel
                           {
                               AgentId = agent.AgadmID,
                               AdminName = agent.AgentAdminName,
                               CompanyName = company.AgencyName,
                               DistributionPointName = distributionpoin.DistributionPointName,
                               MobileNo = agent.MobileNumber,
                               email = agent.email,
                               CityId = distributionpoin.CityID,
                               CityName = distributionpoin.MCity == null?null:distributionpoin.MCity.CityName 
                           };
                agentList = data.ToList();
            }

            return agentList;
        }
        public AgentAdminModel getActiveAgentAdmin(int agentAdminId)
        {
            AgentAdminModel rtns = new AgentAdminModel();
            using (var context = new lpgdevEntities())
            {
                rtns = (from agentadmin in context.AgentAdmins
                        join company in context.Agencies
                        on agentadmin.AgenID equals company.AgenID
                        join distributionpoin in context.DistributionPoints
                        on agentadmin.DbptID equals distributionpoin.DbptID
                        where agentadmin.AgadmID == agentAdminId

                        select new AgentAdminModel()
                        {
                            AgentId = agentadmin.AgadmID,
                            AdminName = agentadmin.AgentAdminName,
                            AgenadminAgenId = company.AgenID,
                            AgenadminDistId = distributionpoin.DbptID,
                            MobileNo = agentadmin.MobileNumber,
                            email = agentadmin.email

                        }).FirstOrDefault();

                rtns.MobileNo = rtns.MobileNo?.ToTrimCode();
                return rtns;
            }
        }

        public AgentAdminModel saveAgentDetails(AgentAdminModel entity, int loggedBy)
        {
            try
            {
                using (var context = new lpgdevEntities())
                {

                    var Data = context.AgentAdmins.Where(r => r.AgadmID == entity.AgentId).FirstOrDefault();
                    if (Data != null)
                    {

                        Data.StatusId = true;
                        Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        Data.UpdatedBy = base.UserId.ToShort();
                        Data.MobileNumber = entity.CountryCode.ToDefaultString() + entity.MobileNo;
                        Data.email = entity.email;
                        Data.AgentAdminName = entity.AdminName;
                        Data.AgenID = entity.AgenadminAgenId;
                        Data.DbptID = entity.AgenadminDistId;

                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggedBy);
                    }
                    else
                    {
                        AgentAdmin objLocal = new AgentAdmin();

                        objLocal.AgadmID = entity.AgentId;
                        objLocal.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        objLocal.MobileNumber = entity.CountryCode.ToDefaultString() + entity.MobileNo;
                        Data.email = entity.email;
                        objLocal.AgentAdminName = entity.AdminName;
                        objLocal.AgenID = entity.AgenadminAgenId;
                        objLocal.DbptID = entity.AgenadminDistId;
                        objLocal.CreatedBy = base.UserId.ToShort();
                        objLocal.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                        context.AgentAdmins.Add(objLocal);
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggedBy);
                    }
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public bool DeleteAgentAdmin(int Id, int loggedBy)
        {
            try
            {
                using (var context = new lpgdevEntities())
                {
                    var Data = context.AgentAdmins.Where(r => r.AgadmID == Id).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.StatusId = false;
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", loggedBy);
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }


        public bool IsExists(int id)
        {
            using (var context = new lpgdevEntities())
            {
                return context.AgentAdmins.Where(c => c.AgadmID == id).Any();
            }

        }
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = "Agen Admin";
            _activityService.AddActivityLog(actModel, "Agen Admin");
        }

        public bool IsExistsMobileNo(string mobileNo, int userId)
        {
            mobileNo = MessageSource.ToStandardPhone(mobileNo);
            using (var context = new lpgdevEntities())
            {
                return context.AgentAdmins.Where(c => c.MobileNumber == mobileNo && c.StatusId == true && c.AgadmID != userId).Any();
            }

        }

        public bool IsExistsEmail(string email, int userId)
        {
            using (var context = new lpgdevEntities())
            {
                return context.AgentAdmins.Where(c => c.email == email && c.StatusId == true && c.AgadmID != userId).Any();
            }

        }
    }
}
