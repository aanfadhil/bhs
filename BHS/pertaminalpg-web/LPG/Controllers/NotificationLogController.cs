﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using LPG.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using ClosedXML.Excel;
using System.Globalization;
using i18n;
using LPG.Reporting.ReportServices;



namespace LPG.Controllers
{
    public class NotificationLogController : AbstractController<INotificationHistoryService>
    {
        SessionManagement sessionManagement;
        private const string SESSION_TIME_OUT = "SESSION_TIMEOUT";

        public NotificationLogController(
            INotificationHistoryService _serviceObject
            ) : base(_serviceObject)
        {

        }
        
        // GET: NotificationLog
        public ActionResult List()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products List" });
            }
            
        }
        [HttpPost]
        public ActionResult List(DatatablesRequest model)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var result = serviceObject.GetList(model);
                    return Json(result);
                }
                else
                {
                    return Json(null);
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult Export()
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    lpgdevEntities entities = new lpgdevEntities();
                    DataTable dt = new DataTable("MasterData");
                    dt.Columns.AddRange(new DataColumn[] {
                        new DataColumn("[[[No]]]"),
                        new DataColumn("[[[Username]]]"),
                        new DataColumn("[[[MobileNumber]]]"),
                        new DataColumn("[[[Role]]]"),
                        new DataColumn("[[[Agen]]]"),
                        new DataColumn("[[[Judul]]]"),
                        new DataColumn("[[[Pesan]]]"),
                        new DataColumn("[[[TerkirimPada]]]"),
                        new DataColumn("[[[Status Pengiriman]]]"),
                        new DataColumn("[[[Dibuka]]]"),
                        new DataColumn("[[[Dibuka Pada]]]")
                    });

                    var notifications = serviceObject.GetAll();
                    
                    int count = 0;
                    foreach (var notification in notifications)
                    {
                        count++;

                        var date = string.Empty;
                        var readdate = string.Empty;
                        var statusPengiriman = "Pengiriman Gagal";
                        var dibaca = "Sudah Dibaca";

                        if (notification.CreatedDate != null)
                        {
                            date = notification.CreatedDate.Value.ToString("dd/MM/yyyy HH:mm");
                        }

                        if (notification.ReadAt != null)
                        {
                            //notification.ReadAt.Value;
                            var rddate = new DateTime(notification.ReadAt.Value.Year, notification.ReadAt.Value.Month, notification.ReadAt.Value.Day, notification.ReadAt.Value.Hour, notification.ReadAt.Value.Minute, notification.ReadAt.Value.Second, DateTimeKind.Utc);
                            readdate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(rddate, "SE Asia Standard Time").ToString("dd/MM/yyyy HH:mm");
                        }

                        if (notification.Success != null)
                        {
                            statusPengiriman = notification.Success.Value > 0? "Pengiriman Berhasil": "Pengiriman Gagal";
                        }

                        if (notification.IsRead != null)
                        {
                            dibaca = notification.IsRead.Value == 0 ? "Belum DIbaca" : "Sudah Dibaca";
                        }

                        dt.Rows.Add
                        (
                            count,
                            notification.Username,
                            notification.MobileNumber,
                            notification.Role,
                            notification.NamaAgen,
                            notification.Title,
                            notification.Message,
                            date,
                            statusPengiriman,
                            dibaca,
                            readdate
                            
                       );
                    }
                    var style = XLWorkbook.DefaultStyle;


                    using (XLWorkbook wb = new XLWorkbook())
                    {


                        wb.Worksheets.Add(dt);
                        using (MemoryStream stream = new MemoryStream())
                        {
                            wb.SaveAs(stream);
                            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Notification History.xlsx");
                        }
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Products List" });
            }

        }
    }
}