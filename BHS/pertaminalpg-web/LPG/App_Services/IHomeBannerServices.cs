﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IHomeBannerServices
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        bool deleteHomBanner(int id, int loggedBy);
        HomeBannerModel getHomeBannerDetails(int homeBannerId);
        List<HomeBannerModel> ListHomeBanner();
        HomeBannerModel saveBannerDetails(HomeBannerModel entity, string Cmd, int loggedBy);
    }
}