﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.SuperUser
{
    public class SendPostNotificationRequest : AuthBase
    {
        public string message { get; set; }
        public string title { get; set; }
        public int user_type { get; set; }
        public List<int> targets { get; set; }
    }


}
