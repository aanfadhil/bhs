namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TeleOrderPrdocuctExchange")]
    public partial class TeleOrderPrdocuctExchange
    {
        [Key]
        public int TelOrdPrdExID { get; set; }

        public int TeleOrdID { get; set; }

        public int ProdID { get; set; }

        [Required]
        [StringLength(150)]
        public string ExchangeWith { get; set; }

        public int ExchangeQuantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ExchangePrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ExchangePromoPrice { get; set; }

        public bool StatusId { get; set; }

        public DateTime CreatedDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SubTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal TotalAmount { get; set; }

        public virtual Product Product { get; set; }

        public virtual TeleOrder TeleOrder { get; set; }
    }
}
