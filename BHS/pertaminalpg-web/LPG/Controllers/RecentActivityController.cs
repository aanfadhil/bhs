﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class RecentActivityController : AbstractController<IActivityService>
    {
        SessionManagement sessionManagement;

        public RecentActivityController(IActivityService _serviceObject) : base(_serviceObject)
        {
        }

        //ActivityService serviceObject = new ActivityService();
        // GET: RecentActivity
        public ActionResult ListActivity()
        {
            var vm = new ActivityModel();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.ActivityList = serviceObject.ListActivity(adminSessionInfo.UserId);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.Log(ex);
                return RedirectToAction("Index", "Error", new { area = "Recent Activity" });
            }
            return View(vm);
        }

    }

}
