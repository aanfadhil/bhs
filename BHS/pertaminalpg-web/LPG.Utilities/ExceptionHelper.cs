﻿using System;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using context = System.Web.HttpContext;
using System.IO;
using System.Web;
using LPG.Utilities;

/// <summary>  
/// Summary description for ExceptionLogging  
/// </summary>  
public static class ExceptionHelper
{

    private static String ErrorlineNo, Errormsg, ErrorLocation, extype, exurl, Frommail, ToMail, Password, Sub, HostAdd, EmailHead, EmailSing;


    public static void Log(Exception exception)
    {

        try
        {
            LogError(exception);

            //var newline = "<br/>";
            //ErrorlineNo = exception.StackTrace.Substring(exception.StackTrace.Length - 7, 7);
            //Errormsg = exception.GetType().Name.ToString();
            //extype = exception.GetType().ToString();
            //exurl = context.Current.Request.Url.ToString();
            //ErrorLocation = exception.Message.ToString();
            //EmailHead = "<b>Dear Team,</b>" + "<br/>" + "An exception occurred in a Application Url" + " " + exurl + " " + "With following Details" + "<br/>" + "<br/>";
            //EmailSing = newline + "Thanks and Regards" + newline + "    " + "     " + "<b>Application Admin </b>" + "</br>";
            //Sub = "Exception occurred" + " " + "in Application" + " " + exurl;
            //HostAdd = "smtp.gmail.com";
            //string errortomail = EmailHead + "<b>Log Written Date: </b>" + " " + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").ToString() + newline + "<b>Error Line No :</b>" + " " + ErrorlineNo + "\t\n" + " " + newline + "<b>Error Message:</b>" + " " + Errormsg + newline + "<b>Exception Type:</b>" + " " + extype + newline + "<b> Error Details :</b>" + " " + ErrorLocation + newline + "<b>Error Page Url:</b>" + " " + exurl + newline + newline + newline + newline + EmailSing;

            //using (MailMessage mailMessage = new MailMessage())
            //{
            //    Frommail = "linson.kj@eraminfotech.in";
            //    ToMail = "linsonkj@gmail.com";
            //    Password = "******";

            //    mailMessage.From = new MailAddress(Frommail);
            //    mailMessage.Subject = "Exception";
            //    mailMessage.Body = errortomail;
            //    mailMessage.IsBodyHtml = true;

            //    string[] MultiEmailId = ToMail.Split(',');
            //    foreach (string userEmails in MultiEmailId)
            //    {
            //        mailMessage.To.Add(new MailAddress(userEmails));
            //    }

            //    SmtpClient smtp = new SmtpClient();  // creating object of smptpclient  
            //    smtp.Host = HostAdd;              //host of emailaddress for example smtp.gmail.com etc  
            //    smtp.EnableSsl = true;
            //    NetworkCredential NetworkCred = new NetworkCredential();
            //    NetworkCred.UserName = mailMessage.From.Address;
            //    NetworkCred.Password = Password;
            //    smtp.UseDefaultCredentials = true;
            //    smtp.Credentials = NetworkCred;
            //    smtp.Port = 587;
            //    smtp.Send(mailMessage); //sending Email  
            //}
        }
        catch (Exception em)
        {
            em.ToString();

        }

    }


    private static void LogError(Exception ex)
    {
        string message = string.Format("Time: {0}", TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").ToString("dd/MM/yyyy hh:mm:ss tt"));
        string date = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").ToString("dd_MMM_yyyy");
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        message += string.Format("Message: {0}", ex.Message);
        message += Environment.NewLine;
        message += string.Format("Inner Exception: {0}", ex.InnerException);
        message += Environment.NewLine;
        message += string.Format("StackTrace: {0}", ex.StackTrace);
        message += Environment.NewLine;
        message += string.Format("Source: {0}", ex.Source);
        message += Environment.NewLine;
        message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        string path = HttpContext.Current.Server.MapPath("~/ErrorLog/");
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        string _fileName = path + "ErrorLog_" + date + ".txt";

        using (StreamWriter writer = new StreamWriter(_fileName, true))
        {
            writer.WriteLine(message);
            writer.Close();
        }
    }

}