﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using LPG.Reporting.ReportServices;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class DashboardController : AbstractController<IDashboardService>
    {
        public AdminSessionEntity AdminSessionInfo { get; private set; }

        //Dependency Injection
        private IDashboardService DashboardService;
        private ISessionManagement SessionManagement;

        public DashboardController(ISessionManagement _SessionManagement, DashboardService _DashboardService) : base()
        {
            SessionManagement = _SessionManagement;
            DashboardService = _DashboardService;
        }

        public ActionResult Index()
        {
            AdminSessionInfo = SessionManagement.getAdminSession();
            ViewBag.Role = AdminSessionInfo.UserRole;

            var vm = new Dashboar2dModel();
            int iMonth = Request.QueryString.Get("month")!=null ? Convert.ToInt16(Request.QueryString.Get("month")) : DateTime.Now.Month;
            int iYear = Request.QueryString.Get("year") != null ? Convert.ToInt16(Request.QueryString.Get("year")) : DateTime.Now.Year;

            vm.iMonth = iMonth;
            vm.iYear = iYear;
            vm.ListOfSalesPerRegion = DashboardService.GetRegionTransaction(iMonth, iYear);

            return View(vm);
        }

        public ActionResult KPI()
        {
            AdminSessionInfo = SessionManagement.getAdminSession();
            ViewBag.Role = AdminSessionInfo.UserRole;

            var vm = new Dashboar2dModel();
            int iMonth = Request.QueryString.Get("month") != null ? Convert.ToInt16(Request.QueryString.Get("month")) : DateTime.Now.Month;
            int iYear = Request.QueryString.Get("year") != null ? Convert.ToInt16(Request.QueryString.Get("year")) : DateTime.Now.Year;

            vm.iMonth = iMonth;
            vm.iYear = iYear;
            vm.KPIReport = DashboardService.GetKPIReport(iMonth, iYear);
            vm.KPISetting = DashboardService.GetKPISetting();

            var region = from i in vm.KPIReport
                         group i by new { i.RegionCode, i.RegionName } into g
                         select new RegionModel { RegionCode = g.Key.RegionCode, RegionName = g.Key.RegionName };

            vm.Region = region.ToList();
            return View(vm);
        }

        public ActionResult NumberOfSales()
        {
            AdminSessionInfo = SessionManagement.getAdminSession();
            ViewBag.Role = AdminSessionInfo.UserRole;

            var vm = new Dashboar2dModel();
            int iMonth = Request.QueryString.Get("month") != null ? Convert.ToInt16(Request.QueryString.Get("month")) : DateTime.Now.Month;
            int iYear = Request.QueryString.Get("year") != null ? Convert.ToInt16(Request.QueryString.Get("year")) : DateTime.Now.Year;

            vm.iMonth = iMonth;
            vm.iYear = iYear;
            vm.NumberOfSales = DashboardService.GetNumberOfSales(iMonth, iYear);
            var agent = from i in vm.NumberOfSales
                        where i.AgentId.HasValue
                        group i by new { i.AgentId, i.AgentName } into g
                        select new AgentAdminModel
                        {
                            AgentId = Convert.ToInt32(g.Key.AgentId),
                            AdminName = g.Key.AgentName
                        };

           vm.Agent = agent.ToList();
            return View(vm);
        }

        public ActionResult KPISetting()
        {
            AdminSessionInfo = SessionManagement.getAdminSession();
            ViewBag.Role = AdminSessionInfo.UserRole;

            MDashKPISetting vm = DashboardService.GetKPISetting();
            return View(vm);
        }

        public ActionResult OrderPercustomer()
        {
            AdminSessionInfo = SessionManagement.getAdminSession();
            ViewBag.Role = AdminSessionInfo.UserRole;
            return View();
        }

        public ActionResult OrderPercustomerDetail()
        {
            int iCustomerId = Request.QueryString.Get("id") != null ? Convert.ToInt16(Request.QueryString.Get("id")) : 0;
            AdminSessionInfo = SessionManagement.getAdminSession();
            ViewBag.Role = AdminSessionInfo.UserRole;
            ViewBag.ID = iCustomerId;
            return View();
        }


        [HttpPost]
        public ActionResult KPISetting(MDashKPISetting kpi)
        {
            DashboardService.UpdateKPISetting(kpi);
            SetFlash(FlashMessageType.Success, "Update KPI setting sukses.");

            return RedirectToAction("KPISetting");
        }

        [HttpPost]
        public ActionResult TotalUsers(ChartRequestModel request)
        {
            int iMonth = Request.Form.Get("month") != null ? Convert.ToInt16(Request.Form.Get("month")) : DateTime.Now.Month;
            int iYear = Request.Form.Get("year") != null ? Convert.ToInt16(Request.Form.Get("year")) : DateTime.Now.Year;

            int usersCount = DashboardService.GetRegisteredUserByMonth(iMonth, iYear);
            var resp = new
            {
                total = String.Format("{0:#,##0}", usersCount)
            };
            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult NumberOfSales(ChartRequestModel request)
        {
            int iMonth = Request.Form.Get("month") != null ? Convert.ToInt16(Request.Form.Get("month")) : DateTime.Now.Month;
            int iYear = Request.Form.Get("year") != null ? Convert.ToInt16(Request.Form.Get("year")) : DateTime.Now.Year;
            int iRegionId = Request.Form.Get("region") != null ? Convert.ToInt16(Request.Form.Get("region")) : 0;

            iMonth = iMonth == 0 ? DateTime.Now.Month : iMonth;
            iYear = iYear == 0 ? DateTime.Now.Year : iYear;
            var resp = DashboardService.GetNumberOfSales(iMonth, iYear, iRegionId);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult NumberOfSalesViaApps(ChartRequestModel request)
        {
            int iMonth = Request.QueryString.Get("month") != null ? Convert.ToInt16(Request.QueryString.Get("month")) : DateTime.Now.Month;
            int iYear = Request.QueryString.Get("year") != null ? Convert.ToInt16(Request.QueryString.Get("year")) : DateTime.Now.Year;
            int iAgentId = Request.QueryString.Get("agent") != null ? Convert.ToInt16(Request.QueryString.Get("agent")) : 0;

            iMonth = iMonth == 0 ? DateTime.Now.Month : iMonth;
            iYear = iYear == 0 ? DateTime.Now.Year : iYear;
            var NumberOfSales = DashboardService.GetNumberOfSales(iMonth, iYear);
            var searchIds = new List<int?> { 1, 2 }; //category order apps
            var resp = from i in NumberOfSales
                        where i.AgentId.Equals(iAgentId)
                        where searchIds.Contains(i.Category)
                        group i by i.ProductCode into gr
                        select new ChartOfSales
                        {
                            name = gr.Key,
                            y = gr.Sum(x => Convert.ToInt32( x.iQuantity ))
                        };

            return Json(resp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ListCustomer(DatatablesRequest model)
        {
            var result = new DataTableModel<SPCustomerTransPaging_Result>();
            result = DashboardService.GetOrderCustomerPaging(model);
        
            return Json(result);
        }

        [HttpPost]
        public ActionResult ListCustomerDetail(DatatablesRequest model)
        {
            int iCustomerId = Request.QueryString.Get("id") != null ? Convert.ToInt16(Request.QueryString.Get("id")) : 0;

            var result = new DataTableModel<SPOrderPerCustomer_Result>();
            result = DashboardService.GetOrderPercustomer(model, iCustomerId, "app");

            return Json(result);
        }

    }
}