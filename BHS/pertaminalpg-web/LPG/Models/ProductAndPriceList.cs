﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class ProductAndPriceList
    {
        public ProductModel product { get; set; }
        public List<DetailPriceModel> priceList { get; set; }
    }
}