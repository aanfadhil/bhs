﻿using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Configuration;
using Microsoft.ApplicationInsights;

namespace LPG.App_Services
{
    public class ContactService : AbstractAppService, IContactService
    {
        public ContactService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<ContactModel> ListContact()
        {
            string filepath = ConfigurationManager.AppSettings["DefaultPath_contact"].ToString();
            var contactList = new List<ContactModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from contact in context.ContactInfoes

                           orderby contact.CinfoID
                           select new ContactModel
                           {
                               ContactId = contact.CinfoID,
                               BannerImage = (contact.ContactInfoImage != "") ? (filepath + contact.ContactInfoImage) : "",
                               Description = contact.Description,
                           };

                contactList = data.ToList();
            }
            return contactList;
        }

        public ContactModel saveContactDetails(ContactModel entity, int userId)
        {

            try
            {
                entity.IsExists = false;
                using (var context = new lpgdevEntities())
                {

                    var Data = context.ContactInfoes.Where(r => r.CinfoID == entity.ContactId).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.Description = entity.Description;
                        Data.ContactInfoImage = (entity.BannerImage != "") ? Path.GetFileName(entity.BannerImage) : "";
                        Data.StatusId = entity.IsActive;
                        Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        Data.UpdatedBy = userId.ToShort();
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", userId);


                    }
                    else
                    {
                        ContactInfo objContact = new ContactInfo();

                        objContact.Description = entity.Description;
                        objContact.ContactInfoImage = (entity.BannerImage != "") ? Path.GetFileName(entity.BannerImage) : "";
                        objContact.StatusId = entity.IsActive;
                        objContact.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        objContact.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        objContact.CreatedBy = userId.ToShort();
                        objContact.UpdatedBy = userId.ToShort();

                        context.ContactInfoes.Add(objContact);
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", userId);
                    }
                }
                return entity;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ContactModel getContactDetails(int contactId)
        {
            string filepath = ConfigurationManager.AppSettings["DefaultPath_contact"].ToString();
            using (var context = new lpgdevEntities())
            {
                var rtns = (from homebanner in context.ContactInfoes
                            where homebanner.CinfoID == contactId
                            select new ContactModel()
                            {
                                ContactId = homebanner.CinfoID,
                                Description = homebanner.Description,
                                BannerImage = (homebanner.ContactInfoImage != "") ? (filepath + homebanner.ContactInfoImage) : "",
                                IsActive = homebanner.StatusId

                            }).FirstOrDefault();

                if (rtns == null)
                {
                    rtns = (from homebanner in context.ContactInfoes
                            where homebanner.CinfoID == contactId
                            select new ContactModel()
                            {
                                ContactId = homebanner.CinfoID,
                                Description = homebanner.Description,
                                BannerImage = string.Empty,
                                IsActive = homebanner.StatusId

                            }).FirstOrDefault();
                }

                return rtns;
            }
        }

        public bool deleteContactDetails(int id, int userId)
        {
            bool status = false;
            using (var context = new lpgdevEntities())
            {
                var data = context.ContactInfoes.Where(r => r.CinfoID == id).FirstOrDefault();
                if (data != null)
                {
                    context.ContactInfoes.Remove(data);
                    context.SaveChanges();
                    CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), "", userId);
                    status = true;
                }
            }
            return status;
        }
        public void CallActivityLog(string action, string action_item, int userId)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = userId;
            actModel.Item = "Hubungi kami";
            _activityService.AddActivityLog(actModel, "Hubungi kami");
        }
    }
}
