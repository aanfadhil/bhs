﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IContactService
    {
        void CallActivityLog(string action, string action_item, int userId);
        bool deleteContactDetails(int id, int userId);
        ContactModel getContactDetails(int contactId);
        List<ContactModel> ListContact();
        ContactModel saveContactDetails(ContactModel entity, int userId);
    }
}