﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LPG.Models
{
    public class OrderModel
    {
        public int OrderId { get; set; }
        public string AgentName { get; set; }
        public string AgentRegion { get; set; }
        public string AgentAddress { get; set; }
        public string ConsumerName { get; set; }
        public string ConsumerAddress { get; set; }
        public string ConsumerRegion { get; set; }
        public string InvoiceNo { get; set; }
        public string AdminMobileNo { get; set; }
        public int Status { get; set; }
        public int BuyingStatus { get; set; }
        public string ProductName { get; set; }
        public decimal ProductAmount { get; set; }
        public int ProductQuantity { get; set; }
        public string UnitPrice { get; set; }
        public string TotalPrice { get; set; }
        public decimal PromoPrice { get; set; }
        public decimal RefillPrice { get; set; }
        public decimal RefillPromo { get; set; }
        public decimal ShippingPrice { get; set; }
        public decimal ShippingPromo { get; set; }
        public int OrderedProductType { get; set; }
        public decimal PostalFee { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string PromoPostage { get; set; }
        public decimal? TotalEarlyCount { get; set; }
        public decimal? TotalPromoTotal { get; set; }
        public decimal? PotonganVoucher { get; set; }
        public string Voucher { get; set; }
        public decimal? TotalTotalEnd { get; set; }
        public string SlotName { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
        public string DeliveryDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public System.DateTime? TimeslotDate { get; set; }
        public List<OrderModel> OrderModelList { get; set; }
        public List<ProductModel> ProductList { get; set; }
        public OrderModel OrderView { get; set; }
        public PengaturanPromoModel PromoVoucher { get; set; }
        public ProductModel productModel { get; set; }
        public DateTime OrderedDate { get; set; }
        public List<GetOrderList_Result> GetOrderList { get; set; }

    }

    public class OrderListPagingModel
    {
        public Nullable<int> NumberOfProducts { get; set; }
        public Nullable<int> OrdrID { get; set; }
        public Nullable<int> CityID { get; set; }
        public string CityName { get; set; }
        public string InvoiceNumber { get; set; }
        public Nullable<int> BuyingStatus { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string AgentAdminName { get; set; }
        public string AgentAdminPhone { get; set; }
        public string MobileNumber { get; set; }
        public string SlotName { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public string OrderDateFormated { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string ProductList { get; set; }
        public string Quantity { get; set; }
        public string Voucher { get; set; }
        public Nullable<int> PromoID { get; set; }
        public string Agen { get; set; }
        public Nullable<int> AgenID { get; set; }
        public string Driver { get; set; }
        public Nullable<int> DriverID { get; set; }
        public Nullable<int> Rating { get; set; }
        public Nullable<int> sort { get; set; }
        public Nullable<int> Total { get; set; }
        public Nullable<decimal> PotonganVoucher { get; set; }
    }
}