﻿using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace LPG.App_Services
{
    public class DetailPriceService : ServiceBase, IDetailPriceService
    {
        public DetailPriceService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<DetailPrice> ListPrices()
        {
            var priceList = new List<DetailPrice>();
            var data = from det in context.DetailPrices
                       select new DetailPrice
                       {
                           CityID = det.CityID,
                           RefillNormalPrice = det.RefillNormalPrice,
                           RefillPromoPrice = det.RefillPromoPrice,
                           TubeNormalPrice = det.TubeNormalPrice,
                           TubePromoPrice = det.TubePromoPrice
                       };
            priceList = data.ToList();
            return priceList;
        }

        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = "Detail Price";
            _activityService.AddActivityLog(actModel, "Detail Price");
        }

        public DetailPriceModel GetDetailPrice(int detailPriceId)
        {
            var data = (from det in context.DetailPrices
                        where det.ID == detailPriceId
                        select new DetailPriceModel
                        {
                            ID = det.ID,
                            ProductID = det.ProductID,
                            ProductName = det.Product.ProductName,
                            ProductImg = det.Product.ProductImage,
                            CityName = det.MCity.CityName,
                            CityID = det.CityID,
                            RefillNormalPrice = det.RefillNormalPrice,
                            RefillPromoPrice = det.RefillPromoPrice,
                            TubeNormalPrice = det.TubeNormalPrice,
                            TubePromoPrice = det.TubePromoPrice
                        }).FirstOrDefault();
            return data;
        }

        public DetailPriceModel SavePrice(DetailPriceModel model, short loggedBy)
        {
            try
            {
                using (var context = new lpgdevEntities())
                {
                    var Data = context.DetailPrices.Where(r => r.ID == model.ID).FirstOrDefault();
                    if (Data != null)
                    {

                        Data.RefillNormalPrice = model.RefillNormalPrice;
                        Data.RefillPromoPrice = model.RefillPromoPrice;
                        Data.TubeNormalPrice = model.TubeNormalPrice;
                        Data.TubePromoPrice = model.TubePromoPrice;

                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["delete"].ToString(), Data.Product.ProductName, loggedBy);
                    }
                    return model;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
                throw;
            }

        }


    }
}