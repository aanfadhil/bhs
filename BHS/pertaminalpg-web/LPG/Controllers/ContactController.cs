﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.IO;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class ContactController : AbstractController<IContactService>
    {
        SessionManagement sessionManagement;

        public ContactController(IContactService _serviceObject) : base(_serviceObject)
        {
        }

        //ContactService serviceObject = new ContactService();
        // GET: Contact

        public ActionResult AddContact(int? id)
        {
            try
            {
                ContactModel model = new ContactModel();
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {


                    var convid = id ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.getContactDetails(convid);
                        if (vm != null)
                        {
                            return View(vm);
                        }
                        else
                        {
                            return View(model);
                        }
                    }
                    else
                    {
                        return View(model);
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Login", new { area = "Admin" });
                }

            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }


        }

        public ActionResult ListContact()
        {
            var vm = new ContactModel();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.ContactList = serviceObject.ListContact();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult AddContact(ContactModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    serviceObject.saveContactDetails(vm, adminSessionInfo.UserId);

                    return RedirectToAction("ListContact", "Contact");

                }
                else
                {
                    return RedirectToAction("Index", "Login", new { area = "Admin" });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error");
            }
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            string _imgname = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {

                var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    _imgname = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/extfiles/contact/ContactImg_") + _imgname + _ext;
                    _imgname = "ContactImg_" + _imgname + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width > 200)
                        img.Resize(350, 350);
                    img.Save(_comPath);
                    // end resize
                }
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DisableContact(int? id)
        {

            if (id != null & id > 0)
            {

                try
                {
                    sessionManagement = new SessionManagement();
                    AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                    ViewBag.Role = adminSessionInfo.UserRole;
                    if (adminSessionInfo != null)
                    {
                        //ContactService serviceObject = new ContactService();

                        serviceObject.deleteContactDetails(id.ToInt(), adminSessionInfo.UserId);

                    }


                }
                catch
                {

                }
            }

            return RedirectToAction("ListContact", "Contact");
        }

    }
}
