﻿function initDT2() {
    $('#dataTable').dataTable({
        "dom": '<"top"Bf>rt<"bottom"ilp><"clear">',//'Blftrip',
        "buttons": [
            {
                extend: 'excel',
                text: 'Export excel',
                className: 'exportExcel btn-success margin-bottom-5',
                filename: 'Daftar Pesanan',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    },
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                }
            }
        ],
        "aaSorting": [[7, "desc"]],
        columnDefs: [
            {
                target: 0,
                render: function (data, type, row) {
                    var res = '';
                    res += '<table width="100%" class="table-noborder">';
                    res += '<tr>';
                    res += '<td width="40"><div class="cust-checkbox"><input type="checkbox" id="' + row.OrdrID + '"><label for="' + row.OrdrID + '"></label></div></td>';
                    res += '<td>' + row.Name + '</td>';
                    res += '</tr>';
                    res += '</table>';
                    return res
                }
            },
            { data: 'InvoiceNumber', target: 1, "width": "5%" },
            { data: 'RegionName', target: 2 },
            { data: 'CityName', target: 3 },
            {
                data: 'ProductList', target: 4,
                render: function (data, type, row) {
                    return row.ProductList.replace(new RegExp(',', 'g'), '<br/>');
            }
            },
            {
                data: 'Quantity', target: 5,
                render: function (data, type, row) {
                    if(row.Quantity)
                    return row.Quantity.replace(new RegExp(',', 'g'), '<br/>');
            }
            },
            {
                data: 'DeliveryDate', target: 6,
                target: function (data, type, row) {
                    var res = '';
                    if (row.DeliveryDate.length > 10) {
                        res += row.DeliveryDate.substring(0, 10);
                    }
                    res += '<br />';

                    res += row.SlotName;

                    return res;
                }
            },
            { data: 'OrderDate', target: 7 },
            {
                data: 'BuyingStatus', target: 8,
                render: function (data, type, row) {
                    var res = '';

                    if (row.BuyingStatus == "1") {
                        res += "Delivery<br>(Aplikasi)";
                    }
                    else if (data.BuyingStatus.ToString() == "2") {
                        res += "Delivery<br>(Telepon)";
                    }
                    else {
                        res += "Delivery<br>(Pickup)";
                    }

                    return res;
                }
            },
            {
                data: 'PromoID', target: 9,
                render: function (data, type, row) {
                    if (row.PromoID) {
                        return '<a href="/PengeturanPromo/AddPromo?PromoID="' + row.PromoID + '" target="_blank">' + row.Voucher + '</a>';
                    }
                }
            },
            {
                data: 'StatusID', target: 10,
                render: function (data, type, row) {

                    var res = '';

                    if (row.StatusID == "1") {
                        res += "Belum diterima";
                    }
                    else if (row.StatusID == "2") {
                        res += "Diterima";
                    }
                    else if (row.StatusID == "3") {
                        res += "Dikirim";
                    }
                    else if (row.StatusID == "4") {
                        res += "Berhasil";
                    }
                    else if (row.StatusID == "5") {
                        res += "Dibatalkanoleh<br>Consumer";
                    }
                    else if (row.StatusID == "6") {
                        res += "Dibatalkanoleh<br>System";
                    }
                    else if (row.StatusID == "7") {
                        res += "Dibatalkanoleh<br>Super User";
                    }
                    else if (row.StatusID == "8") {
                        res += "Dibatalkanoleh<br>HQ Super User";
                    }
                    else if (row.StatusID == "9") {
                        res += "Belum dikirim";
                    }
                    else {
                        res += "";
                    }

                    return res;
                }
            },
            {
                data: 'GrandTotal', target: 11,
                render: function (data, type, row) {
                    return '<b>Rp' + row.GrandTotal + '</b>';
                }
            },
            {
                data: 'StatusID', target: 12,
                render: function (data, type, row) {
                    var res = '';

                    res += '<td class="text-center">'
                    res += '<a href="#" data-toggle="modal" data-target="#orderDetails" class="getorderDetail edit-link" onclick="getorderDetail(' + row.OrdrID + ',' + row.BuyingStatus + ');" data-id="' + row.OrdrID + '">[[[Melihat]]]</a>'
                    if (Convert.ToInt32(data.StatusID) >= 4) {
                        res += '<a href="#" class="disabled_override">[[[Override]]]</a>'
                    }
                    else {
                        res += '<a href="#" class="delete" data-toggle="dropdown">[[[Override]]]</a>'
                        res += '<div class="dropdown-menu override-dropdown">'

                        res += '<a class="dropdown-item close1" id="' + row.StatusID + '"  href="/Order/deleteOrder/' + row.OrdrID + '">[[[Close]]]</a>'
                        res += '<a class="dropdown-item cancel" id="' + row.StatusID + '" href="/Order/CancelOrder/' + row.OrdrID + '">[[[Cancel]]]</a>'
                        res += '</div>'
                    }
                    '</td>'

                    return res;
                }
            }

        { type: 'date', targets: [7] },
            { orderable: false, targets: [11] }
        ],
        "pagingType": "full_numbers",
        "language": {
            "url": "/Content/datatables/indonesia.json"
        },
    });
}