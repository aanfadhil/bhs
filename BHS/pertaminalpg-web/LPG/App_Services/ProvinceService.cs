﻿using LPG.App_Classes;
using LPG.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Configuration;
using System.Web.Configuration;
using System.IO;
using System.Web.Mvc;
using Microsoft.ApplicationInsights;
using OfficeOpenXml;
using System.Web.UI.WebControls;

namespace LPG.App_Services
{
    public class ProvinceService : AbstractAppService, IProvinceService
    {
        public ProvinceService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            throw new NotImplementedException();
        }

        public List<ProvinceModel> ListProvince()
        {
            var provinceList = new List<ProvinceModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from province in context.MProvinces
                           select new ProvinceModel
                           {
                               ID = province.ID,
                               RegID= province.RegID,
                               ProvCode=province.ProvCode,
                               ProvName=province.ProvName
                           };

                provinceList = data.ToList();
            }
            return provinceList;
        }
    }
}