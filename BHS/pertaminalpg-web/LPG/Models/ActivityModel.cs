﻿using System;
using System.Collections.Generic;

namespace LPG.Models
{
    public class ActivityModel
    {
        public int ActvID { get; set; }
        public string Item { get; set; }
        public string Activity { get; set; }
        public int UserType { get; set; }
        public DateTime ActDate { get; set; }
        public TimeSpan ActTime { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UserName { get; set; }
        public string DisplayActTime { get; set; }
        public List<ActivityModel> ActivityList { get; set; }
    }
}