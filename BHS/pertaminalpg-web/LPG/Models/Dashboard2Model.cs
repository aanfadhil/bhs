﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class Dashboar2dModel
    {
        public MDashKPISetting KPISetting { get; set; }
        public int iMonth { get; set; }
        public int iYear { get; set; }
        public List<RegionModel> Region { get; set; }
        public List<AgentAdminModel> Agent { get; set; }
        public List<SalesPerRegion> ListOfSalesPerRegion { get; set; }
        public int NumberOfRegisteredUser;
        public List<SPKPIReport_Result> KPIReport { get; set; }
        public List<SPNumberOfSales_Result> NumberOfSales { get; set; }
    }

    public class SalesPerRegion
    {

        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public int NumberOfAgent { get; set; }
        public int NumberOfTransaction { get; set; }
        public int NumberOfCustomer { get; set; }
        public int NumberOfOntimeDelivery { get; set; }
    }

    public class ChartOfSales
    {
        public string name { get; set; }
        public int y { get; set; }
    }

}