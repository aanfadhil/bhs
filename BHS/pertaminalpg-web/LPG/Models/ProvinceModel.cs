﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class ProvinceModel
    {
        public int ID { get; set; }
        public int RegID { get; set; }
        public String ProvName { get; set; }
        public String ProvCode { get; set; }
    }
}