﻿using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.ApplicationInsights;

namespace LPG.App_Services
{
    public class DashboardService : AbstractAppService, IDashboardService
    {
        public DashboardService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public MDashKPISetting GetKPISetting() {
            return context.MDashKPISettings.FirstOrDefault(x => x.ID.Equals(1));
        }

        public bool UpdateKPISetting(MDashKPISetting kPISetting)
        {
            var find = context.MDashKPISettings.Where(r => r.ID == kPISetting.ID).FirstOrDefault();
            if (find != null) { 
            find.ConversionRateAtRisk = kPISetting.ConversionRateAtRisk;
                find.ConversionRateOnTrack = kPISetting.ConversionRateOnTrack;
                find.IncreaseVolSalesAtRisk = kPISetting.IncreaseVolSalesAtRisk;
                find.IncreaseVolSalesOnTrack = kPISetting.IncreaseVolSalesOnTrack;
                find.OrderAllocatedAtRisk = kPISetting.OrderAllocatedAtRisk;
                find.OrderAllocatedOnTrack = kPISetting.OrderAllocatedOnTrack;
                find.OrderOnTimeAtRisk = kPISetting.OrderOnTimeAtRisk;
                find.OrderOntimeOnTrack = kPISetting.OrderOntimeOnTrack;
                find.RatingAtRisk = kPISetting.RatingAtRisk;
                find.RatingOnTrack = kPISetting.RatingOnTrack;
                find.TotalSalesAtRisk = kPISetting.TotalSalesAtRisk;
                find.TotalSalesOnTrack = kPISetting.TotalSalesOnTrack;
                find.TotalTransHouseholdAtRisk = kPISetting.TotalTransHouseholdAtRisk;
                find.TotalTransHouseholdOnTrack = kPISetting.TotalTransHouseholdOnTrack;

                context.SaveChanges();
            return true;
            }
            else
            {
                return false;
            }
        }

        public List<SalesPerRegion> GetRegionTransaction(int iMonth, int iYear)
        {
            List<SPRegionInTransaction_Result> ListRegion = context.SPRegionInTransaction(iMonth, iYear).ToList();
            var sales = new List<SalesPerRegion>();
            foreach(SPRegionInTransaction_Result row in ListRegion)
            {
                SPTransPerRegion_Result Transaction = context.SPTransPerRegion(iMonth, iYear, row.RegionID).FirstOrDefault();
                int NumberOfOntimeDelivery = (int)context.SPOntimeDeliveryPerRegion(iMonth, iYear, row.RegionID).FirstOrDefault();

                int NumberOfTransaction = 0;
                int NumberOfCustomer = 0;

                if (Transaction != null)
                {
                    NumberOfTransaction = (int)Transaction.NumberOfTransaction;
                    NumberOfCustomer = (int)Transaction.NumberOfCustomer;
                }
                sales.Add(new SalesPerRegion
                {
                    RegionId = row.RegionID,
                    RegionName = row.RegionName,
                    NumberOfAgent = (int)row.NumberOfAgent,
                    NumberOfTransaction = NumberOfTransaction,
                    NumberOfCustomer = NumberOfCustomer,
                    NumberOfOntimeDelivery = NumberOfOntimeDelivery
                });
            }

            return sales.ToList();
        }


        public List<ChartOfSales> GetNumberOfSales(int iMonth, int iYear, int iRegionId)
        {
            List<SPNumberOfSalesPerRegion_Result> NumberOfSales = context.SPNumberOfSalesPerRegion(iMonth, iYear, iRegionId).ToList();
            var sales = new List<ChartOfSales>();
            if(NumberOfSales!=null)
            { 
                foreach (SPNumberOfSalesPerRegion_Result row in NumberOfSales)
                {
                    sales.Add(new ChartOfSales
                    {
                        name = row.iWeek.ToString(),
                        y = (int)row.NumberOfProducts
                    });
                }
            }
            return sales.ToList();
        }


        public int GetRegisteredUserByMonth(int iMonth, int iYear)
        {
            DateTime date = new DateTime(iYear, iMonth, 1).AddMonths(1);
            
            return context.Consumers.
                    Where(c => c.StatusID == 1).
                    Where(c => c.CreatedDate < date).Count();
        }

        public List<SPKPIReport_Result> GetKPIReport(int iMonth, int iYear)
        {
            var dateStart = new DateTime(iYear, iMonth, 1);
            var dateEnd = new DateTime(iYear, iMonth, DateTime.DaysInMonth(iYear, iMonth));

            var orderlist = from i in context.SPKPIReport(dateStart, dateEnd)
                            select new SPKPIReport_Result
                            {
                                RegionCode=i.RegionCode,
                                RegionName = i.RegionName,
                                AgentName = i.AgentName,
                                TotalSales = i.TotalSales,
                                ConvertionRate = i.ConvertionRate,
                                IncreaseOfSales = i.IncreaseOfSales,
                                JumlahHouseHold = i.JumlahHouseHold,
                                OntimeDelivery = i.OntimeDelivery,
                                Allocated = i.Allocated,
                                Rating = i.Rating
                            };
            List<SPKPIReport_Result> KPI = orderlist.ToList();
            return KPI;
        }

        public List<SPNumberOfSales_Result> GetNumberOfSales(int iMonth, int iYear)
        {
            var dateStart = new DateTime(iYear, iMonth, 1);
            var dateEnd = new DateTime(iYear, iMonth, DateTime.DaysInMonth(iYear, iMonth));

            var orderlist = from i in context.SPNumberOfSales(dateStart, dateEnd)
                            select new SPNumberOfSales_Result
                            {
                                Category = i.Category,
                                AgentId = i.AgentId,
                                AgentName = i.AgentName,
                                ProductCode = i.ProductCode,
                                iQuantity = i.iQuantity
                            };
            List<SPNumberOfSales_Result> KPI = orderlist.ToList();
            return KPI;
        }

        public DataTableModel<SPCustomerTransPaging_Result> GetOrderCustomerPaging(DatatablesRequest data)
        {
            DataTableModel<SPCustomerTransPaging_Result> result = new DataTableModel<SPCustomerTransPaging_Result>();

            using (var context = new lpgdevEntities())
            {
                {
                    result.draw = data.draw;

                    result.data = context.SPCustomerTransPaging(data.length, data.start, data.search[0]).ToList();

                    if (result.data.Count > 0)
                    {
                        result.recordsTotal = result.data[0].Total;
                        result.recordsFiltered = result.data[0].Total;
                    }

                }


            }

            return result;
        }

        public DataTableModel<SPOrderPerCustomer_Result> GetOrderPercustomer(DatatablesRequest data, int CustID, string orderWith)
        {
            
            DataTableModel<SPOrderPerCustomer_Result> result = new DataTableModel<SPOrderPerCustomer_Result>();

            if (orderWith.Equals("app"))
            {
                var orderlist = from i in context.SPOrderPerCustomer(CustID)
                              select new SPOrderPerCustomer_Result
                                {
                                    OrdrID = i.OrdrID,
                                    InvoiceNumber = i.InvoiceNumber,
                                    OrderDate = i.OrderDate,
                                    orderStatus = i.orderStatus,
                                    Quantity = i.Quantity,
                                    Weight = i.Weight,
                                    GrandTotal = i.GrandTotal,
                                    Rating = i.Rating,
                                    DeliveryStatus = i.DeliveryStatus
                                };

                result.draw = data.draw;
                result.data = orderlist.ToList();


                if (result.data.Count > 0)
                {
                    result.recordsTotal = result.data.Count;
                    result.recordsFiltered = result.data.Count;
                }

            }
            else
            {
                var orderlist = from i in context.SPTeleOrderPerCustomer(CustID)
                                select new SPOrderPerCustomer_Result
                                {
                                    OrdrID = i.OrdrID,
                                    InvoiceNumber = i.InvoiceNumber,
                                    OrderDate = i.OrderDate,
                                    orderStatus = i.orderStatus,
                                    Quantity = i.Quantity,
                                    Weight = i.Weight,
                                    GrandTotal = i.GrandTotal,
                                    Rating = i.Rating,
                                    DeliveryStatus = i.DeliveryStatus
                                };

                result.draw = data.draw;
                result.data = orderlist.ToList();

                if (result.data.Count > 0)
                {
                    result.recordsTotal = result.data.Count;
                    result.recordsFiltered = result.data.Count;
                }
            }

            return result;
        }
    }
}