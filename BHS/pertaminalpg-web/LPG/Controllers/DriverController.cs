﻿using LPG.App_Classes;
using LPG.App_Services;
using LPG.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LPG.Controllers
{
    [Filters.SessionHandle]
    public class DriverController : AbstractController<IDriverService>
    {
        SessionManagement sessionManagement;

        private IAgencyService agencyObject;// = new AgencyService();
        private IDistributionService distService;// = new DistributionService();

        public DriverController(IDriverService _serviceObject, IAgencyService _agencyObject, IDistributionService _distService) : base(_serviceObject)
        {
            agencyObject = _agencyObject;
            distService = _distService;
        }



        //DriverService serviceObject = new DriverService();
        // GET: Driver
        public ActionResult ListDriver()
        {
            var vm = new DriverModel();
            //DriverService serviceObject = new DriverService();
            try
            {
                sessionManagement = new SessionManagement();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    vm.DriverModelList = serviceObject.ListDriver();
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
            return View(vm);
        }



        [HttpPost]
        public ActionResult GetDist(int agency)
        {
            try
            {
                sessionManagement = new SessionManagement();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {
                    var products = distService.GetByAgency(agency);
                    return Json(products);
                }
                else
                {
                    return Json("SESSION TIMEOUT");
                }
            }
            catch (Exception ex)
            {
                telemectryClient.TrackException(ex);
                ExceptionHelper.Log(ex);
                return Json(null);
            }
        }

        public ActionResult AddDriver(int? id)
        {
            try
            {
                DriverModel model = new DriverModel();


                sessionManagement = new SessionManagement();
                var agencyList = agencyObject.ListComapany();
                var distList = distService.ListDistribution();

                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                if (adminSessionInfo != null)
                {

                    var convid = id ?? 0;
                    if (convid > 0)
                    {
                        var vm = serviceObject.getDriverDetails(convid);
                        if (vm != null)
                        {
                            vm.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                            vm.DistributionList = distService.GetByAgency(vm.AgenDriverAgenId).Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();
                            return View(vm);
                        }
                        else
                        {
                            model.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                            model.DistributionList = new System.Collections.Generic.List<SelectListItem>();
                            return View(model);
                        }
                    }
                    else
                    {
                        model.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        model.DistributionList = new System.Collections.Generic.List<SelectListItem>();
                        return View(model);

                    }

                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public ActionResult AddDriver(DriverModel vm)
        {
            try
            {
                sessionManagement = new SessionManagement();
                //DriverService serviceObject = new DriverService();
                //AgencyService agencyObject = new AgencyService();
                //DistributionService distService = new DistributionService();
                AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
                ViewBag.Role = adminSessionInfo.UserRole;
                var agencyList = agencyObject.ListComapany();
                var distList = distService.ListDistribution();
                if (adminSessionInfo != null)
                {

                    if (vm.AgenDriverAgenId == 0 || vm.AgenDriverDistId == 0)
                    {
                        vm.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        vm.DistributionList = distList.Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();

                        vm.ErrorMessage = MessageSource.GetMessage("required.data");
                        return View(vm);
                    }
                    if (serviceObject.IsExistsMobileNo(vm.CountryCode + vm.MobilePhone, vm.DriverId))
                    {
                        vm.ErrorMessage = MessageSource.GetMessage("Mobile.exists");
                        vm.AgencyList = agencyList.Select(c => new SelectListItem { Text = c.CompanyName.ToString(), Value = c.CompanyId.ToString() }).ToList();
                        vm.DistributionList = distList.Select(x => new SelectListItem { Text = x.DistributionPoint.ToString(), Value = x.DistributionId.ToString() }).ToList();

                        return View(vm);
                    }
                    else
                    {
                        serviceObject.saveDriverDetails(vm, adminSessionInfo.UserId);

                        SetFlash(FlashMessageType.Success, "Input driver berhasil");

                        return RedirectToAction("ListDriver", "Driver");
                    }



                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Index", "Error", new { area = "Admin" });
            }
        }

        public ActionResult DeleteDriver(int id)
        {
            sessionManagement = new SessionManagement();
            AdminSessionEntity adminSessionInfo = sessionManagement.getAdminSession();
            ViewBag.Role = adminSessionInfo.UserRole;
            if (adminSessionInfo != null)
            {
                if (serviceObject.DeleteDriver(id, adminSessionInfo.UserId))
                {
                    //return View();
                    return RedirectToAction("ListDriver", "Driver");
                }
            }
            return RedirectToAction("Index", "Error");
        }
    }
}
