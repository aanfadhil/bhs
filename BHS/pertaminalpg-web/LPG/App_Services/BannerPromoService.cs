﻿using LPG.Models;
using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace LPG.App_Services
{

    public class BannerPromoService : ServiceBase, IBannerPromoService
    {
        public BannerPromoService(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }

        public List<PromoBannerModel> ListHomeBanner()
        {
            var homebannerList = new List<PromoBannerModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from homebanner in context.PromoBanners
                           where homebanner.BannerID == homebanner.BannerID

                           select new PromoBannerModel
                           {
                               HomeBannerId = homebanner.BannerID,
                               BannerName = homebanner.Caption,
                               BannerImage = homebanner.BannerImage,
                               IsActive = homebanner.StatusId
                           };
                homebannerList = data.ToList();
            }

            return homebannerList;
        }


        public List<PromoBannerModel> getActiveBanners()
        {
            var homebannerList = new List<PromoBannerModel>();
            using (var context = new lpgdevEntities())
            {
                var data = from homebanner in context.PromoBanners
                           where homebanner.BannerID == homebanner.BannerID
                           && homebanner.StatusId == true

                           select new PromoBannerModel
                           {
                               HomeBannerId = homebanner.BannerID,
                               BannerName = homebanner.Caption,
                               BannerImage = homebanner.BannerImage,
                               IsActive = homebanner.StatusId
                           };
                if (data.Count() > 0)
                    homebannerList = data.ToList();
            }

            return homebannerList;
        }

        public PromoBannerModel saveHomeBannerDetails(PromoBannerModel entity, string Cmd, int loggedBy)
        {

            try
            {
                entity.IsHomeBannerExists = false;
                using (var context = new lpgdevEntities())
                {
                    bool Status = false;
                    if (Cmd == "btnSaveAndPublish")
                    {
                        Status = true;
                    }
                    else if (Cmd == "btnSaveUnpublish")
                    {
                        Status = false;
                    }

                    var Data = context.PromoBanners.Where(r => r.BannerID == entity.HomeBannerId).FirstOrDefault();
                    if (Data != null)
                    {
                        Data.Caption = entity.BannerName;
                        Data.StatusId = Status;
                        Data.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                        Data.CreatedBy = base.UserId.ToShort();
                        Data.UpdatedBy = base.UserId.ToShort();
                        Data.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["update"].ToString(), "", loggedBy);

                    }
                    else
                    {
                        PromoBanner objhomebanner = new PromoBanner();


                        objhomebanner.Caption = entity.BannerName;
                        objhomebanner.StatusId = Status;
                        objhomebanner.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                        objhomebanner.CreatedBy = base.UserId.ToShort();
                        objhomebanner.UpdatedBy = base.UserId.ToShort();
                        objhomebanner.UpdatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");

                        context.PromoBanners.Add(objhomebanner);
                        context.SaveChanges();
                        CallActivityLog(WebConfigurationManager.AppSettings["add"].ToString(), "", loggedBy);
                    }
                }
                return entity;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public PromoBannerModel getHomeBannerDetails(int homeBannerId)
        {
            using (var context = new lpgdevEntities())
            {
                var rtns = (from homebanner in context.PromoBanners
                            where homebanner.BannerID == homeBannerId
                            select new PromoBannerModel()
                            {
                                HomeBannerId = homebanner.BannerID,
                                BannerImage = homebanner.BannerImage,
                                IsActive = homebanner.StatusId

                            }).FirstOrDefault();

                if (rtns == null)
                {
                    rtns = (from homebanner in context.PromoBanners
                            where homebanner.BannerID == homeBannerId
                            select new PromoBannerModel()
                            {
                                HomeBannerId = homebanner.BannerID,
                                BannerImage = string.Empty,
                                IsActive = homebanner.StatusId

                            }).FirstOrDefault();
                }

                return rtns;
            }
        }

        public bool deleteBannerPromo(int id)
        {
            bool status = false;
            using (var context = new lpgdevEntities())
            {
                var data = context.PromoBanners.Where(r => r.BannerID == id).FirstOrDefault();
                if (data != null)
                {
                    context.PromoBanners.Remove(data);
                    context.SaveChanges();
                    status = true;
                }
            }
            return status;
        }
        public void CallActivityLog(string action, string action_item, int loggedBy)
        {
            ActivityModel actModel = new ActivityModel();
            //ActivityService _activityService = new ActivityService();

            actModel.Activity = action;
            actModel.ActDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.ActTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time").TimeOfDay;
            actModel.CreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "SE Asia Standard Time");
            actModel.UserType = loggedBy;
            actModel.Item = "Daftar Agen";
            _activityService.AddActivityLog(actModel, "Daftar Agen");
        }

    }
}
