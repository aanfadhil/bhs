﻿using System.Collections.Generic;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IDashboardService
    {
        bool UpdateKPISetting(MDashKPISetting kpisetting);
        MDashKPISetting GetKPISetting();
        List<SalesPerRegion> GetRegionTransaction(int iMonth, int iYear);
        List<ChartOfSales> GetNumberOfSales(int iMonth, int iYear, int iRegionId);
        int GetRegisteredUserByMonth(int iMonth, int iYear);
        List<SPKPIReport_Result> GetKPIReport(int iMonth, int iYear);
        List<SPNumberOfSales_Result> GetNumberOfSales(int iMonth, int iYear);
        DataTableModel<SPCustomerTransPaging_Result> GetOrderCustomerPaging(DatatablesRequest model);
        DataTableModel<SPOrderPerCustomer_Result> GetOrderPercustomer(DatatablesRequest model, int CustID, string orderWith);
    }
}