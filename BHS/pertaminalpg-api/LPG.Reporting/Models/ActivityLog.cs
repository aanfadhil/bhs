namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ActivityLog")]
    public partial class ActivityLog
    {
        [Key]
        public int ActvID { get; set; }

        [Required]
        [StringLength(50)]
        public string Item { get; set; }

        [Required]
        [StringLength(50)]
        public string Activity { get; set; }

        public short SAdminID { get; set; }

        [Column(TypeName = "date")]
        public DateTime ActDate { get; set; }

        public TimeSpan ActTime { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual SuperAdmin SuperAdmin { get; set; }
    }
}
