﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LPG.Reporting.DTOs.Orders.Infrastructure;
using LPG.Reporting;

namespace LPG.Reporting.DTOs.Orders
{
    public class TeleOrderDetailDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int TeleOrdDetID { get; set; }
        public int TeleOrdID { get; set; }
        public int ProdID { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal SubTotal { get; set; }
        public decimal? PromoProduct { get; set; }
        public decimal? ShippingCharge { get; set; }
        public decimal? PromoShipping { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime CreatedDate { get; set; }
        public int RefillQuantity { get; set; }
        public decimal RefillPrice { get; set; }
        public decimal PromoRefill { get; set; }
        public decimal RefillSubTotal { get; set; }
        public decimal RefillTotalAmount { get; set; }
    }

    public class TeleOrderDetailMapper : MapperBase<TeleOrderDetail, TeleOrderDetailDTO>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<TeleOrderDetail, TeleOrderDetailDTO>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<TeleOrderDetail, TeleOrderDetailDTO>>) (p => new TeleOrderDetailDTO()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    TeleOrdDetID = p.TeleOrdDetID,
                    TeleOrdID = p.TeleOrdID,
                    ProdID = p.ProdID,
                    Quantity = p.Quantity,
                    UnitPrice = p.UnitPrice,
                    SubTotal = p.SubTotal,
                    PromoProduct = p.PromoProduct,
                    ShippingCharge = p.ShippingCharge,
                    PromoShipping = p.PromoShipping,
                    TotalAmount = p.TotalAmount,
                    CreatedDate = p.CreatedDate,
                    RefillQuantity = p.RefillQuantity,
                    RefillPrice = p.RefillPrice,
                    PromoRefill = p.PromoRefill,
                    RefillSubTotal = p.RefillSubTotal,
                    RefillTotalAmount = p.RefillTotalAmount
                }));
            }
        }

        public override void MapToModel(TeleOrderDetailDTO dto, TeleOrderDetail model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.TeleOrdDetID = dto.TeleOrdDetID;
            model.TeleOrdID = dto.TeleOrdID;
            model.ProdID = dto.ProdID;
            model.Quantity = dto.Quantity;
            model.UnitPrice = dto.UnitPrice;
            model.SubTotal = dto.SubTotal;
            model.PromoProduct = dto.PromoProduct;
            model.ShippingCharge = dto.ShippingCharge;
            model.PromoShipping = dto.PromoShipping;
            model.TotalAmount = dto.TotalAmount;
            model.CreatedDate = dto.CreatedDate;
            model.RefillQuantity = dto.RefillQuantity;
            model.RefillPrice = dto.RefillPrice;
            model.PromoRefill = dto.PromoRefill;
            model.RefillSubTotal = dto.RefillSubTotal;
            model.RefillTotalAmount = dto.RefillTotalAmount;

        }
    }
}
