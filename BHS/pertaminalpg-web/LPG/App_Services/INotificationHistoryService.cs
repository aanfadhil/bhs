﻿using System.Collections.Generic;
using System.Web.Mvc;
using LPG.Models;

namespace LPG.App_Services
{
    public interface INotificationHistoryService
    {
        DataTableModel<NotificationHistory> GetList(DatatablesRequest data);
        List<NotificationHistory> GetAll();

    }
}
