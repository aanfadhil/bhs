﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LPG.Models
{
    public class GetOrderListModel
    {
        public Nullable<int> NumberOfProducts { get; set; }

        public Nullable<int> OrdrID { get; set; }

        public Nullable<int> CityID { get; set; }

        public string CityName { get; set; }

        public string InvoiceNumber { get; set; }

        public Nullable<int> BuyingStatus { get; set; }

        public string BuyingStatusDisplay { get; set; }

        public Nullable<int> StatusID { get; set; }

        public string StatusDisplay { get; set; }

        public Nullable<decimal> GrandTotal { get; set; }

        public Nullable<System.DateTime> DeliveryDate { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string AgentAdminName { get; set; }

        public string MobileNumber { get; set; }

        public string SlotName { get; set; }

        public Nullable<System.DateTime> OrderDate { get; set; }

        public string RegionCode { get; set; }

        public string RegionName { get; set; }

        public string ProductList { get; set; }

        public string Quantity { get; set; }

        public string Voucher { get; set; }

        public Nullable<int> PromoID { get; set; }


    }
}