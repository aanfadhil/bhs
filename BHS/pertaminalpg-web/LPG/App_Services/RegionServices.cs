﻿using LPG.App_Classes;
using LPG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using Microsoft.ApplicationInsights;


namespace LPG.App_Services
{
    public class RegionServices : ServiceBase, IRegionServices
    {
        public RegionServices(lpgdevEntities context, IActivityService activityService, TelemetryClient _telemetry) : base(context, activityService, _telemetry)
        {
        }
        
        public List<RegionModel> GetList()
        {
            return context.MRegions.Select(t => new RegionModel()
            {
                RegionId = t.RegionID,
                RegionCode = t.RegionCode,
                RegionName = t.RegionName
            }).ToList();
        }
    }
}