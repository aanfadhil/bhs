﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pertamina.LPG.API.DTOs.Notifications
{
    public class NotificationRequest
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string search { get; set; }
    }
}
