﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Pertamina.LPG.API.Models;
using Pertamina.LPG.API.Utils;
//using System.Data.Entity.Core.Objects.SqlClient;
using Pertamina.LPG.API.Util;
using System.Data.Entity.SqlServer;
namespace Pertamina.LPG.API.DAOs
{
    public class PromoVoucherDao : DAO
    {
        public PromoVoucher GetByVoucher(string voucher)
        {
            //var all = _context.PromoVouchers.ToList();
            return  _context.PromoVouchers.FirstOrDefault(t => t.Voucher == voucher && t.IsActive);
        }

        public int UserUsingCount(int promoID,int userId)
        {
            var canceled = new int[] { 5, 6, 7, 8 };

            return _context.Orders.Where(t => t.PromoID == promoID && t.ConsID == userId && !canceled.Contains(t.StatusID)).ToList().Count;
        }

        public int GetUsedQuota(int promoID)
        {
            var canceled = new int[] { 5, 6, 7, 8 };

            return _context.Orders.Where(t => t.PromoID == promoID && !canceled.Contains(t.StatusID)).Count();
        }
    }
}
