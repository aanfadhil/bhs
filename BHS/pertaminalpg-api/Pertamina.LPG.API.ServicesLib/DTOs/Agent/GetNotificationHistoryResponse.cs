﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pertamina.LPG.API.Models;
using Pertamina.LPG.API.DTOs.Notifications;

namespace Pertamina.LPG.API.DTOs.Agent
{
    public class GetNotificationHistoryResponse : ResponseDto
    {
        public List<NotificationDto> notifications { get; set; }
        public int totalrow { get; set; }
    }

    public class GetNotificationCountResponse : ResponseDto
    {
        public int notification_count { get; set; }
    }
}
