namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Consumer")]
    public partial class Consumer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Consumer()
        {
            ConsumerAddresses = new HashSet<ConsumerAddress>();
            ConsumerReviews = new HashSet<ConsumerReview>();
            Orders = new HashSet<Order>();
        }

        [Key]
        public int ConsID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }

        [StringLength(50)]
        public string IMEI { get; set; }

        [StringLength(64)]
        public string AccToken { get; set; }

        [StringLength(255)]
        public string AppID { get; set; }

        [StringLength(255)]
        public string AppToken { get; set; }

        [StringLength(255)]
        public string ProfileImage { get; set; }

        public DateTime? LastLogin { get; set; }

        public bool ConsActivated { get; set; }

        public bool ConsBlocked { get; set; }

        public short StatusID { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsumerAddress> ConsumerAddresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsumerReview> ConsumerReviews { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Orders { get; set; }
    }
}
