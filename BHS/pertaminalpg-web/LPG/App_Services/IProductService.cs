﻿using System.Collections.Generic;
using System.Web.Mvc;
using LPG.Models;

namespace LPG.App_Services
{
    public interface IProductService
    {
        void CallActivityLog(string action, string action_item, int loggedBy);
        void DeletedOldExchange(int productID);
        bool DeleteProduct(int productID, int loggedBy);
        bool DeleteProductExchange(int prodExID);
        List<SelectListItem> GetAvailablePositions(int? ProdId);
        ProductModel GetProduct(int productID);
        List<ProductExchangeList> GetProductExchange(ProductModel model, int productID);
        List<ProductChartModel> GetProductsByCode(List<string> productCodes);
        List<ProductModel> ListProducts();
        ProductModel SaveProduct(ProductModel model, short loggeduserId);

        DetailPriceModel SavePrice(DetailPriceModel model, short loggeduserId);

        List<DetailPriceModel> ListPrices(int productID);
        DetailPriceModel GetDetailPrice(int detailPriceId);
    }
}