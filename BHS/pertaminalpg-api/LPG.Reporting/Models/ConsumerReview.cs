namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsumerReview")]
    public partial class ConsumerReview
    {
        [Key]
        public int ReviewID { get; set; }

        public int ConsID { get; set; }

        public int OrdrID { get; set; }

        public int DrvrID { get; set; }

        public int Rating { get; set; }

        public int? ReasonID { get; set; }

        public string Comments { get; set; }

        public virtual Consumer Consumer { get; set; }

        public virtual ConsumerReviewReason ConsumerReviewReason { get; set; }

        public virtual Driver Driver { get; set; }

        public virtual Order Order { get; set; }
    }
}
