namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NotificationLog")]
    public partial class NotificationLog
    {
        [Key]
        public long LogId { get; set; }

        [Required]
        [StringLength(250)]
        public string ReceiverId { get; set; }

        [Required]
        public string JSONMessage { get; set; }

        [Required]
        public string FCMResponse { get; set; }
    }
}
