namespace LPG.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TeleCustomer")]
    public partial class TeleCustomer
    {
        [Key]
        public int TeleCustID { get; set; }

        public int TeleOrdID { get; set; }

        [Required]
        [StringLength(100)]
        public string CustomerName { get; set; }

        [StringLength(20)]
        public string Latitude { get; set; }

        [StringLength(20)]
        public string Longitude { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [StringLength(20)]
        public string MobileNumber { get; set; }

        public bool StatusId { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual AgentAdmin AgentAdmin { get; set; }

        public virtual TeleOrder TeleOrder { get; set; }
    }
}
